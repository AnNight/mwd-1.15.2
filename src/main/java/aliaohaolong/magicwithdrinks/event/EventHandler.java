package aliaohaolong.magicwithdrinks.event;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.api.MWDExes;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotion;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.Difficulty;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.RegistryBuilder;

import java.util.List;
import java.util.Vector;

public class EventHandler {
    @SubscribeEvent
    public static void onNewRegistry(RegistryEvent.NewRegistry event) {
        RegistryBuilder<MWDPotion> drinkRegistryBuilder = new RegistryBuilder<>();
        drinkRegistryBuilder.setType(MWDPotion.class);
        ResourceLocation drinkRL = new ResourceLocation(MWD.MOD_ID, "potion");
        drinkRegistryBuilder.setName(drinkRL);
        drinkRegistryBuilder.setDefaultKey(drinkRL);
        drinkRegistryBuilder.create();

        RegistryBuilder<Extra> itemMEBuilder = new RegistryBuilder<>();
        itemMEBuilder.setType(Extra.class);
        ResourceLocation itemMERL = new ResourceLocation(MWD.MOD_ID, "extra");
        itemMEBuilder.setName(itemMERL);
        itemMEBuilder.setDefaultKey(itemMERL);
        itemMEBuilder.create();
    }

    /**
     * MC invoke twice this per tick.
     * Run on Server & Client.<br>
     */
    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
        if (event.phase == TickEvent.Phase.START && event.side.isServer() && event.player.isAlive() && !event.player.isCreative() && !event.player.isSpectator()) {
            boolean sync = false;
            IWater water = IWater.getFromPlayer(event.player);
            if (event.player.world.getDifficulty() != Difficulty.PEACEFUL) {
                if (water.getValue() > 0) {
                    int temperature = (int) Math.sqrt(event.player.getEntityWorld().getBiome(event.player.getPosition()).getDefaultTemperature() + 0.2) * 3;
                    if (temperature >= 1 && !event.player.isInWaterOrBubbleColumn() && event.player.getEntityWorld().getDayTime() > 2000 && event.player.getEntityWorld().getDayTime() < 14000) if (water.modifyInner(temperature)) sync = true;
                    if (event.player.isSwimming() || event.player.isRidingOrBeingRiddenBy(event.player) || event.player.isElytraFlying()) if (water.modifyInner(2)) sync = true;
                    if (event.player.isBurning() && !event.player.isImmuneToFire()) if (water.modifyInner(80)) sync = true;
                    if (event.player.isInLava() && !event.player.isImmuneToFire()) if (water.modifyInner(120)) sync = true;
                    if (event.player.isSprinting()) {
                        if (water.modifyInner(50)) sync = true;
                    } else if (water.modifyInner(1)) sync = true;
                }
                if (water.getValue() == 0 || water.getValue() > IWater.MAX - 5) if (water.tick(event.player)) sync = true;
            } else if (water.getValue() < IWater.MAX) if (water.modifyInner(-1200)) sync = true;
            if (sync) PacketManager.sendWaterTo((ServerPlayerEntity) event.player, water);
        }
    }

    @SubscribeEvent
    public void onBreak(BlockEvent.BreakEvent event) {
        if (event.getPlayer() instanceof ServerPlayerEntity) {
            ServerPlayerEntity player = (ServerPlayerEntity) event.getPlayer();
            tool(player, 14);
            if ((event.getState().getBlock() == Blocks.GRASS || event.getState().getBlock() == Blocks.TALL_GRASS) &&
                    !player.isCreative() && !player.isSpectator() && event.getWorld().getRandom().nextInt(16) == 0) {
                InventoryHelper.spawnItemStack(event.getWorld().getWorld(), event.getPos().getX(), event.getPos().getY(), event.getPos().getZ(), new ItemStack(MWDItems.COTTON_SEEDS));
            }
        }
    }

    @SubscribeEvent
    public void onEntityPlace(BlockEvent.EntityPlaceEvent event) {
        if (event.getEntity() instanceof ServerPlayerEntity) {
            tool((PlayerEntity) event.getEntity(), 10);
        }
    }

    @SubscribeEvent
    public void onAttackEntity(AttackEntityEvent event) {
        if (event.getPlayer() instanceof ServerPlayerEntity) {
            tool((PlayerEntity) event.getEntityLiving(), 18);
        }
    }

    @SubscribeEvent
    public void onLivingJump(LivingEvent.LivingJumpEvent event) {
        if (event.getEntityLiving() instanceof ServerPlayerEntity) {
            tool((PlayerEntity) event.getEntityLiving(), 100);
        }
    }

    @SubscribeEvent
    public void onUseItem(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof ServerPlayerEntity && event.getEntityLiving().isAlive()) {
            ServerPlayerEntity player = (ServerPlayerEntity) event.getEntityLiving();
            ItemStack stack = event.getItem();
            Extra extra;
            if (stack.getItem() == Items.POTION && PotionUtils.getPotionFromItem(stack) == Potions.WATER)
                extra = MWDExes.V_WATER;
            else extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
            if (extra != null && extra.isFood()) extra.applyAttributes(player);
        }
    }

    @SubscribeEvent
    public void onItemTooltip(ItemTooltipEvent event) {
        Extra extra;
        ItemStack stack = event.getItemStack();
        if (stack.getItem() == Items.POTION && PotionUtils.getPotionFromItem(stack) == Potions.WATER)
            extra = MWDExes.V_WATER;
        else extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
        if (extra != null && extra.isFood()) {
            List<ITextComponent> lores = event.getToolTip();
            Vector<ITextComponent> temp = new Vector<>();
            temp.add(0, lores.get(0));
            temp.addAll(extra.getLores());
            for (int i = 1; i < lores.size(); i++)
                temp.add(lores.get(i));
            lores.clear();
            lores.addAll(temp);
        }
    }

    /**
     * When players are alive, on survival mode and not on peaceful mode, operate there's water.
     * Run on server.
     */
    private static void tool(PlayerEntity player, int value) {
        if (player.isAlive() && !player.isCreative() && !player.isSpectator() && player.world.getDifficulty() != Difficulty.PEACEFUL) {
            IWater water = IWater.getFromPlayer(player);
            if (water.modifyInner(value))
                PacketManager.sendWaterTo((ServerPlayerEntity) player, water);
        }
    }
}