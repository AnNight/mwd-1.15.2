package aliaohaolong.magicwithdrinks.event;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.mana.ManaProvider;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.capability.water.WaterProvider;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.GameRules;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class CapabilityEventHandler {
    /**
     * Attach capabilities to the player.<br>
     * Run on Server & Client.<br>
     */
    @SubscribeEvent
    public void onAttachCapabilities(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof PlayerEntity) {
            event.addCapability(new ResourceLocation(MWD.MOD_ID, "water"), new WaterProvider());
            event.addCapability(new ResourceLocation(MWD.MOD_ID, "mana"), new ManaProvider());
        }
    }

    /**
     * Clone data to the player.<br>
     * Run on Server.<br>
     */
    @SubscribeEvent
    public void onClone(PlayerEvent.Clone event) {
        if (!event.isWasDeath() || event.getPlayer().getEntityWorld().getGameRules().getBoolean(GameRules.KEEP_INVENTORY)) {
            IWater.getFromPlayer(event.getPlayer()).copyForRespawn(IWater.getFromPlayer(event.getOriginal()));
            IMana.getFromPlayer(event.getPlayer()).copyForRespawn(IMana.getFromPlayer(event.getOriginal()));
        }
    }

    /**
     * Sync main world's player data with the end world or the end's player data with the main world.
     * Run on Server & Client.<br>
     */
    @SubscribeEvent
    public void onEntityJoinWorldEvent(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof ServerPlayerEntity) {
            ServerPlayerEntity player = (ServerPlayerEntity) event.getEntity();
            PacketManager.sendWaterTo(player, IWater.getFromPlayer(player));
            PacketManager.sendManaTo(player, IMana.getFromPlayer(player));
        }
    }
}