package aliaohaolong.magicwithdrinks;

/**
 * This is for "accurately" calculate mana value.
 */

public class MyMath {
    /**
     * Define the calculation accuracy that is correct to two decimal places.
     */
    private static final float WEIGHT = 100.0F;

    public static float minus(float a, float b) {
        return toFloat(toInt(a) - toInt(b));
    }

    public static float plus(float a, float b) {
        return toFloat(toInt(a) + toInt(b));
    }

    public static int toInt(float a) {
        return (int) (a * WEIGHT);
    }

    public static float toFloat(int a) {
        return (float) a / WEIGHT;
    }

    /**
     * For calculating the display scaled at container.
     *
     * @param a         IntArray provides the current value.
     * @param b         The mana slot's total value.
     * @param scaled    The total pixel value that be showed.
     * @return          The current pixel value that will be showed.
     */
    public static int getScaled(int a, float b, int scaled) {
        return (int) (toFloat(a) * (float) scaled / b);
    }
}