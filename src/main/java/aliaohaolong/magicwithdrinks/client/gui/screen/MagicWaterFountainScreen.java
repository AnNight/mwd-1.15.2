package aliaohaolong.magicwithdrinks.client.gui.screen;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.MagicWaterFountainContainer;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

@SuppressWarnings("deprecation")
public class MagicWaterFountainScreen extends ContainerScreen<MagicWaterFountainContainer> {
    private final ResourceLocation GUI = new ResourceLocation(MWD.MOD_ID, "textures/gui/container/magic_water_fountain.png");

    public MagicWaterFountainScreen(MagicWaterFountainContainer container, PlayerInventory inv, ITextComponent component) {
        super(container, inv, component);
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        font.drawString(title.getFormattedText(), (float) (xSize / 2 - font.getStringWidth(title.getFormattedText()) / 2), 9F, 4210752);
        String s = container.getBufferS() + "ME";
        font.drawString(s, (float) (xSize / 2 + 64 - font.getStringWidth(s) / 2), 32F, 4210752); // buffer
        s = container.getWaterS() + "ml";
        font.drawString(s, (float) (xSize / 2 - 17 - font.getStringWidth(s) / 2), 30F, 4210752); // water text
        s = container.getPurifiedWaterS() + "ml";
        font.drawString(s, (float) (xSize / 2 - 17 - font.getStringWidth(s) / 2), 39F, 4210752); // purified_water text
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        assert minecraft != null;
        minecraft.getTextureManager().bindTexture(GUI);
        int x = (width - 176) / 2;
        int y = (height - 166) / 2;
        blit(x, y, 0, 0, 176, 166);
        int value = container.getImpurityScaled();
        blit(x + 127, y + 75 - value, 176, 50 - value, 4, value); // impurity slot
        value = container.getBufferScaled();
        blit(x + 138, y + 24, 176, 60, value, 5); // buffer
        if (container.isBurning()) { // fire
            blit(x + 141, y + 68, 176, 50, 9, 10);
        }
        value = container.getWaterScaled();
        blit(x + 22, y + 23, 0, 166, value, 13); // water slot
        value = container.getPurifiedWaterScaled();
        blit(x + 22, y + 41, 0, 179, value, 13); // purified_water slot
    }
}