package aliaohaolong.magicwithdrinks.client.gui.screen;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.MEExtractorContainer;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

@SuppressWarnings("deprecation")
public class MEExtractorScreen extends ContainerScreen<MEExtractorContainer> {
    private final ResourceLocation GUI = new ResourceLocation(MWD.MOD_ID, "textures/gui/container/me_extractor.png");
    private byte catalystHeight = 0;

    public MEExtractorScreen(MEExtractorContainer container, PlayerInventory inv, ITextComponent component) {
        super(container, inv, component);
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        font.drawString(title.getFormattedText(), (float) (xSize / 2 - font.getStringWidth(title.getFormattedText()) / 2), 6F, 4210752);
        font.drawString(playerInventory.getDisplayName().getFormattedText(), 8.0F, 72.0F, 4210752);
        String s = container.getBufferS() + "ME/64.0ME";
        font.drawString(s, (float)(xSize / 2 - font.getStringWidth(s) / 2 - 8), 64F, 4210752);
        s = container.getCatalystS() + "/64";
        font.drawString(s, (float)(xSize / 2 - font.getStringWidth(s) / 2 - 54), 36F, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        assert minecraft != null;
        minecraft.getTextureManager().bindTexture(GUI);
        int relX = (width - 176) / 2;
        int relY = (height - 166) / 2;
        blit(relX, relY, 0, 0, 176, 166);
        if (container.isCooking()) {
            catalystHeight++;
            if (catalystHeight > 28) catalystHeight = 0;
            blit(width / 2 + 16, height / 2 - 56, 187, 0, 7, container.getCookScaled() + 1);
            blit(width / 2 - 16, height / 2 - 61 + 28 - catalystHeight, 176, 28 - catalystHeight, 11, catalystHeight);
        }
        if (container.hasCatalyst()) {
            int scaled = container.getCatalystScaled();
            blit(width / 2 - 24, height / 2 - 31 - scaled, 176, 60 - scaled, 4, scaled);
        }
        if (container.hasBuffer())
            blit(width / 2 - 39, height / 2 - 26, 176, 60, container.getBufferScaled(), 4);
    }
}