package aliaohaolong.magicwithdrinks.client.gui.screen;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.SmeltingBoxContainer;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

@SuppressWarnings("deprecation")
public class SmeltingBoxScreen extends ContainerScreen<SmeltingBoxContainer> {
    private final ResourceLocation GUI = new ResourceLocation(MWD.MOD_ID, "textures/gui/container/smelting_box.png");

    public SmeltingBoxScreen(SmeltingBoxContainer container, PlayerInventory inv, ITextComponent component) {
        super(container, inv, component);
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        font.drawString(title.getFormattedText(), (float) (xSize / 2 - font.getStringWidth(title.getFormattedText()) / 2), 6F, 4210752);
        font.drawString(playerInventory.getDisplayName().getFormattedText(), 8.0F, 72.0F, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        assert minecraft != null;
        minecraft.getTextureManager().bindTexture(GUI);
        int relX = (width - 176) / 2;
        int relY = (height - 166) / 2;
        blit(relX, relY, 0, 0, 176, 166);
    }
}