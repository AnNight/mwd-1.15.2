package aliaohaolong.magicwithdrinks.client.gui.screen;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.WaterPurifierContainer;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

@SuppressWarnings({"deprecation"})
public class WaterPurifierScreen extends ContainerScreen<WaterPurifierContainer> {
    private final ResourceLocation GUI = new ResourceLocation(MWD.MOD_ID, "textures/gui/container/water_purifier.png");

    public WaterPurifierScreen(WaterPurifierContainer container, PlayerInventory inv, ITextComponent component) {
        super(container, inv, component);
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        font.drawString(title.getFormattedText(), 8.0F, 6.0F, 4210752);
        font.drawString(playerInventory.getDisplayName().getFormattedText(), 8.0F, 72.0F, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        assert minecraft != null;
        minecraft.getTextureManager().bindTexture(GUI);
        int x = (width - 176) / 2;
        int y = (height - 166) / 2;
        blit(x, y, 0, 0, 176, 166);
        int impurity = container.getImpurity();
        x = width / 2 + 34;
        y = height / 2 - 65 + 50 - impurity;
        blit(x, y, 176, 79 - impurity, 4, impurity);
        if (container.isBurning()) {
            int burnRemainingScaled = container.getBurnRemainingScaled();
            x = width / 2 - 59;
            y = height / 2 - 35 - burnRemainingScaled;
            blit(x, y, 176, 12 - burnRemainingScaled, 14, burnRemainingScaled + 1);
        }
        if (container.isCooking()) {
            int cookingScaled = container.getCookingScaled();
            if (cookingScaled > 11) {
                blit(width / 2 - 36, height / 2 - 48, 176, 13, 11, 16);
                blit(width / 2 - 7, height / 2 - 48, 187, 13, cookingScaled - 11, 16);
            } else blit(width / 2 - 36, height / 2 - 48, 176, 13, cookingScaled, 16);
        }
    }
}