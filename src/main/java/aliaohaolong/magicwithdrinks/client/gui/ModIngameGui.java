package aliaohaolong.magicwithdrinks.client.gui;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import net.minecraft.client.Minecraft;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class ModIngameGui {
    private static final ResourceLocation ICONS = new ResourceLocation(MWD.MOD_ID,"textures/gui/icons.png");
    private static final ResourceLocation MC_ICONS = new ResourceLocation("minecraft", "textures/gui/icons.png");

    @OnlyIn(Dist.CLIENT)
    public static void render() {
        Minecraft mc = Minecraft.getInstance();
        assert mc.player != null;
        if (!mc.player.isAlive()) return;
        mc.getTextureManager().bindTexture(ICONS);

        // WATER
        int value = IWater.getFromPlayer(mc.player).getValue();
        int x = mc.getMainWindow().getScaledWidth() / 2 + 83;
        int y = mc.player.areEyesInFluid(FluidTags.WATER) || mc.player.getAir() < mc.player.getMaxAir() ? mc.getMainWindow().getScaledHeight() - 59 : mc.getMainWindow().getScaledHeight() - 49;
        if (value == 0) {
            for (int i = 0; i < 10; i++)
                mc.ingameGUI.blit(x - i * 8, y, 18, 0, 7, 9);
        } else if (value <= 25) {
            int control = value / 5;
            int half = 5 - value % 5;
            for (int i = 0; i < 10; i++) {
                if (i < control)
                    mc.ingameGUI.blit(x - i * 8, y, 12, 0, 7, 9);
                else if (i == control) {
                    mc.ingameGUI.blit(x - i * 8, y, 0, 0, 7, 9);
                    mc.ingameGUI.blit(x - i * 8 + half + 1, y, 13 + half, 0, 5 - half, 9);
                } else mc.ingameGUI.blit(x - i * 8, y, 0, 0, 7, 9);
            }
        } else {
            int control = value / 5;
            int half = 5 - value % 5;
            for (int i = 0; i < 10; i++) {
                if (i < control)
                    mc.ingameGUI.blit(x - i * 8, y, 6, 0, 7, 9);
                else if (i == control) {
                    mc.ingameGUI.blit(x - i * 8, y, 0, 0, 7, 9);
                    mc.ingameGUI.blit(x - i * 8 + half + 1, y, 7 + half, 0, 5 - half, 9);
                } else mc.ingameGUI.blit(x - i * 8, y, 0, 0, 7, 9);
            }
        }
        // MANA
        IMana mana = IMana.getFromPlayer(mc.player);
        value = mana.getLevel();
        if (mana.isOpenGui() && value > 0) {
            int scale = (int) (mana.getValue() * 99.0F / mana.getLimit());
            int ty;
            x = mc.getMainWindow().getScaledWidth() / 38;
            y = mc.getMainWindow().getScaledHeight() / 26;
            if (mana.getType() == IMana.Type.NATURE) {
                mc.ingameGUI.blit(x, y, 0, 9, 125, 13);
                ty = 81;
            } else if (mana.getType() == IMana.Type.FIRE) {
                mc.ingameGUI.blit(x, y, 0, 21, 125, 13);
                ty = 116;
            } else {
                mc.ingameGUI.blit(x, y, 0, 33, 125, 13);
                ty = 151;
            }
            if (value <= 5)
                mc.ingameGUI.blit(x + 22, y + 3, 0, 46 + (value - 1) * 7, scale, 7);
            else mc.ingameGUI.blit(x + 22, y + 3, 0, ty + (value - 6) * 7, scale, 7);
            mc.ingameGUI.blit(x + 6, y + 4, 0, 186 + (value - 1) * 5, 9, 5);
//            mc.ingameGUI.drawString(mc.fontRenderer, mana.getValue() + "/" + mana.getLimit(), x + 52, 4, 4210752);
        }

        mc.getTextureManager().bindTexture(MC_ICONS);
    }
}