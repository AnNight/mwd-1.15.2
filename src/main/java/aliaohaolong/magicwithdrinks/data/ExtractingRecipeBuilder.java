package aliaohaolong.magicwithdrinks.data;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.api.MWDRecipes;
import com.google.gson.JsonObject;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.advancements.IRequirementsStrategy;
import net.minecraft.advancements.criterion.RecipeUnlockedTrigger;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.util.ResourceLocation;

import java.util.Objects;
import java.util.function.Consumer;

public class ExtractingRecipeBuilder {
    private final String ingredient;
    private final int cookingTime;
    private final float output;
    private final Advancement.Builder advancementBuilder = Advancement.Builder.builder();

    public ExtractingRecipeBuilder(Item ingredientIn, int cookingTimeIn, float outputIn) {
        if (cookingTimeIn <= 0) throwIllegalArgumentException("CookingTime must be greater than 0.", ingredientIn, cookingTimeIn, outputIn);
        if (outputIn <= 0) throwIllegalArgumentException("Output must be greater than 0.", ingredientIn, cookingTimeIn, outputIn);
        ingredient = Objects.requireNonNull(ingredientIn.getRegistryName()).toString();
        cookingTime = cookingTimeIn;
        output = outputIn;
    }

    private static void throwIllegalArgumentException(String str, Item ingredientIn, int cookingTimeIn, float outputIn) {
        throw new IllegalArgumentException(MWD.EXCEPTION.concat("Build extracting recipes: " + str)
                .concat(" {Ingredient:\"" + ingredientIn.getTranslationKey())
                .concat("\", CookingTime:" + cookingTimeIn)
                .concat(", Output:" + outputIn + "}"));
    }

    public static ExtractingRecipeBuilder recipe(Item ingredientIn, int cookingTimeIn, float outputIn) {
        return new ExtractingRecipeBuilder(ingredientIn, cookingTimeIn, outputIn);
    }

    public ExtractingRecipeBuilder addCriterion(String name, ICriterionInstance criterionIn) {
        advancementBuilder.withCriterion(name, criterionIn);
        return this;
    }

    public void build(Consumer<IFinishedRecipe> consumerIn) {
        build(consumerIn, new ResourceLocation(ingredient));
    }

    public void build(Consumer<IFinishedRecipe> consumerIn, String save) {
        if (save.equals(ingredient)) throw new IllegalStateException("Extracting Recipe " + save + " should remove its 'save' argument");
        else build(consumerIn, new ResourceLocation(save));
    }

    public void build(Consumer<IFinishedRecipe> consumerIn, ResourceLocation idIn) {
        validate(idIn);
        advancementBuilder.withParentId(new ResourceLocation("recipes/root")).withCriterion("has_the_recipe", new RecipeUnlockedTrigger.Instance(idIn)).withRewards(AdvancementRewards.Builder.recipe(idIn)).withRequirementsStrategy(IRequirementsStrategy.OR);
        consumerIn.accept(new Result(idIn, ingredient, cookingTime, output, advancementBuilder, new ResourceLocation(idIn.getNamespace(), "recipes/magicwithdrinks_extracting/" + idIn.getPath())));
    }

    private void validate(ResourceLocation id) {
        if (advancementBuilder.getCriteria().isEmpty())
            throw new IllegalStateException("No way of obtaining recipe " + id);
    }

    @SuppressWarnings("NullableProblems")
    public static class Result implements IFinishedRecipe {
        private final ResourceLocation id;
        private final String ingredient;
        private final int cookingTime;
        private final float output;
        private final Advancement.Builder advancementBuilder;
        private final ResourceLocation advancementId;

        public Result(ResourceLocation idIn, String ingredientIn, int cookingTimeIn, float outputIn, Advancement.Builder advancementBuilderIn, ResourceLocation advancementIdIn) {
            id = idIn;
            ingredient = ingredientIn;
            cookingTime = cookingTimeIn;
            output = outputIn;
            advancementBuilder = advancementBuilderIn;
            advancementId = advancementIdIn;
        }

        @Override
        public void serialize(JsonObject json) {
            json.addProperty("ingredient", ingredient);
            json.addProperty("cookingTime", cookingTime);
            json.addProperty("output", output);
        }

        @Override
        public IRecipeSerializer<?> getSerializer() {
            return MWDRecipes.EXTRACTING_S;
        }

        @Override
        public ResourceLocation getID() {
            return id;
        }

        @Override
        public JsonObject getAdvancementJson() {
            return advancementBuilder.serialize();
        }

        @Override
        public ResourceLocation getAdvancementID() {
            return advancementId;
        }
    }
}