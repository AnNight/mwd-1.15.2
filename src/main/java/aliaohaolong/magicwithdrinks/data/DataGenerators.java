package aliaohaolong.magicwithdrinks.data;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

/**
 * In assets directory, only generate the language files.
 * In data directory, generate loot tables, recipes, tags and advancements.
 */

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGenerators {
    @SubscribeEvent
    public static void gatherData(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();

        LanguageHandler provider = new LanguageHandler();
        generator.addProvider(new ModEnUsProvider(generator, provider.lists));
        generator.addProvider(new ModZhCnProvider(generator, provider.lists));

        generator.addProvider(new ModLootTableProvider(generator));
        generator.addProvider(new ModRecipeProvider(generator));
        generator.addProvider(new ModBlockTagsProvider(generator));
        generator.addProvider(new ModItemTagsProvider(generator));
    }
}