package aliaohaolong.magicwithdrinks.data;

import aliaohaolong.magicwithdrinks.api.*;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

import java.util.*;

public class LanguageHandler {
    protected HashSet<String[]> lists = new HashSet<>();

    public LanguageHandler() {
        initPotions();
        initBlocks();
        initItems();
        initOthers();
    }

    private void initPotions() {
        add(MWDItems.PURIFIED_WATER, "Purified Water", "纯净水");
        add(MWDItems.SUGAR_WATER, "Sugar Water", "糖水");
        add(MWDItems.APPLE_JUICE, "Apple Juice", "苹果汁");
        add(MWDItems.MELON_JUICE, "Melon Juice", "西瓜汁");
        add(MWDItems.CARROT_JUICE, "Carrot Juice", "胡萝卜汁");
        add(MWDItems.PUMPKIN_JUICE, "Pumpkin Juice", "南瓜汁");
        add(MWDItems.SWEET_BERRIES_JUICE, "Sweet Berries Juice", "甜浆果汁");
        add(MWDPotionUtils.addPotionToStack(new ItemStack(MWDItems.POTION), MWDPotions.EMPTY), "Unreal Potion", "虚幻 - 药剂");
        add(MWDPotionUtils.addPotionToStack(new ItemStack(MWDItems.POTION), MWDPotions.WATER), "Water - Potion", "水 - 药剂");
        add(MWDPotionUtils.addPotionToStack(new ItemStack(MWDItems.POTION), MWDPotions.PURIFIED_WATER), "Purified Water - Potion", "纯净水 - 药剂");
        add(MWDPotionUtils.addPotionToStack(new ItemStack(MWDItems.POTION), MWDPotions.CATALYST), "Catalyst - Potion", "催化剂 - 药剂");
    }

    private void add(ItemStack key, String en, String zh) {
        lists.add(new String[]{key.getTranslationKey(), en, zh});
    }

    private void initBlocks() {
        add(MWDBlocks.COTTON, "Cotton Crops", "棉作物");
        add(MWDBlocks.WATER_PURIFIER, "Water Purifier", "净水器");
        add(MWDBlocks.VIOLET_ORE, "Violet Ore", "紫罗兰矿石");
        add(MWDBlocks.VIOLET_GLASS, "Violet Glass", "紫罗兰玻璃");
        add(MWDBlocks.WATER_FOUNTAIN, "Water Fountain", "饮水机");
        add(MWDBlocks.CREATIVE_MANA_BLOCK, "Creative Mana Block", "创造法力方块");
        add(MWDBlocks.BLUE_GOLD_ORE, "Blue Gold Ore", "蓝金矿石");
        add(MWDBlocks.MAGIC_WATER_FOUNTAIN, "Magic Water Fountain", "魔法饮水机");
        add(MWDBlocks.MANA_CONDUIT, "Mana Conduit", "法力导管");
        add(MWDBlocks.MANA_CRYSTAL_ORE, "Mana Crystal Ore", "魔晶石矿石");
        add(MWDBlocks.ME_EXTRACTOR, "ME Extractor", "ME提取机");
        add(MWDBlocks.BLENDER, "Blender", "料理机");
        add(MWDBlocks.SMELTING_BOX, "Smelting Box", "精炼盒子");
    }

    private void add(Block key, String en, String zh) {
        lists.add(new String[]{key.getTranslationKey(), en, zh});
    }

    private void initItems() {
        add(MWDItems.IMPURITY, "Impurity", "杂质");
        add(MWDItems.POTION_BOTTLE, "Potion Bottle", "药剂瓶");
        add(MWDItems.POTION, "Potion", "药剂");
        add(MWDItems.FEATHERY_FILTER_SLICE, "Feathery Filter Slice", "羽毛过滤片");
        add(MWDItems.WOOLEN_FILTER_SLICE, "Woolen Filter Slice", "羊毛过滤片");
        add(MWDItems.COTTONY_FILTER_SLICE, "Cottony Filter Slice", "棉过滤片");
        add(MWDItems.BAMBOO_FILTER_SLICE, "Bamboo Filter Slice", "竹过滤片");
        add(MWDItems.GOLDEN_FILTER_SLICE, "Golden Filter Slice", "金制过滤片");
        add(MWDItems.COTTON_SEEDS, "Cotton Seeds", "棉花种子");
        add(MWDItems.COTTON, "Cotton", "棉花");
        add(MWDItems.VIOLET_INGOT, "Violet Ingot", "紫罗兰锭");
        add(MWDItems.BLUE_GOLD_POWDER, "Blue Gold Powder", "蓝金粉");
        add(MWDItems.MANA_CRYSTAL, "Mana Crystal", "魔晶石");
        add(MWDItems.BLUE_GOLD_CORE, "Blue Gold Core", "蓝金核心");
        add(MWDItems.MANA_CRYSTAL_BATTERY, "Mana Crystal Battery", "魔晶石电池");
        add(MWDItems.ELEMENTARY_ME_CARD, "Elementary ME Card", "基础ME卡");
        add(MWDItems.INTERMEDIATE_ME_CARD, "Intermediate ME Card", "中级ME卡");
        add(MWDItems.ADVANCED_ME_CARD, "Advanced ME Card", "高级ME卡");
        add(MWDItems.ADVANCED_MANA_CRYSTAL_BATTERY, "Advanced Mana Crystal Battery", "高级魔晶石电池");
    }

    private void add(Item key, String en, String zh) {
        lists.add(new String[]{key.getTranslationKey(), en, zh});
    }

    private void initOthers() {
        add(MWDGroupsAndMaterials.BLOCKS, "MWD Blocks", "MWD方块");
        add(MWDGroupsAndMaterials.MATERIALS, "MWD Materials", "MWD材料");
        addCommands("water", "%s has %s water", "%s拥有%s点水分");
        addCommands("water.set.single", "Set water for %s to %s", "将%s的水分设为%s");
        addCommands("water.set.multiple", "Set water for %s players to %s", "将%个玩家的水分设为%s");
        addCommands("water.add.single", "Added %s to water for %s (now %s)", "将%2$s的水分增加了%1$s（现在是%3$s）");
        addCommands("water.add.multiple", "Added %s to water for %s players", "将%2$s个玩家的水分增加了%1$s");
        addCommands("water.remove.single", "Removed %s from water for %s (now %s)", "将%2$s的水分减少了%1$s（现在是%3$s）");
        addCommands("water.remove.multiple", "Removed %s from water for %s players", "将%2$s个玩家的水分减少%1$s");
        addCommands("mana:fire", "[Fire] %s (%s Level): %sME / %sME", "【火】 %s (%s级): %sME / %sME");
        addCommands("mana:ice", "[Ice] %s (%s Level): %sME / %sME", "【冰】 %s (%s级): %sME / %sME");
        addCommands("mana:nature", "[Nature] %s (%s Level): %sME / %sME", "【自然】 %s (%s级): %sME / %sME");
        addCommands("mana.open_gui", "Gui has opened", "Gui已经打开");
        addCommands("mana.close_gui", "Gui has closed", "Gui已经关闭");
        addCommands("mana.set.value.single", "Set mana for %s to %sME", "将%s的法力值设为%sME");
        addCommands("mana.set.value.multiple", "Set mana for %s players to %sME", "将%s个玩家的法力值设为%sME");
        addCommands("mana.set.limit.single", "Set maximum mana for %s to %sME", "将%s的最大法力值设为%sME");
        addCommands("mana.set.limit.multiple", "Set maximum mana for %s players to %sME", "将%s个玩家的最大法力值设为%sME");
        addCommands("mana.set.level.single", "Set level for %s to %s", "将%s的等级设为%s");
        addCommands("mana.set.level.multiple", "Set level for %s players to %s", "将%s个玩家的等级设为%s");
        addCommands("mana.set.type.single.nature", "Set type for %s to Nature", "将%s的属性设为自然");
        addCommands("mana.set.type.multiple.nature", "Set type for %s players to Nature", "将%s个玩家的属性设为自然");
        addCommands("mana.set.type.single.fire", "Set type for %s to Fire", "将%s的属性设为火");
        addCommands("mana.set.type.multiple.fire", "Set type for %s players to Fire", "将%s个玩家的属性设为火");
        addCommands("mana.set.type.single.ice", "Set type for %s to Ice", "将%s的属性设为冰");
        addCommands("mana.set.type.multiple.ice", "Set type for %s players to Ice", "将%s个玩家的属性设为冰");
        addCommands("mana.add.value.single", "Added %s to mana for %sME (now %sME)", "将%2$s的法力值增加了%1$sME（现在是%3$sME）");
        addCommands("mana.add.value.multiple", "Added %s to mana for %s playersME", "将%2$s个玩家的法力值增加了%1$sME");
        addCommands("mana.add.limit.single", "Added %s to maximum mana for %sME (now %sME)", "将%2$s的最大法力值增加了%1$sME（现在是%3$sME）");
        addCommands("mana.add.limit.multiple", "Added %s to maximum mana for %sME players", "将%2$s个玩家的最大法力值增加了%1$sME");
        addCommands("mana.remove.value.single", "Removed %s from mana for %sME (now %sME)", "将%2$s的法力值减少了%1$sME（现在是%3$sME）");
        addCommands("mana.remove.value.multiple", "Removed %s from mana for %sME players", "将%2$s个玩家的法力值减少%1$sME");
        addCommands("mana.remove.limit.single", "Removed %s from maximum mana for %sME (now %sME)", "将%2$s的最大法力值减少了%1$sME（现在是%3$sME）");
        addCommands("mana.remove.limit.multiple", "Removed %s from maximum mana for %sME players", "将%2$s个玩家的最大法力值减少%1$sME");
        add("death.attack.dryout", "%s parched to death", "%s 渴死了");
        add("death.attack.poison", "%s poison to death", "%s 中毒而死");
        addLore("water", "Water %s", "水分 %s");
        addLore("water_single", "Water %s(%s)", "水分 %s(%s)");
        addLore("water_complex", "Water %s(%s) / %s(%s)", "水分 %s(%s) / %s(%s)");
        addLore("mana", "Mana %sME", "法力值 %sME");
        addLore("mana_single", "Mana %sME(%s)", "法力值 %sME(%s)");
        addLore("mana_complex", "Mana %sME(%s) / %sME(%s)", "法力值 %sME(%s) / %sME(%s)");
        addLore("max_mana", "Maximum Mana %sME", "最大法力值 %sME");
        addLore("max_mana_single", "Maximum Mana %sME(%s)", "最大法力值 %sME(%s)");
        addLore("max_mana_complex", "Maximum Mana %sME(%s) / %sME(%s)", "最大法力值 %sME(%s) / %sME(%s)");
        addContainer("water_purifier", "Water Purifier", "净水器");
        addContainer("water_fountain", "Water Fountain", "饮水机");
        addContainer("magic_water_fountain", "Magic Water Fountain", "魔法饮水机");
        addContainer("me_extractor", "ME Extractor", "ME提取机");
        addContainer("blender", "Blender", "料理机");
        addContainer("smelting_box", "Smelting Box", "精炼盒子");
    }

    private void addCommands(String key, String en, String zh) {
        lists.add(new String[]{"commands.magicwithdrinks." + key, en, zh});
    }

    private void addLore(String key, String en, String zh) {
        lists.add(new String[]{"magicwithdrinks.lore." + key, en, zh});
    }

    private void addContainer(String key, String en, String zh) {
        lists.add(new String[]{"container.magicwithdrinks." + key, en, zh});
    }

    private void add(String key, String en, String zh) {
        lists.add(new String[]{key, en, zh});
    }

    private void add(ItemGroup g, String en, String zh) {
        lists.add(new String[]{g.getTranslationKey(), en, zh});
    }
}