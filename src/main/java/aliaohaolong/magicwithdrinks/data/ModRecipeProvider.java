package aliaohaolong.magicwithdrinks.data;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import net.minecraft.advancements.criterion.InventoryChangeTrigger;
import net.minecraft.advancements.criterion.ItemPredicate;
import net.minecraft.data.*;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;

import java.util.function.Consumer;

@SuppressWarnings({"NullableProblems", "ConstantConditions"})
public class ModRecipeProvider extends RecipeProvider {
    public ModRecipeProvider(DataGenerator generator) {
        super(generator);
    }

    @Override
    protected void registerRecipes(Consumer<IFinishedRecipe> consumer) {
        ShapedRecipeBuilder.shapedRecipe(MWDItems.WATER_PURIFIER).setGroup(MWDItems.WATER_PURIFIER.getRegistryName().toString())
                .patternLine("x x").patternLine(" y ").patternLine("x x").key('x', Items.IRON_INGOT).key('y', Items.FURNACE)
                .addCriterion("has_iron_ingot", InventoryChangeTrigger.Instance.forItems(Items.IRON_INGOT))
                .addCriterion("has_furnace", InventoryChangeTrigger.Instance.forItems(Items.FURNACE)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.FEATHERY_FILTER_SLICE).setGroup(MWDItems.FEATHERY_FILTER_SLICE.getRegistryName().toString())
                .patternLine("xxx").patternLine("xyx").patternLine("xxx").key('x', Items.FEATHER).key('y', Items.STICK)
                .addCriterion("has_feathery", InventoryChangeTrigger.Instance.forItems(Items.FEATHER))
                .addCriterion("has_stick", InventoryChangeTrigger.Instance.forItems(Items.STICK)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.WOOLEN_FILTER_SLICE).setGroup(MWDItems.WOOLEN_FILTER_SLICE.getRegistryName().toString())
                .patternLine("yzy").patternLine("yxy").patternLine("yzy").key('x', Items.WHITE_WOOL).key('y', Items.STICK).key('z', Items.STRING)
                .addCriterion("has_white_wool", InventoryChangeTrigger.Instance.forItems(Items.WHITE_WOOL))
                .addCriterion("has_stick", InventoryChangeTrigger.Instance.forItems(Items.STICK))
                .addCriterion("has_string", InventoryChangeTrigger.Instance.forItems(Items.STRING)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.COTTONY_FILTER_SLICE).setGroup(MWDItems.COTTONY_FILTER_SLICE.getRegistryName().toString())
                .patternLine("xzx").patternLine("xyx").patternLine("xzx").key('x', MWDItems.COTTON).key('y', ItemTags.WOODEN_FENCES).key('z', Items.STRING)
                .addCriterion("has_cotton", InventoryChangeTrigger.Instance.forItems(MWDItems.COTTON))
                .addCriterion("has_wooden_fence", InventoryChangeTrigger.Instance.forItems(ItemPredicate.Builder.create().tag(ItemTags.WOODEN_FENCES).build()))
                .addCriterion("has_string", InventoryChangeTrigger.Instance.forItems(Items.STRING)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.BAMBOO_FILTER_SLICE).setGroup(MWDItems.BAMBOO_FILTER_SLICE.getRegistryName().toString())
                .patternLine("yyy").patternLine("yxy").patternLine("yyy").key('x', MWDItems.COTTONY_FILTER_SLICE).key('y', Items.BAMBOO)
                .addCriterion("has_cottony_filter_slice", InventoryChangeTrigger.Instance.forItems(MWDItems.COTTONY_FILTER_SLICE))
                .addCriterion("has_bamboo", InventoryChangeTrigger.Instance.forItems(Items.BAMBOO)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.GOLDEN_FILTER_SLICE).setGroup(MWDItems.GOLDEN_FILTER_SLICE.getRegistryName().toString())
                .patternLine("xxx").patternLine("xyx").patternLine("xxx").key('x', MWDItems.BAMBOO_FILTER_SLICE).key('y', Items.GOLD_INGOT)
                .addCriterion("has_bamboo_filter_slice", InventoryChangeTrigger.Instance.forItems(MWDItems.BAMBOO_FILTER_SLICE))
                .addCriterion("has_gold_ingot", InventoryChangeTrigger.Instance.forItems(Items.GOLD_INGOT)).build(consumer);
        CookingRecipeBuilder.smeltingRecipe(Ingredient.fromItems(MWDBlocks.VIOLET_ORE), MWDItems.VIOLET_INGOT, 0.9f, 200)
                .addCriterion("has_violet_ore", InventoryChangeTrigger.Instance.forItems(MWDItems.VIOLET_ORE)).build(consumer);
        CookingRecipeBuilder.blastingRecipe(Ingredient.fromItems(MWDBlocks.VIOLET_ORE), MWDItems.VIOLET_INGOT, 0.9f, 100)
                .addCriterion("has_violet_ore", InventoryChangeTrigger.Instance.forItems(MWDItems.VIOLET_ORE))
                .build(consumer, new ResourceLocation(MWD.MOD_ID, MWDItems.VIOLET_INGOT.getRegistryName().getPath().concat("_from_blasting")));
        ShapedRecipeBuilder.shapedRecipe(MWDItems.VIOLET_GLASS, 4).setGroup(MWDItems.VIOLET_GLASS.getRegistryName().toString())
                .patternLine(" x ").patternLine("xyx").patternLine(" x ").key('x', Items.GLASS).key('y', MWDItems.VIOLET_INGOT)
                .addCriterion("has_glass", InventoryChangeTrigger.Instance.forItems(Items.GLASS))
                .addCriterion("has_violet_ingot", InventoryChangeTrigger.Instance.forItems(MWDItems.VIOLET_INGOT)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.POTION_BOTTLE, 3).setGroup(MWDItems.POTION_BOTTLE.getRegistryName().toString())
                .patternLine(" y ").patternLine("x x").patternLine(" x ").key('x', MWDItems.VIOLET_GLASS).key('y', Items.GOLD_NUGGET)
                .addCriterion("has_violet_glass", InventoryChangeTrigger.Instance.forItems(MWDItems.VIOLET_GLASS))
                .addCriterion("has_gold_nugget", InventoryChangeTrigger.Instance.forItems(Items.GOLD_NUGGET)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.BLUE_GOLD_CORE).setGroup(MWDItems.BLUE_GOLD_CORE.getRegistryName().toString())
                .patternLine("xxx").patternLine("xyx").patternLine("xxx").key('x', MWDItems.BLUE_GOLD_POWDER).key('y', Items.GOLD_INGOT)
                .addCriterion("has_blue_gold_powder", InventoryChangeTrigger.Instance.forItems(MWDItems.BLUE_GOLD_POWDER))
                .addCriterion("has_gold_ingot", InventoryChangeTrigger.Instance.forItems(Items.GOLD_INGOT)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.WATER_FOUNTAIN).setGroup(MWDItems.WATER_FOUNTAIN.getRegistryName().toString())
                .patternLine("xxx").patternLine("yzy").patternLine("xxx").key('x', Items.IRON_INGOT).key('y', Items.BUCKET).key('z', Items.FURNACE)
                .addCriterion("has_iron_ingot", InventoryChangeTrigger.Instance.forItems(Items.IRON_INGOT))
                .addCriterion("has_bucket", InventoryChangeTrigger.Instance.forItems(Items.BUCKET))
                .addCriterion("has_furnace", InventoryChangeTrigger.Instance.forItems(Items.FURNACE)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.MAGIC_WATER_FOUNTAIN).setGroup(MWDItems.MAGIC_WATER_FOUNTAIN.getRegistryName().toString())
                .patternLine("xxx").patternLine("yzy").patternLine("x#x")
                .key('x', Items.IRON_INGOT).key('y', Items.BUCKET).key('z', MWDItems.BLUE_GOLD_CORE).key('#', MWDItems.MANA_CRYSTAL_BATTERY)
                .addCriterion("has_iron_ingot", InventoryChangeTrigger.Instance.forItems(Items.IRON_INGOT))
                .addCriterion("has_bucket", InventoryChangeTrigger.Instance.forItems(Items.BUCKET))
                .addCriterion("has_blue_gold_core", InventoryChangeTrigger.Instance.forItems(MWDItems.BLUE_GOLD_CORE))
                .addCriterion("has_mana_crystal_battery", InventoryChangeTrigger.Instance.forItems(MWDItems.MANA_CRYSTAL_BATTERY)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.MANA_CRYSTAL_BATTERY).setGroup(MWDItems.MANA_CRYSTAL_BATTERY.getRegistryName().toString())
                .patternLine("xyx").patternLine("yzy").patternLine("xyx").key('x', MWDItems.VIOLET_INGOT).key('y', MWDItems.MANA_CRYSTAL).key('z', MWDItems.BLUE_GOLD_CORE)
                .addCriterion("has_violet_ingot", InventoryChangeTrigger.Instance.forItems(MWDItems.VIOLET_INGOT))
                .addCriterion("has_mana_crystal", InventoryChangeTrigger.Instance.forItems(MWDItems.MANA_CRYSTAL))
                .addCriterion("has_blue_gold_core", InventoryChangeTrigger.Instance.forItems(MWDItems.BLUE_GOLD_CORE)).build(consumer);
        BlendingRecipeBuilder.recipe(DataGenUtils.transfer(MWDItems.PURIFIED_WATER), new ItemStack(Items.SUGAR), DataGenUtils.transfer(MWDItems.SUGAR_WATER), 20)
                .addCriterion("has_sugar", InventoryChangeTrigger.Instance.forItems(Items.SUGAR)).build(consumer);
        BlendingRecipeBuilder.recipe(DataGenUtils.transfer(MWDItems.SUGAR_WATER), new ItemStack(Items.APPLE), DataGenUtils.transfer(MWDItems.APPLE_JUICE), 120)
                .addCriterion("has_apple", InventoryChangeTrigger.Instance.forItems(Items.APPLE)).build(consumer);
        BlendingRecipeBuilder.recipe(DataGenUtils.transfer(MWDItems.SUGAR_WATER), new ItemStack(Items.MELON_SLICE), DataGenUtils.transfer(MWDItems.MELON_JUICE), 100)
                .addCriterion("has_melon_slice", InventoryChangeTrigger.Instance.forItems(Items.MELON_SLICE)).build(consumer);
        BlendingRecipeBuilder.recipe(DataGenUtils.transfer(MWDItems.SUGAR_WATER), new ItemStack(Items.CARROT), DataGenUtils.transfer(MWDItems.CARROT_JUICE), 120)
                .addCriterion("has_carrot", InventoryChangeTrigger.Instance.forItems(Items.CARROT)).build(consumer);
        BlendingRecipeBuilder.recipe(DataGenUtils.transfer(MWDItems.SUGAR_WATER), new ItemStack(Items.PUMPKIN), DataGenUtils.transfer(MWDItems.PUMPKIN_JUICE), 120)
                .addCriterion("has_pumpkin", InventoryChangeTrigger.Instance.forItems(Items.PUMPKIN)).build(consumer);
        BlendingRecipeBuilder.recipe(DataGenUtils.transfer(MWDItems.SUGAR_WATER), new ItemStack(Items.SWEET_BERRIES, 2), DataGenUtils.transfer(MWDItems.SWEET_BERRIES_JUICE), 100)
                .addCriterion("has_sweet_berries", InventoryChangeTrigger.Instance.forItems(Items.SWEET_BERRIES)).build(consumer);
        ShapedRecipeBuilder.shapedRecipe(MWDItems.BLENDER).setGroup(MWDItems.BLENDER.getRegistryName().toString())
                .patternLine("x x").patternLine(" y ").patternLine("x x").key('x', Items.IRON_INGOT).key('y', Items.SMOKER)
                .addCriterion("has_iron_ingot", InventoryChangeTrigger.Instance.forItems(Items.IRON_INGOT))
                .addCriterion("has_smoker", InventoryChangeTrigger.Instance.forItems(Items.SMOKER)).build(consumer);
        ExtractingRecipeBuilder.recipe(Items.POTATO, 100, 1.2F).addCriterion("has_potato", InventoryChangeTrigger.Instance.forItems(Items.POTATO)).build(consumer);
        ExtractingRecipeBuilder.recipe(Items.POISONOUS_POTATO, 160, 6.0F).addCriterion("has_poisonous_potato", InventoryChangeTrigger.Instance.forItems(Items.POISONOUS_POTATO)).build(consumer);
    }
}