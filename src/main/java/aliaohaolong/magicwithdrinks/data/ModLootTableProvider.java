package aliaohaolong.magicwithdrinks.data;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.block.CottonBlock;
import net.minecraft.advancements.criterion.EnchantmentPredicate;
import net.minecraft.advancements.criterion.ItemPredicate;
import net.minecraft.advancements.criterion.MinMaxBounds;
import net.minecraft.advancements.criterion.StatePropertiesPredicate;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.world.storage.loot.*;
import net.minecraft.world.storage.loot.conditions.BlockStateProperty;
import net.minecraft.world.storage.loot.conditions.MatchTool;
import net.minecraft.world.storage.loot.conditions.SurvivesExplosion;
import net.minecraft.world.storage.loot.functions.ApplyBonus;
import net.minecraft.world.storage.loot.functions.ExplosionDecay;
import net.minecraft.world.storage.loot.functions.SetCount;

@SuppressWarnings("ConstantConditions")
public class ModLootTableProvider extends BaseLootTableProvider {
    public ModLootTableProvider(DataGenerator generator) {
        super(generator);
    }

    @Override
    protected void addTables() {
        put(MWDBlocks.WATER_PURIFIER);
        lootTables.put(MWDBlocks.COTTON, LootTable.builder()
                .addLootPool(LootPool.builder().name(MWDBlocks.COTTON.getRegistryName().toString()).rolls(ConstantRange.of(1))
                        .addEntry(ItemLootEntry.builder(MWDItems.COTTON)
                                .acceptCondition(BlockStateProperty.builder(MWDBlocks.COTTON)
                                        .fromProperties(StatePropertiesPredicate.Builder.newBuilder().withIntProp(CottonBlock.AGE, 7)))
                                .acceptFunction(SetCount.builder(new RandomValueRange(1, 2)))
                                .alternatively(ItemLootEntry.builder(MWDItems.COTTON_SEEDS))))
                .addLootPool(LootPool.builder().name(MWDBlocks.COTTON.getRegistryName().toString().concat("_extra")).rolls(ConstantRange.of(1))
                        .addEntry(ItemLootEntry.builder(MWDItems.COTTON_SEEDS)
                                .acceptFunction(ApplyBonus.binomialWithBonusCount(Enchantments.FORTUNE, 0.5714286f, 3)))
                        .acceptCondition(BlockStateProperty.builder(MWDBlocks.COTTON)
                                .fromProperties(StatePropertiesPredicate.Builder.newBuilder().withIntProp(CottonBlock.AGE, 7))))
                .acceptFunction(ExplosionDecay.builder()));
        put(MWDBlocks.VIOLET_ORE);
        lootTables.put(MWDBlocks.VIOLET_GLASS, LootTable.builder().addLootPool(LootPool.builder().name(MWDBlocks.VIOLET_GLASS.getRegistryName().toString()).rolls(ConstantRange.of(1))
                .addEntry(ItemLootEntry.builder(MWDBlocks.VIOLET_GLASS))
                .acceptCondition(MatchTool.builder(ItemPredicate.Builder.create().enchantment(new EnchantmentPredicate(Enchantments.SILK_TOUCH, MinMaxBounds.IntBound.atLeast(1)))))));
        put(MWDBlocks.WATER_FOUNTAIN);
        put(MWDBlocks.CREATIVE_MANA_BLOCK);
        lootTables.put(MWDBlocks.BLUE_GOLD_ORE, LootTable.builder()
                .addLootPool(LootPool.builder().name(MWDBlocks.BLUE_GOLD_ORE.getRegistryName().toString()).rolls(ConstantRange.of(1))
                        .addEntry(ItemLootEntry.builder(MWDItems.BLUE_GOLD_ORE)
                                .acceptCondition(MatchTool.builder(ItemPredicate.Builder.create().enchantment(new EnchantmentPredicate(Enchantments.SILK_TOUCH, MinMaxBounds.IntBound.atLeast(1)))))
                                .alternatively(ItemLootEntry.builder(MWDItems.BLUE_GOLD_POWDER)
                                        .acceptFunction(SetCount.builder(new RandomValueRange(2, 3)))
                                        .acceptFunction(ApplyBonus.uniformBonusCount(Enchantments.FORTUNE, 1))
                                        .acceptFunction(ExplosionDecay.builder())
                                )
                        )
                )
        );
        put(MWDBlocks.MAGIC_WATER_FOUNTAIN);
        put(MWDBlocks.MANA_CONDUIT);
        lootTables.put(MWDBlocks.MANA_CRYSTAL_ORE, LootTable.builder()
                .addLootPool(LootPool.builder().name(MWDBlocks.MANA_CRYSTAL_ORE.getRegistryName().toString()).rolls(ConstantRange.of(1))
                        .addEntry(ItemLootEntry.builder(MWDItems.MANA_CRYSTAL_ORE)
                                .acceptCondition(MatchTool.builder(ItemPredicate.Builder.create().enchantment(new EnchantmentPredicate(Enchantments.SILK_TOUCH, MinMaxBounds.IntBound.atLeast(1)))))
                                .alternatively(ItemLootEntry.builder(MWDItems.MANA_CRYSTAL)
                                        .acceptFunction(ApplyBonus.uniformBonusCount(Enchantments.FORTUNE, 1))
                                        .acceptCondition(SurvivesExplosion.builder())
                                )
                        )
                )
        );
        put(MWDBlocks.ME_EXTRACTOR);
        put(MWDBlocks.BLENDER);
    }

    private void put(Block block) {
        lootTables.put(block, LootTable.builder().addLootPool(
                LootPool.builder().name(block.getRegistryName().toString()).rolls(ConstantRange.of(1))
                        .addEntry(ItemLootEntry.builder(block))
                        .acceptCondition(SurvivesExplosion.builder())
        ));
    }
}