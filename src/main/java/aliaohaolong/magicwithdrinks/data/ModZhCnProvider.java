package aliaohaolong.magicwithdrinks.data;

import aliaohaolong.magicwithdrinks.MWD;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.LanguageProvider;

import java.util.HashSet;

public class ModZhCnProvider extends LanguageProvider {
    private final HashSet<String[]> lists;

    public ModZhCnProvider(DataGenerator dataGenerator, HashSet<String[]> lists) {
        super(dataGenerator, MWD.MOD_ID, "zh_cn");
        this.lists = lists;
    }

    @Override
    protected void addTranslations() {
        for (String[] strings : lists) {
            add(strings[0], strings[2]);
        }
    }
}