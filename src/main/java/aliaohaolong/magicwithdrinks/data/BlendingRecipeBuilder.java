package aliaohaolong.magicwithdrinks.data;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.api.MWDRecipes;
import com.google.gson.JsonObject;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.advancements.IRequirementsStrategy;
import net.minecraft.advancements.criterion.RecipeUnlockedTrigger;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.Objects;
import java.util.function.Consumer;

/**
 *  An example:
 *  {
 *    "type": "magicwithdrinks:blending",
 *    "vanilla": true,
 *    "ingredient": "magicwithdrinks:purified_water"
 *    "ingredient2": {
 *      "item": "minecraft:apple",
 *      "count": 2
 *    },
 *    "result": "magicwithdrinks:apple_juice",
 *    "cookingTime": 400
 *  }
 */

public class BlendingRecipeBuilder {
    private final JsonObject ingredient;
    private final ItemStack ingredient2;
    private final JsonObject result;
    private final int cookingTime;
    private final Advancement.Builder advancementBuilder = Advancement.Builder.builder();

    public BlendingRecipeBuilder(JsonObject ingredientIn, ItemStack ingredient2In, JsonObject resultIn, int cookingTimeIn) {
        if (cookingTimeIn <= 0)
            throw new IllegalArgumentException(MWD.EXCEPTION.concat("Build blending recipes: CookingTime must be greater than 0."));
        ingredient = ingredientIn;
        ingredient2 = ingredient2In;
        result = resultIn;
        cookingTime = cookingTimeIn;
    }

    public static BlendingRecipeBuilder recipe(JsonObject ingredientIn, ItemStack ingredient2In, JsonObject resultIn, int cookingTimeIn) {
        return new BlendingRecipeBuilder(ingredientIn, ingredient2In, resultIn, cookingTimeIn);
    }

    public BlendingRecipeBuilder addCriterion(String name, ICriterionInstance criterionIn) {
        advancementBuilder.withCriterion(name, criterionIn);
        return this;
    }

    public void build(Consumer<IFinishedRecipe> consumerIn) {
        if (result.has("item"))
            build(consumerIn, new ResourceLocation(JSONUtils.getString(result, "item")));
        else if (result.has("potion"))
            build(consumerIn, new ResourceLocation(JSONUtils.getString(result, "item")));
        else throw new IllegalStateException("Blending Recipe build failure");
    }

    public void build(Consumer<IFinishedRecipe> consumerIn, String suffix) {
        if (result.has("item"))
            build(consumerIn, new ResourceLocation(JSONUtils.getString(result, "item").concat(suffix)));
        else if (result.has("potion"))
            build(consumerIn, new ResourceLocation(JSONUtils.getString(result, "item").concat(suffix)));
        else throw new IllegalStateException("Blending Recipe build failure");
    }

    public void build(Consumer<IFinishedRecipe> consumerIn, ResourceLocation id) {
        validate(id);
        advancementBuilder.withParentId(new ResourceLocation("recipes/root")).withCriterion("has_the_recipe", new RecipeUnlockedTrigger.Instance(id)).withRewards(AdvancementRewards.Builder.recipe(id)).withRequirementsStrategy(IRequirementsStrategy.OR);
        consumerIn.accept(new BlendingRecipeBuilder.Result(id, ingredient, ingredient2, result, cookingTime, advancementBuilder, new ResourceLocation(id.getNamespace(), "recipes/magicwithdrinks_blending/" + id.getPath())));
    }

    private void validate(ResourceLocation id) {
        if (this.advancementBuilder.getCriteria().isEmpty())
            throw new IllegalStateException("No way of obtaining recipe " + id);
    }

    @SuppressWarnings("NullableProblems")
    public static class Result implements IFinishedRecipe {
        private final ResourceLocation id;
        private final JsonObject ingredient;
        private final ItemStack ingredient2;
        private final JsonObject result;
        private final int cookingTime;
        private final Advancement.Builder advancementBuilder;
        private final ResourceLocation advancementId;

        public Result(ResourceLocation idIn, JsonObject ingredientIn, ItemStack ingredient2In, JsonObject resultIn, int cookingTimeIn, Advancement.Builder advancementBuilderIn, ResourceLocation advancementIdIn) {
            id = idIn;
            ingredient = ingredientIn;
            ingredient2 = ingredient2In;
            result = resultIn;
            cookingTime = cookingTimeIn;
            advancementBuilder = advancementBuilderIn;
            advancementId = advancementIdIn;
        }

        @Override
        public void serialize(JsonObject json) {
            json.add("ingredient", ingredient);
            JsonObject ingredient2 = new JsonObject();
            ingredient2.addProperty("item", Objects.requireNonNull(ForgeRegistries.ITEMS.getKey(this.ingredient2.getItem())).toString());
            int count = this.ingredient2.getCount();
            if (count > 1)
                ingredient2.addProperty("count", count);
            json.add("ingredient2", ingredient2);
            json.add("result", result);
            json.addProperty("cookingTime", cookingTime);
        }

        @Override
        public IRecipeSerializer<?> getSerializer() {
            return MWDRecipes.BLENDING_S;
        }

        @Override
        public ResourceLocation getID() {
            return id;
        }

        @Override
        public JsonObject getAdvancementJson() {
            return advancementBuilder.serialize();
        }

        @Override
        public ResourceLocation getAdvancementID() {
            return advancementId;
        }
    }
}