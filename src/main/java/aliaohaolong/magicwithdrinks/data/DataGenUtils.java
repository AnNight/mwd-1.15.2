package aliaohaolong.magicwithdrinks.data;

import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotion;
import com.google.gson.JsonObject;
import net.minecraft.item.Item;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.Objects;

public class DataGenUtils {
    protected static JsonObject transfer(Item item) {
        JsonObject json = new JsonObject();
        json.addProperty("item", Objects.requireNonNull(ForgeRegistries.ITEMS.getKey(item)).toString());
        return json;
    }

    protected static JsonObject transfer(MWDPotion potion) {
        JsonObject json = new JsonObject();
        json.addProperty("potion", Objects.requireNonNull(MWDForgeRegistries.getPotions().getKey(potion)).toString());
        return json;
    }
}