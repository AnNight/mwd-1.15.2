package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.common.extra.AbstractNumberUnit;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import aliaohaolong.magicwithdrinks.common.extra.AbstractNumberUnit.IntUnit;
import aliaohaolong.magicwithdrinks.common.extra.attr.HungerAttribute;
import aliaohaolong.magicwithdrinks.common.extra.attr.WaterAttribute;
import net.minecraft.item.Items;

import java.util.Vector;

@SuppressWarnings("unused")
public class MWDExes {
    public static final Vector<Extra> ITEM_MES = new Vector<>();

    public static final Extra V_WATER = new Extra.Properties(null).isFood(new WaterAttribute(new AbstractNumberUnit.IntUnit(16, 0.4F, -20), 4)).build();
    public static final Extra M_WATER = new Extra.Properties(null).isFood(new WaterAttribute(new AbstractNumberUnit.IntUnit(8, 0.4F, -10), 2)).build();
    public static final Extra M_PURIFIED_WATER = new Extra.Properties(null).isFood(new WaterAttribute(8)).build();

    private static final Extra PURIFIED_WATER = init(new Extra.Properties(MWDItems.PURIFIED_WATER).isFood(new WaterAttribute(16)).build());
    private static final Extra SUGAR_WATER = init(new Extra.Properties(MWDItems.SUGAR_WATER).isFood(new WaterAttribute(15), new HungerAttribute(1, 0.2F)).build());
    private static final Extra APPLE_JUICE = init(new Extra.Properties(MWDItems.APPLE_JUICE).isFood(new WaterAttribute(12), new HungerAttribute(5, 0.5F)).build());
    private static final Extra MELON_JUICE = init(new Extra.Properties(MWDItems.MELON_JUICE).isFood(new WaterAttribute(12), new HungerAttribute(5, 0.8F)).build());
    private static final Extra CARROT_JUICE = init(new Extra.Properties(MWDItems.CARROT_JUICE).isFood(new WaterAttribute(12), new HungerAttribute(4, 0.8F)).build());
    private static final Extra PUMPKIN_JUICE = init(new Extra.Properties(MWDItems.PUMPKIN_JUICE).isFood(new WaterAttribute(12), new HungerAttribute(5, 0.3F)).build());
    private static final Extra SWEET_BERRIES_JUICE = init(new Extra.Properties(MWDItems.SWEET_BERRIES_JUICE).isFood(new WaterAttribute(12), new HungerAttribute(5, 0.4F)).build());
//    private static final Extra APPLE_WINE = new Extra.Properties(null).isFood(new WaterAttribute(5), new HungerAttribute(2, 0.15F)).build();
//    private static final Extra SWEET_BERRIES_WINE = new Extra.Properties(null).isFood(new WaterAttribute(5), new HungerAttribute(1, 0.05F)).build();

    private static final Extra APPLE = init(new Extra.Properties(Items.APPLE).isFood(new WaterAttribute(3)).build());
    private static final Extra MELON_SLICE = init(new Extra.Properties(Items.MELON_SLICE).isFood(new WaterAttribute(3)).build());
    private static final Extra SWEET_BERRIES = init(new Extra.Properties(Items.SWEET_BERRIES).isFood(new WaterAttribute(2)).build());
    private static final Extra MILK_BUCKET = init(new Extra.Properties(Items.MILK_BUCKET).isFood(new WaterAttribute(4)).build());
    private static final Extra MUSHROOM_STEW = init(new Extra.Properties(Items.MUSHROOM_STEW).isFood(new WaterAttribute(3)).build());
    private static final Extra RABBIT_STEW = init(new Extra.Properties(Items.RABBIT_STEW).isFood(new WaterAttribute(3)).build());
    private static final Extra BEETROOT_SOUP = init(new Extra.Properties(Items.BEETROOT_SOUP).isFood(new WaterAttribute(3)).build());
    private static final Extra PORKCHOP = init(new Extra.Properties(Items.PORKCHOP).isFood(new WaterAttribute(new IntUnit(0, 0.2F, -5), 2)).build());
    private static final Extra COD = init(new Extra.Properties(Items.COD).isFood(new WaterAttribute(new IntUnit(0, 0.2F, -5), 2)).build());
    private static final Extra BEEF = init(new Extra.Properties(Items.BEEF).isFood(new WaterAttribute(new IntUnit(0, 0.2F, -5), 2)).build());
    private static final Extra CHICKEN = init(new Extra.Properties(Items.CHICKEN).isFood(new WaterAttribute(new IntUnit(0, 0.2F, -5), 2)).build());
    private static final Extra RABBIT = init(new Extra.Properties(Items.RABBIT).isFood(new WaterAttribute(new IntUnit(0, 0.2F, -5), 2)).build());
    private static final Extra MUTTON = init(new Extra.Properties(Items.MUTTON).isFood(new WaterAttribute(new IntUnit(0, 0.2F, -5), 2)).build());
    private static final Extra SALMON = init(new Extra.Properties(Items.SALMON).isFood(new WaterAttribute(new IntUnit(0, 0.2F, -5), 2)).build());
    private static final Extra TROPICAL_FISH = init(new Extra.Properties(Items.TROPICAL_FISH).isFood(new WaterAttribute(new IntUnit(0, 0.2F, -5), 2)).build());
    private static final Extra ROTTEN_FLESH = init(new Extra.Properties(Items.ROTTEN_FLESH).isFood(new WaterAttribute(new IntUnit(0, 0.1F, -5), 2)).build());
    private static final Extra PUFFERFISH = init(new Extra.Properties(Items.PUFFERFISH).isFood(new WaterAttribute(-20)).build());
    private static final Extra POISONOUS_POTATO = init(new Extra.Properties(Items.POISONOUS_POTATO).isFood(new WaterAttribute(-15)).build());

    private static final Extra MANA_CRYSTAL = init(new Extra.Properties(MWDItems.MANA_CRYSTAL, 16).canStore(10, 0.04F, 0.08F).build());
    private static final Extra MANA_CRYSTAL_BATTERY = init(new Extra.Properties(MWDItems.MANA_CRYSTAL_BATTERY, 80).canStore(0, 0.08F, 0.16F).build());
    private static final Extra ADVANCED_MANA_CRYSTAL_BATTERY = init(new Extra.Properties(MWDItems.ADVANCED_MANA_CRYSTAL_BATTERY, 560).canStore(0, 0.16F, 0.32F).build());

    private static Extra init(Extra extra) {
        if (extra.getItem() != Items.AIR && extra.getItem().getRegistryName() != null) {
            extra.setRegistryName(extra.getItem().getRegistryName());
            ITEM_MES.add(extra);
        }
        return extra;
    }
}