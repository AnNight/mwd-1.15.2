package aliaohaolong.magicwithdrinks.api;

import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class MWDGroupsAndMaterials {
    public static final ItemGroup BLOCKS = new ItemGroup("magicwithdrinks_blocks") {
        public ItemStack createIcon() {
            return new ItemStack(MWDItems.VIOLET_GLASS);
        }
    };

    public static final ItemGroup MATERIALS = new ItemGroup("magicwithdrinks_materials") {
        public ItemStack createIcon() {
            return new ItemStack(MWDItems.POTION_BOTTLE);
        }
    };

    public static final Material MANA_CONDUIT = (new Material(MaterialColor.BLACK, false, false, true, false, true, false, false, PushReaction.DESTROY));
    public static final Material MACHINE = (new Material(MaterialColor.BLACK, false, true, false, true, false, false, false, PushReaction.DESTROY));
}