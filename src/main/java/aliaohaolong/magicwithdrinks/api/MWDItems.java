package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.common.item.*;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockNamedItem;
import net.minecraft.item.Item;

import java.util.Objects;

public class MWDItems {
    // drink
    public static final Item PURIFIED_WATER = createPotion().setRegistryName("purified_water");
    public static final Item SUGAR_WATER = createPotion().setRegistryName("sugar_water");
    public static final Item APPLE_JUICE = createPotion().setRegistryName("apple_juice");
    public static final Item MELON_JUICE = createPotion().setRegistryName("melon_juice");
    public static final Item CARROT_JUICE = createPotion().setRegistryName("carrot_juice");
    public static final Item PUMPKIN_JUICE = createPotion().setRegistryName("pumpkin_juice");
    public static final Item SWEET_BERRIES_JUICE = createPotion().setRegistryName("sweet_berries_juice");

    // material
    public static final Item IMPURITY = create().setRegistryName("impurity");

    // middleware
    public static final Item BLUE_GOLD_CORE = create().setRegistryName("blue_gold_core");

    // finished
    public static final Item MANA_CRYSTAL_BATTERY = create().setRegistryName("mana_crystal_battery");
    public static final Item ADVANCED_MANA_CRYSTAL_BATTERY = create().setRegistryName("advanced_mana_crystal_battery");

    // potion
    public static final Item POTION_BOTTLE = new PotionBottleItem(new Item.Properties().group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("potion_bottle");
    public static final Item POTION = new MWDPotionItem(new Item.Properties().maxStackSize(1).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("potion");

    // slice
    public static final Item FEATHERY_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(10).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("feathery_filter_slice");
    public static final Item WOOLEN_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(24).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("woolen_filter_slice");
    public static final Item COTTONY_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(64).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("cottony_filter_slice");
    public static final Item BAMBOO_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(88).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("bamboo_filter_slice");
    public static final Item GOLDEN_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(800).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("golden_filter_slice");
    public static final Item ELEMENTARY_ME_CARD = new MEExtractorCardItem(new Item.Properties().maxStackSize(1).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("elementary_me_card");
    public static final Item INTERMEDIATE_ME_CARD = new MEExtractorCardItem(new Item.Properties().maxStackSize(1).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("intermediate_me_card");
    public static final Item ADVANCED_ME_CARD = new MEExtractorCardItem(new Item.Properties().maxStackSize(1).group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("advanced_me_card");

    // crop
    public static final Item COTTON_SEEDS = new BlockNamedItem(MWDBlocks.COTTON, new Item.Properties().group(MWDGroupsAndMaterials.MATERIALS)).setRegistryName("cotton_seeds");
    public static final Item COTTON = create().setRegistryName("cotton");

    // machine
    public static final Item MANA_CONDUIT = build(MWDBlocks.MANA_CONDUIT);
    public static final Item CREATIVE_MANA_BLOCK = build(MWDBlocks.CREATIVE_MANA_BLOCK);
    public static final Item WATER_PURIFIER = build(MWDBlocks.WATER_PURIFIER);
    public static final Item WATER_FOUNTAIN = build(MWDBlocks.WATER_FOUNTAIN);
    public static final Item MAGIC_WATER_FOUNTAIN = build(MWDBlocks.MAGIC_WATER_FOUNTAIN);
    public static final Item ME_EXTRACTOR = build(MWDBlocks.ME_EXTRACTOR);
    public static final Item BLENDER = build(MWDBlocks.BLENDER);
    public static final Item SMELTING_BOX = build(MWDBlocks.SMELTING_BOX);

    // ore
    public static final Item VIOLET_ORE = build(MWDBlocks.VIOLET_ORE);
    public static final Item VIOLET_INGOT = create().setRegistryName("violet_ingot");
    public static final Item VIOLET_GLASS = build(MWDBlocks.VIOLET_GLASS);
    public static final Item BLUE_GOLD_ORE = build(MWDBlocks.BLUE_GOLD_ORE);
    public static final Item BLUE_GOLD_POWDER = create().setRegistryName("blue_gold_powder");
    public static final Item MANA_CRYSTAL_ORE = build(MWDBlocks.MANA_CRYSTAL_ORE);
    public static final Item MANA_CRYSTAL = create().setRegistryName("mana_crystal");

    private static Item create() {
        return new Item(new Item.Properties().group(MWDGroupsAndMaterials.MATERIALS));
    }

    private static Item createPotion() {
        return new VanillaPotionItem(new Item.Properties().maxStackSize(1).group(MWDGroupsAndMaterials.MATERIALS));
    }

    private static Item build(Block block) {
        return new BlockItem(block, new Item.Properties().group(MWDGroupsAndMaterials.BLOCKS)).setRegistryName(Objects.requireNonNull(block.getRegistryName()));
    }
}