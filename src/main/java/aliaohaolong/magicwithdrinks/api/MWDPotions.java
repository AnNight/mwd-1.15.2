package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotion;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotion.Properties;
import net.minecraft.util.ResourceLocation;

public class MWDPotions {
//    public static final Potion V_APPLE_WINE = init(new Potion(new EffectInstance(Effects.REGENERATION, 320), new EffectInstance(Effects.NAUSEA, 240)), "apple_wine");
//    public static final Potion V_SWEET_BERRIES_WINE = init(new Potion(new EffectInstance(Effects.SPEED, 480), new EffectInstance(Effects.NAUSEA, 360)), "sweet_berries_wine");

    public static final MWDPotion EMPTY = init(new Properties(new ResourceLocation(MWD.MOD_ID, "empty"), 0));
    public static final MWDPotion WATER = init(new Properties(new ResourceLocation(MWD.MOD_ID, "water"), 3694022));
    public static final MWDPotion PURIFIED_WATER = init(new Properties(new ResourceLocation(MWD.MOD_ID, "purified_water"), 7915760));
    public static final MWDPotion CATALYST = init(new Properties(new ResourceLocation(MWD.MOD_ID, "catalyst"), 16711782));

    private static MWDPotion init(Properties properties) {
        MWDPotion potion = properties.build();
        return potion.setRegistryName(potion.getResourceLocation());
    }
}