package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.common.block.*;
import net.minecraft.block.Block;
import net.minecraft.block.GlassBlock;
import net.minecraft.block.OreBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.state.BooleanProperty;

public class MWDBlocks {
    public static final BooleanProperty SWITCH = BooleanProperty.create("switch");

    // crop
    public static final Block COTTON = new CottonBlock(Block.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.CROP)).setRegistryName("cotton");

    // machine
    public static final Block MANA_CONDUIT = new ManaConduitBlock(Block.Properties.create(MWDGroupsAndMaterials.MANA_CONDUIT).hardnessAndResistance(2.0F, 6.0F).sound(SoundType.STONE), 2).setRegistryName("mana_conduit");
    public static final Block CREATIVE_MANA_BLOCK = new CreativeManaBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).sound(SoundType.STONE), 0.32F).setRegistryName("creative_mana_block");
    public static final Block WATER_PURIFIER = new WaterPurifierBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).lightValue(13).sound(SoundType.STONE)).setRegistryName("water_purifier");
    public static final Block WATER_FOUNTAIN = new WaterFountainBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).lightValue(13).sound(SoundType.STONE)).setRegistryName("water_fountain");
    public static final Block MAGIC_WATER_FOUNTAIN = new MagicWaterFountainBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).lightValue(13).sound(SoundType.STONE)).setRegistryName("magic_water_fountain");
    public static final Block ME_EXTRACTOR = new MEExtractorBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).lightValue(13).sound(SoundType.STONE)).setRegistryName("me_extractor");
    public static final Block BLENDER = new BlenderBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).lightValue(13).sound(SoundType.STONE)).setRegistryName("blender");
    public static final Block SMELTING_BOX = new SmeltingBoxBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).sound(SoundType.STONE)).setRegistryName("smelting_box");

    // ore
    public static final Block VIOLET_ORE = new OreBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F, 3.0F).sound(SoundType.STONE)).setRegistryName("violet_ore");
    public static final Block VIOLET_GLASS = new GlassBlock(Block.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()).setRegistryName("violet_glass");
    public static final Block BLUE_GOLD_ORE = new OreBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F, 3.0F).sound(SoundType.STONE)).setRegistryName("blue_gold_ore");
    public static final Block MANA_CRYSTAL_ORE = new OreBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F, 3.0F).sound(SoundType.STONE)).setRegistryName("mana_crystal_ore");
}