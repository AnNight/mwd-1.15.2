package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.item.crafting.BlendingRecipe;
import aliaohaolong.magicwithdrinks.common.item.crafting.ExtractingRecipe;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;

@SuppressWarnings("SameParameterValue")
public class MWDRecipes {
    public static IRecipeSerializer<BlendingRecipe> BLENDING_S = initS(new BlendingRecipe.Serializer(), "blending");
    public static IRecipeType<BlendingRecipe> BLENDING_T = initT("blending");
    public static IRecipeSerializer<ExtractingRecipe> EXTRACTING_S = initS(new ExtractingRecipe.Serializer(), "extracting");
    public static IRecipeType<ExtractingRecipe> EXTRACTING_T = initT("extracting");

    private static <R extends IRecipe<?>, RS extends IRecipeSerializer<R>> RS initS(RS recipeSerializer, String name) {
        recipeSerializer.setRegistryName(new ResourceLocation(MWD.MOD_ID, name));
        return recipeSerializer;
    }

    static <T extends IRecipe<?>> IRecipeType<T> initT(String name) {
        return Registry.register(Registry.RECIPE_TYPE, new ResourceLocation(MWD.MOD_ID, name), new IRecipeType<T>() {
            @Override
            public String toString() {
                return name;
            }
        });
    }
}