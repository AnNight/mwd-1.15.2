package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.*;
import aliaohaolong.magicwithdrinks.common.tile.*;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;

@SuppressWarnings("ConstantConditions")
public class MWDTypes {
    // TileEntity
    public static final TileEntityType<ManaConduitTile> T_MANA_CONDUIT = init(TileEntityType.Builder.create(ManaConduitTile::new, MWDBlocks.MANA_CONDUIT).build(null), "mana_conduit");
    public static final TileEntityType<CreativeManaBlockTile> T_CREATIVE_MANA_BLOCK = init(TileEntityType.Builder.create(CreativeManaBlockTile::new, MWDBlocks.CREATIVE_MANA_BLOCK).build(null), "creative_mana_block");
    public static final TileEntityType<WaterPurifierTile> T_WATER_PURIFIER = init(TileEntityType.Builder.create(WaterPurifierTile::new, MWDBlocks.WATER_PURIFIER).build(null), "water_purifier");
    public static final TileEntityType<WaterFountainTile> T_WATER_FOUNTAIN = init(TileEntityType.Builder.create(WaterFountainTile::new, MWDBlocks.WATER_FOUNTAIN).build(null), "water_fountain");
    public static final TileEntityType<MagicWaterFountainTile> T_MAGIC_WATER_FOUNTAIN = init(TileEntityType.Builder.create(MagicWaterFountainTile::new, MWDBlocks.MAGIC_WATER_FOUNTAIN).build(null), "magic_water_fountain");
    public static final TileEntityType<MEExtractorTile> T_ME_EXTRACTOR = init(TileEntityType.Builder.create(MEExtractorTile::new, MWDBlocks.ME_EXTRACTOR).build(null), "me_extractor");
    public static final TileEntityType<BlenderTile> T_BLENDER = init(TileEntityType.Builder.create(BlenderTile::new, MWDBlocks.BLENDER).build(null), "blender");
    public static final TileEntityType<SmeltingBoxTile> T_SMELTING_BOX = init(TileEntityType.Builder.create(SmeltingBoxTile::new, MWDBlocks.SMELTING_BOX).build(null), "smelting_box");

    // Container
    public static final ContainerType<WaterPurifierContainer> C_WATER_PURIFIER = init(new ContainerType<>(WaterPurifierContainer::new), "water_purifier");
    public static final ContainerType<WaterFountainContainer> C_WATER_FOUNTAIN = init(new ContainerType<>(WaterFountainContainer::new), "water_fountain");
    public static final ContainerType<MagicWaterFountainContainer> C_MAGIC_WATER_FOUNTAIN = init(new ContainerType<>(MagicWaterFountainContainer::new), "magic_water_fountain");
    public static final ContainerType<MEExtractorContainer> C_ME_EXTRACTOR = init(new ContainerType<>(MEExtractorContainer::new), "me_extractor");
    public static final ContainerType<BlenderContainer> C_BLENDER = init(new ContainerType<>(BlenderContainer::new), "blender");
    public static final ContainerType<SmeltingBoxContainer> C_SMELTING_BOX = init(new ContainerType<>(SmeltingBoxContainer::new), "smelting_box");

    private static <T extends TileEntity, TT extends TileEntityType<T>> TT init(TT tileEntityType, String name) {
        tileEntityType.setRegistryName(new ResourceLocation(MWD.MOD_ID, name));
        return tileEntityType;
    }

    private static <C extends Container, CT extends ContainerType<C>> CT init(CT containerType, String name) {
        containerType.setRegistryName(new ResourceLocation(MWD.MOD_ID, name));
        return containerType;
    }
}