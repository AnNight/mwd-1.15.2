package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.common.extra.Extra;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotion;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class MWDForgeRegistries {
    private static IForgeRegistry<MWDPotion> potions;
    private static IForgeRegistry<Extra> exes;

    public static IForgeRegistry<MWDPotion> getPotions() {
        if (potions == null)
            potions = GameRegistry.findRegistry(MWDPotion.class);
        return potions;
    }

    public static IForgeRegistry<Extra> getExes() {
        if (exes == null)
            exes = GameRegistry.findRegistry(Extra.class);
        return exes;
    }
}