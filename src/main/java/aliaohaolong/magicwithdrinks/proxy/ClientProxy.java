package aliaohaolong.magicwithdrinks.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

@SuppressWarnings("unused")
public class ClientProxy implements IProxy {
    @Override
    public void init() {
//        ClientRegistry.bindTileEntityRenderer(MWDTileEntityType.TEST, TestTileEntityRenderer::new);
    }

    @Override
    public World getClientWorld() {
        return Minecraft.getInstance().world;
    }

    @Override
    public PlayerEntity getClientPlayer() {
        return Minecraft.getInstance().player;
    }
}