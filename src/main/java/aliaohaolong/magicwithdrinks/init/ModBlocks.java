package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import net.minecraft.block.Block;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModBlocks {
    @SubscribeEvent
    public static void onRegisterBlocks(RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll(
                MWDBlocks.COTTON,

                MWDBlocks.MANA_CONDUIT,
                MWDBlocks.CREATIVE_MANA_BLOCK,
                MWDBlocks.WATER_PURIFIER,
                MWDBlocks.WATER_FOUNTAIN,
                MWDBlocks.MAGIC_WATER_FOUNTAIN,
                MWDBlocks.ME_EXTRACTOR,
                MWDBlocks.BLENDER,
                MWDBlocks.SMELTING_BOX,

                MWDBlocks.VIOLET_ORE,
                MWDBlocks.VIOLET_GLASS,
                MWDBlocks.BLUE_GOLD_ORE,
                MWDBlocks.MANA_CRYSTAL_ORE
        );
    }
}