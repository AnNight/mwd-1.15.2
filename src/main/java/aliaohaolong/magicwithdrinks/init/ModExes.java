package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDExes;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModExes {
    @SubscribeEvent
    public static void onRegisterExes(RegistryEvent.Register<Extra> event) {
        for (Extra extra : MWDExes.ITEM_MES)
            event.getRegistry().register(extra);
    }
}