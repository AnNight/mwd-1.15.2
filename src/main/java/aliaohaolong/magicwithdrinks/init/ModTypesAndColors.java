package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModTypesAndColors {
    @SubscribeEvent
    public static void onRegisterContainerTypes(final RegistryEvent.Register<ContainerType<?>> event) {
        event.getRegistry().registerAll(
                MWDTypes.C_WATER_PURIFIER,
                MWDTypes.C_WATER_FOUNTAIN,
                MWDTypes.C_MAGIC_WATER_FOUNTAIN,
                MWDTypes.C_ME_EXTRACTOR,
                MWDTypes.C_BLENDER,
                MWDTypes.C_SMELTING_BOX
        );
    }

    @SubscribeEvent
    public static void onRegisterTileEntityTypes(final RegistryEvent.Register<TileEntityType<?>> event) {
        event.getRegistry().registerAll(
                MWDTypes.T_MANA_CONDUIT,
                MWDTypes.T_CREATIVE_MANA_BLOCK,
                MWDTypes.T_WATER_PURIFIER,
                MWDTypes.T_WATER_FOUNTAIN,
                MWDTypes.T_MAGIC_WATER_FOUNTAIN,
                MWDTypes.T_ME_EXTRACTOR,
                MWDTypes.T_BLENDER,
                MWDTypes.T_SMELTING_BOX
        );
    }

    @SubscribeEvent
    public static void onColorHandlerItems(ColorHandlerEvent.Item event) {
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : MWDPotionUtils.getColorFromStack(stack), MWDItems.POTION);
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : 6591981, MWDItems.PURIFIED_WATER);
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : 15721648, MWDItems.SUGAR_WATER);
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : 15841885, MWDItems.APPLE_JUICE);
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : 12529955, MWDItems.MELON_JUICE);
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : 16748041, MWDItems.CARROT_JUICE);
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : 14912029, MWDItems.PUMPKIN_JUICE);
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : 14632574, MWDItems.SWEET_BERRIES_JUICE);
//        event.getItemColors().register((stack, p_getColor_2_) -> 7903880, MWDItems.CYAN_LEAVES);
    }

    @SubscribeEvent
    public static void onColorHandlerBlocks(ColorHandlerEvent.Block event) {
//        event.getBlockColors().register((blockState, reader, pos, p_getColor_4_) -> reader.getBiome(pos).getFoliageColor(pos),
//                MWDBlocks.MOONLIGHT_VINE,
//                MWDBlocks.MOONLIGHT_VINE_PLANT
//        );
//        event.getBlockColors().register((blockState, reader, pos, p_getColor_4_) -> 7903880, MWDBlocks.CYAN_LEAVES);
    }
}