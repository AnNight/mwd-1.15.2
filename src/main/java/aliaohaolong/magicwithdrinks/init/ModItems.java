package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModItems {
    @SubscribeEvent
    public static void onRegisterItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(
                MWDItems.PURIFIED_WATER,
                MWDItems.SUGAR_WATER,
                MWDItems.APPLE_JUICE,
                MWDItems.MELON_JUICE,
                MWDItems.CARROT_JUICE,
                MWDItems.PUMPKIN_JUICE,
                MWDItems.SWEET_BERRIES_JUICE,

                MWDItems.IMPURITY,

                MWDItems.BLUE_GOLD_CORE,

                MWDItems.MANA_CRYSTAL_BATTERY,
                MWDItems.ADVANCED_MANA_CRYSTAL_BATTERY,

                MWDItems.POTION_BOTTLE,
                MWDItems.POTION,

                MWDItems.FEATHERY_FILTER_SLICE, MWDItems.WOOLEN_FILTER_SLICE, MWDItems.COTTONY_FILTER_SLICE, MWDItems.BAMBOO_FILTER_SLICE, MWDItems.GOLDEN_FILTER_SLICE,
                MWDItems.ELEMENTARY_ME_CARD, MWDItems.INTERMEDIATE_ME_CARD, MWDItems.ADVANCED_ME_CARD,

                MWDItems.COTTON_SEEDS,
                MWDItems.COTTON,

                MWDItems.MANA_CONDUIT,
                MWDItems.CREATIVE_MANA_BLOCK,
                MWDItems.WATER_PURIFIER,
                MWDItems.WATER_FOUNTAIN,
                MWDItems.MAGIC_WATER_FOUNTAIN,
                MWDItems.ME_EXTRACTOR,
                MWDItems.BLENDER,
                MWDItems.SMELTING_BOX,

                MWDItems.VIOLET_ORE, MWDItems.VIOLET_INGOT, MWDItems.VIOLET_GLASS,
                MWDItems.BLUE_GOLD_ORE, MWDItems.BLUE_GOLD_POWDER,
                MWDItems.MANA_CRYSTAL_ORE, MWDItems.MANA_CRYSTAL
        );
    }
}