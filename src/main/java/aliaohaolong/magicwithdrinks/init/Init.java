package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.common.BiomeDictionary;

public class Init {
    public static void initOresGeneration() {
        for (Biome biome : BiomeDictionary.getBiomes(BiomeDictionary.Type.OVERWORLD)) {
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES,
                    Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, MWDBlocks.VIOLET_ORE.getDefaultState(), 9))
                            .withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(3, 0, 0, 32) /*0-32*/))
            );
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES,
                    Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, MWDBlocks.BLUE_GOLD_ORE.getDefaultState(), 6))
                            .withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(2, 0, 0, 16) /*0-16*/))
            );
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES,
                    Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, MWDBlocks.MANA_CRYSTAL_ORE.getDefaultState(), 8))
                            .withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(1, 0, 0, 16) /*0-16*/))
            );
        }
    }
}