package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDPotions;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotion;
import net.minecraft.potion.Potion;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModPotions {
    @SubscribeEvent
    public static void onRegisterDrinks(RegistryEvent.Register<MWDPotion> event) {
        event.getRegistry().registerAll(
                MWDPotions.EMPTY,
                MWDPotions.WATER,
                MWDPotions.PURIFIED_WATER,
                MWDPotions.CATALYST
//                MWDPotions.SUGAR_WATER,
//
//                MWDPotions.APPLE_JUICE,
//                MWDPotions.MELON_JUICE,
//                MWDPotions.CARROT_JUICE,
//                MWDPotions.PUMPKIN_JUICE,
//                MWDPotions.SWEET_BERRIES_JUICE,
//
//                MWDPotions.APPLE_WINE,
//                MWDPotions.SWEET_BERRIES_WINE,
//
//                MWDPotions.ACTIVE_SUBSTANCE
        );
    }
}