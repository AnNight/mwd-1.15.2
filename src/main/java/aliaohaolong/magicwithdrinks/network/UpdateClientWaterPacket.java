package aliaohaolong.magicwithdrinks.network;

import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.UUID;
import java.util.function.Supplier;

public class UpdateClientWaterPacket {
    private final CompoundNBT nbt;

    public UpdateClientWaterPacket(UUID uuid, IWater water){
        CompoundNBT nbt = new CompoundNBT();
        nbt.putLong("uL", uuid.getLeastSignificantBits());
        nbt.putLong("uM", uuid.getMostSignificantBits());
        nbt.putInt("v", water.getValue());
        this.nbt = nbt;
    }

    public UpdateClientWaterPacket(CompoundNBT nbt){
        this.nbt = nbt;
    }

    public static void encoder(UpdateClientWaterPacket msg, PacketBuffer buf){
        buf.writeCompoundTag(msg.nbt);
    }

    public static UpdateClientWaterPacket decoder(PacketBuffer buf){
        return new UpdateClientWaterPacket(buf.readCompoundTag());
    }

    public static void handle(UpdateClientWaterPacket msg, Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(() -> {
            assert Minecraft.getInstance().world != null;
            PlayerEntity player = Minecraft.getInstance().world.getPlayerByUuid(new UUID(msg.nbt.getLong("uM"), msg.nbt.getLong("uL")));
            if (player == null) return;
            IWater.getFromPlayer(player).setValue(msg.nbt.getInt("v"));
        });
        ctx.get().setPacketHandled(true);
    }
}