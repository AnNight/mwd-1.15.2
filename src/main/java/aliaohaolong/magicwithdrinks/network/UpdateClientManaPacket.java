package aliaohaolong.magicwithdrinks.network;

import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.UUID;
import java.util.function.Supplier;

public class UpdateClientManaPacket {
    private final CompoundNBT nbt;

    public UpdateClientManaPacket(UUID uuid, IMana mana){
        CompoundNBT nbt = new CompoundNBT();
        nbt.putLong("uL", uuid.getLeastSignificantBits());
        nbt.putLong("uM", uuid.getMostSignificantBits());
        nbt.putByte("type", mana.getType().getByte());
        nbt.putFloat("limit", mana.getLimit());
        nbt.putFloat("v", mana.getValue());
        nbt.putByte("level", mana.getLevel());
        nbt.putBoolean("gui", mana.isOpenGui());
        this.nbt = nbt;
    }

    public UpdateClientManaPacket(CompoundNBT nbt){
        this.nbt = nbt;
    }

    public static void encoder(UpdateClientManaPacket msg, PacketBuffer buf){
        buf.writeCompoundTag(msg.nbt);
    }

    public static UpdateClientManaPacket decoder(PacketBuffer buf){
        return new UpdateClientManaPacket(buf.readCompoundTag());
    }

    public static void handle(UpdateClientManaPacket msg, Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(() -> {
            assert Minecraft.getInstance().world != null;
            PlayerEntity player = Minecraft.getInstance().world.getPlayerByUuid(new UUID(msg.nbt.getLong("uM"),msg.nbt.getLong("uL")));
            if (player == null) return;
            IMana mana = IMana.getFromPlayer(player);
            mana.setType(IMana.Type.getType(msg.nbt.getByte("type")));
            mana.setLimit(msg.nbt.getFloat("limit"));
            mana.setValue(msg.nbt.getFloat("v"));
            mana.setLevel(msg.nbt.getByte("level"));
            if (msg.nbt.getBoolean("gui")) mana.openGui();
            else mana.closeGui();
        });
        ctx.get().setPacketHandled(true);
    }
}