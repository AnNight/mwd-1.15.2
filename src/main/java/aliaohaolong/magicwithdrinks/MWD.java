package aliaohaolong.magicwithdrinks;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.client.gui.screen.*;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.mana.Mana;
import aliaohaolong.magicwithdrinks.common.capability.mana.ManaStorage;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.capability.water.Water;
import aliaohaolong.magicwithdrinks.common.capability.water.WaterStorage;
import aliaohaolong.magicwithdrinks.common.command.ManaCommands;
import aliaohaolong.magicwithdrinks.common.command.WaterCommands;
import aliaohaolong.magicwithdrinks.event.CapabilityEventHandler;
import aliaohaolong.magicwithdrinks.event.ClientEventHandler;
import aliaohaolong.magicwithdrinks.event.EventHandler;
import aliaohaolong.magicwithdrinks.init.Init;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import aliaohaolong.magicwithdrinks.proxy.ClientProxy;
import aliaohaolong.magicwithdrinks.proxy.IProxy;
import aliaohaolong.magicwithdrinks.proxy.ServerProxy;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.command.Commands;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * For store mana data at machines:
 * "Mana":{"Buffer":float}
 * For store extra data at items:
 * "Extra":{"Food":{"Water":{"V":float,"Poss":float},"Mana"}}
 */

@Mod(MWD.MOD_ID)
public class MWD {
    public static IProxy proxy = DistExecutor.runForDist(() -> ClientProxy::new, () -> ServerProxy::new);
    public static final String MOD_ID = "magicwithdrinks";
    public static final String EXCEPTION = "[Magic with drinks] ";
    public static final Logger LOGGER = LogManager.getLogger();

    public MWD() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(EventHandler::onNewRegistry);
        MinecraftForge.EVENT_BUS.register(this); // For registering commands.
    }

    void setup(final FMLCommonSetupEvent event) {
        proxy.init();
        MinecraftForge.EVENT_BUS.register(new CapabilityEventHandler());
        MinecraftForge.EVENT_BUS.register(new EventHandler());
        PacketManager.register();
        Init.initOresGeneration();
        CapabilityManager.INSTANCE.register(IWater.class, new WaterStorage(), Water::new);
        CapabilityManager.INSTANCE.register(IMana.class, new ManaStorage(), Mana::new);
    }

    void doClientStuff(final FMLClientSetupEvent event) {
        MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
        ScreenManager.registerFactory(MWDTypes.C_WATER_PURIFIER, WaterPurifierScreen::new);
        ScreenManager.registerFactory(MWDTypes.C_WATER_FOUNTAIN, WaterFountainScreen::new);
        ScreenManager.registerFactory(MWDTypes.C_MAGIC_WATER_FOUNTAIN, MagicWaterFountainScreen::new);
        ScreenManager.registerFactory(MWDTypes.C_ME_EXTRACTOR, MEExtractorScreen::new);
        ScreenManager.registerFactory(MWDTypes.C_BLENDER, BlenderScreen::new);
        ScreenManager.registerFactory(MWDTypes.C_SMELTING_BOX, SmeltingBoxScreen::new);
        RenderTypeLookup.setRenderLayer(MWDBlocks.COTTON, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(MWDBlocks.VIOLET_GLASS, RenderType.getCutout());
    }

    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        event.getCommandDispatcher().register(Commands.literal(MOD_ID)
                .then(WaterCommands.register())
                .then(ManaCommands.register())
        );
    }
}