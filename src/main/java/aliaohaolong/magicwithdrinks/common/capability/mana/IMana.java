package aliaohaolong.magicwithdrinks.common.capability.mana;

import net.minecraft.entity.LivingEntity;

@SuppressWarnings({"UnusedReturnValue"})
public interface IMana {
    Type getType();
    boolean setType(Type newType);

    float getValue();
    boolean setValue(float value);
    boolean modifyValue(float value);

    float getLimit();
    boolean setLimit(float value);
    boolean modifyLimit(float value);

    byte getLevel();
    boolean setLevel(int value);

    boolean isOpenGui();
    void openGui();
    void closeGui();

    default void copyForRespawn(IMana oldCap) {
        setType(oldCap.getType());
        setLimit(oldCap.getLimit());
        setValue(oldCap.getValue());
        setLevel(oldCap.getLevel());
        if (oldCap.isOpenGui()) openGui();
        else closeGui();
    }

    static IMana getFromPlayer(LivingEntity player) {
        return player.getCapability(ManaProvider.MANA_CAP, null).orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!"));
    }

    enum Type {
        NATURE( 0),
        FIRE( 1),
        ICE( 2);

        private final byte index;

        Type(int index) {
            this.index = (byte) index;
        }

        public byte getByte() {
            return index;
        }

        public static Type getType(int index) {
            if (index == 0) return NATURE;
            if (index == 1) return FIRE;
            return ICE;
        }
    }
}