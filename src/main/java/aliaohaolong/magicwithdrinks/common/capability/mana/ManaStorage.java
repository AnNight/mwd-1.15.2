package aliaohaolong.magicwithdrinks.common.capability.mana;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class ManaStorage implements Capability.IStorage<IMana> {
    @Nullable
    @Override
    public INBT writeNBT(Capability<IMana> capability, IMana instance, Direction side) {
        CompoundNBT compoundNBT = new CompoundNBT();
        compoundNBT.putByte("mwd.mana.type", instance.getType().getByte());
        compoundNBT.putFloat("mwd.mana.value", instance.getValue());
        compoundNBT.putFloat("mwd.mana.limit", instance.getLimit());
        compoundNBT.putByte("mwd.mana.level", instance.getLevel());
        compoundNBT.putBoolean("mwd.mana.gui", instance.isOpenGui());
        return compoundNBT;
    }

    @Override
    public void readNBT(Capability<IMana> capability, IMana instance, Direction side, INBT nbt) {
        CompoundNBT compoundNBT = (CompoundNBT) nbt;
        instance.setType(IMana.Type.getType(compoundNBT.getByte("mwd.mana.type")));
        instance.setLimit(compoundNBT.getFloat("mwd.mana.limit"));
        instance.setValue(compoundNBT.getFloat("mwd.mana.value"));
        instance.setLevel(compoundNBT.getByte("mwd.mana.level"));
        if (compoundNBT.getBoolean("mwd.mana.gui")) instance.openGui();
        else instance.closeGui();
    }
}