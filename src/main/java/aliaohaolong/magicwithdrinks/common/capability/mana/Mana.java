package aliaohaolong.magicwithdrinks.common.capability.mana;

public class Mana implements IMana {
    private Type type;
    private float value;
    private float limit;
    // between 0 to 10;
    private byte level;
    private boolean open_gui;

    public Mana() {
        type = Type.NATURE;
        value = 0;
        limit = 0;
        level = 0;
        open_gui = true;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public boolean setType(Type newType) {
        boolean result = newType != type;
        type = newType;
        return result;
    }

    @Override
    public float getValue() {
        return value;
    }

    @Override
    public boolean setValue(float value) {
        float original = this.value;
        this.value = Math.min(Math.max(0, value), limit);
        return original != this.value;
    }

    @Override
    public boolean modifyValue(float value) {
        float original = this.value;
        float newValue = this.value + value;
        this.value = Math.min(Math.max(0, newValue), limit);
        return original != this.value;
    }

    @Override
    public float getLimit() {
        return limit;
    }

    /**
     * When the new limit is positive, set it. If the new limit less than value, set the value to the limit.
     * When the new value is negative, return false.
     */
    @Override
    public boolean setLimit(float value) {
        float original_value = this.value;
        float original_limit = limit;
        limit = Math.max(0, value);
        if (this.value > limit) this.value = limit;
        return original_value != this.value || original_limit != limit;
    }

    @Override
    public boolean modifyLimit(float value) {
        float original_value = this.value;
        float original_limit = limit;
        limit = Math.max(this.limit + value, 0);
        if (this.value > limit) this.value = limit;
        return original_value != this.value || original_limit != limit;
    }

    @Override
    public byte getLevel() {
        return level;
    }

    @Override
    public boolean setLevel(int value) {
        byte original = level;
        level = (byte) Math.min(Math.max(value, 0), 10);
        return original != level;
    }

    @Override
    public boolean isOpenGui() {
        return open_gui;
    }

    @Override
    public void openGui() {
        open_gui = true;
    }

    @Override
    public void closeGui() {
        open_gui = false;
    }
}