package aliaohaolong.magicwithdrinks.common.capability.water;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;

@SuppressWarnings({"UnusedReturnValue"})
public interface IWater {
    int MAX = 50;
    int INNER_MAX = 9600;

    int getValue();
    boolean setValue(int value);
    boolean increase(int value);
    boolean reduce(int value);

    int getInner();
    boolean setInner(int value);
    boolean modifyInner(int value);

    byte getSecond();
    boolean setSecond(byte value);

    boolean tick(PlayerEntity player);

    default void copyForRespawn(IWater oldCap) {
        this.setValue(oldCap.getValue());
        this.setInner(oldCap.getInner());
        this.setSecond(oldCap.getSecond());
    }

    static IWater getFromPlayer(LivingEntity player) {
        return player.getCapability(WaterProvider.WATER_CAP, null).orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!"));
    }
}