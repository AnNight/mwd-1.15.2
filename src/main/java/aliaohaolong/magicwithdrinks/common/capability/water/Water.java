package aliaohaolong.magicwithdrinks.common.capability.water;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;

public class Water implements IWater {
    private int value;
    private int inner;
    private byte second;

    public Water() {
        value = 50;
        inner = INNER_MAX;
        second = 0;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public int getInner() {
        return inner;
    }

    @Override
    public byte getSecond() {
        return second;
    }

    @Override
    public boolean setInner(int value) {
        if (value <= INNER_MAX && value >= 0) {
            this.inner = value;
            return true;
        }
        return false;
    }

    /**
     * When the new value is available, set the current value to the new value and return true.
     * When the new value is unavailable, return false.
     */
    @Override
    public boolean setValue(int value) {
        if (value <= MAX && value >= 0) {
            this.value = value;
            return true;
        } else return false;
    }

    @Override
    public boolean setSecond(byte value) {
        if (value <= 40 && value >= 0) {
            this.second = value;
            return true;
        }
        return false;
    }

    @Override
    public boolean increase(int value) {
        int newValue = this.value + value;
        if (newValue > MAX)
            this.inner = INNER_MAX;
        this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
        return true;
    }

    @Override
    public boolean reduce(int value) {
        int newValue = this.value - value;
        if (newValue > MAX)
            this.inner = INNER_MAX;
        this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
        return true;
    }

    /**
     * Run on Server.<br>
     */
    @Override
    public boolean modifyInner(int value) {
        boolean sync = false;
        inner -= value;
        while (inner <= 0 || inner > INNER_MAX) {
            if (inner <= 0) {
                if (this.value > 0) {
                    this.value--;
                    sync = true;
                }
                inner += INNER_MAX;
            }
            if (inner > INNER_MAX) {
                if (this.value < MAX) {
                    this.value++;
                    sync = true;
                }
                inner -= INNER_MAX;
            }
        }
        return sync;
    }

    /**
     * handler.ThirstHandler#onPlayerTick<br>
     * Run on Server.<br>
     */
    @Override
    public boolean tick(PlayerEntity player) {
        boolean sync = false;
        second--;
        if (second <= 0) {
            second = 20;
            if (value <= 25)
                player.addPotionEffect(new EffectInstance(Effects.WEAKNESS, 60));
            if (value == 0) {
                player.addPotionEffect(new EffectInstance(Effects.NAUSEA, 100));
                player.attackEntityFrom(DamageSource.DRYOUT, 2.0F);
            }
            if (value > MAX - 5) {
                if (player.getHealth() < player.getMaxHealth()) {
                    player.heal(0.25F);
                    sync = this.modifyInner(1920);
                }
            }
        }
        return sync;
    }
}