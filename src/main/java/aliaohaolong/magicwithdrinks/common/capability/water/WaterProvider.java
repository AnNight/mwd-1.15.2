package aliaohaolong.magicwithdrinks.common.capability.water;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class WaterProvider implements ICapabilitySerializable<CompoundNBT> {
    @CapabilityInject(IWater.class)
    public static final Capability<IWater> WATER_CAP = null;

    private final LazyOptional<IWater> instance = LazyOptional.of(WATER_CAP::getDefaultInstance);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return cap == WATER_CAP ? instance.cast() : LazyOptional.empty();
    }

    @Override
    public CompoundNBT serializeNBT() {
        return (CompoundNBT) WATER_CAP.getStorage().writeNBT(WATER_CAP, this.instance.orElseThrow(() -> new IllegalArgumentException("MWD[WaterAttribute]: LazyOptional must not be empty!")), null);
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        WATER_CAP.getStorage().readNBT(WATER_CAP, this.instance.orElseThrow(() -> new IllegalArgumentException("MWD[WaterAttribute]: LazyOptional must not be empty!")),null, nbt);
    }
}