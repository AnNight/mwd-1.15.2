package aliaohaolong.magicwithdrinks.common.capability.water;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class WaterStorage implements Capability.IStorage<IWater> {
    @Nullable
    @Override
    public INBT writeNBT(Capability<IWater> capability, IWater instance, Direction side) {
        CompoundNBT compoundNBT = new CompoundNBT();
        compoundNBT.putInt("mwd.water.inner", instance.getInner());
        compoundNBT.putByte("mwd.water.second", instance.getSecond());
        compoundNBT.putInt("mwd.water.value", instance.getValue());
        return compoundNBT;
    }

    @Override
    public void readNBT(Capability<IWater> capability, IWater instance, Direction side, INBT nbt) {
        CompoundNBT compoundNBT = (CompoundNBT) nbt;
        instance.setInner(compoundNBT.getInt("mwd.water.inner"));
        instance.setSecond(compoundNBT.getByte("mwd.water.second"));
        instance.setValue(compoundNBT.getInt("mwd.water.value"));
    }
}