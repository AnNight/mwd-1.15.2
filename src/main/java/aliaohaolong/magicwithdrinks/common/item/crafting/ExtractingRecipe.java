package aliaohaolong.magicwithdrinks.common.item.crafting;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDRecipes;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistryEntry;

@SuppressWarnings("NullableProblems")
public class ExtractingRecipe implements IRecipe<IInventory> {
    private final ResourceLocation id;
    private final Item ingredient;
    private final int cookingTime;
    private final float output;

    public ExtractingRecipe(ResourceLocation idIn, Item ingredientIn, int cookingTimeIn, float outputIn) {
        id = idIn;
        ingredient = ingredientIn;
        cookingTime = cookingTimeIn;
        output = outputIn;
    }

    public Item getIngredient() {
        return ingredient;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public float getOutput() {
        return output;
    }

    @Override
    public ItemStack getIcon() {
        return new ItemStack(MWDItems.ME_EXTRACTOR);
    }

    @Override
    public boolean matches(IInventory inv, World worldIn) {
        ItemStack stack = inv.getStackInSlot(0);
        return !stack.isEmpty() && stack.getItem() == ingredient;
    }

    @Override
    public ItemStack getCraftingResult(IInventory inv) {
        return new ItemStack(ingredient);
    }

    @Override
    public boolean canFit(int width, int height) {
        return true;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return new ItemStack(ingredient);
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return MWDRecipes.EXTRACTING_S;
    }

    @Override
    public IRecipeType<?> getType() {
        return MWDRecipes.EXTRACTING_T;
    }

    /**
     * An example:
     * {
     *     "type": "magicwithdrinks:extracting",
     *     "ingredient": "minecraft:potato",
     *     "cookingTime": 160,
     *     "output": 0.2
     * }
     */

    public static class Serializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<ExtractingRecipe> {
        @Override
        public ExtractingRecipe read(ResourceLocation recipeId, JsonObject json) {
            if (!json.has("ingredient")) throw new JsonSyntaxException("Missing vanilla, expected to find a string or object");
            if (!json.has("cookingTime")) throw new JsonSyntaxException("Missing vanilla, expected to find a string or object");
            if (!json.has("output")) throw new JsonSyntaxException("Missing vanilla, expected to find a string or object");
            Item ingredient = ForgeRegistries.ITEMS.getValue(new ResourceLocation(JSONUtils.getString(json, "ingredient")));
            int cookingTime = JSONUtils.getInt(json, "cookingTime");
            float output = JSONUtils.getFloat(json, "output");
            return new ExtractingRecipe(recipeId, ingredient, cookingTime, output);
        }

        @Override
        public ExtractingRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
            return new ExtractingRecipe(recipeId, Item.getItemById(buffer.readVarInt()), buffer.readInt(), buffer.readFloat());
        }

        @Override
        public void write(PacketBuffer buffer, ExtractingRecipe recipe) {
            buffer.writeVarInt(Item.getIdFromItem(recipe.getIngredient()));
            buffer.writeInt(recipe.getCookingTime());
            buffer.writeFloat(recipe.getOutput());
        }
    }
}