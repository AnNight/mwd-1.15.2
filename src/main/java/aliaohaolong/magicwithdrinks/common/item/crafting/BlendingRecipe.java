package aliaohaolong.magicwithdrinks.common.item.crafting;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDRecipes;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;

@SuppressWarnings("NullableProblems")
public class BlendingRecipe implements IRecipe<IInventory> {
    private final ResourceLocation id;
    private final ItemStack ingredient;
    private final ItemStack ingredient2;
    private final ItemStack result;
    private final int cookingTime;

    public BlendingRecipe(ResourceLocation idIn, ItemStack ingredientIn, ItemStack ingredient2In, ItemStack resultIn, int cookingTimeIn) {
        id = idIn;
        ingredient = ingredientIn;
        ingredient2 = ingredient2In;
        result = resultIn;
        cookingTime = cookingTimeIn;
    }

    public ItemStack getIngredient() {
        return ingredient;
    }

    public ItemStack getIngredient2() {
        return ingredient2;
    }

    public int getCookTime() {
        return cookingTime;
    }

    @Override
    public ItemStack getIcon() {
        return new ItemStack(MWDItems.BLENDER);
    }

    @Override
    public boolean matches(IInventory inv, World worldIn) {
        ItemStack i = inv.getStackInSlot(0);
        ItemStack i2 = inv.getStackInSlot(1);
        if (i.isEmpty() || i2.isEmpty() || ingredient.getItem() != i.getItem() || ingredient2.getItem() != i2.getItem()) return false;
        boolean isPotion = ingredient.getItem() == MWDItems.POTION;
        if (isPotion) return MWDPotionUtils.getPotionFromStack(ingredient) == MWDPotionUtils.getPotionFromStack(i) && ingredient2.getCount() <= i2.getCount();
        return ingredient2.getCount() <= i2.getCount();
    }

    @Override
    public ItemStack getCraftingResult(IInventory inv) {
        return result.copy();
    }

    @Override
    public boolean canFit(int width, int height) {
        return true;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return result.copy();
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return MWDRecipes.BLENDING_S;
    }

    @Override
    public IRecipeType<?> getType() {
        return MWDRecipes.BLENDING_T;
    }

    /**
     *  An example:
     *  {
     *    "type": "magicwithdrinks:blending",
     *    "ingredient": {
     *      "potion": "magicwithdrinks:purified_water"
     *      // or
     *      "item": "magicwithdrinks:purified_water"
     *    },
     *    "ingredient2": {
     *      "item": "minecraft:apple",
     *      "count": 2
     *    },
     *    "result": {
     *       "potion": "magicwithdrinks:apple_juice"
     *       // or
     *       "item": "magicwithdrinks:apple_juice"
     *    },
     *    "cookingTime": 400
     *  }
     */

    public static class Serializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<BlendingRecipe> {
        @Override
        public BlendingRecipe read(ResourceLocation recipeId, JsonObject json) {
            if (!json.has("ingredient")) throw new JsonSyntaxException("Missing ingredient, expected to find a string or object");
            ItemStack ingredient = deserializeIAR(JSONUtils.getJsonObject(json, "ingredient"), "ingredient");
            if (!json.has("ingredient2")) throw new JsonSyntaxException("Missing ingredient2, expected to find a string or object");
            ItemStack ingredient2 = deserializeI2(JSONUtils.getJsonObject(json, "ingredient2"));
            if (!json.has("result")) throw new JsonSyntaxException("Missing result, expected to find a string or object");
            ItemStack result = deserializeIAR(JSONUtils.getJsonObject(json, "result"), "result");
            int cookingTime = JSONUtils.getInt(json, "cookingTime", 200);
            return new BlendingRecipe(recipeId, ingredient, ingredient2, result, cookingTime);
        }

        @Nullable
        @Override
        public BlendingRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
            return new BlendingRecipe(recipeId, buffer.readItemStack(), buffer.readItemStack(), buffer.readItemStack(), buffer.readInt());
        }

        @Override
        public void write(PacketBuffer buffer, BlendingRecipe recipe) {
            buffer.writeItemStack(recipe.ingredient);
            buffer.writeItemStack(recipe.ingredient2);
            buffer.writeItemStack(recipe.result);
            buffer.writeInt(recipe.cookingTime);
        }

        private static ItemStack deserializeIAR(JsonObject object, String str) {
            if (object.has("potion")) {
                return MWDPotionUtils.addPotionToStack(new ItemStack(MWDItems.POTION), MWDPotionUtils.getPotionFromName(JSONUtils.getString(object, "potion")));
            } else if (object.has("item")) {
                return new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(JSONUtils.getString(object, "item"))));
            } else throw new JsonSyntaxException("Missing " + str + ", expected to find a string or object");
        }

        private static ItemStack deserializeI2(JsonObject object) {
            String name = JSONUtils.getString(object, "item");
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(name));
            int count = JSONUtils.getInt(object, "count", 1);
            return new ItemStack(item, count);
        }
    }
}