package aliaohaolong.magicwithdrinks.common.item;

import aliaohaolong.magicwithdrinks.api.*;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.extra.ExtraUtils;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotion;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.EffectUtils;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.List;

@SuppressWarnings("NullableProblems")
public class MWDPotionItem extends Item {
    public MWDPotionItem(Properties builder) {
        super(builder);
    }

    @Override
    public ItemStack getDefaultInstance() {
        return MWDPotionUtils.addPotionToStack(super.getDefaultInstance(), MWDPotions.WATER);
    }

    @Override
    public String getTranslationKey(ItemStack stack) {
        return this.getTranslationKey() + "." + MWDPotionUtils.getPotionFromStack(stack).getResourceLocation().toString();
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return super.hasEffect(stack) || !MWDPotionUtils.getEffectsFromStack(stack).isEmpty();
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        if (this.isInGroup(group)) {
            for (MWDPotion potion : MWDForgeRegistries.getPotions()) {
                if (potion == MWDPotions.EMPTY) continue;
                items.add(MWDPotionUtils.addPotionToStack(new ItemStack(this), potion));
            }
        }
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.DRINK;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 16;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        if (IWater.getFromPlayer(playerIn).getValue() < IWater.MAX || MWDPotionUtils.getPotionFromStack(playerIn.getHeldItem(handIn)).canEatWhenFull()) {
            playerIn.setActiveHand(handIn);
            return new ActionResult<>(ActionResultType.SUCCESS, playerIn.getHeldItem(handIn));
        }
        return new ActionResult<>(ActionResultType.FAIL, playerIn.getHeldItem(handIn));
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity livingEntity) {
        PlayerEntity playerEntity = livingEntity instanceof PlayerEntity ? (PlayerEntity) livingEntity : null;
        if (playerEntity == null || !playerEntity.abilities.isCreativeMode)
            stack.shrink(1);
        if (playerEntity instanceof ServerPlayerEntity) {
            ServerPlayerEntity player = (ServerPlayerEntity) playerEntity;
            CriteriaTriggers.CONSUME_ITEM.trigger(player, stack);
//            MWDCriteriaTriggers.drinkTrigger.trigger((ServerPlayerEntity) player, stack);
            MWDPotion potion = MWDPotionUtils.getPotionFromStack(stack);
            Extra extra = ExtraUtils.getExtraFromMPotion(potion);
            if (extra != null && extra.isFood()) extra.applyAttributes(player);
            for (EffectInstance effectinstance : MWDPotionUtils.getEffectsFromStack(stack))
                livingEntity.addPotionEffect(new EffectInstance(effectinstance));
        }
        if (playerEntity != null)
            playerEntity.addStat(Stats.ITEM_USED.get(this));
        if (playerEntity == null || !playerEntity.abilities.isCreativeMode) {
            if (stack.isEmpty())
                return new ItemStack(MWDItems.POTION_BOTTLE);
            if (playerEntity != null)
                playerEntity.inventory.addItemStackToInventory(new ItemStack(MWDItems.POTION_BOTTLE));
        }
        return stack;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> lores, ITooltipFlag flagIn) {
        MWDPotion potion = MWDPotionUtils.getPotionFromStack(stack);
        Extra extra = ExtraUtils.getExtraFromMPotion(potion);
        if (extra != null) lores.addAll(extra.getLores());
        if (!potion.getEffects().isEmpty()) {
            for(EffectInstance instance : potion.getEffects()) {
                ITextComponent component = new TranslationTextComponent(instance.getEffectName());
                Effect effect = instance.getPotion();
                if (instance.getAmplifier() > 0)
                    component.appendText(" ").appendSibling(new TranslationTextComponent("potion.potency." + instance.getAmplifier()));
                if (instance.getDuration() > 20)
                    component.appendText(" (").appendText(EffectUtils.getPotionDurationString(instance, 1)).appendText(")");
                lores.add(component.applyTextStyle(effect.getEffectType().getColor()));
            }
        }
    }
}