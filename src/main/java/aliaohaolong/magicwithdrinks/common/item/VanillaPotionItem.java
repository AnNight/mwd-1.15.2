package aliaohaolong.magicwithdrinks.common.item;

import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.UseAction;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

@SuppressWarnings("NullableProblems")
public class VanillaPotionItem extends Item {
    public VanillaPotionItem(Properties properties) {
        super(properties);
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.DRINK;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 32;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        if (IWater.getFromPlayer(playerIn).getValue() < IWater.MAX) {
            playerIn.setActiveHand(handIn);
            return new ActionResult<>(ActionResultType.SUCCESS, playerIn.getHeldItem(handIn));
        }
        return new ActionResult<>(ActionResultType.FAIL, playerIn.getHeldItem(handIn));
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity livingEntity) {
        PlayerEntity playerEntity = livingEntity instanceof PlayerEntity ? (PlayerEntity) livingEntity : null;
        if (playerEntity == null || !playerEntity.abilities.isCreativeMode)
            stack.shrink(1);
        if (playerEntity instanceof ServerPlayerEntity) {
            ServerPlayerEntity player = (ServerPlayerEntity) playerEntity;
            CriteriaTriggers.CONSUME_ITEM.trigger(player, stack);
//            MWDCriteriaTriggers.drinkTrigger.trigger((ServerPlayerEntity) player, stack);
        }
        if (playerEntity != null)
            playerEntity.addStat(Stats.ITEM_USED.get(this));
        if (playerEntity == null || !playerEntity.abilities.isCreativeMode) {
            if (stack.isEmpty())
                return new ItemStack(Items.GLASS_BOTTLE);
            if (playerEntity != null)
                playerEntity.inventory.addItemStackToInventory(new ItemStack(Items.GLASS_BOTTLE));
        }
        return stack;
    }
}