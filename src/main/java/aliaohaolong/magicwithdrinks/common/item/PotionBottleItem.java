package aliaohaolong.magicwithdrinks.common.item;

import aliaohaolong.magicwithdrinks.api.MWDPotions;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

@SuppressWarnings("NullableProblems")
public class PotionBottleItem extends Item {
    public PotionBottleItem(Item.Properties builder) {
        super(builder);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        ItemStack itemStack = playerIn.getHeldItem(handIn);
        RayTraceResult raytraceresult = rayTrace(worldIn, playerIn, RayTraceContext.FluidMode.SOURCE_ONLY);
        if (raytraceresult.getType() == RayTraceResult.Type.BLOCK) {
            BlockPos blockpos = ((BlockRayTraceResult) raytraceresult).getPos();
            if (worldIn.isBlockModifiable(playerIn, blockpos) && worldIn.getFluidState(blockpos).isTagged(FluidTags.WATER)) {
                worldIn.playSound(playerIn, playerIn.getPosX(), playerIn.getPosY(), playerIn.getPosZ(), SoundEvents.ITEM_BOTTLE_FILL, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                return new ActionResult<>(ActionResultType.SUCCESS, turnBottleIntoItem(itemStack, playerIn, MWDPotionUtils.addPotionToStack(new ItemStack(MWDItems.POTION), MWDPotions.WATER)));
            }
        }
        return new ActionResult<>(ActionResultType.PASS, itemStack);
    }

    protected ItemStack turnBottleIntoItem(ItemStack heldItem, PlayerEntity player, ItemStack stack) {
        heldItem.shrink(1);
        player.addStat(Stats.ITEM_USED.get(this));
        if (heldItem.isEmpty())
            return stack;
        else if (!player.inventory.addItemStackToInventory(stack))
            player.dropItem(stack, false);
        return heldItem;
    }
}
