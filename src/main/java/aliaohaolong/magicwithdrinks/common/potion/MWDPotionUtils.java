package aliaohaolong.magicwithdrinks.common.potion;

import aliaohaolong.magicwithdrinks.api.MWDPotions;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;
import java.util.List;

public class MWDPotionUtils {
    public static ItemStack addPotionToStack(ItemStack stack, MWDPotion MWDPotion) {
        stack.getOrCreateTag().putString("MWDPotion", MWDPotion.getResourceLocation().toString());
        return stack;
    }

    public static MWDPotion getPotionFromName(String name) {
        return MWDForgeRegistries.getPotions().getValue(new ResourceLocation(name));
    }

    public static MWDPotion getPotionFromNBT(@Nullable CompoundNBT tag) {
        return tag == null ? MWDPotions.EMPTY : getPotionFromName(tag.getString("MWDPotion"));
    }

    public static MWDPotion getPotionFromStack(ItemStack stack) {
        return getPotionFromNBT(stack.getTag());
    }

    public static int getColorFromStack(ItemStack stack) {
        return getPotionFromStack(stack).getColor();
    }

    public static List<EffectInstance> getEffectsFromTag(CompoundNBT tag) {
        return getPotionFromNBT(tag).getEffects();
    }

    public static List<EffectInstance> getEffectsFromStack(ItemStack stack) {
        return getEffectsFromTag(stack.getTag());
    }
}
