package aliaohaolong.magicwithdrinks.common.potion;

import com.google.common.collect.ImmutableList;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;

import java.util.List;

public class MWDPotion extends ForgeRegistryEntry<MWDPotion> {
    private final ResourceLocation resourceLocation;
    private final int color;
    private final boolean canEatWhenFull;
    private final ImmutableList<EffectInstance> effects;

    private MWDPotion(Properties properties) {
        this.resourceLocation = properties.resourceLocation;
        this.color = properties.color;
        this.canEatWhenFull = properties.alwaysEdible;
        this.effects = properties.effects;
    }

    public ResourceLocation getResourceLocation() {
        return this.resourceLocation;
    }

    public int getColor() {
        return this.color;
    }

    public boolean canEatWhenFull() {
        return this.canEatWhenFull;
    }

    public List<EffectInstance> getEffects() {
        return effects;
    }

    public static class Properties {
        private final ResourceLocation resourceLocation;
        private final int color;
        private boolean alwaysEdible = false;
        private final ImmutableList<EffectInstance> effects;

        public Properties(ResourceLocation resource, int color, EffectInstance... effects) {
            this.resourceLocation = resource;
            this.color = Math.max(0, color);
            this.effects = ImmutableList.copyOf(effects);
        }

        public Properties setAlwaysEdible() {
            this.alwaysEdible = true;
            return this;
        }

        public MWDPotion build() {
            return new MWDPotion(this);
        }
    }
}
