package aliaohaolong.magicwithdrinks.common.block;

import aliaohaolong.magicwithdrinks.common.tile.IManableTileEntity;
import aliaohaolong.magicwithdrinks.common.tile.ManaConduitTile;
import net.minecraft.block.*;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import java.util.List;

@SuppressWarnings({"NullableProblems", "deprecation"})
public class ManaConduitBlock extends Block implements IWaterLoggable {
    private final int MPT;

    private static final VoxelShape MAIN_SHAPE = Block.makeCuboidShape(4, 4, 4, 12, 12, 12);
    private static final VoxelShape UP_SHAPE = Block.makeCuboidShape(4, 12, 4, 12, 16, 12);
    private static final VoxelShape DOWN_SHAPE = Block.makeCuboidShape(4, 0, 4, 12, 4, 12);
    private static final VoxelShape NORTH_SHAPE = Block.makeCuboidShape(4, 4, 0, 12, 12, 4);
    private static final VoxelShape SOUTH_SHAPE = Block.makeCuboidShape(4, 4, 12, 12, 12, 16);
    private static final VoxelShape EAST_SHAPE = Block.makeCuboidShape(12, 4, 4, 16, 12, 12);
    private static final VoxelShape WEST_SHAPE = Block.makeCuboidShape(0, 4, 4, 4, 12, 12);

    public static final BooleanProperty WATER = BlockStateProperties.WATERLOGGED;
    public static final BooleanProperty NORTH = BlockStateProperties.NORTH;
    public static final BooleanProperty EAST = BlockStateProperties.EAST;
    public static final BooleanProperty SOUTH = BlockStateProperties.SOUTH;
    public static final BooleanProperty WEST = BlockStateProperties.WEST;
    public static final BooleanProperty UP = BlockStateProperties.UP;
    public static final BooleanProperty DOWN = BlockStateProperties.DOWN;

    public ManaConduitBlock(Properties properties, int maxPerTick) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(WATER, Boolean.FALSE)
                .with(UP, Boolean.FALSE).with(DOWN, Boolean.FALSE)
                .with(NORTH, Boolean.FALSE).with(SOUTH, Boolean.FALSE)
                .with(EAST, Boolean.FALSE).with(WEST, Boolean.FALSE));
        MPT = maxPerTick;
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new ManaConduitTile(MPT);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return makeShape(state.get(UP), state.get(DOWN), state.get(NORTH), state.get(SOUTH), state.get(EAST), state.get(WEST));
    }

    // 64 types of shape
    private VoxelShape makeShape(boolean up, boolean down, boolean north, boolean south, boolean east, boolean west) {
        if (up) {
            if (down) {
                if (north) {
                    if (south) {
                        if (east) {
                            if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                            return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, EAST_SHAPE);
                        } else if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, WEST_SHAPE);
                        return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE);
                    } else if (east) {
                        if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, NORTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                        return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, NORTH_SHAPE, EAST_SHAPE);
                    } else if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, NORTH_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, NORTH_SHAPE);
                } else if (south) {
                    if (east) {
                        if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, SOUTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                        return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, SOUTH_SHAPE, EAST_SHAPE);
                    } else if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, SOUTH_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, SOUTH_SHAPE);
                } else if (east) {
                    if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, EAST_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, EAST_SHAPE);
                } else if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE, WEST_SHAPE);
                return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, DOWN_SHAPE);
            } else if (north) {
                if (south) {
                    if (east) {
                        if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, NORTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                        return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, NORTH_SHAPE, EAST_SHAPE);
                    } else if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, NORTH_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, NORTH_SHAPE);
                } else if (east) {
                    if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, NORTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, NORTH_SHAPE, EAST_SHAPE);
                } else if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, NORTH_SHAPE, WEST_SHAPE);
                return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, NORTH_SHAPE);
            } else if (south) {
                if (east) {
                    if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, SOUTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, SOUTH_SHAPE, EAST_SHAPE);
                } else if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, SOUTH_SHAPE, WEST_SHAPE);
                return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, SOUTH_SHAPE);
            } else if (east) {
                if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, EAST_SHAPE, WEST_SHAPE);
                return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, EAST_SHAPE);
            } else if (west) return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE, WEST_SHAPE);
            return VoxelShapes.or(MAIN_SHAPE, UP_SHAPE);
        } else {
            if (down) {
                if (north) {
                    if (south) {
                        if (east) {
                            if (west) return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                            return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, EAST_SHAPE);
                        } else if (west) return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, WEST_SHAPE);
                        return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE);
                    } else if (east) {
                        if (west) return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, NORTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                        return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, NORTH_SHAPE, EAST_SHAPE);
                    } else if (west) return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, NORTH_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, NORTH_SHAPE);
                } else if (south) {
                    if (east) {
                        if (west) return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, SOUTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                        return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, SOUTH_SHAPE, EAST_SHAPE);
                    } else if (west) return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, SOUTH_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, SOUTH_SHAPE);
                } else if (east) {
                    if (west) return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, EAST_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, EAST_SHAPE);
                } else if (west) return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE, WEST_SHAPE);
                return VoxelShapes.or(MAIN_SHAPE, DOWN_SHAPE);
            } else if (north) {
                if (south) {
                    if (east) {
                        if (west) return VoxelShapes.or(MAIN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                        return VoxelShapes.or(MAIN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, EAST_SHAPE);
                    } else if (west) return VoxelShapes.or(MAIN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, NORTH_SHAPE, SOUTH_SHAPE);
                } else if (east) {
                    if (west) return VoxelShapes.or(MAIN_SHAPE, NORTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, NORTH_SHAPE, EAST_SHAPE);
                } else if (west) return VoxelShapes.or(MAIN_SHAPE, NORTH_SHAPE, WEST_SHAPE);
                return VoxelShapes.or(MAIN_SHAPE, NORTH_SHAPE);
            } else if (south) {
                if (east) {
                    if (west) return VoxelShapes.or(MAIN_SHAPE, SOUTH_SHAPE, EAST_SHAPE, WEST_SHAPE);
                    return VoxelShapes.or(MAIN_SHAPE, SOUTH_SHAPE, EAST_SHAPE);
                } else if (west) return VoxelShapes.or(MAIN_SHAPE, SOUTH_SHAPE, WEST_SHAPE);
                return VoxelShapes.or(MAIN_SHAPE, SOUTH_SHAPE);
            } else if (east) {
                if (west) return VoxelShapes.or(MAIN_SHAPE, EAST_SHAPE, WEST_SHAPE);
                return VoxelShapes.or(MAIN_SHAPE, EAST_SHAPE);
            } else if (west) return VoxelShapes.or(MAIN_SHAPE, WEST_SHAPE);
            return VoxelShapes.or(MAIN_SHAPE);
        }
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        BlockPos pos = context.getPos();
        World world = context.getWorld();
        return this.getDefaultState().with(WATER, world.getFluidState(context.getPos()).getFluid() == Fluids.WATER)
                .with(UP, world.getTileEntity(pos.up()) instanceof IManableTileEntity).with(DOWN, world.getTileEntity(pos.down()) instanceof IManableTileEntity)
                .with(NORTH, world.getTileEntity(pos.north()) instanceof IManableTileEntity).with(SOUTH, world.getTileEntity(pos.south()) instanceof IManableTileEntity)
                .with(EAST, world.getTileEntity(pos.east()) instanceof IManableTileEntity).with(WEST, world.getTileEntity(pos.west()) instanceof IManableTileEntity);
    }

    @Override
    public BlockState updatePostPlacement(BlockState state, Direction facing, BlockState facingState, IWorld world, BlockPos currentPos, BlockPos facingPos) {
        if (state.get(WATER)) {
            world.getPendingFluidTicks().scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickRate(world));
        }
        return state.with(UP, facing == Direction.UP ? world.getTileEntity(facingPos) instanceof IManableTileEntity : state.get(UP))
                .with(DOWN, facing == Direction.DOWN ? world.getTileEntity(facingPos) instanceof IManableTileEntity : state.get(DOWN))
                .with(NORTH, facing == Direction.NORTH ? world.getTileEntity(facingPos) instanceof IManableTileEntity : state.get(NORTH))
                .with(SOUTH, facing == Direction.SOUTH ? world.getTileEntity(facingPos) instanceof IManableTileEntity : state.get(SOUTH))
                .with(EAST, facing == Direction.EAST ? world.getTileEntity(facingPos) instanceof IManableTileEntity : state.get(EAST))
                .with(WEST, facing == Direction.WEST ? world.getTileEntity(facingPos) instanceof IManableTileEntity : state.get(WEST));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(NORTH, EAST, SOUTH, WEST, UP, DOWN, WATER);
    }

    @Override
    public boolean propagatesSkylightDown(BlockState state, IBlockReader reader, BlockPos pos) {
        return !state.get(WATER);
    }

    @Override
    public IFluidState getFluidState(BlockState state) {
        return state.get(WATER) ? Fluids.WATER.getStillFluidState(false) : super.getFluidState(state);
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> lores, ITooltipFlag flagIn) {
        lores.add(new StringTextComponent(MPT + "ME/Tick").applyTextStyle(TextFormatting.BLUE));
    }
}