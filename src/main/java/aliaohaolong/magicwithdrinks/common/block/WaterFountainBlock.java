package aliaohaolong.magicwithdrinks.common.block;

import aliaohaolong.magicwithdrinks.api.*;
import aliaohaolong.magicwithdrinks.common.tile.WaterFountainTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.*;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.stats.Stats;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;
import java.util.Random;

@SuppressWarnings({"NullableProblems", "deprecation"})
public class WaterFountainBlock extends Block {
    public static final BooleanProperty SWITCH = MWDBlocks.SWITCH;

    public WaterFountainBlock(Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(SWITCH, Boolean.FALSE));
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(SWITCH, false);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(SWITCH);
    }

    @Override
    public int getLightValue(BlockState state) {
        return state.get(SWITCH) ? super.getLightValue(state) : 0;
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return MWDTypes.T_WATER_FOUNTAIN.create();
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            TileEntity tileentity = worldIn.getTileEntity(pos);
            if (tileentity instanceof WaterFountainTile)
                InventoryHelper.dropInventoryItems(worldIn, pos, (WaterFountainTile) tileentity);
            super.onReplaced(state, worldIn, pos, newState, isMoving);
        }
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote) {
            TileEntity tileEntity = worldIn.getTileEntity(pos);
            if (tileEntity instanceof WaterFountainTile) {
                WaterFountainTile tile = (WaterFountainTile) tileEntity;
                ItemStack stack = tile.extractPurifiedWater(player.getHeldItem(handIn));
                if (stack != ItemStack.EMPTY)
                    player.setHeldItem(handIn, turnBottleIntoItem(player.getHeldItem(handIn), player, stack));
                else NetworkHooks.openGui((ServerPlayerEntity) player, tile, tile.getPos());
            }
        }
        return ActionResultType.SUCCESS;
    }

    protected ItemStack turnBottleIntoItem(ItemStack heldItem, PlayerEntity player, ItemStack stack) {
        heldItem.shrink(1);
        player.addStat(Stats.ITEM_USED.get(heldItem.getItem()));
        if (heldItem.isEmpty())
            return stack;
        else if (!player.inventory.addItemStackToInventory(stack))
            player.dropItem(stack, false);
        return heldItem;
    }

    @OnlyIn(Dist.CLIENT)
    public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        if (stateIn.get(SWITCH))
            worldIn.addParticle(ParticleTypes.FALLING_WATER, pos.getX() + 0.5D, pos.getY() + 1, pos.getZ() + 0.5D, 0.0D, 0.0D, 0.0D);
    }
}