package aliaohaolong.magicwithdrinks.common.block;

import aliaohaolong.magicwithdrinks.common.tile.CreativeManaBlockTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.IBlockReader;

import java.util.List;

@SuppressWarnings("NullableProblems")
public class CreativeManaBlock extends Block {
    private final float MSPT;

    public CreativeManaBlock(Properties properties, float maxSendingPerTick) {
        super(properties);
        MSPT = maxSendingPerTick;
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new CreativeManaBlockTile(MSPT);
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> lores, ITooltipFlag flagIn) {
        lores.add(new StringTextComponent(MSPT + "ME/Tick").applyTextStyle(TextFormatting.BLUE));
    }
}