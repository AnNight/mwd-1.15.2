package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.common.block.SmeltingBoxBlock;
import aliaohaolong.magicwithdrinks.common.inventory.SmeltingBoxContainer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@SuppressWarnings("NullableProblems")
public class SmeltingBoxTile extends AbstractSidedInventoryTile implements INamedContainerProvider, ITickableTileEntity {
    /**
     * 0    ingredientPotion    up
     * 2/1  lattice0    east
     * 4/3  lattice1    south
     * 6/5  lattice2    west
     * 8/7  lattice3    north
     * 9    resultPotion        down
     * 10   fuel        up
     */
    private static final int[] SLOTS_EAST = new int[]{2, 1};
    private static final int[] SLOTS_SOUTH = new int[]{4, 3};
    private static final int[] SLOTS_WEST = new int[]{6, 5};
    private static final int[] SLOTS_NORTH = new int[]{8, 7};
    private static final int[] SLOTS_UP = new int[]{0, 10};
    private static final int[] SLOT_DOWN = new int[]{9};
    private int cookTime = 0;
    private final IIntArray data = new IIntArray() {
        @Override
        public int get(int index) {
            return 0;
        }

        @Override
        public void set(int index, int value) {

        }

        @Override
        public int size() {
            return 0;
        }
    };

    public SmeltingBoxTile() {
        super(MWDTypes.T_SMELTING_BOX, 11);
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.smelting_box");
    }

    // IContainerProvider
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new SmeltingBoxContainer(windowId, playerInventory, this, data);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        cookTime = compound.getInt("CookTime");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putInt("CookTime", cookTime);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return false;
    }

    // ISideInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.EAST) return SLOTS_EAST;
        if (side == Direction.SOUTH) return SLOTS_SOUTH;
        if (side == Direction.WEST) return SLOTS_WEST;
        if (side == Direction.NORTH) return SLOTS_NORTH;
        if (side == Direction.UP) return SLOTS_UP;
        return SLOT_DOWN;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        return false;
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        assert world != null;
        if (!world.isRemote) {
            boolean originalState = cookTime > 0;
            if (cookTime > 0) {
                markDirty = true;
                cookTime--;
            }
            if (cookTime > 0 || !stacks.get(0).isEmpty() && !stacks.get(1).isEmpty() && stacks.get(9).isEmpty() && !stacks.get(10).isEmpty()) {
                if (cookTime == 0) {
                }
            }
            if (originalState != (cookTime > 0)) {
                markDirty = true;
                world.setBlockState(pos, world.getBlockState(pos).with(SmeltingBoxBlock.SWITCH, cookTime > 0), 3);
            }
        }
        if (markDirty) markDirty();
    }

    // ICapabilityProvider
    protected LazyOptional<? extends IItemHandler>[] handlers = SidedInvWrapper.create(this, Direction.EAST, Direction.SOUTH, Direction.WEST, Direction.NORTH, Direction.UP, Direction.DOWN);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (!this.removed && side != null && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            if (side == Direction.EAST) return handlers[0].cast();
            if (side == Direction.SOUTH) return handlers[1].cast();
            if (side == Direction.WEST) return handlers[2].cast();
            if (side == Direction.NORTH) return handlers[3].cast();
            if (side == Direction.UP) return handlers[4].cast();
            return handlers[5].cast();
        }
        return super.getCapability(cap, side);
    }
}