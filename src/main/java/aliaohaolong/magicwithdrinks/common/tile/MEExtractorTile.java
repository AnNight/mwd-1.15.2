package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.*;
import aliaohaolong.magicwithdrinks.common.block.MEExtractorBlock;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import aliaohaolong.magicwithdrinks.common.extra.ExtraUtils;
import aliaohaolong.magicwithdrinks.common.inventory.MEExtractorContainer;
import aliaohaolong.magicwithdrinks.common.item.MEExtractorCardItem;
import aliaohaolong.magicwithdrinks.common.item.crafting.ExtractingRecipe;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import javax.annotation.Nullable;

@SuppressWarnings("NullableProblems")
public class MEExtractorTile extends AbstractHighSidedInventoryTile implements INamedContainerProvider, ITickableTileEntity, IManableTileEntity {
    /**
     * 0    ingredient  top
     * 1    result      side/down[when full]
     * 2    catalyst    side/down[when empty]
     * 3    card        side
     */
    private static final int[] SLOT_UP = new int[]{0};
    private static final int[] SLOTS_HORIZONTAL = new int[]{1, 2, 3};
    private static final int[] SLOTS_DOWN = new int[]{1, 2};
    private int cookTime = 0;
    private int totalTime = 0;
    private int buffer = 0; // multiply 1000
    private byte catalyst = 0; // maximum 32
    private final IIntArray data = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return MEExtractorTile.this.cookTime;
                case 1:
                    return MEExtractorTile.this.totalTime;
                case 2:
                    return MEExtractorTile.this.buffer;
                case 3:
                    return MEExtractorTile.this.catalyst;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    MEExtractorTile.this.cookTime = (short) value;
                    break;
                case 1:
                    MEExtractorTile.this.totalTime = (short) value;
                    break;
                case 2:
                    MEExtractorTile.this.buffer = (short) value;
                    break;
                case 3:
                    MEExtractorTile.this.catalyst = (byte) value;
                    break;
            }
        }

        @Override
        public int size() {
            return 4;
        }
    };

    public MEExtractorTile() {
        super(MWDTypes.T_ME_EXTRACTOR, 4);
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.me_extractor");
    }

    // IContainerProvider
    @Nullable
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new MEExtractorContainer(windowId, playerInventory, this, data);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        cookTime = compound.getInt("CookTime");
        totalTime = compound.getInt("CookTimeTotal");
        buffer = (int) (compound.getFloat("Buffer") * 1000.0F);
        catalyst = compound.getByte("Catalyst");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putInt("CookTime", cookTime);
        compound.putInt("CookTimeTotal", totalTime);
        compound.putFloat("Buffer", ((float) buffer) / 1000.0F);
        compound.putByte("Catalyst", catalyst);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 1) {
            Extra extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
            return !getStackInSlot(index).isEmpty() && extra != null && extra.canStore();
        }
        if (index == 2) return MWDPotionUtils.getPotionFromStack(stack) == MWDPotions.CATALYST;
        if (index == 3) return stack.getItem() instanceof MEExtractorCardItem;
        return true;
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.UP) return SLOT_UP;
        if (side == Direction.DOWN) return SLOTS_DOWN;
        return SLOTS_HORIZONTAL;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN) {
            if (index == 1 && ExtraUtils.isFullMana(stack))
                return true;
            return index == 2 && stack.getItem() == MWDItems.POTION_BOTTLE;
        }
        return false;
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        assert world != null;
        if (!world.isRemote) {
            boolean originalState = cookTime > 0;
            float surplus = send(world, pos, (float) buffer / 1000.0F);
            if (buffer != surplus) {
                markDirty = true;
                buffer = (int) (surplus * 1000.0F);
            }
            if (catalyst <= 0 && !stacks.get(2).isEmpty() && MWDPotionUtils.getPotionFromStack(stacks.get(2)) == MWDPotions.CATALYST) {
                stacks.set(2, new ItemStack(MWDItems.POTION_BOTTLE));
                catalyst += 32;
            }
            if (cookTime > 0 || catalyst > 0 && !stacks.get(0).isEmpty()) {
                ExtractingRecipe recipe = world.getRecipeManager().getRecipe(MWDRecipes.EXTRACTING_T, this, world).orElse(null);
                if (recipe != null && recipe.matches(this, world) && (recipe.getOutput() * 1000.0F) + buffer <= 64000) {
                    markDirty = true;
                    if (cookTime == 0) {
                        catalyst--;
                        totalTime = recipe.getCookingTime();
                    }
                    ++cookTime;
                    if (cookTime > recipe.getCookingTime()) {
                        cookTime = 0;
                        if (stacks.get(0).hasContainerItem())
                            stacks.set(0, stacks.get(0).getContainerItem());
                        else stacks.get(0).shrink(1);
                        buffer += (int) (recipe.getOutput() * 1000.0F);
                    }
                } else {
                    cookTime = 0;
                }
            }
            if (originalState != (cookTime > 0)) {
                markDirty = true;
                this.world.setBlockState(pos, world.getBlockState(pos).with(MEExtractorBlock.SWITCH, cookTime > 0), 3);
            }
        }
        if (markDirty) this.markDirty();
    }
}