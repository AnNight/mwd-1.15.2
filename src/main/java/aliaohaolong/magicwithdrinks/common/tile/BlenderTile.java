package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDRecipes;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.common.block.BlenderBlock;
import aliaohaolong.magicwithdrinks.common.inventory.BlenderContainer;
import aliaohaolong.magicwithdrinks.common.item.crafting.BlendingRecipe;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import javax.annotation.Nullable;

@SuppressWarnings("NullableProblems")
public class BlenderTile extends AbstractHighSidedInventoryTile implements INamedContainerProvider, ITickableTileEntity {
    private static final int[] SLOT_UP = new int[]{0, 1};
    private static final int[] SLOTS_DOWN = new int[]{2, 3};
    private static final int[] SLOT_HORIZONTAL = new int[]{2};
    private int burnTime = 0;
    private int burnTotalTime = 0;
    private int cookTime = 0;
    private int cookTotalTime = 0;
    private final IIntArray packet = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return BlenderTile.this.burnTime;
                case 1:
                    return BlenderTile.this.burnTotalTime;
                case 2:
                    return BlenderTile.this.cookTime;
                case 3:
                    return BlenderTile.this.cookTotalTime;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    BlenderTile.this.burnTime = value;
                    break;
                case 1:
                    BlenderTile.this.burnTotalTime = value;
                    break;
                case 2:
                    BlenderTile.this.cookTime = value;
                    break;
                case 3:
                    BlenderTile.this.cookTotalTime = value;
            }
        }

        @Override
        public int size() {
            return 4;
        }
    };

    public BlenderTile() {
        super(MWDTypes.T_BLENDER, 4);
    }

    // MWD
    private boolean canBlend(@Nullable final BlendingRecipe recipe) {
        if (recipe == null || !stacks.get(3).isEmpty()) return false;
        assert world != null;
        return recipe.matches(this, world);
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.blender");
    }

    // IContainerProvider
    @Nullable
    @Override
    public Container createMenu(int window, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new BlenderContainer(window, playerInventory, this, packet);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        burnTotalTime = compound.getInt("burnTotalTime");
        burnTime = compound.getInt("burnTime");
        cookTime = compound.getInt("cookTime");
        cookTotalTime = compound.getInt("cookTotalTime");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putInt("burnTotalTime", burnTotalTime);
        compound.putInt("burnTime", burnTime);
        compound.putInt("cookTime", cookTime);
        compound.putInt("cookTotalTime", cookTotalTime);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 0)
            return stack.getItem() == MWDItems.POTION || stack.getItem() == Items.POTION;
        if (index == 2)
            return net.minecraftforge.common.ForgeHooks.getBurnTime(stacks.get(1)) > 0;
        return index != 3;
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.DOWN)
            return SLOTS_DOWN;
        return side == Direction.UP ? SLOT_UP : SLOT_HORIZONTAL;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN && index == 2) {
            Item item = stack.getItem();
            return item == Items.WATER_BUCKET || item == Items.BUCKET;
        }
        return true;
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        assert world != null;
        if (!world.isRemote) {
            boolean originalState = burnTime > 0;
            if (burnTime > 0)
                burnTime--;
            if (burnTime > 0 || !stacks.get(0).isEmpty() && !stacks.get(1).isEmpty() && !stacks.get(2).isEmpty()) {
                BlendingRecipe recipe = world.getRecipeManager().getRecipe(MWDRecipes.BLENDING_T, this, world).orElse(null);
                if (burnTime == 0 && canBlend(recipe)) {
                    burnTime = net.minecraftforge.common.ForgeHooks.getBurnTime(stacks.get(2));
                    burnTotalTime = burnTime;
                    if (burnTime > 0) {
                        markDirty = true;
                        if (!stacks.get(2).hasContainerItem())
                            stacks.get(2).shrink(1);
                        else
                            stacks.set(2, stacks.get(2).getContainerItem());
                    }
                }
                if (burnTime > 0 && canBlend(recipe)) {
                    if (cookTime == 0 && cookTotalTime == 0) {
                        cookTotalTime = recipe.getCookTime();
                    }
                    ++cookTime;
                    if (cookTime == cookTotalTime) {
                        cookTime = 0;
                        cookTotalTime = 0;
                        stacks.set(0, ItemStack.EMPTY);
                        stacks.get(1).shrink(recipe.getIngredient2().getCount());
                        stacks.set(3, recipe.getRecipeOutput());
                        markDirty = true;
                    }
                } else {
                    cookTime = 0;
                    cookTotalTime = 0;
                }
            } else if (burnTime == 0 && cookTime > 0) {
                cookTime = MathHelper.clamp(cookTime - 2, 0, cookTotalTime);
            }
            if (originalState != (burnTime > 0)) {
                markDirty = true;
                world.setBlockState(pos, world.getBlockState(pos).with(BlenderBlock.SWITCH, burnTime > 0), 3);
            }
        }
        if (markDirty) this.markDirty();
    }
}