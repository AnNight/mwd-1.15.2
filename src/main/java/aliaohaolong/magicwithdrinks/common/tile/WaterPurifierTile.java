package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDPotions;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.common.block.WaterPurifierBlock;
import aliaohaolong.magicwithdrinks.common.inventory.WaterPurifierContainer;
import aliaohaolong.magicwithdrinks.common.item.FilterSliceItem;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.Random;

import static net.minecraftforge.common.ForgeHooks.getBurnTime;

@SuppressWarnings("NullableProblems")
public class WaterPurifierTile extends AbstractHighSidedInventoryTile implements INamedContainerProvider, ITickableTileEntity {
    // water0 fuel1 slice2 result3 impurity4
    private static final int[] SLOT_UP = new int[]{0};
    private static final int[] SLOTS_HORIZONTAL = new int[]{1, 2};
    private static final int[] SLOTS_DOWN = new int[]{3, 4};
    public static final byte TOTAL_IMPURITY = 50;
    private int impurity = 0;
    private int time = 0;
    private int totalBurnTime = 0;
    private int burnTime = 0;

    private final IIntArray data = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return WaterPurifierTile.this.time;
                case 1:
                    return WaterPurifierTile.this.burnTime;
                case 2:
                    return WaterPurifierTile.this.totalBurnTime;
                case 3:
                    return WaterPurifierTile.this.impurity;
                case 4:
                    return WaterPurifierTile.this.stacks.get(0).getItem() == Items.POTION ? 32 : 16;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    WaterPurifierTile.this.time = value;
                    break;
                case 1:
                    WaterPurifierTile.this.burnTime = value;
                    break;
                case 2:
                    WaterPurifierTile.this.totalBurnTime = value;
                    break;
                case 3:
                    WaterPurifierTile.this.impurity = value;
                    break;
                case 4:
                    break;
            }
        }

        @Override
        public int size() {
            return 5;
        }
    };

    public WaterPurifierTile() {
        super(MWDTypes.T_WATER_PURIFIER, 5);
    }

    // MWD
    protected static int getImpurity(ItemStack stack) {
        return stack.getItem() == Items.POTION ? 2 : 1;
    }

    private boolean checkWater(final ItemStack stack) {
        return PotionUtils.getPotionFromItem(stack) == Potions.WATER || MWDPotionUtils.getPotionFromStack(stack) == MWDPotions.WATER;
    }

    private boolean checkImpurity(final ItemStack stack) {
        if (stack.isEmpty() || stack.getItem() == MWDItems.IMPURITY && stack.getCount() < stack.getMaxStackSize())
            return true;
        return TOTAL_IMPURITY - impurity >= getImpurity(stacks.get(0));
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.water_purifier");
    }

    // IContainerProvider
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity player) {
        return new WaterPurifierContainer(windowId, playerInventory, this, data);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        totalBurnTime = getBurnTime(stacks.get(1));
        time = compound.getInt("CookTime");
        burnTime = compound.getInt("BurnTime");
        impurity = compound.getByte("ImpurityScale");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putInt("CookTime", time);
        compound.putInt("BurnTime", burnTime);
        compound.putByte("ImpurityScale", (byte) impurity);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 0) return stack.getItem() == Items.POTION || stack.getItem() == MWDItems.POTION;
        if (index == 1) return getBurnTime(stack) > 0;
        if (index == 2) return stack.getItem() instanceof FilterSliceItem;
        return false;
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.UP)
            return SLOT_UP;
        return side == Direction.DOWN ? SLOTS_DOWN : SLOTS_HORIZONTAL;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN) {
            if (index == 0) return stack.getItem() == MWDItems.PURIFIED_WATER || MWDPotionUtils.getPotionFromStack(stack) == MWDPotions.PURIFIED_WATER;
            if (index == 1) return getBurnTime(stack) <= 0;
            return index != 2;
        }
        return false;
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        if (world != null && !world.isRemote) {
            boolean originalState = burnTime > 0;
            if (burnTime > 0) {
                markDirty = true;
                burnTime--;
            }
            if (burnTime > 0 || !stacks.get(0).isEmpty() && !stacks.get(1).isEmpty() && !stacks.get(2).isEmpty() && stacks.get(3).isEmpty()) {
                if (burnTime == 0 && checkWater(stacks.get(0)) && getBurnTime(stacks.get(1)) > 0 && stacks.get(2).getItem() instanceof FilterSliceItem && stacks.get(3).isEmpty() && checkImpurity(stacks.get(4))) {
                    burnTime = getBurnTime(stacks.get(1));
                    totalBurnTime = burnTime;
                    if (burnTime > 0) {
                        markDirty = true;
                        ItemStack fuelStack = stacks.get(1);
                        if (fuelStack.hasContainerItem())
                            stacks.set(1, fuelStack.getContainerItem());
                        else stacks.get(1).setCount(fuelStack.getCount() - 1);
                    }
                }
                if (burnTime > 0 && checkWater(stacks.get(0)) && getBurnTime(stacks.get(1)) > 0 && stacks.get(2).getItem() instanceof FilterSliceItem && stacks.get(3).isEmpty() && checkImpurity(stacks.get(4))) {
                    markDirty = true;
                    time++;
                    boolean isVanilla = stacks.get(0).getItem() == Items.POTION;
                    if (time >= (isVanilla ? 32 : 16)) {
                        time = 0;
                        stacks.set(0, ItemStack.EMPTY);
                        if (stacks.get(2).attemptDamageItem(isVanilla ? 2 : 1, new Random(), null)) stacks.set(2, ItemStack.EMPTY);
                        if (isVanilla) stacks.set(3, new ItemStack(MWDItems.PURIFIED_WATER));
                        else stacks.set(3, MWDPotionUtils.addPotionToStack(new ItemStack(MWDItems.POTION), MWDPotions.PURIFIED_WATER));
                        impurity += (isVanilla ? 2 : 1);
                    }
                } else
                    time = 0;
            } else if (burnTime == 0 && time > 0) {
                markDirty = true;
                time = Math.max(time - 2, 0);
            }
            if (impurity >= TOTAL_IMPURITY) {
                ItemStack stack = stacks.get(4);
                if (stack.isEmpty()) {
                    markDirty = true;
                    impurity -= TOTAL_IMPURITY;
                    stacks.set(4, new ItemStack(MWDItems.IMPURITY));
                } else if (stack.getItem() == MWDItems.IMPURITY && stack.getCount() < stack.getMaxStackSize()) {
                    markDirty = true;
                    impurity -= TOTAL_IMPURITY;
                    stacks.get(4).setCount(stack.getCount() + 1);
                }
            }
            if (originalState != (burnTime > 0)) {
                markDirty = true;
                world.setBlockState(pos, world.getBlockState(pos).with(WaterPurifierBlock.SWITCH, burnTime > 0), 3);
            }
        }
        if (markDirty) markDirty();
    }
}