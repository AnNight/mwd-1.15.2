package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.MWDTypes;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;

@SuppressWarnings("NullableProblems")
public class CreativeManaBlockTile extends TileEntity implements ITickableTileEntity, IManableTileEntity {
    private float mspt = 0;

    public CreativeManaBlockTile() {
        this(0);
    }

    public CreativeManaBlockTile(float maxSendingPerTick) {
        super(MWDTypes.T_CREATIVE_MANA_BLOCK);
        if (mspt == 0 && maxSendingPerTick != 0) mspt = maxSendingPerTick;
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        mspt = compound.getFloat("MSPT");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putFloat("MSPT", mspt);
        return compound;
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        if (world != null && !world.isRemote) {
            if (send(world, pos, mspt) != mspt)
                markDirty();
        }
    }
}