package aliaohaolong.magicwithdrinks.common.tile;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;

public interface IManableTileEntity {
    /**
     * Return true if your machine can receives mana, or else return false.
     */
    default boolean canReceive() {
        return false;
    }

    /**
     * If your machine needs to receive mana, please implement it.
     *
     * @param maxValue the max mana need to manage.
     * @return the surplus.
     */
    default float receiver(float maxValue) {
        return maxValue;
    }

    /**
     * Call it when your machine needs to send mana.
     * This method only should be called at {@link ITickableTileEntity#tick()}.
     *
     * @param pos position of this tile
     * @param maxValue the max mana to need to send.
     * @return the surplus.
     */
    default float send(IWorld world, BlockPos pos, float maxValue) {
        for (Direction direction : Direction.values()) {
            if (maxValue > 0) {
                TileEntity tileEntity = world.getTileEntity(pos.offset(direction));
                if (tileEntity instanceof IManableTileEntity) {
                    IManableTileEntity tile = (IManableTileEntity) tileEntity;
                    if (tile.canReceive()) maxValue = tile.receiver(maxValue);
                }
            } else break;
        }
        return maxValue;
    }
}