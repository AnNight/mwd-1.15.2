package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.MyMath;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.common.block.MagicWaterFountainBlock;
import aliaohaolong.magicwithdrinks.common.extra.ExtraUtils;
import aliaohaolong.magicwithdrinks.common.inventory.MagicWaterFountainContainer;
import aliaohaolong.magicwithdrinks.common.item.FilterSliceItem;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.Random;

@SuppressWarnings("NullableProblems")
public class MagicWaterFountainTile extends AbstractWaterFountainTile implements IManableTileEntity {
    // 0 Water 1 Empty 2 slice 3 impurity 4 mana
    private float buffer = 0.0F;

    /** {@link MWDItems#MANA_CRYSTAL_BATTERY} */
    public static final float MAX_BUFFER = 80.0F;

    // Whether the receiver had be triggered at this tick.
    private boolean flag = false;

    private final IIntArray data = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return MagicWaterFountainTile.this.water;
                case 1:
                    return MagicWaterFountainTile.this.purified_water;
                case 2:
                    return MagicWaterFountainTile.this.impurity;
                case 3:
                    return MagicWaterFountainTile.this.cookTime;
                case 4:
                    return MyMath.toInt(MagicWaterFountainTile.this.buffer);
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    MagicWaterFountainTile.this.water = value;
                    break;
                case 1:
                    MagicWaterFountainTile.this.purified_water = value;
                    break;
                case 2:
                    MagicWaterFountainTile.this.impurity = (byte) value;
                    break;
                case 3:
                    MagicWaterFountainTile.this.cookTime = value;
                    break;
                case 4:
                    MagicWaterFountainTile.this.buffer = MyMath.toFloat(value);
                    break;
            }
        }

        @Override
        public int size() {
            return 5;
        }
    };

    public MagicWaterFountainTile() {
        super(MWDTypes.T_MAGIC_WATER_FOUNTAIN);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        buffer = compound.getFloat("Buffer");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putFloat("Buffer", buffer);
        return compound;
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.magic_water_fountain");
    }

    // IContainerProvider
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity player) {
        return new MagicWaterFountainContainer(windowId, playerInventory, this, data);
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 0) return getWaterContent(stack) > 0;
        if (index == 2) return stack.getItem() instanceof FilterSliceItem;
        Extra extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
        if (index == 4 && extra != null) return extra.canStore();
        return false;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN && index == 4) {
            return ExtraUtils.isFullMana(stack);
        }
        return true;
    }

    // IManableTileEntity
    @Override
    public boolean canReceive() {
        return true;
    }

    /**
     * This mrpt is 0.08F.
     */
    @Override
    public float receiver(float maxValue) {
        if (!flag && buffer < MAX_BUFFER) {
            float temp = Math.min(MyMath.minus(MAX_BUFFER, buffer), Math.min(0.08F, maxValue));
            buffer = MyMath.plus(buffer, temp);
            maxValue = MyMath.minus(maxValue, temp);
            flag = true;
        }
        return maxValue;
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        if (world != null && !world.isRemote) {
            boolean originalState = cookTime > 0;
            // input water
            ItemStack in = stacks.get(0);
            ItemStack out = stacks.get(1);
            if (!in.isEmpty() && water < MAX_CAPACITY) {
                int value = getWaterContent(in);
                if (water + value <= MAX_CAPACITY && (out.isEmpty() || out.getCount() < out.getMaxStackSize())) {
                    if (out.isEmpty() || (in.getItem() == Items.WATER_BUCKET && out.getItem() == Items.BUCKET) ||
                            (in.getItem() == Items.POTION && out.getItem() == Items.GLASS_BOTTLE) || (in.getItem() == MWDItems.POTION && out.getItem() == MWDItems.POTION_BOTTLE)) {
                        water = inputWater(water, value, stacks, 0, 1);
                        markDirty = true;
                    }
                }
            }
            if (flag) {
                markDirty = true;
                flag = false;
            }
            if (buffer >= 0.8F && water >= 80 && purified_water + 80 <= MAX_CAPACITY && stacks.get(2).getItem() instanceof FilterSliceItem && checkImpurity()) {
                if (cookTime <= 1) {
                    cookTime = 2;
                } else {
                    cookTime++;
                    if (cookTime >= 17) {
                        cookTime = 1;
                        water -= 80;
                        purified_water += 80;
                        impurity++;
                        buffer = MyMath.minus(buffer, 0.8F);
                        if (stacks.get(2).attemptDamageItem(1, new Random(), null)) stacks.set(2, ItemStack.EMPTY);
                    }
                }
            } else cookTime = 0;
            if (impurity >= MAX_IMPURITY) {
                ItemStack stack = stacks.get(3);
                if (stack.isEmpty()) {
                    markDirty = true;
                    impurity -= MAX_IMPURITY;
                    stacks.set(3, new ItemStack(MWDItems.IMPURITY));
                } else if (stack.getItem() == MWDItems.IMPURITY && stack.getCount() < stack.getMaxStackSize()) {
                    markDirty = true;
                    impurity -= MAX_IMPURITY;
                    stacks.get(3).setCount(stack.getCount() + 1);
                }
            }
            if (originalState != (cookTime > 0)) {
                world.setBlockState(pos, world.getBlockState(pos).with(MagicWaterFountainBlock.SWITCH, cookTime > 0), 3);
                markDirty = true;
            }
        }
        if (markDirty) markDirty();
    }
}