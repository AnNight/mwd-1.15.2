package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDPotions;
import aliaohaolong.magicwithdrinks.common.block.MagicWaterFountainBlock;
import aliaohaolong.magicwithdrinks.common.block.WaterFountainBlock;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;

@SuppressWarnings("NullableProblems")
public abstract class AbstractWaterFountainTile extends AbstractHighSidedInventoryTile implements INamedContainerProvider, ITickableTileEntity {
    private static final int[] SLOT_UP = new int[]{0};
    private static final int[] SLOTS_HORIZONTAL = new int[]{2, 4};
    private static final int[] SLOTS_DOWN = new int[]{1, 3, 4};
    protected static final int MAX_IMPURITY = 50;
    public static final int MAX_CAPACITY = 20000;
    protected int water = 0;
    protected int purified_water = 0;
    protected byte impurity = 0;
    protected int cookTime = -1;

    protected AbstractWaterFountainTile(TileEntityType<?> type) {
        super(type, 5);
    }

    // MWD
    public static int getWaterContent(ItemStack stack) {
        if (stack.isEmpty()) return 0;
        if (stack.getItem() == Items.WATER_BUCKET) return 20000;
        if (PotionUtils.getPotionFromItem(stack) == Potions.WATER) return 160;
        if (MWDPotionUtils.getPotionFromStack(stack) == MWDPotions.WATER) return 80;
        return 0;
    }

    /**
     * User 1 {@link WaterFountainBlock#onBlockActivated(BlockState, World, BlockPos, PlayerEntity, Hand, BlockRayTraceResult)}
     * User 2 {@link MagicWaterFountainBlock#onBlockActivated(BlockState, World, BlockPos, PlayerEntity, Hand, BlockRayTraceResult)}
     * @param stack a players' stack.
     * @return a corresponding purified water stack.
     */
    public ItemStack extractPurifiedWater(ItemStack stack) {
        if (stack.getItem() == Items.GLASS_BOTTLE && purified_water >= 160) {
            purified_water -= 160;
            markDirty();
            return new ItemStack(MWDItems.PURIFIED_WATER);
        }
        if (stack.getItem() == MWDItems.POTION_BOTTLE && purified_water >= 80) {
            purified_water -= 80;
            markDirty();
            return MWDPotionUtils.addPotionToStack(new ItemStack(MWDItems.POTION), MWDPotions.PURIFIED_WATER);
        }
        return ItemStack.EMPTY;
    }

    @SuppressWarnings("SameParameterValue")
    protected static int inputWater(int water, int value, NonNullList<ItemStack> stacks, int inIndex, int outIndex) {
        if (stacks.get(outIndex).isEmpty()) {
            ItemStack in = stacks.get(inIndex);
            if (in.getItem() == Items.WATER_BUCKET) stacks.set(outIndex, new ItemStack(Items.BUCKET));
            if (in.getItem() == Items.POTION) stacks.set(outIndex, new ItemStack(Items.GLASS_BOTTLE));
            if (in.getItem() == MWDItems.POTION) stacks.set(outIndex, new ItemStack(MWDItems.POTION_BOTTLE));
        } else stacks.get(outIndex).grow(1);
        stacks.set(inIndex, ItemStack.EMPTY);
        return water + value;
    }

    protected boolean checkImpurity() {
        ItemStack stack = stacks.get(3);
        return stack.isEmpty() || stack.getItem() == MWDItems.IMPURITY && (stack.getCount() < stack.getMaxStackSize() || impurity + 1 <= MAX_IMPURITY);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        water = compound.getInt("Water");
        purified_water = compound.getInt("PurifiedWater");
        impurity = compound.getByte("ImpurityScale");
        cookTime = compound.getInt("CookTime");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putInt("Water", water);
        compound.putInt("PurifiedWater", purified_water);
        compound.putByte("ImpurityScale", impurity);
        compound.putInt("CookTime", cookTime);
        return compound;
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.UP)
            return SLOT_UP;
        return side == Direction.DOWN ? SLOTS_DOWN : SLOTS_HORIZONTAL;
    }
}