package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.common.block.WaterFountainBlock;
import aliaohaolong.magicwithdrinks.common.inventory.WaterFountainContainer;
import aliaohaolong.magicwithdrinks.common.item.FilterSliceItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.Random;

import static net.minecraftforge.common.ForgeHooks.getBurnTime;

@SuppressWarnings("NullableProblems")
public class WaterFountainTile extends AbstractWaterFountainTile {
    // 0 Water 1 Empty 2 slice 3 impurity 4 fuel
    private int burnTime = 0;
    private int totalBurnTime = 0;
    private final IIntArray data = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return WaterFountainTile.this.water;
                case 1:
                    return WaterFountainTile.this.purified_water;
                case 2:
                    return WaterFountainTile.this.burnTime;
                case 3:
                    return WaterFountainTile.this.totalBurnTime;
                case 4:
                    return WaterFountainTile.this.impurity;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    WaterFountainTile.this.water = value;
                    break;
                case 1:
                    WaterFountainTile.this.purified_water = value;
                    break;
                case 2:
                    WaterFountainTile.this.burnTime = value;
                    break;
                case 3:
                    WaterFountainTile.this.totalBurnTime = value;
                    break;
                case 4:
                    WaterFountainTile.this.impurity = (byte) value;
                    break;
            }
        }

        @Override
        public int size() {
            return 5;
        }
    };

    public WaterFountainTile() {
        super(MWDTypes.T_WATER_FOUNTAIN);
    }

    // MWD
    private boolean canPurify() {
        return water >= 80 && purified_water + 80 <= MAX_CAPACITY && stacks.get(2).getItem() instanceof FilterSliceItem && checkImpurity();
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.water_fountain");
    }

    // IContainerProvider
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity player) {
        return new WaterFountainContainer(windowId, playerInventory, this, data);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        totalBurnTime = getBurnTime(stacks.get(4));
        burnTime = compound.getInt("BurnTime");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putInt("BurnTime", burnTime);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 0) return getWaterContent(stack) > 0;
        if (index == 2) return stack.getItem() instanceof FilterSliceItem;
        if (index == 4) return getBurnTime(stack) > 0;
        return false;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN && index == 4)
            return getBurnTime(stack) <= 0;
        return true;
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        if (world != null && !world.isRemote) {
            boolean originalState = burnTime > 0;
            if (burnTime > 0) {
                markDirty = true;
                burnTime--;
            }
            // input water
            ItemStack in = stacks.get(0);
            ItemStack out = stacks.get(1);
            if (!in.isEmpty() && water < MAX_CAPACITY) {
                int value = getWaterContent(in);
                if (water + value <= MAX_CAPACITY && (out.isEmpty() || out.getCount() < out.getMaxStackSize())) {
                    if (out.isEmpty() || (in.getItem() == Items.WATER_BUCKET && out.getItem() == Items.BUCKET) ||
                            (in.getItem() == Items.POTION && out.getItem() == Items.GLASS_BOTTLE) || (in.getItem() == MWDItems.POTION && out.getItem() == MWDItems.POTION_BOTTLE)) {
                        water = inputWater(water, value, stacks, 0, 1);
                        markDirty = true;
                    }
                }
            }
            // 5ml/tick
            if (burnTime > 0 || canPurify()) {
                if (burnTime == 0) {
                    burnTime = getBurnTime(stacks.get(4));
                    totalBurnTime = burnTime;
                    if (burnTime > 0) {
                        markDirty = true;
                        ItemStack fuelStack = stacks.get(4);
                        if (fuelStack.hasContainerItem())
                            stacks.set(4, fuelStack.getContainerItem());
                        else stacks.get(4).setCount(fuelStack.getCount() - 1);
                    }
                }
                // 16tick
                if (burnTime > 0 && canPurify()) {
                    markDirty = true;
                    cookTime++;
                    if (cookTime >= 16) {
                        cookTime = 0;
                        if (stacks.get(2).attemptDamageItem(1, new Random(), null)) stacks.set(2, ItemStack.EMPTY);
                        water -= 80;
                        purified_water += 80;
                        impurity++;
                    }
                } else {
                    cookTime = 0;
                }
            }
            if (impurity >= MAX_IMPURITY) {
                ItemStack stack = stacks.get(3);
                if (stack.isEmpty()) {
                    markDirty = true;
                    impurity -= MAX_IMPURITY;
                    stacks.set(3, new ItemStack(MWDItems.IMPURITY));
                } else if (stack.getItem() == MWDItems.IMPURITY && stack.getCount() < stack.getMaxStackSize()) {
                    markDirty = true;
                    impurity -= MAX_IMPURITY;
                    stacks.get(3).setCount(stack.getCount() + 1);
                }
            }
            if (originalState != (burnTime > 0)) {
                markDirty = true;
                world.setBlockState(pos, world.getBlockState(pos).with(WaterFountainBlock.SWITCH, burnTime > 0), 3);
            }
        }
        if (markDirty) markDirty();
    }
}