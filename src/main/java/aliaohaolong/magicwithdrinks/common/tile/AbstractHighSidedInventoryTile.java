package aliaohaolong.magicwithdrinks.common.tile;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class AbstractHighSidedInventoryTile extends AbstractSidedInventoryTile {
    public AbstractHighSidedInventoryTile(TileEntityType<?> tileEntityType, int size) {
        super(tileEntityType, size);
    }

    // ICapabilityProvider
    protected LazyOptional<? extends IItemHandler>[] handlers = SidedInvWrapper.create(this, Direction.UP, Direction.DOWN, Direction.NORTH);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (!this.removed && side != null && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            if (side == Direction.UP)
                return handlers[0].cast();
            return side == Direction.DOWN ? handlers[1].cast() : handlers[2].cast();
        }
        return super.getCapability(cap, side);
    }
}