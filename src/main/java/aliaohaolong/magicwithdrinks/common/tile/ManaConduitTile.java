package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.MWDTypes;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;

@SuppressWarnings("NullableProblems")
public class ManaConduitTile extends TileEntity implements ITickableTileEntity, IManableTileEntity {
    private float mspt = 0;
    private boolean isProcessing;
    // this tick remain
    private float remaining = 0;

    public ManaConduitTile() {
        this(0);
    }

    public ManaConduitTile(int maxPerTick) {
        super(MWDTypes.T_MANA_CONDUIT);
        if (mspt == 0 && maxPerTick != 0) mspt = maxPerTick;
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        mspt = compound.getFloat("MSPT");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putFloat("MSPT", mspt);
        return compound;
    }

    // IManableTileEntity
    @Override
    public boolean canReceive() {
        return true;
    }

    @Override
    public float receiver(float maxValue) {
        if (isProcessing || remaining == 0) return maxValue;
        isProcessing = true;
        for (Direction direction : Direction.values()) {
            if (maxValue > 0) {
                assert world != null;
                TileEntity tileEntity = world.getTileEntity(pos.offset(direction));
                if (tileEntity instanceof IManableTileEntity && ((IManableTileEntity) tileEntity).canReceive()) {
                    float push = Math.min(maxValue, remaining);
                    float hadReceived = push - ((IManableTileEntity) tileEntity).receiver(push);
                    remaining -= hadReceived;
                    maxValue -= hadReceived;
                }
            } else break;
        }
        isProcessing = false;
        return maxValue;
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        remaining = mspt;
    }
}