package aliaohaolong.magicwithdrinks.common.extra;

import aliaohaolong.magicwithdrinks.common.capability.mana.IMana.Type;

/**
 * Save inner attributes of items in order to make potions at smelting boxes.
 * This attributes will be called for calculating attributes of potions when finish the process of making potions.
 */

public class Herb {
    public static final Herb EMPTY = new Herb();

    private final Type type = Type.NATURE;
    private final byte cookingLattice = 0;

    private Herb() {
    }
}