package aliaohaolong.magicwithdrinks.common.extra.attr;

import aliaohaolong.magicwithdrinks.common.extra.AbstractNumberUnit.FloatUnit;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.ITextComponent;

import java.util.HashSet;

public class ManaAttribute extends AbstractAttribute<Float, FloatUnit> {
    public static final ManaAttribute EMPTY = new ManaAttribute(0);

    private final FloatUnit MAX;

    public ManaAttribute(float mana) {
        this(new FloatUnit(mana), new FloatUnit(0));
    }

    public ManaAttribute(float mana, float max) {
        this(new FloatUnit(mana), new FloatUnit(max));
    }

    public ManaAttribute(FloatUnit manaUnit, float max) {
        this(manaUnit, new FloatUnit(max));
    }

    public ManaAttribute(float mana, FloatUnit maxUnit) {
        this(new FloatUnit(mana), maxUnit);
    }

    public ManaAttribute(FloatUnit floatUnit, FloatUnit maxUnit) {
        super(floatUnit, "magicwithdrinks.lore.mana");
        MAX = maxUnit;
    }

    @Override
    public boolean apply(ServerPlayerEntity player) {
        IMana mana = IMana.getFromPlayer(player);
        boolean sync = false;
        if (DATA.randomIsPositive()) {
            float f = DATA.getVALUE();
            float f1 = mana.getLimit() - mana.getValue();
            if (f > f1) player.attackEntityFrom(DamageSource.MAGIC, (f1 - f) * 1.75F);
            if (mana.modifyValue(DATA.getVALUE())) sync = true;
        } else {
            player.addPotionEffect(new EffectInstance(Effects.WEAKNESS, 320));
            if (mana.modifyValue(DATA.getDEVALUE())) sync = true;
        }
        if (MAX.randomIsPositive() && mana.modifyLimit(DATA.getVALUE())) sync = true;
        else {
            player.addPotionEffect(new EffectInstance(Effects.WEAKNESS, 320));
            if (mana.modifyLimit(DATA.getDEVALUE())) sync = true;
        }
        return sync;
    }

    @Override
    public HashSet<ITextComponent> getLores() {
        HashSet<ITextComponent> lores = super.getLores();
        if (!MAX.isEmpty())
            lores.addAll(getLores("magicwithdrinks.lore.max_mana", MAX));
        return lores;
    }
}