package aliaohaolong.magicwithdrinks.common.extra.attr;

import aliaohaolong.magicwithdrinks.common.extra.AbstractNumberUnit;
import net.minecraft.entity.player.ServerPlayerEntity;

public class HungerAttribute extends AbstractAttribute<Integer, AbstractNumberUnit.IntUnit> {
    public static final HungerAttribute EMPTY = new HungerAttribute(0);

    private final float SATURATION;

    public HungerAttribute(int hunger) {
        this(hunger, 0.0F);
    }

    public HungerAttribute(int hunger, float saturation) {
        super(new AbstractNumberUnit.IntUnit(hunger));
        SATURATION = saturation;
    }

    /**
     * @return Always return false because we don't need to manage the sync of the hunger value and saturation value.
     */
    @Override
    public boolean apply(ServerPlayerEntity player) {
        player.getFoodStats().addStats(DATA.getWithPositive(), SATURATION);
        return false;
    }

    @Override
    public boolean isNullLore() {
        return true;
    }
}