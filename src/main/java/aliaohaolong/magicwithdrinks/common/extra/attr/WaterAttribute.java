package aliaohaolong.magicwithdrinks.common.extra.attr;

import aliaohaolong.magicwithdrinks.common.extra.AbstractNumberUnit.IntUnit;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;

public class WaterAttribute extends AbstractAttribute<Integer, IntUnit> {
    public static final WaterAttribute EMPTY = new WaterAttribute(0);

    private final int ATTACK;

    public WaterAttribute(int value) {
        this(new IntUnit(value), 0);
    }

    public WaterAttribute(IntUnit intUnit, int attack) {
        super(intUnit, "magicwithdrinks.lore.water");
        ATTACK = attack;
    }

    @Override
    public boolean apply(ServerPlayerEntity player) {
        IWater water = IWater.getFromPlayer(player);
        if (DATA.randomIsPositive()) return water.increase(DATA.getVALUE());
        player.attackEntityFrom(DamageSource.DRYOUT, ATTACK);
        player.addPotionEffect(new EffectInstance(Effects.NAUSEA, 300));
        return water.increase(DATA.getDEVALUE());
    }
}