package aliaohaolong.magicwithdrinks.common.extra.attr;

import aliaohaolong.magicwithdrinks.common.extra.AbstractNumberUnit;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.HashSet;

public abstract class AbstractAttribute<N extends Number, T extends AbstractNumberUnit<N>> {
    protected final T DATA;
    private final String KEY;

    public AbstractAttribute(T dataUnit) {
        this(dataUnit, "");
    }

    public AbstractAttribute(T dataUnit, String key) {
        DATA = dataUnit;
        KEY = key;
    }

    abstract public boolean apply(ServerPlayerEntity player);

    public boolean isNullLore() {
        return DATA.isEmpty();
    }

    public HashSet<ITextComponent> getLores() {
        return getLores(KEY, DATA);
    }

    protected HashSet<ITextComponent> getLores(final String key, final AbstractNumberUnit<?> data) {
        if (isNullLore()) return null;
        HashSet<ITextComponent> lores = new HashSet<>();
        if (data.isMaximinPossibility())
            lores.add(new TranslationTextComponent(key, data.getPOSSIBILITY() == 1 ? "+".concat(String.valueOf(data.getVALUE())) : data.getDEVALUE()).applyTextStyle(data.getTextFormatting()));
        else {
            int percent = data.getPercent();
            if (data.getVALUE().floatValue() == 0.0F || data.getDEVALUE().floatValue() == 0.0F) {
                boolean flag = data.getVALUE().floatValue() == 0.0F;
                lores.add(new TranslationTextComponent(
                        key.concat("_single"),
                        flag ? data.getDEVALUE() : "+".concat(String.valueOf(data.getVALUE())),
                        flag ? String.valueOf(100 - percent).concat("%") : String.valueOf(percent).concat("%")
                ).applyTextStyle(data.getVALUE().floatValue() == 0.0F ? TextFormatting.RED : TextFormatting.BLUE));
            } else lores.add(new TranslationTextComponent(
                    key.concat("_complex"),
                    "+".concat(String.valueOf(data.getVALUE())), String.valueOf(percent).concat("%"), data.getDEVALUE(), String.valueOf(100 - percent).concat("%")
            ).applyTextStyle(TextFormatting.RED));
        }
        return lores;
    }
}