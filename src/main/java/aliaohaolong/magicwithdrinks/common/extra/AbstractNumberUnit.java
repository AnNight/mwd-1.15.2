package aliaohaolong.magicwithdrinks.common.extra;

import net.minecraft.util.text.TextFormatting;

import java.util.Random;

public abstract class AbstractNumberUnit<T extends Number> {
    protected static Random random = new Random();

    // Value of recovery
    private final T VALUE;

    // Possibility of recovery. Between 0 and 1.
    private final float POSSIBILITY;

    // For water
    private final T DEVALUE;

    private AbstractNumberUnit(T value, float possibility, T devalue) {
        VALUE = value;
        try { checkP(possibility); } catch (Exception e) { e.printStackTrace(); }
        POSSIBILITY = possibility;
        try { checkD(devalue); } catch (Exception e) { e.printStackTrace(); }
        DEVALUE = devalue;
    }

    public T getVALUE() {
        return VALUE;
    }

    public float getPOSSIBILITY() {
        return POSSIBILITY;
    }

    public T getDEVALUE() {
        return DEVALUE;
    }

    // Whether is empty value.
    public boolean isEmpty() {
        return getVALUE().doubleValue() == 0 && POSSIBILITY == 1.0F && getDEVALUE().doubleValue() == 0;
    }

    public boolean randomIsPositive() {
        return random.nextFloat() < POSSIBILITY;
    }

    public T getWithPositive() {
        return random.nextFloat() < POSSIBILITY ? VALUE : DEVALUE;
    }

    // Whether is 0.0F or 1.0F possibility.
    public boolean isMaximinPossibility() {
        return POSSIBILITY == 0.0F || POSSIBILITY == 1.0F;
    }

    public TextFormatting getTextFormatting() {
        return POSSIBILITY == 1.0F ? TextFormatting.BLUE : TextFormatting.RED;
    }

    public int getPercent() {
        return (int) (POSSIBILITY * 100.0F);
    }

    private void checkD(final T devalue) throws Exception {
        if (devalue.doubleValue() > 0)
            throw new Exception("[Magic With Drinks]: Init: Devalue must is negative.");
    }

    private void checkP(final float possibility) throws Exception {
        if (possibility > 1.0F || possibility < 0.0F)
            throw new Exception("[Magic With Drinks]: Init: Possibility must is between 0.0 and 1.0.");
    }

    public static class IntUnit extends AbstractNumberUnit<Integer> {
        public static final IntUnit EMPTY = new IntUnit(0);

        public IntUnit(int value) {
            this(value, value < 0 ? 0.0f : 1.0f, Math.min(value, 0));
        }

        public IntUnit(int value, float possibility, int devalue) {
            super(value, possibility, devalue);
        }
    }

    public static class FloatUnit extends AbstractNumberUnit<Float> {
        public static final FloatUnit EMPTY = new FloatUnit(0);

        public FloatUnit(float value) {
            this(value, value < 0 ? 0.0f : 1.0f, Math.min(value, 0));
        }

        public FloatUnit(float value, float possibility, float devalue) {
            super(value, possibility, devalue);
        }
    }
}