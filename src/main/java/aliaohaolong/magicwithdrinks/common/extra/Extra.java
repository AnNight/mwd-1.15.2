package aliaohaolong.magicwithdrinks.common.extra;

import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.extra.attr.AbstractAttribute;
import aliaohaolong.magicwithdrinks.common.extra.attr.ManaAttribute;
import aliaohaolong.magicwithdrinks.common.extra.attr.WaterAttribute;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import com.google.common.collect.ImmutableList;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;
import java.util.HashSet;

public class Extra extends ForgeRegistryEntry<Extra> {
    public static final Extra EMPTY = new Extra.Properties(null).build();

    private final Item item;
    private final boolean isFood;
    private final boolean canStore;
    private final boolean isHerb;
    private final float capacity;
    private final float initial;
    private final float mspt;
    private final float mrpt;
    private final Herb herb;
    private final ImmutableList<AbstractAttribute<?, ?>> attributes;

    private Extra(Item itemIn, boolean isFoodIn, boolean canStoreIn, boolean isHerbIn, float capacityIn, float initialIn, float msptIn, float mrptIn, Herb herbIn, ImmutableList<AbstractAttribute<?, ?>> attributesIn) {
        item = itemIn;
        isFood = isFoodIn;
        canStore = canStoreIn;
        isHerb = isHerbIn;
        capacity = capacityIn;
        initial = initialIn;
        mspt = msptIn;
        mrpt = mrptIn;
        herb = herbIn;
        attributes = attributesIn;
    }

    public Item getItem() {
        return item;
    }

    public boolean isFood() {
        return isFood;
    }

    public boolean canStore() {
        return canStore;
    }

    /**
     * Whether is herb. That means you can make some potions by it.
     */
    public boolean isHerb() {
        return isHerb;
    }

    /**
     * @return Return the capacity that the maximum that can store if this item can store mana, or
     *  return the value that can be extracted mana if can extracting mana from this item.
     */
    public float getCapacity() {
        return capacity;
    }

    /**
     * @return Return the initial mana value if this item can store mana, or return the value of extracted time if can extract mana from this item.
     */
    public float getInitial() {
        return initial;
    }

    /**
     * Max sending per tick(abbreviated to MSPT).
     */
    public float getMSPT() {
        return mspt;
    }

    /**
     * Max receiving per tick(abbreviated to MRPT).
     */
    public float getMRPT() {
        return mrpt;
    }

    public Herb getHerb() {
        if (!isHerb)
            return Herb.EMPTY;
        return herb;
    }

    public void applyAttributes(ServerPlayerEntity player) {
        if (!isFood) return;
        for (AbstractAttribute<?, ?> attribute : attributes) {
            boolean sync = attribute.apply(player);
            if (sync && attribute instanceof WaterAttribute)
                PacketManager.sendWaterTo(player, IWater.getFromPlayer(player));
            if (sync && attribute instanceof ManaAttribute)
                PacketManager.sendManaTo(player, IMana.getFromPlayer(player));
        }
    }

    public HashSet<ITextComponent> getLores() {
        HashSet<ITextComponent> lores = new HashSet<>();
        for (AbstractAttribute<?, ?> attribute: attributes) {
            if (!attribute.isNullLore()) lores.addAll(attribute.getLores());
        }
        return lores;
    }

    public static class Properties {
        private final Item item;
        private boolean isFood = false;
        private boolean canStore = false;
        private boolean isHerb = false;
        private final float capacity;
        private float initial = 0.0F;
        private float mspt = 0.0F;
        private float mrpt = 0.0F;
        private Herb herb = Herb.EMPTY;
        private ImmutableList<AbstractAttribute<?, ?>> attributes = null;

        public Properties(@Nullable Item itemIn) {
            this(itemIn, 0);
        }

        public Properties(@Nullable Item itemIn, float capacityIn) {
            if (itemIn == null)
                itemIn = Items.AIR;
            item = itemIn;
            capacity = capacityIn;
        }

        public Properties isFood(AbstractAttribute<?, ?>... attributesIn) {
            isFood = true;
            attributes = ImmutableList.copyOf(attributesIn);
            canStore = false;
            return this;
        }

        public Properties canStore(float initialIn, float msptIn, float mrptIn) {
            canStore = true;
            initial = initialIn;
            mspt = msptIn;
            mrpt = mrptIn;
            isFood = false;
            isHerb = false;
            return this;
        }

        public Properties isHerb(Herb herbIn) {
            isHerb = true;
            herb = herbIn;
            canStore = false;
            return this;
        }

        public Extra build() {
            return new Extra(item, isFood, canStore, isHerb, capacity, initial, mspt, mrpt, herb, attributes);
        }
    }
}