package aliaohaolong.magicwithdrinks.common.extra;

import aliaohaolong.magicwithdrinks.api.MWDExes;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.api.MWDPotions;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotion;
import net.minecraft.item.ItemStack;

import java.util.Objects;

public class ExtraUtils {
    public static Extra getExtraFromMPotion(MWDPotion potion) {
        if (potion == MWDPotions.WATER) return MWDExes.M_WATER;
        if (potion == MWDPotions.PURIFIED_WATER) return MWDExes.M_PURIFIED_WATER;
        return null;
    }

    public static float getMana(ItemStack stack) {
        if (!stack.hasTag() || !Objects.requireNonNull(stack.getTag()).contains("Mana")) {
            if (!writeInitialManaTag(stack, MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName())))
                return 0;
        }
        return stack.getOrCreateTag().getFloat("Mana");
    }

    public static float getSurplusMana(ItemStack stack) {
        Extra extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
        if (extra == null) return 0;
        if (!stack.hasTag() || !Objects.requireNonNull(stack.getTag()).contains("Mana")) {
            if (!writeInitialManaTag(stack, extra))
                return 0;
        }
        return extra.getCapacity() - stack.getOrCreateTag().getFloat("Mana");
    }

    public static boolean isFullMana(ItemStack stack) {
        Extra extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
        return extra != null && extra.canStore() && extra.getCapacity() == getMana(stack);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean writeInitialManaTag(ItemStack stack, Extra extra) {
        if (extra != null && extra.canStore()) {
            writeManaTag(stack, extra.getInitial());
            return true;
        }
        return false;
    }

    public static void writeManaTag(ItemStack stack, float value) {
//        CompoundNBT nbt = new CompoundNBT();
//        nbt.putFloat("mana", value);
//        stack.getOrCreateTag().put("Mana", nbt);
        stack.getOrCreateTag().putFloat("Mana", value);
    }
}