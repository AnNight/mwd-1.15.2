package aliaohaolong.magicwithdrinks.common.command;

import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana.Type;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.Collection;

public class ManaCommands {
    public static ArgumentBuilder<CommandSource, ?> register() {
        return Commands.literal("mana")
                .requires(commandSource -> commandSource.hasPermissionLevel(0))
                .executes(ManaCommands::query)
                .then(Commands.argument("target", EntityArgument.player())
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .executes((d) -> ManaCommands.query(d, EntityArgument.getPlayer(d, "target")))
                )
                .then(Commands.literal("openGui").executes((d) -> ManaCommands.gui(d, true)))
                .then(Commands.literal("closeGui").executes((d) -> ManaCommands.gui(d, false)))
                .then(Commands.literal("set")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.literal("value")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("float", FloatArgumentType.floatArg(0))
                                                .executes((d) -> ManaCommands.set(d, EntityArgument.getPlayers(d, "targets"), FloatArgumentType.getFloat(d, "float"), 0))
                                        )
                                )
                        )
                        .then(Commands.literal("limit")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("float", FloatArgumentType.floatArg(0))
                                                .executes((d) -> ManaCommands.set(d, EntityArgument.getPlayers(d, "targets"), FloatArgumentType.getFloat(d, "float"), 1))
                                        )
                                )
                        )
                        .then(Commands.literal("level")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("int", IntegerArgumentType.integer(0, 10))
                                                .executes((d) -> ManaCommands.set(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), 2))
                                        )
                                )
                        )
                        .then(Commands.literal("type")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.literal("Nature")
                                                .executes((d) -> ManaCommands.setType(d, EntityArgument.getPlayers(d, "targets"), (byte) 0))
                                        )
                                        .then(Commands.literal("Fire")
                                                .executes((d) -> ManaCommands.setType(d, EntityArgument.getPlayers(d, "targets"), (byte) 1))
                                        )
                                        .then(Commands.literal("Ice")
                                                .executes((d) -> ManaCommands.setType(d, EntityArgument.getPlayers(d, "targets"), (byte) 2))
                                        )
                                )
                        )
                )
                .then(Commands.literal("add")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.literal("value")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("float", FloatArgumentType.floatArg(0))
                                                .executes((d) -> ManaCommands.add(d, EntityArgument.getPlayers(d, "targets"), FloatArgumentType.getFloat(d, "float"), true))
                                        )
                                )
                        )
                        .then(Commands.literal("limit")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("float", FloatArgumentType.floatArg(0))
                                                .executes((d) -> ManaCommands.add(d, EntityArgument.getPlayers(d, "targets"), FloatArgumentType.getFloat(d, "float"), false))
                                        )
                                )
                        )
                )
                .then(Commands.literal("remove")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.literal("value")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("float", FloatArgumentType.floatArg(0))
                                                .executes((d) -> ManaCommands.remove(d, EntityArgument.getPlayers(d, "targets"), FloatArgumentType.getFloat(d, "float"), true))
                                        )
                                )
                        )
                        .then(Commands.literal("limit")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("float", FloatArgumentType.floatArg(0))
                                                .executes((d) -> ManaCommands.remove(d, EntityArgument.getPlayers(d, "targets"), FloatArgumentType.getFloat(d, "float"), false))
                                        )
                                )
                        )
                );
    }

    private static int query(CommandContext<CommandSource> context) throws CommandSyntaxException {
        return query(context, context.getSource().asPlayer());
    }

    private static int query(CommandContext<CommandSource> context, ServerPlayerEntity player) {
        IMana mana = IMana.getFromPlayer(player);
        Type type = mana.getType();
        String str;
        if (type == Type.FIRE) str = "fire";
        else str = type == Type.ICE ? "ice" : "nature";
        context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana:".concat(str), player.getName(), mana.getLevel(), mana.getValue(), mana.getLimit()), false);
        return 1;
    }

    private static int gui(CommandContext<CommandSource> context, boolean openGui) throws CommandSyntaxException {
        ServerPlayerEntity player = context.getSource().asPlayer();
        if (openGui) {
            IMana.getFromPlayer(player).openGui();
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.open_gui"), true);
        } else {
            IMana.getFromPlayer(player).closeGui();
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.close_gui"), true);
        }
        PacketManager.sendManaTo(player, IMana.getFromPlayer(player));
        return 1;
    }

    private static int set(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, float value, int type) {
        IMana mana;
        for (ServerPlayerEntity player : targets) {
            mana = IMana.getFromPlayer(player);
            if (type == 0) {
                if (mana.setValue(value)) PacketManager.sendManaTo(player, mana);
            } else if (type == 1) {
                if (mana.setLimit(value)) PacketManager.sendManaTo(player, mana);
            } else {
                if (mana.setLevel((int) value)) PacketManager.sendManaTo(player, mana);
            }
        }
        if (type == 0) {
            if (targets.size() == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.set.value.single", targets.iterator().next().getName(), value), true);
            else context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.set.value.multiple", targets.size(), value), true);
        } else if (type == 1) {
            if (targets.size() == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.set.limit.single", targets.iterator().next().getName(), value), true);
            else context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.set.limit.multiple", targets.size(), value), true);
        } else {
            if (targets.size() == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.set.level.single", targets.iterator().next().getName(), (int) value), true);
            else context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.set.level.multiple", targets.size(), (int) value), true);
        }
        return targets.size();
    }

    private static int setType(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, byte type) {
        IMana mana;
        for (ServerPlayerEntity player : targets) {
            mana = IMana.getFromPlayer(player);
            if (type == 0) {
                if (mana.setType(Type.NATURE)) PacketManager.sendManaTo(player, mana);
            } else if (type == 1) {
                if (mana.setType(Type.FIRE)) PacketManager.sendManaTo(player, mana);
            } else {
                if (mana.setType(Type.ICE)) PacketManager.sendManaTo(player, mana);
            }
        }
        String key;
        if (type == 0) {
            key = ".nature";
        } else if (type == 1) {
            key = ".fire";
        } else key = ".ice";
        if (targets.size() == 1)
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.set.type.single".concat(key), targets.iterator().next().getName()), true);
        else context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.set.type.multiple".concat(key), targets.size()), true);
        return targets.size();
    }

    private static int add(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, float value, boolean isValue) {
        IMana mana;
        float result = -1;
        for (ServerPlayerEntity player : targets) {
            mana = IMana.getFromPlayer(player);
            if (isValue) {
                if (mana.modifyValue(value)) PacketManager.sendManaTo(player, mana);
            } else {
                if (mana.modifyLimit(value)) PacketManager.sendManaTo(player, mana);
            }
            if (targets.size() == 1 && result == -1) result = mana.getValue();
        }
        if (isValue) {
            if (targets.size() == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.add.value.single", value, targets.iterator().next().getName(), result), true);
            else context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.add.value.multiple", value, targets.size()), true);
        } else {
            if (targets.size() == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.add.limit.single", value, targets.iterator().next().getName(), result), true);
            else context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.add.limit.multiple", value, targets.size()), true);
        }
        return targets.size();
    }

    private static int remove(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, float value, boolean isValue) {
        IMana mana;
        float result = -1;
        for (ServerPlayerEntity player : targets) {
            mana = IMana.getFromPlayer(player);
            if (isValue) {
                if (mana.modifyValue(-value)) PacketManager.sendManaTo(player, mana);
            } else {
                if (mana.modifyLimit(-value)) PacketManager.sendManaTo(player, mana);
            }
            if (targets.size() == 1 && result == -1) {
                if (isValue) result = mana.getValue();
                else result = mana.getLimit();
            }
        }
        if (isValue) {
            if (targets.size() == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.remove.value.single", value, targets.iterator().next().getName(), result), true);
            else context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.remove.value.multiple", value, targets.size()), true);
        } else {
            if (targets.size() == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.remove.limit.single", value, targets.iterator().next().getName(), result), true);
            else context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.mana.remove.limit.multiple", value, targets.size()), true);
        }
        return targets.size();
    }
}