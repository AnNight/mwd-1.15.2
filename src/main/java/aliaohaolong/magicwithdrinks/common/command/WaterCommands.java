package aliaohaolong.magicwithdrinks.common.command;

import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.Collection;

public class WaterCommands {
    public static ArgumentBuilder<CommandSource, ?> register() {
        return Commands.literal("water")
                .executes(WaterCommands::query)
                .requires(commandSource -> commandSource.hasPermissionLevel(0))
                .then(Commands.argument("target", EntityArgument.player())
                        .executes((d) -> WaterCommands.query(d, EntityArgument.getPlayer(d, "target")))
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                )
                .then(Commands.literal("set")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.argument("targets", EntityArgument.players())
                                .then(Commands.argument("int", IntegerArgumentType.integer(0, IWater.MAX))
                                        .executes((d) -> WaterCommands.set(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int")))
                                )
                        )
                )
                .then(Commands.literal("add")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.argument("targets", EntityArgument.players())
                                .then(Commands.argument("int", IntegerArgumentType.integer(0, IWater.MAX))
                                        .executes((d) -> WaterCommands.modified(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), true))
                                )
                        )
                )
                .then(Commands.literal("remove")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.argument("targets", EntityArgument.players())
                                .then(Commands.argument("int", IntegerArgumentType.integer(0, IWater.MAX))
                                        .executes((d) -> WaterCommands.modified(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), false))
                                )
                        )
                );
    }

    private static int query(CommandContext<CommandSource> context) throws CommandSyntaxException {
        return query(context, context.getSource().asPlayer());
    }

    private static int query(CommandContext<CommandSource> context, ServerPlayerEntity player) {
        IWater water = IWater.getFromPlayer(player);
        context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.water", player.getName(), water.getValue()), false);
        return 1;
    }

    private static int set(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, int value) {
        IWater water;
        int count = 0;
        for (ServerPlayerEntity player : targets) {
            water = IWater.getFromPlayer(player);
            if (water.setValue(value)) PacketManager.sendWaterTo(player, water);
            count++;
        }
        if (count == 1)
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.water.set.single", targets.iterator().next().getName(), value), true);
        else if (count > 1)
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.water.set.multiple", count, value), true);
        return count;
    }

    private static int modified(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, int value, boolean isAdd) {
        IWater water;
        int count = 0;
        int result = 0;
        for (ServerPlayerEntity player : targets) {
            water = IWater.getFromPlayer(player);
            if (isAdd ? water.increase(value) : water.reduce(value)) PacketManager.sendWaterTo(player, water);
            if (count == 0) result = water.getValue();
            count++;
        }
        if (isAdd) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.water.add.single", value, targets.iterator().next().getName(), result), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.water.add.multiple", value, count), true);
        } else {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.water.remove.single", value, targets.iterator().next().getName(), result), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.water.remove.multiple", value, count), true);
        }
        return count;
    }
}