package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDPotions;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import aliaohaolong.magicwithdrinks.common.inventory.container.FuelSlot;
import aliaohaolong.magicwithdrinks.common.inventory.container.ResultSlot;
import aliaohaolong.magicwithdrinks.common.item.MEExtractorCardItem;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;

@SuppressWarnings("NullableProblems")
public class SmeltingBoxContainer extends AbstractContainer {
    public SmeltingBoxContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(11), new IntArray(4));
    }

    public SmeltingBoxContainer(int windowId, PlayerInventory playerInventory, IInventory inventoryIn, IIntArray dataIn) {
        super(MWDTypes.C_SMELTING_BOX, windowId);
        assertInventorySize(inventoryIn, 11);
        assertIntArraySize(dataIn, 4);
        inventory = inventoryIn;
        data = dataIn;
        // Custom inventory
        addSlot(new Slot(inventory, 0, 18, 24) {
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() == MWDItems.POTION;
            }

            @Override
            public int getSlotStackLimit() {
                return 0;
            }
        });
        addSlot(new Slot(inventory, 1, 42, 42));
        addSlot(new Slot(inventory, 2, 42, 60));
        addSlot(new Slot(inventory, 3, 63, 42));
        addSlot(new Slot(inventory, 4, 63, 60));
        addSlot(new Slot(inventory, 5, 84, 42));
        addSlot(new Slot(inventory, 6, 84, 60));
        addSlot(new Slot(inventory, 7, 105, 42));
        addSlot(new Slot(inventory, 8, 105, 60));
        addSlot(new ResultSlot(inventory, 9, 142, 24));
        addSlot(new FuelSlot(inventory, 10, 142, 60));
        this.trackIntArray(dataIn);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack originalStack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack stack = slot.getStack();
            originalStack = stack.copy();
            if (index < 4) {
                if (!this.mergeItemStack(stack, 4, 40, false)) {
                    return ItemStack.EMPTY;
                }
            } else {
                Extra extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
                if (extra != null && extra.canStore()) {
                    if (!this.mergeItemStack(stack, 1, 2, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                if (MWDPotionUtils.getPotionFromStack(stack) == MWDPotions.CATALYST) {
                    if (!this.mergeItemStack(stack, 2, 3, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                if (stack.getItem() instanceof MEExtractorCardItem) {
                    if (!this.mergeItemStack(stack, 3, 4, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                if (!this.mergeItemStack(stack, 0, 1, false)) {
                    return ItemStack.EMPTY;
                }
            }
            if (index >= 4 && index < 31) {
                if (!this.mergeItemStack(stack, 31, 40, false)) {
                    return ItemStack.EMPTY;
                }
            }
            if (index >= 31 && index < 40 && !this.mergeItemStack(stack, 4, 31, false)) {
                return ItemStack.EMPTY;
            }
            if (stack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (stack.getCount() == originalStack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, stack);
        }
        return originalStack;
    }
}