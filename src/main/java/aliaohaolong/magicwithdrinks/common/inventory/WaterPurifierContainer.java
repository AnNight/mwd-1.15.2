package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.inventory.container.FilterSliceSlot;
import aliaohaolong.magicwithdrinks.common.inventory.container.FuelSlot;
import aliaohaolong.magicwithdrinks.common.inventory.container.ResultSlot;
import aliaohaolong.magicwithdrinks.common.item.FilterSliceItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@SuppressWarnings({"NullableProblems", "FieldMayBeFinal"})
public class WaterPurifierContainer extends AbstractContainer {
    public WaterPurifierContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(5), new IntArray(5));
    }

    public WaterPurifierContainer(int windowId, PlayerInventory playerInventory, IInventory inventory, IIntArray data) {
        super(MWDTypes.C_WATER_PURIFIER, windowId);
        assertInventorySize(inventory, 5);
        assertIntArraySize(data, 5);
        this.inventory = inventory;
        this.data = data;
        // Custom inventory
        this.addSlot(new Slot(this.inventory, 0, 28, 17) {
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() == MWDItems.POTION || stack.getItem() == Items.POTION;
            }
        });
        this.addSlot(new FuelSlot(this.inventory, 1, 28, 53));
        this.addSlot(new FilterSliceSlot(this.inventory, 2, 64, 35));
        this.addSlot(new ResultSlot(this.inventory, 3, 100, 35));
        this.addSlot(new ResultSlot(this.inventory, 4, 132, 35));
        this.trackIntArray(data);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack clickStack = slot.getStack();
            stack = clickStack.copy();
            if (index < 5) {
                if (!this.mergeItemStack(clickStack, 5, 41, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(clickStack, stack);
            } else {
                if (clickStack.getItem() == MWDItems.POTION || clickStack.getItem() == Items.POTION) {
                    if (!this.mergeItemStack(clickStack, 0, 1, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (net.minecraftforge.common.ForgeHooks.getBurnTime(clickStack) > 0) {
                    if (!this.mergeItemStack(clickStack, 1, 2, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (clickStack.getItem() instanceof FilterSliceItem) {
                    if (!this.mergeItemStack(clickStack, 2, 3, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index < 32) {
                    if (!this.mergeItemStack(clickStack, 32, 41, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index < 41) {
                    if (!this.mergeItemStack(clickStack, 5, 32, false)) {
                        return ItemStack.EMPTY;
                    }
                }
            }
            if (clickStack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (clickStack.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, clickStack);
        }
        return stack;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCookingScaled() {
        return data.get(0) * 22 / data.get(4);
    }

    @OnlyIn(Dist.CLIENT)
    public int getBurnRemainingScaled() {
        return data.get(1) * 13 / data.get(2);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isCooking() {
        return data.get(0) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isBurning() {
        return data.get(1) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public int getImpurity() {
        return data.get(3);
    }
}