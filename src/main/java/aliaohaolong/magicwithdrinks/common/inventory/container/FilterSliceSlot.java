package aliaohaolong.magicwithdrinks.common.inventory.container;

import aliaohaolong.magicwithdrinks.common.item.FilterSliceItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

public class FilterSliceSlot extends Slot {
    public FilterSliceSlot(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return stack.getItem() instanceof FilterSliceItem;
    }
}