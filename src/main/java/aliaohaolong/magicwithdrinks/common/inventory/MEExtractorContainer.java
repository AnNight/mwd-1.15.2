package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.api.MWDPotions;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import aliaohaolong.magicwithdrinks.common.item.MEExtractorCardItem;
import aliaohaolong.magicwithdrinks.common.potion.MWDPotionUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@SuppressWarnings("NullableProblems")
public class MEExtractorContainer extends AbstractContainer {
    public MEExtractorContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(4), new IntArray(4));
    }

    public MEExtractorContainer(int windowId, PlayerInventory playerInventory, IInventory inventoryIn, IIntArray dataIn) {
        super(MWDTypes.C_ME_EXTRACTOR, windowId);
        assertInventorySize(inventoryIn, 4);
        assertIntArraySize(dataIn, 4);
        inventory = inventoryIn;
        data = dataIn;
        // Custom inventory
        addSlot(new Slot(inventory, 0, 86, 28));
        addSlot(new Slot(inventory, 1, 134, 62) {
            @Override
            public boolean isItemValid(ItemStack stack) {
                Extra extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
                return extra != null && extra.canStore();
            }

            @Override
            public int getSlotStackLimit() {
                return 1;
            }
        });
        addSlot(new Slot(inventory, 2, 26, 17) {
            @Override
            public boolean isItemValid(ItemStack stack) {
                return MWDPotionUtils.getPotionFromStack(stack) == MWDPotions.CATALYST;
            }
        });
        addSlot(new Slot(inventory, 3, 152, 8) {
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() instanceof MEExtractorCardItem;
            }
        });
        trackIntArray(dataIn);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack originalStack = ItemStack.EMPTY;
        Slot slot = inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack stack = slot.getStack();
            originalStack = stack.copy();
            if (index < 4) {
                if (!mergeItemStack(stack, 4, 40, false)) {
                    return ItemStack.EMPTY;
                }
            } else {
                Extra extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
                if (extra != null && extra.canStore()) {
                    if (!mergeItemStack(stack, 1, 2, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                if (MWDPotionUtils.getPotionFromStack(stack) == MWDPotions.CATALYST) {
                    if (!mergeItemStack(stack, 2, 3, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                if (stack.getItem() instanceof MEExtractorCardItem) {
                    if (!mergeItemStack(stack, 3, 4, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                if (!mergeItemStack(stack, 0, 1, false)) {
                    return ItemStack.EMPTY;
                }
            }
            if (index >= 4 && index < 31) {
                if (!mergeItemStack(stack, 31, 40, false)) {
                    return ItemStack.EMPTY;
                }
            }
            if (index >= 31 && index < 40 && !mergeItemStack(stack, 4, 31, false)) {
                return ItemStack.EMPTY;
            }
            if (stack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (stack.getCount() == originalStack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, stack);
        }
        return originalStack;
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isCooking() {
        return data.get(0) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCookScaled() {
        return data.get(0) * 27 / data.get(1);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean hasCatalyst() {
        return data.get(3) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCatalystScaled() {
        return (int) Math.ceil((double) data.get(3) / 2);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean hasBuffer() {
        return data.get(2) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public int getBufferScaled() {
        return data.get(2) / 1000;
    }

    @OnlyIn(Dist.CLIENT)
    public String getBufferS() {
        return String.valueOf(((float) data.get(2)) / 1000.0F);
    }

    @OnlyIn(Dist.CLIENT)
    public String getCatalystS() {
        return String.valueOf(data.get(3));
    }
}