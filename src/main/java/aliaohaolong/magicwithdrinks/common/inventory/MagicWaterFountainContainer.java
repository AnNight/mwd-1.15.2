package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.MyMath;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.common.inventory.container.FilterSliceSlot;
import aliaohaolong.magicwithdrinks.common.inventory.container.ResultSlot;
import aliaohaolong.magicwithdrinks.common.item.FilterSliceItem;
import aliaohaolong.magicwithdrinks.common.extra.Extra;
import aliaohaolong.magicwithdrinks.common.tile.AbstractWaterFountainTile;
import aliaohaolong.magicwithdrinks.common.tile.MagicWaterFountainTile;
import aliaohaolong.magicwithdrinks.common.tile.WaterFountainTile;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@SuppressWarnings("NullableProblems")
public class MagicWaterFountainContainer extends AbstractContainer {
    public MagicWaterFountainContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(5), new IntArray(5));
    }

    public MagicWaterFountainContainer(int windowId, PlayerInventory playerInventory, IInventory inventory, IIntArray data) {
        super(MWDTypes.C_MAGIC_WATER_FOUNTAIN, windowId);
        assertInventorySize(inventory, 5);
        assertIntArraySize(data, 5);
        this.inventory = inventory;
        this.data = data;
        // Custom inventory
        addSlot(new Slot(this.inventory, 0, 22, 61) {
            @SuppressWarnings("NullableProblems")
            @Override
            public boolean isItemValid(ItemStack stack) {
                return WaterFountainTile.getWaterContent(stack) > 0;
            }
        });
        addSlot(new ResultSlot(this.inventory, 1, 44, 61));
        addSlot(new FilterSliceSlot(this.inventory, 2, 74, 61));
        addSlot(new ResultSlot(this.inventory, 3, 104, 61));
        addSlot(new Slot(this.inventory, 4, 138, 46) {
            @SuppressWarnings("NullableProblems")
            @Override
            public boolean isItemValid(ItemStack stack) {
                Extra extra = MWDForgeRegistries.getExes().getValue(stack.getItem().getRegistryName());
                return extra != null && extra.canStore();
            }
        });
        this.trackIntArray(data);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity player, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack clickStack = slot.getStack();
            stack = clickStack.copy();
            if (index < 5) {
                if (!this.mergeItemStack(clickStack, 5, 41, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(clickStack, stack);
            } else {
                Extra extra = MWDForgeRegistries.getExes().getValue(clickStack.getItem().getRegistryName());
                if (WaterFountainTile.getWaterContent(stack) > 0) {
                    if (!this.mergeItemStack(clickStack, 0, 1, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (clickStack.getItem() instanceof FilterSliceItem) {
                    if (!this.mergeItemStack(clickStack, 2, 3, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (extra != null && extra.canStore()) {
                    if (!this.mergeItemStack(clickStack, 4, 5, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index < 32) {
                    if (!this.mergeItemStack(clickStack, 32, 41, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index < 41) {
                    if (!this.mergeItemStack(clickStack, 5, 32, false)) {
                        return ItemStack.EMPTY;
                    }
                }
            }
            if (clickStack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (clickStack.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(player, clickStack);
        }
        return stack;
    }

    @OnlyIn(Dist.CLIENT)
    public int getWaterScaled() {
        return data.get(0) * 98 / AbstractWaterFountainTile.MAX_CAPACITY;
    }

    @OnlyIn(Dist.CLIENT)
    public int getPurifiedWaterScaled() {
        return data.get(1) * 98 / AbstractWaterFountainTile.MAX_CAPACITY;
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isBurning() {
        return data.get(3) != -1;
    }

    @OnlyIn(Dist.CLIENT)
    public int getImpurityScaled() {
        return data.get(2);
    }

    @OnlyIn(Dist.CLIENT)
    public int getBufferScaled() {
        return MyMath.getScaled(data.get(4), MagicWaterFountainTile.MAX_BUFFER, 16);
    }

    @OnlyIn(Dist.CLIENT)
    public String getWaterS() {
        return String.valueOf(data.get(0));
    }

    @OnlyIn(Dist.CLIENT)
    public String getPurifiedWaterS() {
        return String.valueOf(data.get(1));
    }

    @OnlyIn(Dist.CLIENT)
    public String getBufferS() {
        return String.valueOf(MyMath.toFloat(data.get(4)));
    }
}