package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDTypes;
import aliaohaolong.magicwithdrinks.common.inventory.container.FuelSlot;
import aliaohaolong.magicwithdrinks.common.inventory.container.ResultSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@SuppressWarnings("NullableProblems")
public class BlenderContainer extends AbstractContainer {
    private static final int OFFSET = 18;

    public BlenderContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(4), new IntArray(4));
    }

    public BlenderContainer(int windowId, PlayerInventory playerInventory, IInventory inventory, IIntArray data) {
        super(MWDTypes.C_BLENDER, windowId);
        assertInventorySize(inventory, 4);
        assertIntArraySize(data, 4);
        this.inventory = inventory;
        this.data = data;
        // Custom inventory
        this.addSlot(new Slot(this.inventory, 0, 34, 17){
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() == MWDItems.POTION || stack.getItem() == Items.POTION;
            }

            @Override
            public int getSlotStackLimit() {
                return 1;
            }
        });
        this.addSlot(new Slot(this.inventory, 1, 56, 17));
        this.addSlot(new FuelSlot(this.inventory, 2, 56, 53));
        this.addSlot(new ResultSlot(this.inventory, 3, 116, 35));
        this.trackIntArray(data);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack clickStack = slot.getStack();
            stack = clickStack.copy();
            if (index < 4) {
                if (!this.mergeItemStack(clickStack, 4, 40, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(clickStack, stack);
            } else {
                if (net.minecraftforge.common.ForgeHooks.getBurnTime(clickStack) > 0) {
                    if (!this.mergeItemStack(clickStack, 2, 3, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (clickStack.getItem() == MWDItems.POTION || clickStack.getItem() == Items.POTION) {
                    if (!this.mergeItemStack(clickStack, 0, 1, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (!this.mergeItemStack(clickStack, 1, 2, false)) {
                    if (index < 31) {
                        if (!this.mergeItemStack(clickStack, 31, 40, false)) {
                            return ItemStack.EMPTY;
                        }
                    } else if (index < 40) {
                        if (!this.mergeItemStack(clickStack, 4, 31, false)) {
                            return ItemStack.EMPTY;
                        }
                    }
                }
            }
            if (clickStack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (clickStack.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, clickStack);
        }
        return stack;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCookProgressionScaled() {
        return data.get(2) * 22 / data.get(3);
    }

    @OnlyIn(Dist.CLIENT)
    public int getBurnLeftScaled() {
        return data.get(0) * 13 / data.get(1);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isCooking() {
        return data.get(2) > 0 && data.get(3) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isBurning() {
        return data.get(0) > 0 && data.get(1) > 0;
    }
}