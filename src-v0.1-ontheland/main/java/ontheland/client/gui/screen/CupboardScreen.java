package ontheland.client.gui.screen;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import ontheland.common.inventory.container.CupboardContainer;
import ontheland.OnTheLand;

public class CupboardScreen extends ContainerScreen<CupboardContainer> {
    private final ResourceLocation GUI = new ResourceLocation(OnTheLand.MOD_ID, "textures/gui/cupboard.png");

    public CupboardScreen(CupboardContainer container, PlayerInventory inventory, ITextComponent component) {
        super(container, inventory, component);
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        this.renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.font.drawString(this.title.getFormattedText(), 8.0F, 6.0F, 4210752);
        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, (float)(this.ySize - 114 + 3), 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.minecraft.getTextureManager().bindTexture(GUI);
        int relX = (this.width - this.xSize) / 2;
        int relY = (this.height - this.ySize) / 2;
        this.blit(relX, relY, 0, 0, this.xSize, this.ySize);
    }
}
