package ontheland.config;

import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.AtSurfaceWithExtraConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.FrequencyConfig;
import ontheland.api.block.OTLBlocks;

public class Config {
    public static final OreFeatureConfig AMBER_FEATURE = new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, OTLBlocks.amber_ore.getDefaultState(), 6); // 琥珀矿石
    public static final CountRangeConfig AMBER_RANGE = new CountRangeConfig(6,0,0,56);
    public static final FrequencyConfig PINUS_FLOWER_CONFIG = new FrequencyConfig(2);
    public static final AtSurfaceWithExtraConfig TREE_CONFIG = new AtSurfaceWithExtraConfig(8,2,4); // 三种生物群系的树
}
