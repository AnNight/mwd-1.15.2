package ontheland.common.inventory.container;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;

public class BarrelContainer extends Container {
    public BarrelContainer(ContainerType<?> type, int id) {
        super(type, id);
    }

    IInventory iInventory;
    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return this.iInventory.isUsableByPlayer(playerIn);
    }
}
