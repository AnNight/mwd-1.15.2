package ontheland.common.inventory.container;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

public class FoodSlot extends Slot {
    public FoodSlot(IInventory iInventory, int index, int posX, int posY) {
        super(iInventory, index, posX, posY);
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return stack.getItem().isFood();
    }
}