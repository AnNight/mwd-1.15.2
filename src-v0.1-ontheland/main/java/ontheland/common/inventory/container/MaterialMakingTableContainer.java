package ontheland.common.inventory.container;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import ontheland.api.block.OTLBlocks;
import ontheland.api.containertype.OTLContainerType;
import ontheland.common.tileentity.MaterialMakingTableTileEntity;

public class MaterialMakingTableContainer extends Container {
    private TileEntity tileEntity;
    private static final int OFFSET = 18;
    private static final int SLOT_COUNT = 3;

    protected int addDefaultSlot(PlayerInventory playerInventory, int index, int posX, int posY, int amount, int dx) {
        for (int i = 0 ; i < amount ; i++) {
            addSlot(new Slot(playerInventory, index, posX, posY));
            posX += dx;
            index++;
        }
        return index;
    }

    public static MaterialMakingTableContainer create(int id, PlayerInventory playerInventory) {
        return new MaterialMakingTableContainer(OTLContainerType.materialMakingTableContainerType, id, playerInventory, new MaterialMakingTableTileEntity());
    }

    public MaterialMakingTableContainer(ContainerType<?> type, int id, PlayerInventory playerInventory, TileEntity tileEntity) {
        super(type, id);
        this.tileEntity = tileEntity;
        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {
            addSlot(new SlotItemHandler(h, 0, 38, 32));
            addSlot(new SlotItemHandler(h, 1, 66, 32));
            addSlot(new SlotItemHandler(h, 2, 122, 32));
        });
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58, 9, OFFSET);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY, 9, OFFSET);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET, 9, OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY += OFFSET, 9, OFFSET);
    }

    /** Determines whether supplied player can use this container */
    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return isWithinUsableDistance(IWorldPosCallable.of(tileEntity.getWorld(), tileEntity.getPos()), playerIn, OTLBlocks.material_making_table);
    }

    /** Handle when the stack in slot is shift-clicked. Normally this moves the stack between the player */
    public ItemStack transferStackInSlot(PlayerEntity player, int index) {
        ItemStack itemStack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack itemStack1 = slot.getStack();
            itemStack = itemStack1.copy();
            if (index < 3) {
                if (!this.mergeItemStack(itemStack1, SLOT_COUNT, this.inventorySlots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.mergeItemStack(itemStack1, 0, SLOT_COUNT, false)) {
                return ItemStack.EMPTY;
            }

            if (itemStack1.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
        }
        return itemStack;
    }

    /** Called when the container is closed. */
    public void onContainerClosed(PlayerEntity playerIn) {
        super.onContainerClosed(playerIn);
        ((IInventory) tileEntity).closeInventory(playerIn);
    }
}
/* this.tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {
            // glass fiber making machine
            addSlot(new SlotItemHandler(h, 40, 27, 18));
            addSlot(new SlotItemHandler(h, 41, 45, 18));
            addSlot(new SlotItemHandler(h, 42, 63, 18));
            addSlot(new SlotItemHandler(h, 43, 27, 36));
            addSlot(new SlotItemHandler(h, 44, 45, 36));
            addSlot(new SlotItemHandler(h, 45, 63, 36));
            addSlot(new SlotItemHandler(h, 46, 34, 60));
            addSlot(new SlotItemHandler(h, 47, 125, 32));
        }); */