package ontheland.common.inventory.container;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import ontheland.api.containertype.OTLContainerType;

public class CupboardContainer extends Container {
    private final IInventory iInventory;
    private static final int OFFSET = 18;
    private static final int SLOT_COUNT = 18;

    /** Add default slot horizontally */
    protected int addDefaultSlot(PlayerInventory playerInventory, int index, int posX, int posY) {
        for (int i = 0; i < 9; i++) {
            addSlot(new Slot(playerInventory, index, posX, posY));
            posX += OFFSET;
            index++;
        }
        return index;
    }

    /** Add custom slot horizontally */
    protected int addCustomSlot(IInventory iInventory, int index, int posX, int posY, int amount) {
        for (int i = 0; i < amount; i++) {
            addSlot(new FoodSlot(iInventory, index, posX, posY));
            posX += OFFSET;
            index++;
        }
        return index;
    }

    public static CupboardContainer create(int id, PlayerInventory playerInventory) {
        return new CupboardContainer(OTLContainerType.cupboardContainerType, id ,playerInventory);
    }

    private CupboardContainer(ContainerType<?> type, int id, PlayerInventory playerInventory) {
        this(type, id, playerInventory, new Inventory(18));
    }

    public CupboardContainer(ContainerType<?> type, int id, PlayerInventory playerInventory, IInventory iInventory) {
        super(type, id);
        assertInventorySize(iInventory, SLOT_COUNT);
        this.iInventory = iInventory;
        iInventory.openInventory(playerInventory.player);
        // Custom inventory
        int posX = 8, posY = 18;
        int index = addCustomSlot(iInventory, 0, posX, posY, 9);
        addCustomSlot(iInventory, index, posX, posY + OFFSET, 9);
        posY = 66;
        // Default inventory - Hot bar
        index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    /** Determines whether supplied player can use this container */
    public boolean canInteractWith(PlayerEntity playerIn) { return this.iInventory.isUsableByPlayer(playerIn); }

    /** Handle when the stack in slot is shift-clicked. Normally this moves the stack between the player */
    public ItemStack transferStackInSlot(PlayerEntity player, int index) {
        ItemStack itemStack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack itemStack1 = slot.getStack();
            itemStack = itemStack1.copy();
            if (index < 3) {
                if (!this.mergeItemStack(itemStack1, SLOT_COUNT, this.inventorySlots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.mergeItemStack(itemStack1, 0, SLOT_COUNT, false)) {
                return ItemStack.EMPTY;
            }

            if (itemStack1.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
        }
        return itemStack;
    }

    /** Called when the container is closed. */
    public void onContainerClosed(PlayerEntity playerIn) {
        super.onContainerClosed(playerIn);
        this.iInventory.closeInventory(playerIn);
    }
}
