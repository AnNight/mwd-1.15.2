package ontheland.common.tileentity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import ontheland.common.inventory.container.MaterialMakingTableContainer;
import ontheland.api.tileentitytype.OTLTileEntityType;
import ontheland.api.containertype.OTLContainerType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class MaterialMakingTableTileEntity extends TileEntity implements IInventory, INamedContainerProvider {
    private static final int SLOT_COUNT = 3;
    protected NonNullList<ItemStack> list = NonNullList.withSize(SLOT_COUNT,ItemStack.EMPTY);
    private LazyOptional<IItemHandler> handler = LazyOptional.of(this::createHandler);

    public MaterialMakingTableTileEntity() {
        super(OTLTileEntityType.materialMakingTableTileEntityType);
    }

    @Override
    public ITextComponent getDisplayName() { return new TranslationTextComponent("container.ontheland.material_making_table"); }

    @Nullable
    @Override
    public Container createMenu(int id, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new MaterialMakingTableContainer(OTLContainerType.materialMakingTableContainerType, id, playerInventory, this);
    }

    @Override
    public void read(CompoundNBT compound) {
        handler.ifPresent(h -> {
            CompoundNBT compoundNBT = compound.getCompound("item");
            ((INBTSerializable<CompoundNBT>)h).deserializeNBT(compoundNBT);
        });
        super.read(compound);
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        handler.ifPresent(h -> {
            CompoundNBT compoundNBT = ((INBTSerializable<CompoundNBT>)h).serializeNBT();
            compound.put("item", compoundNBT);
        });
        return super.write(compound);
    }

    private IItemHandler createHandler() {
        ItemStackHandler handler = new ItemStackHandler(){
            @Override
            public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                return stack.getItem().isFood();
            }
            @Nonnull
            @Override
            public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                if (!isItemValid(slot, stack)) { return stack; }
                return super.insertItem(slot, stack, simulate);
            }
        };
        handler.setSize(3);
        return handler;
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return handler.cast();
        }
        return super.getCapability(cap, side);
    }

    @Override
    public int getSizeInventory() { return SLOT_COUNT; }

    @Override
    public boolean isEmpty() {
        for(ItemStack itemstack : this.list) {
            if (!itemstack.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    // Returns the stack in the given slot.
    @Override
    public ItemStack getStackInSlot(int index) { return this.list.get(index); }

    // Removes up to a specified number of items from an inventory slot and returns them in a new stack.
    @Override
    public ItemStack decrStackSize(int index, int count) { return ItemStackHelper.getAndSplit(this.list, index, count); }

    // Removes a stack from the given slot and returns it.
    @Override
    public ItemStack removeStackFromSlot(int index) { return ItemStackHelper.getAndRemove(this.list, index); }

    // Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        this.list.set(index, stack);
        if (stack.getCount() > this.getInventoryStackLimit()) {
            stack.setCount(this.getInventoryStackLimit());
        }
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        if (this.world.getTileEntity(this.pos) != this) {
            return false;
        } else {
            return !(player.getDistanceSq((double)this.pos.getX() + 0.5D, (double)this.pos.getY() + 0.5D, (double)this.pos.getZ() + 0.5D) > 64.0D);
        }
    }

    @Override
    public void clear() { this.list.clear(); }
}