package ontheland.common.tileentity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import ontheland.api.containertype.OTLContainerType;
import ontheland.api.tileentitytype.OTLTileEntityType;
import ontheland.common.inventory.container.CupboardContainer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CupboardTileEntity extends TileEntity implements IInventory, INamedContainerProvider {
    private static final int SLOT_COUNT = 18;
    private NonNullList<ItemStack> list = NonNullList.withSize(SLOT_COUNT, ItemStack.EMPTY);

    public CupboardTileEntity() {
        super(OTLTileEntityType.cupboardTileEntityType);
    }

    public ITextComponent getDisplayName() { return new TranslationTextComponent("container.ontheland.cupboard"); }

    public Container createMenu(int id, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new CupboardContainer(OTLContainerType.cupboardContainerType, id, playerInventory, this);
    }

    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return stack.isFood();
    }

    public void read(CompoundNBT compound) {
        super.read(compound);
        this.list = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
        ItemStackHelper.loadAllItems(compound, this.list);
    }

    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        ItemStackHelper.saveAllItems(compound, this.list);
        return compound;
    }

    private LazyOptional<IItemHandler> handler = LazyOptional.of(this::createUnSidedHandler);
    protected net.minecraftforge.items.IItemHandler createUnSidedHandler() {
        return new net.minecraftforge.items.wrapper.InvWrapper(this);
    }

    public int getSizeInventory() {
        return SLOT_COUNT;
    }

    public boolean isEmpty() {
        for(ItemStack itemstack : this.list) {
            if (!itemstack.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public ItemStack getStackInSlot(int index) {
        return this.list.get(index);
    }

    public ItemStack decrStackSize(int index, int count) { return ItemStackHelper.getAndSplit(this.list, index, count); }

    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.list, index);
    }

    public void setInventorySlotContents(int index, ItemStack stack) {
        this.list.set(index, stack);
        if (stack.getCount() > this.getInventoryStackLimit()) {
            stack.setCount(this.getInventoryStackLimit());
        }
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        if (this.world.getTileEntity(this.pos) != this) {
            return false;
        } else {
            return !(player.getDistanceSq((double)this.pos.getX() + 0.5D, (double)this.pos.getY() + 0.5D, (double)this.pos.getZ() + 0.5D) > 64.0D);
        }
    }

    public void clear() {
        this.list.clear();
    }

    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return handler.cast();
        }
        return super.getCapability(cap, side);
    }
}