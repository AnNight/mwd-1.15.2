package ontheland.common.item;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.RotatedPillarBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import ontheland.api.block.OTLBlocks;
import ontheland.api.tooltype.OTLToolType;

import java.util.Map;
import java.util.Set;

public class CuttingDeviceItem extends ToolItem {
    private static final Set<Block> EFFECTIVE_ON = Sets.newHashSet();
    protected static final Map<Block, Block> BLOCK_GATHER_MAP = (new ImmutableMap.Builder<Block, Block>())
            /*.put(OTLBlocks.pinus_caribaea_log, OTLBlocks.pinus_caribaea_log_1)
            .put(OTLBlocks.pinus_caribaea_log_1, OTLBlocks.pinus_caribaea_log_2)
            .put(OTLBlocks.pinus_caribaea_log_2, OTLBlocks.pinus_caribaea_log_3)
            .put(OTLBlocks.pinus_caribaea_log_3, OTLBlocks.pinus_caribaea_log_4)
            .put(OTLBlocks.pinus_palustris_log, OTLBlocks.pinus_palustris_log_1)
            .put(OTLBlocks.pinus_palustris_log_1, OTLBlocks.pinus_palustris_log_2)
            .put(OTLBlocks.pinus_palustris_log_2, OTLBlocks.pinus_palustris_log_3)
            .put(OTLBlocks.pinus_palustris_log_3, OTLBlocks.pinus_palustris_log_4)
            .put(OTLBlocks.pinus_taeda_log, OTLBlocks.pinus_taeda_log_1)
            .put(OTLBlocks.pinus_taeda_log_1, OTLBlocks.pinus_taeda_log_2)
            .put(OTLBlocks.pinus_taeda_log_2, OTLBlocks.pinus_taeda_log_3)
            .put(OTLBlocks.pinus_taeda_log_3, OTLBlocks.pinus_taeda_log_4)*/.build();

    public CuttingDeviceItem(IItemTier tier, float attackDamageIn, float attackSpeedIn, Item.Properties builder) {
        super(attackDamageIn, attackSpeedIn, tier, EFFECTIVE_ON, builder.addToolType(OTLToolType.CUTTING_DEVICE, tier.getHarvestLevel()));
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        World world = context.getWorld();
        BlockPos blockpos = context.getPos();
        BlockState blockstate = world.getBlockState(blockpos);
        Block block = BLOCK_GATHER_MAP.get(blockstate.getBlock());
        if (block != null) {
            PlayerEntity playerentity = context.getPlayer();
            world.playSound(playerentity, blockpos, SoundEvents.ITEM_AXE_STRIP, SoundCategory.BLOCKS, 1.0F, 1.0F);
            if (!world.isRemote) {
                world.setBlockState(blockpos, block.getDefaultState().with(RotatedPillarBlock.AXIS, blockstate.get(RotatedPillarBlock.AXIS)), 11);
                if (playerentity != null) {
                    context.getItem().damageItem(1, playerentity, (p_220040_1_) -> {
                        p_220040_1_.sendBreakAnimation(context.getHand());
                    });
                }
            }
            return ActionResultType.SUCCESS;
        } else {
            return ActionResultType.PASS;
        }
    }
}