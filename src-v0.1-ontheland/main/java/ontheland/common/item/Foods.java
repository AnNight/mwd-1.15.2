package ontheland.common.item;

public class Foods {
    public static final net.minecraft.item.Food PINE_NUT = (new net.minecraft.item.Food.Builder()).hunger(1).saturation(0.3F).build();
    public static final net.minecraft.item.Food FRUIT = (new net.minecraft.item.Food.Builder()).hunger(3).saturation(0.6F).build();
}
