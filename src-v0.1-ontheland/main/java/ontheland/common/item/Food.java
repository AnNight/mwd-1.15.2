package ontheland.common.item;

import net.minecraft.item.Item;
import ontheland.api.group.OTLGroup;

public class Food extends Item {
    public Food(net.minecraft.item.Food food) {
        super(new Item.Properties().food(food).group(OTLGroup.materials));
    }
}