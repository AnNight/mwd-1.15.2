package ontheland.common.item;

import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import ontheland.api.block.OTLBlocks;
import ontheland.api.material.OTLMaterial;
import ontheland.api.tooltype.OTLToolType;

import java.util.Set;

public class HammerItem extends ToolItem {
    private static final Set<Block> EFFECTIVE_ON = Sets.newHashSet(
            OTLBlocks.pyrophyllite_stone,
            OTLBlocks.calcium_carbonate_stone,
            OTLBlocks.dolomite_stone,
            OTLBlocks.colemanite_stone,
            OTLBlocks.ascharite_stone
    );

    public HammerItem(IItemTier tier, float attackDamageIn, float attackSpeedIn, Item.Properties builder) {
        super(attackDamageIn, attackSpeedIn, tier, EFFECTIVE_ON, builder.addToolType(OTLToolType.HAMMER, tier.getHarvestLevel()));
    }

    public float getDestroySpeed(ItemStack stack, BlockState state) {
        Material material = state.getMaterial();
        return material != OTLMaterial.MOD_STONE ? super.getDestroySpeed(stack, state) : this.efficiency;
    }

    @Override
    public boolean canHarvestBlock(BlockState blockIn) {
        return blockIn.getHarvestTool() == OTLToolType.HAMMER;
    }
}
