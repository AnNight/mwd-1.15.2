package ontheland.common.world.biome.magic;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;

public class VolcanicIslandBiome extends Biome {
    public VolcanicIslandBiome() {
        super((new Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, SurfaceBuilder.GRASS_DIRT_SAND_CONFIG)
                .precipitation(RainType.RAIN)
                .category(Category.PLAINS)
                .depth(0.1F)
                .scale(0.1F)
                .temperature(0.4F)
                .downfall(0.1F)
                .waterColor(25600)
                .waterFogColor(25600)
                .parent((String) null)
        );
    }
}
