package ontheland.common.world.biome.magic;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;

public class MagicOceanBiome extends Biome {
    public MagicOceanBiome() {
        super((new Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, SurfaceBuilder.GRAVEL_CONFIG)
                .precipitation(RainType.RAIN)
                .category(Category.OCEAN)
                .depth(-1.0F)
                .scale(0.1F)
                .temperature(0.5F)
                .downfall(0.6F)
                .waterColor(4159204)
                .waterFogColor(329011)
                .parent(null)
        );
    }
}
