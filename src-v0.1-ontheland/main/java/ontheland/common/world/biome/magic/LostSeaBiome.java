package ontheland.common.world.biome.magic;

import net.minecraft.block.Blocks;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;

public class LostSeaBiome extends Biome {
    private static final SurfaceBuilderConfig SAND_STONE_STONE = new SurfaceBuilderConfig(Blocks.SAND.getDefaultState(), Blocks.STONE.getDefaultState(), Blocks.STONE.getDefaultState());

    public LostSeaBiome() {
        super((new Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, SAND_STONE_STONE)
                .precipitation(RainType.RAIN)
                .category(Category.OCEAN)
                .depth(-0.8F)
                .scale(0.1F)
                .temperature(0.4F)
                .downfall(0.1F)
                .waterColor(25600)
                .waterFogColor(25600)
                .parent((String) null)
        );
    }
}