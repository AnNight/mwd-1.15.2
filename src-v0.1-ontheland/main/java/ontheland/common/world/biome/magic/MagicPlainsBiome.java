package ontheland.common.world.biome.magic;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;

public class MagicPlainsBiome extends Biome {
    public MagicPlainsBiome() {
        super((new Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, SurfaceBuilder.GRAVEL_CONFIG)
                .precipitation(RainType.RAIN)
                .category(Category.OCEAN)
                .depth(0.2F)
                .scale(0.1F)
                .temperature(0.5F)
                .downfall(0.1F)
                .waterColor(4159204)
                .waterFogColor(329011)
                .parent(null)
        );
    }
}
