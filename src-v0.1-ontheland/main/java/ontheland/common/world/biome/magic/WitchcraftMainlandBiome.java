package ontheland.common.world.biome.magic;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;

public class WitchcraftMainlandBiome extends Biome {
    public WitchcraftMainlandBiome() {
        super((new Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, SurfaceBuilder.GRASS_DIRT_SAND_CONFIG)
                .precipitation(RainType.RAIN)
                .category(Category.FOREST)
                .depth(0.2F)
                .scale(0.2F)
                .temperature(0.5F)
                .downfall(0.1F)
                .waterColor(25600)
                .waterFogColor(25600)
                .parent((String) null)
        );
    }
}
