package ontheland.common.world.biome.magic;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;

public class DarkMainlandBiome extends Biome {
    public DarkMainlandBiome() {
        super((new Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, SurfaceBuilder.GRASS_DIRT_GRAVEL_CONFIG)
                .precipitation(RainType.RAIN)
                .category(Category.FOREST)
                .depth(0.2F)
                .scale(0.2F)
                .temperature(0.8F)
                .downfall(0.1F)
                .waterColor(4159204)
                .waterFogColor(4159204)
                .parent((String) null)
        );
    }
}
