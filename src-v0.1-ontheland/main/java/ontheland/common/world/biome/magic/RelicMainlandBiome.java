package ontheland.common.world.biome.magic;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;

public class RelicMainlandBiome extends Biome {
    public RelicMainlandBiome() {
        super((new Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, SurfaceBuilder.GRASS_DIRT_GRAVEL_CONFIG)
                .precipitation(Biome.RainType.RAIN)
                .category(Biome.Category.PLAINS)
                .depth(0.2F)
                .scale(0.2F)
                .temperature(0.7F)
                .downfall(0.4F)
                .waterColor(4159204)
                .waterFogColor(4159204)
                .parent(null)
        );
    }
}