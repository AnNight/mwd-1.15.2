package ontheland.common.world.biome.magic;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;

public class WildMainlandBiome extends Biome {
    private static final SurfaceBuilderConfig WILD_MAINLAND_CONFIG = new SurfaceBuilderConfig(SurfaceBuilder.GRAVEL, SurfaceBuilder.STONE, SurfaceBuilder.SAND);

    public WildMainlandBiome() {
        super((new Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, WILD_MAINLAND_CONFIG)
                .precipitation(RainType.NONE)
                .category(Category.DESERT)
                .depth(0.2F)
                .scale(0.1F)
                .temperature(0.1F)
                .downfall(0.1F)
                .waterColor(4159204)
                .waterFogColor(329011)
                .parent((String) null)
        );
    }
}
