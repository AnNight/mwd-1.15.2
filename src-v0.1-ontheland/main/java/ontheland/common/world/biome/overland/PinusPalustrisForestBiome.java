package ontheland.common.world.biome.overland;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.DefaultBiomeFeatures;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.LakesConfig;
import net.minecraft.world.gen.feature.structure.MineshaftConfig;
import net.minecraft.world.gen.feature.structure.MineshaftStructure;
import net.minecraft.world.gen.placement.LakeChanceConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import ontheland.api.feature.OTLFeatures;
import ontheland.config.Config;

public class PinusPalustrisForestBiome extends Biome {
    public PinusPalustrisForestBiome() {
        super((new Biome.Builder())
                .surfaceBuilder(SurfaceBuilder.DEFAULT, SurfaceBuilder.GRASS_DIRT_GRAVEL_CONFIG)
                .precipitation(RainType.RAIN).category(Category.FOREST)
                .depth(0.4F).scale(0.1F)
                .temperature(0.3F).downfall(0.7F)
                .waterColor(4159204).waterFogColor(308210)
                .parent((String)null)
        );
        // Structures
        this.addStructure(Feature.MINESHAFT, new MineshaftConfig(0.004D, MineshaftStructure.Type.NORMAL));
        this.addStructure(Feature.STRONGHOLD, IFeatureConfig.NO_FEATURE_CONFIG);
        // Underground
        DefaultBiomeFeatures.addCarvers(this); // ???{???,???}
        DefaultBiomeFeatures.addStructures(this); // ????
        DefaultBiomeFeatures.addMonsterRooms(this);
        this.addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, Biome.createDecoratedFeature(Feature.LAKE, new LakesConfig(Blocks.WATER.getDefaultState()), Placement.WATER_LAKE, new LakeChanceConfig(4))); // water lake
        DefaultBiomeFeatures.addStoneVariants(this); // ??????{??????????????????????????????}
        DefaultBiomeFeatures.addOres(this); // ???{ú,??,??,???,???,????}
        DefaultBiomeFeatures.addExtraEmeraldOre(this); // ????
        DefaultBiomeFeatures.addSedimentDisks(this); // ??????{???,???,???}
        DefaultBiomeFeatures.addTaigaRocks(this); // ???????
        DefaultBiomeFeatures.addTaigaLargeFerns(this); // ???????????
        DefaultBiomeFeatures.addGrass(this); // ??
        DefaultBiomeFeatures.addTaigaGrassAndMushrooms(this); // {??????,??????,??????}
        DefaultBiomeFeatures.addReedsAndPumpkins(this);
        DefaultBiomeFeatures.addBerryBushes(this); // ??????
        DefaultBiomeFeatures.addSprings(this);
        DefaultBiomeFeatures.addFreezeTopLayer(this);
        DefaultBiomeFeatures.addFossils(this); // ???
        DefaultBiomeFeatures.func_222309_aj(this); // ????
        ////////////////////////////////////////////////////////////
        // Trees
        this.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Biome.createDecoratedFeature(OTLFeatures.PINUS_PALUSTRIS_TREE, IFeatureConfig.NO_FEATURE_CONFIG, Placement.COUNT_EXTRA_HEIGHTMAP, Config.TREE_CONFIG));
        // Flower
        this.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Biome.createDecoratedFeature(OTLFeatures.PINUS_PALUSTRIS_FLOWER, IFeatureConfig.NO_FEATURE_CONFIG, Placement.COUNT_HEIGHTMAP_32, Config.PINUS_FLOWER_CONFIG));
        // Ore
        this.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(Feature.ORE, Config.AMBER_FEATURE, Placement.COUNT_RANGE, Config.AMBER_RANGE));
        ////////////////////////////////////////////////////////////
        // Entities
        this.addSpawn(EntityClassification.CREATURE, new Biome.SpawnListEntry(EntityType.FOX, 8, 2, 4)); // ????
        this.addSpawn(EntityClassification.CREATURE, new Biome.SpawnListEntry(EntityType.PIG, 10, 4, 4)); // ??
        this.addSpawn(EntityClassification.CREATURE, new Biome.SpawnListEntry(EntityType.COW, 8, 4, 4)); // ?
        this.addSpawn(EntityClassification.CREATURE, new Biome.SpawnListEntry(EntityType.CHICKEN, 10, 4, 4)); // ??
        this.addSpawn(EntityClassification.CREATURE, new Biome.SpawnListEntry(EntityType.SHEEP, 12, 4, 4)); // ??
        this.addSpawn(EntityClassification.CREATURE, new Biome.SpawnListEntry(EntityType.WOLF, 8, 4, 4)); // ??
        this.addSpawn(EntityClassification.CREATURE, new Biome.SpawnListEntry(EntityType.RABBIT, 4, 2, 3)); // ??
        this.addSpawn(EntityClassification.AMBIENT, new Biome.SpawnListEntry(EntityType.BAT, 10, 6, 6)); // ????
        this.addSpawn(EntityClassification.MONSTER, new Biome.SpawnListEntry(EntityType.SPIDER, 100, 4, 5)); // ???
        this.addSpawn(EntityClassification.MONSTER, new Biome.SpawnListEntry(EntityType.ZOMBIE, 95, 4, 5)); // ???
        this.addSpawn(EntityClassification.MONSTER, new Biome.SpawnListEntry(EntityType.ZOMBIE_VILLAGER, 5, 1, 1)); // ???????
        this.addSpawn(EntityClassification.MONSTER, new Biome.SpawnListEntry(EntityType.SKELETON, 100, 3, 4)); // ????
        this.addSpawn(EntityClassification.MONSTER, new Biome.SpawnListEntry(EntityType.CREEPER, 100, 2, 4)); // ??????
        this.addSpawn(EntityClassification.MONSTER, new Biome.SpawnListEntry(EntityType.SLIME, 100, 4, 4)); // ????
        this.addSpawn(EntityClassification.MONSTER, new Biome.SpawnListEntry(EntityType.ENDERMAN, 10, 1, 3)); // ????
        this.addSpawn(EntityClassification.MONSTER, new Biome.SpawnListEntry(EntityType.WITCH, 5, 1, 1)); // ???
    }

    @Override
    public int getGrassColor(BlockPos pos) { return 4025123; }
}
