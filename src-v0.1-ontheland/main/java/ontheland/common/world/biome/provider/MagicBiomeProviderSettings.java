package ontheland.common.world.biome.provider;

import net.minecraft.world.biome.provider.IBiomeProviderSettings;
import ontheland.common.world.gen.MagicGenSettings;

public class MagicBiomeProviderSettings implements IBiomeProviderSettings {
    private long seed;
    private MagicGenSettings generatorSettings;
    private boolean large = false;

    public MagicBiomeProviderSettings setGeneratorSettings(MagicGenSettings magicGenSettings) {
        this.generatorSettings = magicGenSettings;
        return this;
    }

    public MagicGenSettings getGeneratorSettings() {
        return this.generatorSettings;
    }

    public MagicBiomeProviderSettings setSeed(long seed) {
        this.seed = seed;
        return this;
    }

    public long getSeed() {
        return this.seed;
    }

    public MagicBiomeProviderSettings setLarge(boolean large) {
        this.large = large;
        return this;
    }

    public boolean getLarge() {
        return this.large;
    }
}