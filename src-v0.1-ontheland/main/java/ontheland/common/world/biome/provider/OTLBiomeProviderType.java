package ontheland.common.world.biome.provider;

import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.biome.provider.BiomeProviderType;
import net.minecraft.world.biome.provider.IBiomeProviderSettings;

import java.util.function.Function;
import java.util.function.Supplier;

public class OTLBiomeProviderType {
    public static final BiomeProviderType<MagicBiomeProviderSettings, MagicBiomeProvider> MAGIC_BIOME_PROVIDER_TYPE = gen(MagicBiomeProviderSettings::new, MagicBiomeProvider::new, "magic");
    public static final BiomeProviderType<SpacialRiftBiomeProviderSettings, SpacialRiftBiomeProvider> SPACIAL_RIFT_BIOME_PROVIDER_TYPE = gen(SpacialRiftBiomeProviderSettings::new, SpacialRiftBiomeProvider::new, "spacial_rift");

    private static <C extends IBiomeProviderSettings, T extends BiomeProvider> BiomeProviderType<C, T> gen(Supplier<C> settings, Function<C, T> biomeProvider, String name) {
        BiomeProviderType<C, T> biomeProviderType = new BiomeProviderType<>(biomeProvider, settings);
        biomeProviderType.setRegistryName(name);
        return biomeProviderType;
    }
}