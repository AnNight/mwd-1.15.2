package ontheland.common.world.dimension.magic;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import ontheland.api.biome.OTLBiomes;
import ontheland.common.world.biome.provider.MagicBiomeProviderSettings;
import ontheland.common.world.biome.provider.OTLBiomeProviderType;
import ontheland.common.world.gen.MagicGenSettings;
import ontheland.common.world.gen.OTLChunkGeneratorType;

import javax.annotation.Nullable;

public class MagicDimension extends Dimension {
    private BiomeProvider biomeProvider;
    private Biome currentBiome = Biomes.OCEAN;

    public MagicDimension(World world, DimensionType type) {
        super(world, type);
    }

    private BiomeProvider getBiomeProvider() {
        if (this.biomeProvider == null) {
            MagicBiomeProviderSettings biomeProviderSettings = OTLBiomeProviderType.MAGIC_BIOME_PROVIDER_TYPE.createSettings()
                    .setGeneratorSettings(new MagicGenSettings()).setSeed(this.world.getSeed()).setLarge(this.world.getWorldType() == WorldType.LARGE_BIOMES);
            this.biomeProvider = OTLBiomeProviderType.MAGIC_BIOME_PROVIDER_TYPE.create(biomeProviderSettings);
        }
        return this.biomeProvider;
    }

    @Override
    public ChunkGenerator<?> createChunkGenerator() {
        MagicGenSettings chunkGenSettings = OTLChunkGeneratorType.MAGIC.createSettings();
        chunkGenSettings.setDefaultBlock(Blocks.STONE.getDefaultState());
        chunkGenSettings.setDefaultFluid(Blocks.WATER.getDefaultState());
        return OTLChunkGeneratorType.MAGIC.create(this.world, getBiomeProvider(), chunkGenSettings);
    }

    @Nullable
    @Override
    public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
        return null;
    }

    @Nullable
    @Override
    public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
        return null;
    }

    @Override
    public float calculateCelestialAngle(long worldTime, float partialTicks) {
        double d0 = MathHelper.frac((double) worldTime / 24000.0D - 0.25D);
        double d1 = 0.5D - Math.cos(d0 * Math.PI) / 2.0D;
        return (float) (d0 * 2.0D + d1) / 3.0F;
    }

    @Override
    public boolean isSurfaceWorld() {
        return true;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public Vec3d getFogColor(float celestialAngle, float partialTicks) {
        float red = 0, green = 0, blue = 0;
        if (isFogBiome(currentBiome)) {
            if (currentBiome == OTLBiomes.lost_sea) {
                green = 100F / 256F;
            }
            if (currentBiome == Biomes.PLAINS) {
                red = 0.5F;
                green = 0.3F;
                blue = 0.7F;
            }
        }
        if (celestialAngle < 0.5) {
            celestialAngle = (0.5f - celestialAngle) * 2;
        } else if (celestialAngle >= 0.5) {
            celestialAngle = (celestialAngle - 0.5f) * 2;
        }
        float r = red * celestialAngle;
        float g = green * celestialAngle;
        float b = blue * celestialAngle;
        return new Vec3d(r, g, b);
    }

    @Override
    public SleepResult canSleepAt(PlayerEntity player, BlockPos pos) {
        return SleepResult.ALLOW;
    }

    @Override
    public boolean canRespawnHere() {
        return true;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public boolean doesXZShowFog(int x, int z) {
        this.currentBiome = this.world.getBiome(new BlockPos(x, 0, z));
        return isFogBiome(currentBiome);
    }

    private boolean isFogBiome(Biome biome) {
        return biome == OTLBiomes.lost_sea || biome == Biomes.PLAINS;
    }
}