package ontheland.common.world.dimension.spacialrift;

import net.minecraft.world.World;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.common.ModDimension;

import java.util.function.BiFunction;

import static ontheland.common.world.dimension.OTLDimensions.SPACIAL_RIFT_DIM_ID;

public class SpacialRiftModDimension extends ModDimension {
    @Override
    public BiFunction<World, DimensionType, ? extends Dimension> getFactory() {
        return SpacialRiftDimension::new;
    }
}
