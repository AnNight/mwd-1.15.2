package ontheland.common.world.dimension.spacialrift;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import ontheland.common.world.biome.provider.OTLBiomeProviderType;
import ontheland.common.world.biome.provider.SpacialRiftBiomeProviderSettings;
import ontheland.common.world.gen.OTLChunkGeneratorType;
import ontheland.common.world.gen.SpacialRiftGenSettings;

import javax.annotation.Nullable;

public class SpacialRiftDimension extends Dimension {
    public SpacialRiftDimension(World worldIn, DimensionType typeIn) {
        super(worldIn, typeIn);
    }

    @Override
    public ChunkGenerator<?> createChunkGenerator() {
        SpacialRiftGenSettings chunkGenSettings = OTLChunkGeneratorType.SPACIAL_RIFT.createSettings();
        chunkGenSettings.setDefaultBlock(Blocks.STONE.getDefaultState());
        chunkGenSettings.setDefaultFluid(Blocks.WATER.getDefaultState());
        SpacialRiftBiomeProviderSettings biomeProviderSettings = OTLBiomeProviderType.SPACIAL_RIFT_BIOME_PROVIDER_TYPE.createSettings();
        BiomeProvider biomeProvider = OTLBiomeProviderType.SPACIAL_RIFT_BIOME_PROVIDER_TYPE.create(biomeProviderSettings);
        return OTLChunkGeneratorType.SPACIAL_RIFT.create(this.world, biomeProvider, chunkGenSettings);
    }

    @Nullable
    @Override
    public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
        return null;
    }

    @Nullable
    @Override
    public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
        return null;
    }

    @Override
    public float calculateCelestialAngle(long worldTime, float partialTicks) {
        return 0F;
    }

    @Override
    public boolean hasSkyLight() {
        return false;
    }

    @Override
    public boolean isSurfaceWorld() {
        return false;
    }

    @Override
    public Vec3d getFogColor(float celestialAngle, float partialTicks) {
        return new Vec3d(0, 0, 0);
    }

    @Override
    public boolean canRespawnHere() {
        return true;
    }

    @Override
    public SleepResult canSleepAt(PlayerEntity player, BlockPos pos) {
        return SleepResult.BED_EXPLODES;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public boolean doesXZShowFog(int x, int z) {
        return false;
    }
}