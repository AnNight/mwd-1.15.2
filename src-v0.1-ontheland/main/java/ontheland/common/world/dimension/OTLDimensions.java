package ontheland.common.world.dimension;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.common.ModDimension;
import net.minecraftforge.registries.ObjectHolder;
import ontheland.OnTheLand;

public class OTLDimensions {
    public static final ResourceLocation MAGIC_DIM_ID = new ResourceLocation(OnTheLand.MOD_ID, "magic_dimension");
    public static final ResourceLocation SPACIAL_RIFT_DIM_ID = new ResourceLocation(OnTheLand.MOD_ID, "spacial_rift");

    @ObjectHolder("ontheland:magic_dimension")
    public static ModDimension MAGIC_DIM;
    @ObjectHolder("ontheland:spacial_rift")
    public static ModDimension SPACIAL_RIFT_DIM;

    public static DimensionType MAGIC_DIM_TYPE;
    public static DimensionType SPACIAL_RIFT_DIM_TYPE;
}