package ontheland.common.world.dimension;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.play.server.*;
import net.minecraft.potion.EffectInstance;
import net.minecraft.server.management.PlayerList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.WorldInfo;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.hooks.BasicEventHooks;

import java.util.Random;

public class TeleporterManager {
    public static void teleportCheck(PlayerEntity player, World world, BlockPos pos, DimensionType destination) {
        // server && !ride && !be ridden && !boss
        if (!player.world.isRemote && !player.isPassenger() && !player.isBeingRidden() && player.isNonBoss()) {
            teleport(player, world, pos, destination);
        }
    }

    /**
     * find destination teleporter
     */
    private static void teleport(PlayerEntity player, World world, BlockPos pos, DimensionType destination) {
        Block block;
        Random random = new Random();
        int randomOffsetX, randomOffsetY, count = 0, range = 32;
        boolean notFoundBlock = true, strict = true;
        World destinationWorld = world.getServer().func_71218_a(destination); // getWorld
        BlockPos.MutableBlockPos mutableBlockPos = new BlockPos.MutableBlockPos();
        BlockPos blockPos = new BlockPos(pos);
        do {
            randomOffsetX = random.nextInt(2) == 0 ? -random.nextInt(range) : random.nextInt(range);
            randomOffsetY = random.nextInt(2) == 0 ? -random.nextInt(range) : random.nextInt(range);
            for (int y = 255; y > 0; y--) {
                mutableBlockPos.setPos(pos.getX() + randomOffsetX, y, pos.getZ() + randomOffsetY);
                block = destinationWorld.getBlockState(mutableBlockPos).getBlock();
                if (block == Blocks.BEDROCK && strict) continue; // if == bedrock, skip
                if ((block == Blocks.WATER || block == Blocks.LAVA) && strict) break; // if == water or lava, regenerate pos
                if (destinationWorld.getBlockState(mutableBlockPos).getBlock() != Blocks.AIR) {
                    blockPos = new BlockPos(pos.getX() + randomOffsetX, y + 1, pos.getZ() + randomOffsetY);
                    notFoundBlock = false;
                    break;
                }
            }
            if (notFoundBlock && count < 4096) count++;
            else if (notFoundBlock) {
                strict = false;
                range = 128;
            }
            else changeDim(((ServerPlayerEntity) player), blockPos, destination);
        } while (notFoundBlock);
    }

    /**
     * copy from ServerPlayerEntity#changeDimension
     */
    private static void changeDim(ServerPlayerEntity player, BlockPos pos, DimensionType destination) {
        if (!ForgeHooks.onTravelToDimension(player, destination)) return;
        DimensionType oldDimension = player.dimension;
        ServerWorld oldServerWorld = player.server.func_71218_a(oldDimension); // getWorld
        player.dimension = destination;
        ServerWorld newServerWorld = player.server.func_71218_a(destination);
        WorldInfo worldinfo = player.world.getWorldInfo();
        player.connection.sendPacket(new SRespawnPacket(destination, worldinfo.getGenerator(), player.interactionManager.getGameType()));
        player.connection.sendPacket(new SServerDifficultyPacket(worldinfo.getDifficulty(), worldinfo.isDifficultyLocked()));
        PlayerList playerlist = player.server.getPlayerList();
        playerlist.updatePermissionLevel(player);
        oldServerWorld.removeEntity(player, true);
        player.revive();
        float f = player.rotationPitch;
        float f1 = player.rotationYaw;

        oldServerWorld.getProfiler().startSection("moving");
        player.setLocationAndAngles(pos.getX() + .5, pos.getY() + .5, pos.getZ() + .5, f1, f);
        oldServerWorld.getProfiler().endSection();

        oldServerWorld.getProfiler().startSection("placing");
        player.setLocationAndAngles(pos.getX() + .5, pos.getY() + .5, pos.getZ() + .5, f1, f);
        // You can place some blocks in here.
        oldServerWorld.getProfiler().endSection();
        player.setWorld(newServerWorld);
        newServerWorld.func_217447_b(player);
        player.connection.setPlayerLocation(pos.getX() + .5, pos.getY() + .5, pos.getZ() + .5, f1, f);
        player.interactionManager.func_73080_a(newServerWorld); // setWorld
        player.connection.sendPacket(new SPlayerAbilitiesPacket(player.abilities));
        playerlist.func_72354_b(player, newServerWorld); // sendWorldInfo
        playerlist.sendInventory(player);

        for (EffectInstance effectinstance : player.getActivePotionEffects()) {
            player.connection.sendPacket(new SPlayEntityEffectPacket(player.getEntityId(), effectinstance));
        }

        player.connection.sendPacket(new SPlaySoundEventPacket(1032, BlockPos.ZERO, 0, false));
        BasicEventHooks.firePlayerChangedDimensionEvent(player, oldDimension, destination);
    }
}
