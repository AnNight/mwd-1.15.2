package ontheland.common.world.gen;

import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.ChunkGeneratorType;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.IChunkGeneratorFactory;
import ontheland.OnTheLand;

import java.util.function.Supplier;

public class OTLChunkGeneratorType {
    public static final ChunkGeneratorType<MagicGenSettings, MagicChunkGenerator> MAGIC = gen(MagicGenSettings::new, MagicChunkGenerator::new, "magic");
    public static final ChunkGeneratorType<SpacialRiftGenSettings, SpacialRiftChunkGenerator> SPACIAL_RIFT = gen(SpacialRiftGenSettings::new, SpacialRiftChunkGenerator::new, "spacial_rift");

    private static <C extends GenerationSettings, T extends ChunkGenerator<C>> ChunkGeneratorType<C, T> gen(Supplier<C> settings, IChunkGeneratorFactory<C, T> factory, String name) {
        ChunkGeneratorType<C, T> generatorType = new ChunkGeneratorType<>(factory, false, settings);
        generatorType.setRegistryName(OnTheLand.MOD_ID, name);
        return generatorType;
    }
}