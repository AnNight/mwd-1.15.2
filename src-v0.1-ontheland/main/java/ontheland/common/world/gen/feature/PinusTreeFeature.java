package ontheland.common.world.gen.feature;

import com.mojang.datafixers.Dynamic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.gen.IWorldGenerationReader;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import ontheland.api.block.OTLBlocks;

import java.util.Random;
import java.util.Set;
import java.util.function.Function;

public class PinusTreeFeature extends AbstractTreeFeature<NoFeatureConfig> {
    private final BlockState log;
    private final BlockState leaf;
    private final boolean useExtraRandomHeight;

    public PinusTreeFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> configIn, boolean doBlockNotifyIn, boolean extraRandomHeightIn, BlockState log, BlockState leaf, Block sapling) {
        super(configIn, doBlockNotifyIn);
        this.log = log;
        this.leaf = leaf;
        this.useExtraRandomHeight = extraRandomHeightIn;
        this.setSapling((net.minecraftforge.common.IPlantable) sapling);
    }

    public boolean place(Set<BlockPos> changedBlocks, IWorldGenerationReader worldIn, Random rand, BlockPos position, MutableBoundingBox p_208519_5_) {
        int i = rand.nextInt(7) + 4;
        if (this.useExtraRandomHeight) {
            return false;
        }
        // Select different generating methods on the basis of the generation height.
        if (i == 4) {
            if (position.getY() >= 1 && position.getY() + i + 1 <= worldIn.getMaxHeight()) { // Check if this generation method is greater than the world maximum height.
                boolean flag = true;
                // Check
                for (int y = position.getY(); y <= position.getY() + 2 + i; ++y) {
                    int k;
                    if (y == position.getY()) {
                        k = 0;
                    } else if (y == position.getY() + 2) {
                        k = 2;
                    } else {
                        k = 1;
                    }
                    BlockPos.MutableBlockPos mutableBlockPos = new BlockPos.MutableBlockPos();
                    for(int x = position.getX() - k; x <= position.getX() + k && flag; ++x) {
                        for (int z = position.getZ() - k; z <= position.getZ() + k && flag; ++z) {
                            if (y >= 0 && y < worldIn.getMaxHeight()) {
                                if (!func_214587_a(worldIn, mutableBlockPos.setPos(x, y, z))) {
                                    flag = false;
                                }
                            } else {
                                flag = false;
                            }
                        }
                    }
                }
                // Generate
                if (!flag) {
                    return false;
                } else if ((isSoil(worldIn, position.down(), getSapling())) && position.getY() < worldIn.getMaxHeight() - i - 1) {
                    this.setDirtAt(worldIn, position.down(), position);
                    BlockPos blockPos;
                    int y = position.getY() + 2;
                    /**
                     * r000r
                     * 00000
                     * 00000
                     * 00000
                     * r000r
                     */
                    for (int x = position.getX() - 2; x <= position.getX() + 2; ++x) {
                        for (int z = position.getZ() - 2; z <= position.getZ() + 2; ++z) {
                            if (Math.abs(position.getX() - x) == 2 && Math.abs(position.getZ() - z) == 2) {
                                if (rand.nextInt(2) == 0) {
                                    blockPos = new BlockPos(x,y,z);
                                    if (isAirOrLeaves(worldIn, blockPos)) {
                                        this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                    }
                                }
                            } else {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    y++;
                    /**
                     * x0x
                     * 000
                     * x0x
                     */
                    for (int x = position.getX() - 1; x <= position.getX() + 1; ++x) {
                        for (int z = position.getZ() - 1; z <= position.getZ() + 1; ++z) {
                            if (Math.abs(position.getX() - x) != 1 || Math.abs(position.getZ() - z) != 1) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    blockPos = new BlockPos(position.getX(), ++y, position.getZ());
                    if (isAirOrLeaves(worldIn, blockPos)) {
                        this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                    }
                    y++;
                    /**
                     * x0x
                     * 000
                     * x0x
                     */
                    for (int x = position.getX() - 1; x <= position.getX() + 1; ++x) {
                        for (int z = position.getZ() - 1; z <= position.getZ() + 1; ++z) {
                            if (Math.abs(position.getX() - x) != 1 || Math.abs(position.getZ() - z) != 1) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    blockPos = new BlockPos(position.getX(), ++y, position.getZ());
                    if (isAirOrLeaves(worldIn, blockPos)) {
                        this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                    }
                    // Place logs
                    for (int y1 = 0; y1 < i; ++y1) {
                        if (isAirOrLeaves(worldIn, position.up(y1))) {
                            this.setLogState(changedBlocks, worldIn, position.up(y1), log, p_208519_5_);
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        else if (i >= 5 && i <= 7) {
            if (position.getY() >= 1 && position.getY() + i + 1 <= worldIn.getMaxHeight()) {
                boolean flag = true;
                for (int y = position.getY(); y <= position.getY() + 1 + i; ++y) {
                    int k = 1;
                    if (y == position.getY()) {
                        k = 0;
                    }
                    if (y == position.getY() + i - 3 || y == position.getY() + i - 2) {
                        k = 2;
                    }
                    BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();
                    for(int x = position.getX() - k; x <= position.getX() + k && flag; ++x) {
                        for (int z = position.getZ() - k; z <= position.getZ() + k && flag; ++z) {
                            if (y >= 0 && y < worldIn.getMaxHeight()) {
                                if (!func_214587_a(worldIn, blockpos$mutableblockpos.setPos(x, y, z))) {
                                    flag = false;
                                }
                            } else {
                                flag = false;
                            }
                        }
                    }
                }

                if (!flag) {
                    return false;
                } else if ((isSoil(worldIn, position.down(), getSapling())) && position.getY() < worldIn.getMaxHeight() - i - 1) {
                    this.setDirtAt(worldIn, position.down(), position);
                    BlockPos blockPos;
                    int y = position.getY() + i - 3;
                    /**
                     * r000r
                     * 00000
                     * 00000
                     * 00000
                     * r000r
                     */
                    for (int x = position.getX() - 2; x <= position.getX() + 2; ++x) {
                        for (int z = position.getZ() - 2; z <= position.getZ() + 2; ++z) {
                            if (Math.abs(position.getX() - x) == 2 && Math.abs(position.getZ() - z) == 2) {
                                if (rand.nextInt(2) == 0) {
                                    blockPos = new BlockPos(x,y,z);
                                    if (isAirOrLeaves(worldIn, blockPos)) {
                                        this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                    }
                                }
                            } else {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    y++;
                    /**
                     * xx0xx
                     * x000x
                     * 00000
                     * x000x
                     * xx0xx
                     */
                    for (int x = position.getX() - 2; x <= position.getX() + 2; ++x) {
                        for (int z = position.getZ() - 2; z <= position.getZ() + 2; ++z) {
                            if (
                                    (Math.abs(position.getX() - x) != 2 && Math.abs(position.getZ() - z) != 1) ||
                                    (Math.abs(position.getX() - x) != 1 && Math.abs(position.getZ() - z) != 2) ||
                                    (Math.abs(position.getX() - x) != 2 || Math.abs(position.getZ() - z) != 2)
                            ) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    y++;
                    /**
                     * r0r
                     * 000
                     * r0r
                     */
                    for (int x = position.getX() - 1; x <= position.getX() + 1; ++x) {
                        for (int z = position.getZ() - 1; z <= position.getZ() + 1; ++z) {
                            if (Math.abs(position.getX() - x) == 1 && Math.abs(position.getZ() - z) == 1) {
                                if (rand.nextInt(2) == 0) {
                                    blockPos = new BlockPos(x,y,z);
                                    if (isAirOrLeaves(worldIn, blockPos)) {
                                        this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                    }
                                }
                            } else {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    y++;
                    /**
                     * x0x
                     * 000
                     * x0x
                     */
                    for (int x = position.getX() - 1; x <= position.getX() + 1; ++x) {
                        for (int z = position.getZ() - 1; z <= position.getZ() + 1; ++z) {
                            if (Math.abs(position.getX() - x) != 1 || Math.abs(position.getZ() - z) != 1) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    blockPos = new BlockPos(position.getX(), ++y, position.getZ());
                    if (isAirOrLeaves(worldIn, blockPos)) {
                        this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                    }
                    // Place logs
                    for (int y1 = 0; y1 < i; ++y1) { // Y轴相对坐标
                        if (isAirOrLeaves(worldIn, position.up(y1))) {
                            this.setLogState(changedBlocks, worldIn, position.up(y1), log, p_208519_5_);
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        else { // log 8/9/10
            if (position.getY() >= 1 && position.getY() + i + 1 <= worldIn.getMaxHeight()) { // 检查本次高度是否大于世界最大高度
                // 检查树苗上方空间是否允许生成树
                // 忽略的方块：空气、树叶、草方块、泥土、原木、树苗、藤蔓
                boolean flag = true;
                for (int y = position.getY(); y <= position.getY() + i; ++y) { // Y轴起始坐标
                    int k = 1; // x & z 的偏移量
                    if (y == position.getY()) {
                        k = 0;
                    } else if (y < position.getY() + i - 5) {
                        k = 1;
                    } else if (y < position.getY() + i - 3) {
                        k = 3;
                    } else if (y < position.getY() + i - 1) {
                        k = 2;
                    }
                    BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();
                    for(int x = position.getX() - k; x <= position.getX() + k && flag; ++x) {
                        for (int z = position.getZ() - k; z <= position.getZ() + k && flag; ++z) {
                            if (y >= 0 && y < worldIn.getMaxHeight()) {
                                if (!func_214587_a(worldIn, blockpos$mutableblockpos.setPos(x, y, z))) {
                                    flag = false;
                                }
                            } else {
                                flag = false;
                            }
                        }
                    }
                }
                // 生成
                if (!flag) {
                    return false;
                } else if ((isSoil(worldIn, position.down(), getSapling())) && position.getY() < worldIn.getMaxHeight() - i - 1) { // 检测下方是否为泥土 并检查生成高度
                    this.setDirtAt(worldIn, position.down(), position);
                    // Place leaves
                    BlockPos blockPos;
                    int y = position.getY() + i - 5;
                    /**
                     * xx000xx
                     * x00000x
                     * 0000000
                     * 0000000
                     * 0000000
                     * x00000x
                     * xx000xx
                     */
                    for (int x = position.getX() - 3; x <= position.getX() + 3; ++x) {
                        for (int z = position.getZ() - 3; z <= position.getZ() + 3; ++z) {
                            if (Math.abs(position.getX() - x) + Math.abs(position.getZ() - z) < 5) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    y++;
                    /**
                     * xxx0xxx
                     * x00000x
                     * x00000x
                     * 0000000
                     * x00000x
                     * x00000x
                     * xxx0xxx
                     */
                    for (int x = position.getX() - 3; x <= position.getX() + 3; ++x) {
                        for (int z = position.getZ() - 3; z <= position.getZ() + 3; ++z) {
                            if (
                                    (Math.abs(position.getX() - x) < 3 && Math.abs(position.getZ() - z) < 3) ||
                                    (Math.abs(position.getZ() - z) == 3 && Math.abs(position.getX() - x) == 0) ||
                                    (Math.abs(position.getZ() - z) == 0 && Math.abs(position.getX() - x) == 3)
                            ) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    y++;
                    /**
                     * x000x
                     * 00000
                     * 00000
                     * 00000
                     * x000x
                     */
                    for (int x = position.getX() - 2; x <= position.getX() + 2; ++x) {
                        for (int z = position.getZ() - 2; z <= position.getZ() + 2; ++z) {
                            if (Math.abs(position.getX() - x) != 2 || Math.abs(position.getZ() - z) != 2) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    y++;
                    /**
                     * xx0xx
                     * x000x
                     * 00000
                     * x000x
                     * xx0xx
                     */
                    for (int x = position.getX() - 2; x <= position.getX() + 2; ++x) {
                        for (int z = position.getZ() - 2; z <= position.getZ() + 2; ++z) {
                            if (Math.abs(position.getX() - x) + Math.abs(position.getZ() - z) < 3) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    y++;
                    /**
                     * 000
                     * 000
                     * 000
                     */
                    for (int x = position.getX() - 1; x <= position.getX() + 1; ++x) {
                        for (int z = position.getZ() - 1; z <= position.getZ() + 1; ++z) {
                            blockPos = new BlockPos(x,y,z);
                            if (isAirOrLeaves(worldIn, blockPos)) {
                                this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                            }
                        }
                    }
                    y++;
                    /**
                     * x0x
                     * 000
                     * x0x
                     */
                    for (int x = position.getX() - 1; x <= position.getX() + 1; ++x) {
                        for (int z = position.getZ() - 1; z <= position.getZ() + 1; ++z) {
                            if (Math.abs(position.getX() - x) != 1 || Math.abs(position.getZ() - z) != 1) {
                                blockPos = new BlockPos(x,y,z);
                                if (isAirOrLeaves(worldIn, blockPos)) {
                                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                                }
                            }
                        }
                    }
                    blockPos = new BlockPos(position.getX(), ++y, position.getZ());
                    if (isAirOrLeaves(worldIn, blockPos)) {
                        this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                    }
                    // Place logs
                    for (int y1 = 0; y1 < i; ++y1) { // Y轴相对坐标
                        if (isAirOrLeaves(worldIn, position.up(y1))) {
                            this.setLogState(changedBlocks, worldIn, position.up(y1), log, p_208519_5_);
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}