package ontheland.common.world.gen.feature;

import com.mojang.datafixers.Dynamic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.feature.FlowersFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import ontheland.api.block.OTLBlocks;

import java.util.Random;
import java.util.function.Function;

public class PinusCaribaeaFlowersFeature extends FlowersFeature {
    public PinusCaribaeaFlowersFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> deserializer) {
        super(deserializer);
        setRegistryName("pinus_caribaea_flower");
    }

    @Override
    public BlockState getRandomFlower(Random random, BlockPos pos) {
        Block[] normal = new Block[]{OTLBlocks.hai_flower, OTLBlocks.ling_flower};
        Block[] luminous = new Block[]{OTLBlocks.ji_flower, OTLBlocks.xin_flower};

        int i = random.nextInt(5);
        if (i == 0) {
            int j = random.nextInt(luminous.length);
            return luminous[j].getDefaultState();
        } else {
            int j = random.nextInt(normal.length);
            return normal[j].getDefaultState();
        }
    }
}