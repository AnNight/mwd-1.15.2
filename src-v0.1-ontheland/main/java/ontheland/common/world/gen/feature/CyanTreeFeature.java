package ontheland.common.world.gen.feature;

import com.mojang.datafixers.Dynamic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.gen.IWorldGenerationReader;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

import java.util.Random;
import java.util.Set;
import java.util.function.Function;

public class CyanTreeFeature extends AbstractTreeFeature<NoFeatureConfig> {
    private final BlockState log;
    private final BlockState leaf;
    private final boolean useExtraRandomHeight;

    public CyanTreeFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> configIn, boolean doBlockNotifyIn, boolean extraRandomHeightIn, BlockState log, BlockState leaf, Block sapling) {
        super(configIn, doBlockNotifyIn);
        this.log = log;
        this.leaf = leaf;
        this.useExtraRandomHeight = extraRandomHeightIn;
        this.setSapling((net.minecraftforge.common.IPlantable) sapling);
    }

    @Override
    protected boolean place(Set<BlockPos> changedBlocks, IWorldGenerationReader worldIn, Random rand, BlockPos position, MutableBoundingBox p_208519_5_) {
        int i = rand.nextInt(3) + 2;
        if (this.useExtraRandomHeight) {
            return false;
        }
        if (position.getY() >= 1 && position.getY() + i + 1 <= worldIn.getMaxHeight()) { // Check if this generation method is greater than the world maximum height.
            boolean flag = true;
            // Check
            for (int y = position.getY(); y <= position.getY() + 1 + i; ++y) {
                int k;
                if (y == position.getY()) {
                    k = 0;
                } else {
                    k = 1;
                }
                BlockPos.MutableBlockPos mutableBlockPos = new BlockPos.MutableBlockPos();
                for(int x = position.getX() - k; x <= position.getX() + k && flag; ++x) {
                    for (int z = position.getZ() - k; z <= position.getZ() + k && flag; ++z) {
                        if (y >= 0 && y < worldIn.getMaxHeight()) {
                            if (!func_214587_a(worldIn, mutableBlockPos.setPos(x, y, z))) {
                                flag = false;
                            }
                        } else {
                            flag = false;
                        }
                    }
                }
            }
            // Generate
            if (!flag) {
                return false;
            } else if ((isSoil(worldIn, position.down(), getSapling())) && position.getY() < worldIn.getMaxHeight() - i - 1) {
                this.setDirtAt(worldIn, position.down(), position);
                BlockPos blockPos;
                int y = position.getY() + i - 1;
                /**
                 * x0x
                 * 000
                 * x0x
                 */
                for (int x = position.getX() - 1; x <= position.getX() + 1; ++x) {
                    for (int z = position.getZ() - 1; z <= position.getZ() + 1; ++z) {
                        if (Math.abs(position.getX() - x) != 1 || Math.abs(position.getZ() - z) != 1) {
                            blockPos = new BlockPos(x,y,z);
                            if (isAirOrLeaves(worldIn, blockPos)) {
                                this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                            }
                        }
                    }
                }
                blockPos = new BlockPos(position.getX(), ++y, position.getZ());
                if (isAirOrLeaves(worldIn, blockPos)) {
                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, p_208519_5_);
                }
                // Place logs
                for (int y1 = 0; y1 < i; ++y1) {
                    if (isAirOrLeaves(worldIn, position.up(y1))) {
                        this.setLogState(changedBlocks, worldIn, position.up(y1), log, p_208519_5_);
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
