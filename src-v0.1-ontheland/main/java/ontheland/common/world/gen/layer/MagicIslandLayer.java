package ontheland.common.world.gen.layer;

import net.minecraft.util.registry.Registry;
import net.minecraft.world.gen.INoiseRandom;
import net.minecraft.world.gen.layer.traits.IBishopTransformer;
import ontheland.api.biome.OTLBiomes;

public enum MagicIslandLayer implements IBishopTransformer {
    INSTANCE;

    private static final int MAGIC_ISLAND = Registry.BIOME.getId(OTLBiomes.magic_island);
    private static final int GAS_ISLAND = Registry.BIOME.getId(OTLBiomes.gas_island);
    private static final int VOLCANIC_ISLAND = Registry.BIOME.getId(OTLBiomes.volcanic_island);

    @Override
    public int apply(INoiseRandom context, int north, int west, int south, int east, int center) {
        if (north == MLayerUtil.OCEAN && west == MLayerUtil.OCEAN && south == MLayerUtil.OCEAN && east == MLayerUtil.OCEAN && center == MLayerUtil.OCEAN) {
            int i = context.random(100);
            if (i == 0) return MAGIC_ISLAND;
            if (i == 1) return GAS_ISLAND;
            if (i == 2) return VOLCANIC_ISLAND;
        }
        return center;
    }
}
