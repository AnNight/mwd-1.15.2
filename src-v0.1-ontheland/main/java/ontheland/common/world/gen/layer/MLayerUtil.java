package ontheland.common.world.gen.layer;

import com.google.common.collect.ImmutableList;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.IExtendedNoiseRandom;
import net.minecraft.world.gen.LazyAreaLayerContext;
import net.minecraft.world.gen.OverworldGenSettings;
import net.minecraft.world.gen.area.IArea;
import net.minecraft.world.gen.area.IAreaFactory;
import net.minecraft.world.gen.area.LazyArea;
import net.minecraft.world.gen.layer.*;
import ontheland.api.biome.OTLBiomes;
import ontheland.common.world.gen.MagicGenSettings;

import java.util.function.LongFunction;

@SuppressWarnings("deprecation")
public class MLayerUtil {
    protected static final int OCEAN = Registry.BIOME.getId(Biomes.OCEAN);
//    protected static final int LOST_SEA = Registry.BIOME.getId(OTLBiomes.lost_sea);
//    protected static final int WILD_MAINLAND = Registry.BIOME.getId(OTLBiomes.wild_mainland);

    private static <T extends IArea, C extends IExtendedNoiseRandom<T>> IAreaFactory<T> getBiomeLayer(WorldType worldType, IAreaFactory<T> factory, OverworldGenSettings chunkSettings, LongFunction<C> contextFactory) {
//        factory = (new BiomeLayer(worldType, chunkSettings)).apply(contextFactory.apply(200L), factory);
//        factory = AddBambooForestLayer.INSTANCE.apply(contextFactory.apply(1001L), factory);
        factory = LayerUtil.repeat(1000L, ZoomLayer.NORMAL, factory, 2, contextFactory); // zoom the factory 2 times+++
//        factory = EdgeBiomeLayer.INSTANCE.apply(contextFactory.apply(1000L), factory);
        return factory;
    }

    private static <T extends IArea, C extends IExtendedNoiseRandom<T>> IAreaFactory<T> createPlainAndOcean(LongFunction<C> contextFactory) {/*
        // if =! ocean, return 4<forest>[1/6] or 3<mountain>[1/6] or 1<plain>[2/3]
        factory = AddSnowLayer.INSTANCE.apply(contextFactory.apply(2L), factory); // add forest or mountain
        factory = EdgeLayer.CoolWarm.INSTANCE.apply(contextFactory.apply(2L), factory); // add 2<desert>
        factory = EdgeLayer.HeatIce.INSTANCE.apply(contextFactory.apply(2L), factory); // add 3<mountain>
        factory = EdgeLayer.Special.INSTANCE.apply(contextFactory.apply(3L), factory); // add spacial*/
        IAreaFactory<T> factory = IslandLayer.INSTANCE.apply(contextFactory.apply(1L));
        factory = ZoomLayer.FUZZY.apply(contextFactory.apply(2000L), factory);
        factory = AddIslandLayer.INSTANCE.apply(contextFactory.apply(1L), factory);
        factory = ZoomLayer.NORMAL.apply(contextFactory.apply(2001L), factory);
        factory = AddIslandLayer.INSTANCE.apply(contextFactory.apply(2L), factory);
        factory = AddIslandLayer.INSTANCE.apply(contextFactory.apply(50L), factory);
        factory = AddIslandLayer.INSTANCE.apply(contextFactory.apply(70L), factory);
        // if all==shallowOcean return 1[1/2] or center[1/2]
        factory = RemoveTooMuchOceanLayer.INSTANCE.apply(contextFactory.apply(2L), factory);
        // add forest or mountain
        factory = AddIslandLayer.INSTANCE.apply(contextFactory.apply(3L), factory);
        // // // add 2|3|spacial
        factory = ZoomLayer.NORMAL.apply(contextFactory.apply(2002L), factory);
        factory = ZoomLayer.NORMAL.apply(contextFactory.apply(2003L), factory);
        factory = AddIslandLayer.INSTANCE.apply(contextFactory.apply(4L), factory);
        return factory;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static <T extends IArea, C extends IExtendedNoiseRandom<T>> ImmutableList<IAreaFactory<T>> buildMagicProcedure(MagicGenSettings settings, boolean large, LongFunction<C> contextFactory) {
        /*IAreaFactory<T> landSeaFactory = createInitialLandAndSeaFactory(contextFactory);+++

        IAreaFactory<T> oceanFactory = OceanLayer.INSTANCE.apply(contextFactory.apply(2L)); // gen ocean
        oceanFactory = LayerUtil.repeat(2001L, ZoomLayer.NORMAL, oceanFactory, 6, contextFactory); // zoom the ocean 6 times
        // if all==shallowOcean return 14<mushroom field>[1/100]
        landSeaFactory = AddMushroomIslandLayer.INSTANCE.apply(contextFactory.apply(5L), landSeaFactory); // add mushroom field+++
        landSeaFactory = DeepOceanLayer.INSTANCE.apply(contextFactory.apply(4L), landSeaFactory); // add deep ocean+++
        landSeaFactory = LayerUtil.repeat(1000L, ZoomLayer.NORMAL, landSeaFactory, 0, contextFactory); // zoom the landSea 0 times+++

        int biomeSize = 6;
        int riverSize = biomeSize;
        if (settings != null) {
            biomeSize = large ? 8 : settings.getBiomeSize();
            riverSize = settings.getRiverSize();
        }

        WorldType worldType = large ? WorldType.LARGE_BIOMES : WorldType.DEFAULT;
        biomeSize = LayerUtil.getModdedBiomeSize(worldType, biomeSize);

        IAreaFactory<T> riverFactory = LayerUtil.repeat(1000L, ZoomLayer.NORMAL, landSeaFactory, 0, contextFactory); // zoom the landSea 0 times
        // use random numbers[2~300000] to fill factory except shallow ocean
        riverFactory = StartRiverLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(100L), riverFactory); // initialization river

        IAreaFactory<T> biomesFactory = getBiomeLayer(worldType, landSeaFactory, settings, contextFactory);

        IAreaFactory<T> lvt_9_1_ = LayerUtil.repeat(1000L, ZoomLayer.NORMAL, riverFactory, 2, contextFactory); // zoom the riverFactory 2 times

        biomesFactory = HillsLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(1000L), biomesFactory, lvt_9_1_); // add hill
        riverFactory = LayerUtil.repeat(1000L, ZoomLayer.NORMAL, riverFactory, 2, contextFactory); // zoom the riverFactory 2 times
        riverFactory = LayerUtil.repeat(1000L, ZoomLayer.NORMAL, riverFactory, riverSize, contextFactory); // zoom the riverFactory riverSize times
        // [0,2,3] riverFilter, return -1<ocean!> or 7<river>
        riverFactory = RiverLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(1L), riverFactory); // add river
        riverFactory = SmoothLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(1000L), riverFactory); // smooth
        biomesFactory = RareBiomeLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(1001L), biomesFactory); // plain to sunflowerPlain[1/57]

        for(int k = 0; k < biomeSize; ++k) {}

        biomesFactory = SmoothLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(1000L), biomesFactory); // smooth

        biomesFactory = MixRiverLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(100L), biomesFactory, riverFactory); // mix river into biomes
        biomesFactory = MixOceansLayer.INSTANCE.apply(contextFactory.apply(100L), biomesFactory, oceanFactory); // mix ocean into biomes*/
        /////////////////////////////////////////////////////
        IAreaFactory<T> landSeaFactory = createPlainAndOcean(contextFactory);

        landSeaFactory = MagicIslandLayer.INSTANCE.apply(contextFactory.apply(5L), landSeaFactory);
        landSeaFactory = MagicLostSeaLayer.INSTANCE.apply(contextFactory.apply(4L), landSeaFactory); // 0 -> [0 & LostSea]
        landSeaFactory = LayerUtil.repeat(1000L, ZoomLayer.NORMAL, landSeaFactory, 0, contextFactory);

        int biomeSize = 4;
        int riverSize = 4;
        if (settings != null) {
            biomeSize = large ? 6 : settings.getBiomeSize();
            riverSize = large ? 5 : settings.getRiverSize();
        }
        WorldType worldType = large ? WorldType.LARGE_BIOMES : WorldType.DEFAULT;
        biomeSize = LayerUtil.getModdedBiomeSize(worldType, biomeSize);

        landSeaFactory = LayerUtil.repeat(1000L, ZoomLayer.NORMAL, landSeaFactory, 2, contextFactory);

        for (int i = 0; i < biomeSize; ++i) {
            landSeaFactory = ZoomLayer.NORMAL.apply((IExtendedNoiseRandom)contextFactory.apply(1000 + i), landSeaFactory);
            if (i == 0) {
                landSeaFactory = AddIslandLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(3L), landSeaFactory);
            }
            if (i == 1 || biomeSize == 1) {
//                landSeaFactory = ShoreLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(1000L), landSeaFactory); // add edge biomes
            }
        }

        landSeaFactory = SmoothLayer.INSTANCE.apply((IExtendedNoiseRandom)contextFactory.apply(1000L), landSeaFactory);

        IAreaFactory<T> voronoiZoomBiomesLayer = VoroniZoomLayer.INSTANCE.apply(contextFactory.apply(10L), landSeaFactory);
        return ImmutableList.of(landSeaFactory, voronoiZoomBiomesLayer);
    }

    public static Layer[] buildMagicProcedure(long seed, boolean large, MagicGenSettings settings) {
        ImmutableList<IAreaFactory<LazyArea>> immutableList = buildMagicProcedure(settings, large, (contextFactory) -> new LazyAreaLayerContext(25, seed, contextFactory));
        Layer biomesLayer = new Layer(immutableList.get(0));
        Layer voronoiLayer = new Layer(immutableList.get(1));
        return new Layer[]{biomesLayer, voronoiLayer};
    }
}