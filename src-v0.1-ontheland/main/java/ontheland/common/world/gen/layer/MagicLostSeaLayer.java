package ontheland.common.world.gen.layer;

import net.minecraft.util.registry.Registry;
import net.minecraft.world.gen.INoiseRandom;
import net.minecraft.world.gen.layer.traits.ICastleTransformer;
import ontheland.api.biome.OTLBiomes;

public enum MagicLostSeaLayer implements ICastleTransformer {
    INSTANCE;

    private static final int LOST_SEA = Registry.BIOME.getId(OTLBiomes.lost_sea);

    @Override
    public int apply(INoiseRandom context, int north, int west, int south, int east, int center) {
        if (center == MLayerUtil.OCEAN) {
            int result = 0;
            if (north == MLayerUtil.OCEAN) result++;
            if (west == MLayerUtil.OCEAN) result++;
            if (south == MLayerUtil.OCEAN) result++;
            if (east == MLayerUtil.OCEAN) result++;
            if (result > 3) return LOST_SEA;
        }
        return center;
    }
}
