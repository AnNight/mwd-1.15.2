package ontheland.common.world.gen;

import net.minecraft.world.gen.GenerationSettings;

public class MagicGenSettings extends GenerationSettings {
    public int getBiomeSize() {
        return 4;
    }

    public int getRiverSize() {
        return 4;
    }

    @Override
    public int getBedrockFloorHeight() {
        return 0;
    }
}