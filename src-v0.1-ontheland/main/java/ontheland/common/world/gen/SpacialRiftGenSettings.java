package ontheland.common.world.gen;

import net.minecraft.world.gen.GenerationSettings;

public class SpacialRiftGenSettings extends GenerationSettings {
    @Override
    public int getBedrockFloorHeight() {
        return 0;
    }

    @Override
    public int getBedrockRoofHeight() {
        return 255;
    }
}