package ontheland.common.world.gen;

import net.minecraft.world.IWorld;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.NoiseChunkGenerator;

public class SpacialRiftChunkGenerator extends NoiseChunkGenerator<SpacialRiftGenSettings> {
    public SpacialRiftChunkGenerator(IWorld worldIn, BiomeProvider provider, SpacialRiftGenSettings settingsIn) {
        super(worldIn, provider, 2, 20, 256, settingsIn, true);
    }

    @Override
    protected double[] getBiomeNoiseColumn(int i, int i1) {
        return new double[]{(double) this.biomeProvider.func_222365_c(i, i1), 0.0D};
    }

    @Override
    protected void fillNoiseColumn(double[] noiseColumn, int noiseX, int noiseZ) {
        double d0 = 684.412D;
        double d1 = 684.412D;
        double d2 = 8.555149841308594D;
        double d3 = 4.277574920654297D;
        int i = 3, j = -10;
        this.func_222546_a(noiseColumn, noiseX, noiseZ, d0, d1, d2, d3, i, j);
    }

    protected double func_222545_a(double v, double v1, int i) {
        return 8.0D - v;
    }

    protected double func_222551_g() {
        return (double) ((int) super.func_222551_g() / 2);
    }

    protected double func_222553_h() {
        return 8.0D;
    }

    public int getGroundHeight() {
        return getSeaLevel() + 1;
    }

    public int getSeaLevel() {
        return 0;
    }
}
