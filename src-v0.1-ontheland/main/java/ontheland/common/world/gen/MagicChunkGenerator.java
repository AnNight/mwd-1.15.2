package ontheland.common.world.gen;

import net.minecraft.util.Util;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.NoiseChunkGenerator;
import net.minecraft.world.gen.OctavesNoiseGenerator;

public class MagicChunkGenerator extends NoiseChunkGenerator<MagicGenSettings> {
    private static final float[] FLOATS = Util.make(new float[25], (floats) -> {
        for (int i = -2; i <= 2; ++i) {
            for (int j = -2; j <= 2; ++j) {
                float f = 10.0F / MathHelper.sqrt((float) (i * i + j * j) + 0.2F);
                floats[i + 2 + (j + 2) * 5] = f;
            }
        }
    });
    private final OctavesNoiseGenerator depthNoise;

    public MagicChunkGenerator(IWorld world, BiomeProvider provider, MagicGenSettings genSettings) {
        super(world, provider, 4, 8, 256, genSettings, true);
        this.randomSeed.skip(666);
        this.depthNoise = new OctavesNoiseGenerator(this.randomSeed, 16);
    }

    @Override
    protected double[] getBiomeNoiseColumn(int noiseX, int noiseZ) {
        double[] doubles = new double[2];
        float f = 0.0F;
        float f1 = 0.0F;
        float f2 = 0.0F;
        float f3 = this.biomeProvider.getBiomeAtFactorFour(noiseX, noiseZ).getDepth();

        for (int j = -2; j <= 2; ++j) {
            for (int k = -2; k <= 2; ++k) {
                Biome biome = this.biomeProvider.getBiomeAtFactorFour(noiseX + j, noiseZ + k);
                float f4 = biome.getDepth();
                float f5 = biome.getScale();

                float f6 = FLOATS[j + 2 + (k + 2) * 5] / (f4 + 2.0F);
                if (biome.getDepth() > f3) {
                    f6 /= 2.0F;
                }

                f += f5 * f6;
                f1 += f4 * f6;
                f2 += f6;
            }
        }

        f = f / f2;
        f1 = f1 / f2;
        f = f * 0.9F + 0.1F;
        f1 = (f1 * 4.0F - 1.0F) / 8.0F;
        doubles[0] = (double) f1 + this.getNoiseDepthAt(noiseX, noiseZ);
        doubles[1] = (double) f;
        return doubles;
    }

    private double getNoiseDepthAt(int noiseX, int noiseZ) {
        double result = this.depthNoise.getValue((double) (noiseX * 200), 10.0D, (double) (noiseZ * 200), 1.0D, 0.0D, true) / 8000.0D;
        if (result < 0.0D) {
            result = -result * 0.3D;
        }
        result = result * 3.0D - 2.0D;
        if (result < 0.0D) {
            result = result / 28.0D;
        } else {
            if (result > 1.0D) {
                result = 1.0D;
            }
            result = result / 40.0D;
        }
        return result;
    }

    @Override
    protected double func_222545_a(double p_222545_1_, double p_222545_3_, int p_222545_5_) {
        double d0 = 8.5D;
        double d1 = ((double)p_222545_5_ - (d0 + p_222545_1_ * d0 / 8.0D * 4.0D)) * 12.0D * 128.0D / 256.0D / p_222545_3_;
        if (d1 < 0.0D) {
            d1 *= 4.0D;
        }
        return d1;
    }

    @Override
    protected void fillNoiseColumn(double[] noiseColumn, int noiseX, int noiseZ) {
        double d0 = 684.412D;
        double d1 = 684.412D;
        double d2 = 8.555149841308594D;
        double d3 = 4.277574920654297D;
        int i = 3, j = -10;
        this.func_222546_a(noiseColumn, noiseX, noiseZ, d0, d1, d2, d3, i, j);
    }

    @Override
    public int getGroundHeight() {
        return this.getSeaLevel() + 1;
    }
}