package ontheland.common.capability.mana;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class ManaStorage implements Capability.IStorage<IMana> {
    @Nullable
    @Override
    public INBT writeNBT(Capability<IMana> capability, IMana instance, Direction side) {
        CompoundNBT compoundNBT = new CompoundNBT();
        compoundNBT.putInt("otl.mana.limit", instance.getLimit());
        compoundNBT.putInt("otl.mana.value", instance.getValue());
        compoundNBT.putBoolean("otl.mana.lock", instance.getLock());
        return compoundNBT;
    }

    @Override
    public void readNBT(Capability<IMana> capability, IMana instance, Direction side, INBT nbt) {
        CompoundNBT compoundNBT = (CompoundNBT) nbt;
        instance.setLimit(compoundNBT.getInt("otl.mana.limit"));
        instance.setValue(compoundNBT.getInt("otl.mana.value"));
        instance.setLock(compoundNBT.getBoolean("otl.mana.lock"));
    }
}