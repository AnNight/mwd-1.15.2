package ontheland.common.capability.mana;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import ontheland.network.PacketManager;
import ontheland.network.UpdateClientMana;

public class Mana implements IMana {
    private int value;
    private int limit;
    private boolean lock;

    public Mana() {
        value = 8;
        limit = 8;
        lock = false;
    }

    @Override
    public int getMax() {
        return MAX;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public int getLimit() {
        return limit;
    }

    @Override
    public boolean getLock() {
        return lock;
    }

    /**
     * When the new value is available, set the current value to the new value and return true.
     * When the new value is unavailable, return false.
     */
    @Override
    public boolean setValue(int value) {
        check();
        if (value <= limit && value >= 0) {
            this.value = value;
            return true;
        } else return false;
    }

    @Override
    public boolean setLimit(int value) {
        check();
        if (value <= MAX && value >= 0) {
            this.limit = value;
            if (this.value > this.limit) this.value = this.limit;
            return true;
        } else return false;
    }

    @Override
    public void setLock(boolean value) {
        check();
        this.lock = value;
    }

    @Override
    public boolean limitedIncreaseValue(int value) {
        check();
        if (this.lock) return false;
        else {
            int newValue = this.value + value;
            this.value = newValue < 0 ? 0 : Math.min(newValue, this.limit);
            return true;
        }
    }

    @Override
    public boolean limitedReduceValue(int value) {
        check();
        if (this.lock) return false;
        else {
            int newValue = this.value - value;
            this.value = newValue < 0 ? 0 : Math.min(newValue, this.limit);
            return true;
        }
    }

    @Override
    public boolean limitedIncreaseLimit(int value) {
        check();
        if (this.lock) return false;
        else {
            int newValue = this.limit + value;
            this.limit = newValue < 0 ? 0 : Math.min(newValue, MAX);
            if (this.value > this.limit) this.value = this.limit;
            return true;
        }
    }

    @Override
    public boolean limitedReduceLimit(int value) {
        check();
        if (this.lock) return false;
        else {
            int newValue = this.limit - value;
            this.limit = newValue < 0 ? 0 : Math.min(newValue, MAX);
            if (this.value > this.limit) this.value = this.limit;
            return true;
        }
    }

    @Override
    public void unlimitedIncreaseValue(int value) {
        check();
        int newValue = this.value + value;
        this.value = newValue < 0 ? 0 : Math.min(newValue, this.limit);
    }

    @Override
    public void unlimitedReduceValue(int value) {
        check();
        int newValue = this.value - value;
        this.value = newValue < 0 ? 0 : Math.min(newValue, this.limit);
    }

    @Override
    public void unlimitedIncreaseLimit(int value) {
        check();
        int newValue = this.limit + value;
        this.limit = newValue < 0 ? 0 : Math.min(newValue, MAX);
        if (this.value > this.limit) this.value = this.limit;
    }

    @Override
    public void unlimitedReduceLimit(int value) {
        check();
        int newValue = this.limit - value;
        this.limit = newValue < 0 ? 0 : Math.min(newValue, MAX);
        if (this.value > this.limit) this.value = this.limit;
    }

    @Override
    public void copyForRespawn(IMana oldCap) {
        this.setLimit(oldCap.getLimit());
        this.setValue(oldCap.getValue());
        this.setLock(oldCap.getLock());
    }

    private void check() {
        if (limit < 0) limit = 0;
        if (limit > MAX) limit = MAX;
        if (value < 0) value = 0;
        if (value > limit) value = limit;
    }

    public static IMana getFromPlayer(LivingEntity player) {
        return player.getCapability(ManaProvider.MANA_CAP, null).orElseThrow(() -> new IllegalArgumentException("OnTheLand[Mana Capability]: LazyOptional must not be empty!"));
    }

    public static void updateClient(ServerPlayerEntity player, IMana cap) {
        //noinspection ConstantConditions
        PacketManager.sendManaTo(player, new UpdateClientMana(player.getEntityId(), (CompoundNBT) ManaProvider.MANA_CAP.writeNBT(cap, null)));
    }
}