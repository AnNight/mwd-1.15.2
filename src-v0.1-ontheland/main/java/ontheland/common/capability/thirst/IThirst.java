package ontheland.common.capability.thirst;

import net.minecraft.entity.player.PlayerEntity;

public interface IThirst {
    public static final int MAX = 50;
    public static final int INNER_MAX = 3600;

    public abstract int getMax();

    public abstract int getInner();

    public abstract int getValue();

    public abstract boolean getLock();

    public abstract boolean setInner(int value);

    public abstract boolean setValue(int value);

    public abstract void setLock(boolean value);

    public abstract boolean limitedIncrease(int value);

    public abstract boolean limitedReduce(int value);

    public abstract void unlimitedIncrease(int value);

    public abstract void unlimitedReduce(int value);

    public abstract void reduceInner(int value);

    public abstract void tick(PlayerEntity player);

    public abstract void copyForRespawn(IThirst oldCap);
}