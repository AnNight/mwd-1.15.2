package ontheland.common.capability.thirst;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ThirstProvider implements ICapabilitySerializable<CompoundNBT> {
    @CapabilityInject(IThirst.class)
    public static final Capability<IThirst> THIRST_CAP = null;

    private final LazyOptional<IThirst> instance = LazyOptional.of(THIRST_CAP::getDefaultInstance);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return cap == THIRST_CAP ? instance.cast() : LazyOptional.empty();
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap) {
        return getCapability(cap, null);
    }

    @Override
    public CompoundNBT serializeNBT() {
        return (CompoundNBT) THIRST_CAP.getStorage().writeNBT(THIRST_CAP, this.instance.orElseThrow(() -> new IllegalArgumentException("OnTheLand[Thirst]: LazyOptional must not be empty!")), null);
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        THIRST_CAP.getStorage().readNBT(THIRST_CAP, this.instance.orElseThrow(() -> new IllegalArgumentException("OnTheLand[Thirst]: LazyOptional must not be empty!")),null, nbt);
    }
}