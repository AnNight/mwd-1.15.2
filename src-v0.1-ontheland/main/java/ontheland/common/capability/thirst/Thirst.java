package ontheland.common.capability.thirst;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import ontheland.common.OTLDamageSource;
import ontheland.network.PacketManager;
import ontheland.network.UpdateClientThirst;

import java.util.Random;

public class Thirst implements IThirst {
    private int value;
    private int inner;
    private byte second = 0;
    private boolean lock;

    public Thirst() {
        value = 50;
        inner = INNER_MAX;
        lock = false;
    }

    @Override
    public int getMax() {
        return MAX;
    }

    @Override
    public int getInner() {
        return inner;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public boolean getLock() {
        return lock;
    }

    @Override
    public boolean setInner(int value) {
        if (value <= INNER_MAX && value >= 0) {
            this.inner = value;
            return true;
        }
        return false;
    }

    /**
     * When the new value is available, set the current value to the new value and return true.
     * When the new value is unavailable, return false.
     */
    @Override
    public boolean setValue(int value) {
        check();
        if (value <= MAX && value >= 0) {
            this.value = value;
            return true;
        } else return false;
    }

    @Override
    public void setLock(boolean value) {
        check();
        this.lock = value;
    }

    /**
     * When the lock is true, return false.
     * When the new value is less then the minimum, set to the minimum and return true.
     * When the new value is between the minimum and the maximum, set to the new value and return true.
     * When the new value is greater then the maximum, set to the maximum and return true.
     */
    @Override
    public boolean limitedIncrease(int value) {
        check();
        if (this.lock) return false;
        else {
            int newValue = this.value + value;
            this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
            return true;
        }
    }

    @Override
    public boolean limitedReduce(int value) {
        check();
        if (this.lock) return false;
        else {
            int newValue = this.value - value;
            this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
            return true;
        }
    }

    /**
     * When the new value is less then the minimum, set to the minimum.
     * When the new value is between the minimum and the maximum, set to the new value.
     * When the new value is greater then the maximum, set to the maximum.
     */
    @Override
    public void unlimitedIncrease(int value) {
        check();
        int newValue = this.value + value;
        this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
    }

    @Override
    public void unlimitedReduce(int value) {
        check();
        int newValue = this.value - value;
        this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
    }

    /**
     * @param value The value is subtracted from the player's inner
     */
    @Override
    public void reduceInner(int value) {
        if (!lock && value > 0) {
            for (; value > 0; value--) {
                inner--;
                if (inner < 0) {
                    this.value--;
                    inner = INNER_MAX;
                }
            }
        }
    }

    /**
     * The basic
     * ontheland.handler.EventHandler#onPlayerTick
     */
    @Override
    public void tick(PlayerEntity player) {
        if (!lock) {
            if (value > 0) {
                reduceInner(1);
                if (value < 25)
                    reduceInner(1);
            }
            if (value <= 25) {
                second--;
                if (second < 0) {
                    second = 20;
                    player.addPotionEffect(new EffectInstance(Effects.WEAKNESS, 60));
                    if (value == 0) {
                        player.addPotionEffect(new EffectInstance(Effects.NAUSEA, 100));
                        player.attackEntityFrom(OTLDamageSource.PARCHED, 1F);
                    }
                }
            }
        }
    }

    @Override
    public void copyForRespawn(IThirst oldCap) {
        this.setValue(oldCap.getValue());
        this.setLock(oldCap.getLock());
        this.setInner(oldCap.getInner());
    }

    private void check() {
        if (value < 0) value = 0;
        if (value > MAX) value = MAX;
        if (inner < 0) inner = 0;
        if (inner > INNER_MAX) inner = INNER_MAX;
    }

    public static IThirst getFromPlayer(LivingEntity player) {
        return player.getCapability(ThirstProvider.THIRST_CAP, null).orElseThrow(() -> new IllegalArgumentException("OnTheLand[Thirst Capability]: LazyOptional must not be empty!"));
    }

    public static void updateClient(ServerPlayerEntity player, IThirst cap) {
        //noinspection ConstantConditions
        PacketManager.sendThirstTo(player, new UpdateClientThirst(player.getEntityId(), (CompoundNBT) ThirstProvider.THIRST_CAP.writeNBT(cap, null)));
    }
}