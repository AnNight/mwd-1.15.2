package ontheland.common.capability.thirst;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class ThirstStorage implements Capability.IStorage<IThirst> {
    @Nullable
    @Override
    public INBT writeNBT(Capability<IThirst> capability, IThirst instance, Direction side) {
        CompoundNBT compoundNBT = new CompoundNBT();
        compoundNBT.putInt("otl.thirst.inner", instance.getInner());
        compoundNBT.putInt("otl.thirst.value", instance.getValue());
        compoundNBT.putBoolean("otl.thirst.lock", instance.getLock());
        return compoundNBT;
    }

    @Override
    public void readNBT(Capability<IThirst> capability, IThirst instance, Direction side, INBT nbt) {
        CompoundNBT compoundNBT = (CompoundNBT) nbt;
        instance.setInner(compoundNBT.getInt("otl.thirst.inner"));
        instance.setValue(compoundNBT.getInt("otl.thirst.value"));
        instance.setLock(compoundNBT.getBoolean("otl.thirst.lock"));
    }
}