package ontheland.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MaterialColor;
import ontheland.api.material.OTLMaterial;
import ontheland.api.tooltype.OTLToolType;

public class StoneBlock extends Block {
    public StoneBlock(MaterialColor materialColor) {
        super(Properties.create(OTLMaterial.MOD_STONE, materialColor)
                .harvestTool(OTLToolType.HAMMER)
                .sound(SoundType.STONE)
                .hardnessAndResistance(1.5F)
                .harvestLevel(0)
        );
    }
}
