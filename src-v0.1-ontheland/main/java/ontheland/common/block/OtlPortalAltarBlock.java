package ontheland.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import ontheland.OnTheLand;
import ontheland.api.block.OTLBlocks;
import ontheland.api.soundevent.OTLSoundEvents;

import javax.annotation.Nullable;
import java.util.Random;

public class OtlPortalAltarBlock extends Block {
    public OtlPortalAltarBlock() {
        super(Properties.create(Material.ROCK, MaterialColor.SNOW)
                .doesNotBlockMovement()
                .hardnessAndResistance(2.0F)
                .lightValue(11)
                .sound(SoundType.STONE)
        );
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        boolean conformation = true;
        int y = pos.getY();
        BlockPos.MutableBlockPos mutableBlockPos = new BlockPos.MutableBlockPos(pos);
        for (int x = pos.getX() - 1; x <= pos.getX() + 1; x++)
            for (int z = pos.getZ() - 1; z <= pos.getZ() + 1; z++)
                if (worldIn.getBlockState(mutableBlockPos.setPos(x, y, z)).getBlock() != OTLBlocks.otl_portal_frame && !(x == pos.getX() && z == pos.getZ())) {
                    conformation = false;
                    break;
                }
        if (conformation) worldIn.playSound(pos.getX() + 0.5D, pos.getY() + 0.5D, pos.getZ() + 0.5D, OTLSoundEvents.PORTAL_CREATE, SoundCategory.AMBIENT, 0.5F, 0.8F, false);
        if (conformation && !worldIn.isRemote)
            for (int x = pos.getX() - 1; x <= pos.getX() + 1; x++)
                for (int z = pos.getZ() - 1; z <= pos.getZ() + 1; z++)
                    if (!(x == pos.getX() && z == pos.getZ()))
                        worldIn.setBlockState(mutableBlockPos.setPos(x, y, z), OTLBlocks.otl_portal.getDefaultState(), 3);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        double d0 = ((float)pos.getX() + rand.nextFloat());
        double d1 = ((float)pos.getY() + 0.8F);
        double d2 = ((float)pos.getZ() + rand.nextFloat());
        worldIn.addParticle(ParticleTypes.SMOKE, d0, d1, d2, 0.0D, 0.0D, 0.0D);
    }
}
