package ontheland.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;

public class DoorBlock extends net.minecraft.block.DoorBlock {
    public DoorBlock(MaterialColor color) {
        super(Block.Properties.create(Material.WOOD, color).hardnessAndResistance(3.0F).sound(SoundType.WOOD));
    }
}
