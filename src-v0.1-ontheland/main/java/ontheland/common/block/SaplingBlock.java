package ontheland.common.block;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.trees.Tree;

public class SaplingBlock extends net.minecraft.block.SaplingBlock {
    public SaplingBlock(Tree tree) {
        super(tree, Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.PLANT));
    }
}
