package ontheland.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class WoodenLightingBlock extends Block {
    private static final BooleanProperty LIT = BlockStateProperties.LIT;

    public WoodenLightingBlock() {
        super(Properties.create(Material.WOOD, MaterialColor.BROWN).hardnessAndResistance(1.5F).lightValue(13));
        this.setDefaultState(this.stateContainer.getBaseState().with(LIT, false));
    }

    @Override
    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote) {
            interactWith(state, worldIn, pos);
        }
        return true;
    }

    protected void interactWith(BlockState state, World world, BlockPos pos) {
        if (state.get(LIT)) {
            world.setBlockState(pos, state.with(LIT, false), 2);
        } else {
            world.setBlockState(pos, state.cycle(LIT), 2);
        }
    }

    public int getLightValue(BlockState state) {
        return state.get(LIT) ? super.getLightValue(state) : 0;
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(LIT, false);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(LIT);
    }
}