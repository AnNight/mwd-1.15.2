package ontheland.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.Effect;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.Random;

public class FlowerBlock extends net.minecraft.block.FlowerBlock {
    public FlowerBlock(Effect effect, int effectDuration, int lightValue) {
        super(effect, effectDuration, Block.Properties.create(Material.PLANTS).doesNotBlockMovement().hardnessAndResistance(0f).lightValue(lightValue).sound(SoundType.PLANT));
    }
}