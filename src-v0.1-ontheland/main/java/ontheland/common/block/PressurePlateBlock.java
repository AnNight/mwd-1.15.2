package ontheland.common.block;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;

public class PressurePlateBlock extends net.minecraft.block.PressurePlateBlock {
    public PressurePlateBlock(MaterialColor color) {
        super(net.minecraft.block.PressurePlateBlock.Sensitivity.EVERYTHING, Properties.create(Material.WOOD, color).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD));
    }
}
