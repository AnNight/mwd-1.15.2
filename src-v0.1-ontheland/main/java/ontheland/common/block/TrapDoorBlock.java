package ontheland.common.block;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;

public class TrapDoorBlock extends net.minecraft.block.TrapDoorBlock {
    public TrapDoorBlock(MaterialColor color) {
        super(Properties.create(Material.WOOD, color).hardnessAndResistance(3.0F).sound(SoundType.WOOD));
    }
}
