package ontheland.common.block.trees;

import net.minecraft.block.trees.Tree;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import ontheland.api.block.OTLBlocks;
import ontheland.common.world.gen.feature.CyanTreeFeature;

import javax.annotation.Nullable;
import java.util.Random;

public class CyanTree extends Tree {
    @Nullable
    @Override
    protected AbstractTreeFeature<NoFeatureConfig> getTreeFeature(Random random) {
        return new CyanTreeFeature(NoFeatureConfig::deserialize, true, false, OTLBlocks.cyan_tree_log.getDefaultState(), OTLBlocks.cyan_tree_leaves.getDefaultState(), OTLBlocks.cyan_tree_sapling);
    }
}