package ontheland.common.block.trees;

import net.minecraft.block.trees.Tree;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import ontheland.api.block.OTLBlocks;
import ontheland.common.world.gen.feature.PinusTreeFeature;

import javax.annotation.Nullable;
import java.util.Random;

public class PinusCaribaeaTree extends Tree {
    @Nullable
    protected AbstractTreeFeature<NoFeatureConfig> getTreeFeature(Random random) {
        return new PinusTreeFeature(NoFeatureConfig::deserialize, true, false, OTLBlocks.pinus_caribaea_log.getDefaultState(), OTLBlocks.pinus_caribaea_leaves.getDefaultState(), OTLBlocks.pinus_caribaea_sapling);
    }
}