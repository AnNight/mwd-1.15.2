package ontheland.common.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;

public class LogBlock extends net.minecraft.block.LogBlock {
    public LogBlock(MaterialColor color) {
        super(color, Properties.create(Material.WOOD, color).hardnessAndResistance(2.0F).sound(SoundType.WOOD));
    }

    public LogBlock(MaterialColor colorTop, MaterialColor colorSide) {
        super(colorTop, Properties.create(Material.WOOD, colorSide).hardnessAndResistance(2.0F).sound(SoundType.WOOD));
    }
    /* private Boolean canStrip = false;
    private BlockState stripped;

    public LogBlock(MaterialColor colorTop, MaterialColor colorSide, Block stripped) {
        super(colorTop, Properties.create(Material.WOOD, colorSide).hardnessAndResistance(2.0F).sound(SoundType.WOOD));
        this.canStrip = true;
        this.stripped = stripped.getDefaultState();
    }

    // right
    @Override
    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote) {
            Item mainHandItem = player.getHeldItemMainhand().getItem();
            if ((mainHandItem == Items.WOODEN_AXE || mainHandItem == Items.STONE_AXE || mainHandItem == Items.IRON_AXE || mainHandItem == Items.GOLDEN_AXE || mainHandItem == Items.DIAMOND_AXE) && canStrip) {
                worldIn.playSound(player, pos, SoundEvents.ITEM_AXE_STRIP, SoundCategory.BLOCKS, 1.0F, 1.0F);
                worldIn.setBlockState(pos, stripped.with(RotatedPillarBlock.AXIS, state.get(RotatedPillarBlock.AXIS)), 11);
                player.getHeldItemMainhand().damageItem(1, player, (p_220040_1_) -> {
                    p_220040_1_.sendBreakAnimation(player.getActiveHand());
                });
                return true;
            }
        }
        return false;
    }
    */
}