package ontheland.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import ontheland.common.world.dimension.OTLDimensions;
import ontheland.common.world.dimension.TeleporterManager;

import java.util.Random;

public class MagicDimensionPortalBlock extends Block {
    public MagicDimensionPortalBlock() {
        super(Properties.create(Material.PORTAL, MaterialColor.BLACK)
                .doesNotBlockMovement()
                .tickRandomly()
                .hardnessAndResistance(-1.0F, 3600000.0F)
                .sound(SoundType.GLASS)
                .lightValue(11)
                .noDrops()
        );
    }

    public ItemStack getItem(IBlockReader worldIn, BlockPos pos, BlockState state) { return ItemStack.EMPTY; }

    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.TRANSLUCENT;
    }

    @Override
    public void onEntityCollision(BlockState state, World worldIn, BlockPos pos, Entity entityIn) {
        if (entityIn instanceof PlayerEntity)
            if (worldIn.dimension.getType() == DimensionType.OVERWORLD || worldIn.dimension.getType() == OTLDimensions.MAGIC_DIM_TYPE)
                TeleporterManager.teleportCheck((PlayerEntity) entityIn, worldIn, pos, worldIn.dimension.getType() == DimensionType.OVERWORLD ? OTLDimensions.MAGIC_DIM_TYPE : DimensionType.OVERWORLD);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        if (rand.nextInt(200) == 0) {
            worldIn.playSound(
                    (double)pos.getX() + 0.5D, (double)pos.getY() + 0.5D, (double)pos.getZ() + 0.5D,
                    SoundEvents.BLOCK_PORTAL_AMBIENT, SoundCategory.BLOCKS,
                    0.5F, rand.nextFloat() * 0.4F + 0.8F, false
            );
        }
        double d0 = ((float)pos.getX() + rand.nextFloat());
        double d1 = ((float)pos.getY() + 0.8F);
        double d2 = ((float)pos.getZ() + rand.nextFloat());
        worldIn.addParticle(ParticleTypes.SMOKE, d0, d1, d2, 0.0D, 0.0D, 0.0D);
    }
}