package ontheland.common;

import net.minecraft.util.DamageSource;

public class OTLDamageSource {
    public static final DamageSource PARCHED = (new DamageSource("parched")).setDamageBypassesArmor().setDamageIsAbsolute();
}