package ontheland;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import ontheland.common.capability.mana.IMana;
import ontheland.common.capability.mana.Mana;
import ontheland.common.capability.mana.ManaStorage;
import ontheland.common.capability.thirst.IThirst;
import ontheland.common.capability.thirst.Thirst;
import ontheland.common.capability.thirst.ThirstStorage;
import ontheland.event.EventHandler;
import ontheland.init.Init;
import ontheland.network.PacketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(OnTheLand.MOD_ID)
public class OnTheLand {
    public static final String MOD_ID = "ontheland";
    public static final Logger LOGGER = LogManager.getLogger();

    public OnTheLand() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::common);
    }

    void common(final FMLCommonSetupEvent event) {
        MinecraftForge.EVENT_BUS.register(new EventHandler());
        Init.initBiomes();
        Init.initOres();
        Init.initFlammability();
        Init.initStrippability();
        CapabilityManager.INSTANCE.register(IThirst.class, new ThirstStorage(), Thirst::new);
        CapabilityManager.INSTANCE.register(IMana.class, new ManaStorage(), Mana::new);
        PacketManager.register();
    }
}