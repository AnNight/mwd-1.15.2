package ontheland.init;

import com.google.common.collect.Maps;
import io.netty.buffer.Unpooled;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.FireBlock;
import net.minecraft.item.AxeItem;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.registries.ForgeRegistries;
import ontheland.api.biome.OTLBiomes;
import ontheland.api.block.OTLBlocks;
import ontheland.common.world.dimension.OTLDimensions;

public class Init {
    public static void initBiomes() {
        BiomeManager.addBiome(BiomeManager.BiomeType.COOL, new BiomeManager.BiomeEntry(OTLBiomes.pinus_caribaea_forest, 10));
        BiomeManager.addBiome(BiomeManager.BiomeType.COOL, new BiomeManager.BiomeEntry(OTLBiomes.pinus_palustris_forest, 10));
        BiomeManager.addBiome(BiomeManager.BiomeType.COOL, new BiomeManager.BiomeEntry(OTLBiomes.pinus_taeda_forest, 10));
        BiomeManager.addSpawnBiome(OTLBiomes.pinus_caribaea_forest);
        BiomeManager.addSpawnBiome(OTLBiomes.pinus_palustris_forest);
        BiomeManager.addSpawnBiome(OTLBiomes.pinus_taeda_forest);
        BiomeProvider.BIOMES_TO_SPAWN_IN.add(OTLBiomes.pinus_caribaea_forest);
        BiomeProvider.BIOMES_TO_SPAWN_IN.add(OTLBiomes.pinus_palustris_forest);
        BiomeProvider.BIOMES_TO_SPAWN_IN.add(OTLBiomes.pinus_taeda_forest);
    }

    public static void initStrippability() {
        // logs
        registerStrippability(OTLBlocks.pinus_caribaea_log, OTLBlocks.stripped_pinus_caribaea_log);
        registerStrippability(OTLBlocks.pinus_palustris_log, OTLBlocks.stripped_pinus_palustris_log);
        registerStrippability(OTLBlocks.pinus_taeda_log, OTLBlocks.stripped_pinus_taeda_log);
        registerStrippability(OTLBlocks.cyan_tree_log, OTLBlocks.stripped_cyan_tree_log);
        // woods
        registerStrippability(OTLBlocks.pinus_caribaea_wood, OTLBlocks.stripped_pinus_caribaea_wood);
        registerStrippability(OTLBlocks.pinus_palustris_wood, OTLBlocks.stripped_pinus_palustris_wood);
        registerStrippability(OTLBlocks.pinus_taeda_wood, OTLBlocks.stripped_pinus_taeda_wood);
        registerStrippability(OTLBlocks.cyan_tree_wood, OTLBlocks.stripped_cyan_tree_wood);
    }

    private static void registerStrippability(Block original, Block stripped) {
        AxeItem.BLOCK_STRIPPING_MAP = Maps.newHashMap(AxeItem.BLOCK_STRIPPING_MAP);
        AxeItem.BLOCK_STRIPPING_MAP.put(original, stripped);
    }

    public static void initOres() {
        for (Biome biome : ForgeRegistries.BIOMES) {
            if (biome != Biomes.NETHER && biome != Biomes.END_BARRENS && biome != Biomes.END_HIGHLANDS && biome != Biomes.END_MIDLANDS) {
                addOres(biome, OTLBlocks.ascharite_stone, 12, 3, 62); // 硼镁
                addOres(biome, OTLBlocks.calcium_carbonate_stone, 20, 2, 80); // 石灰
                addOres(biome, OTLBlocks.colemanite_stone, 12, 3, 62); // Ying Peng Gai
                addOres(biome, OTLBlocks.dolomite_stone, 10, 4, 62); // 白云
                addOres(biome, OTLBlocks.pyrophyllite_stone, 10, 4, 62); // 叶腊
            }
        }
    }

    private static void addOres(Biome biome, Block block, int size, int count, int maximum) {
        biome.addFeature(
                GenerationStage.Decoration.UNDERGROUND_ORES,
                Biome.createDecoratedFeature(
                        Feature.ORE,
                        new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, block.getDefaultState(), size),
                        Placement.COUNT_RANGE,
                        new CountRangeConfig(count, 0, 0, maximum)
                )
        );
    }

    public static void initFlammability() {
        // Planks, slabs, stairs and fences.
        registerFlammability(OTLBlocks.cyan_tree_planks, 5, 20);
        registerFlammability(OTLBlocks.pinus_caribaea_planks, 5, 20);
        registerFlammability(OTLBlocks.pinus_palustris_planks, 5, 20);
        registerFlammability(OTLBlocks.pinus_taeda_planks, 5, 20);
        registerFlammability(OTLBlocks.pinus_caribaea_slab, 5, 20);
        registerFlammability(OTLBlocks.pinus_palustris_slab, 5, 20);
        registerFlammability(OTLBlocks.pinus_taeda_slab, 5, 20);
        registerFlammability(OTLBlocks.pinus_caribaea_stairs, 5, 20);
        registerFlammability(OTLBlocks.pinus_palustris_stairs, 5, 20);
        registerFlammability(OTLBlocks.pinus_taeda_stairs, 5, 20);
        registerFlammability(OTLBlocks.pinus_caribaea_fence, 5, 20);
        registerFlammability(OTLBlocks.pinus_palustris_fence, 5, 20);
        registerFlammability(OTLBlocks.pinus_taeda_fence, 5, 20);
        registerFlammability(OTLBlocks.pinus_caribaea_fence_gate, 5, 20);
        registerFlammability(OTLBlocks.pinus_palustris_fence_gate, 5, 20);
        registerFlammability(OTLBlocks.pinus_taeda_fence_gate, 5, 20);
        // Logs
        registerFlammability(OTLBlocks.cyan_tree_log, 5, 5);
        registerFlammability(OTLBlocks.pinus_caribaea_log, 5, 5);
        registerFlammability(OTLBlocks.pinus_palustris_log, 5, 5);
        registerFlammability(OTLBlocks.pinus_taeda_log, 5, 5);
        // Woods
        registerFlammability(OTLBlocks.pinus_caribaea_wood, 5, 5);
        registerFlammability(OTLBlocks.pinus_palustris_wood, 5, 5);
        registerFlammability(OTLBlocks.pinus_taeda_wood, 5, 5);
        // Stripped logs
        registerFlammability(OTLBlocks.stripped_cyan_tree_log, 5, 5);
        registerFlammability(OTLBlocks.stripped_pinus_caribaea_log, 5, 5);
        registerFlammability(OTLBlocks.stripped_pinus_palustris_log, 5, 5);
        registerFlammability(OTLBlocks.stripped_pinus_taeda_log, 5, 5);
        // Stripped woods
        registerFlammability(OTLBlocks.stripped_cyan_tree_wood, 5, 5);
        registerFlammability(OTLBlocks.stripped_pinus_caribaea_wood, 5, 5);
        registerFlammability(OTLBlocks.stripped_pinus_palustris_wood, 5, 5);
        registerFlammability(OTLBlocks.stripped_pinus_taeda_wood, 5, 5);
        // Leaves and flowers
        registerFlammability(OTLBlocks.cyan_tree_leaves, 30, 60);
        registerFlammability(OTLBlocks.pinus_caribaea_leaves, 30, 60);
        registerFlammability(OTLBlocks.pinus_palustris_leaves, 30, 60);
        registerFlammability(OTLBlocks.pinus_taeda_leaves, 30, 60);
        registerFlammability(OTLBlocks.hai_flower, 60, 100);
        registerFlammability(OTLBlocks.ling_flower, 60, 100);
        registerFlammability(OTLBlocks.ji_flower, 60, 100);
        registerFlammability(OTLBlocks.long_flower, 60, 100);
        registerFlammability(OTLBlocks.ming_flower, 60, 100);
        registerFlammability(OTLBlocks.ye_flower, 60, 100);
        registerFlammability(OTLBlocks.liu_flower, 60, 100);
        registerFlammability(OTLBlocks.qing_flower, 60, 100);
        registerFlammability(OTLBlocks.xin_flower, 60, 100);
    }

    private static void registerFlammability(Block blockIn, int encouragement, int flammability) {
        FireBlock fireblock = (FireBlock) Blocks.FIRE;
        fireblock.setFireInfo(blockIn, encouragement, flammability);
    }
}
