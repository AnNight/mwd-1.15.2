package ontheland.init;

import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import ontheland.OnTheLand;
import ontheland.api.biome.OTLBiomes;
import ontheland.common.world.biome.magic.*;
import ontheland.common.world.biome.overland.*;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModBiomes {
    @SubscribeEvent
    public static void onRegisterBiomes(RegistryEvent.Register<Biome> event) {
        OTLBiomes.pinus_caribaea_forest = register(new PinusCaribaeaForestBiome(), BiomeDictionary.Type.FOREST,"pinus_caribaea_forest");
        OTLBiomes.pinus_palustris_forest = register(new PinusPalustrisForestBiome(), BiomeDictionary.Type.FOREST,"pinus_palustris_forest");
        OTLBiomes.pinus_taeda_forest = register(new PinusTaedaForestBiome(), BiomeDictionary.Type.FOREST,"pinus_taeda_forest");
        /////////////////////////////////////////////////////////////////////
        // Mainland
        OTLBiomes.magic_plains = register(new MagicPlainsBiome(), BiomeDictionary.Type.PLAINS, "magic_plains");
        OTLBiomes.wild_mainland = register(new WildMainlandBiome(), BiomeDictionary.Type.HOT, "wild_mainland");
        OTLBiomes.witchcraft_mainland = register(new WitchcraftMainlandBiome(), BiomeDictionary.Type.FOREST, "witchcraft_mainland");
        OTLBiomes.dark_mainland = register(new DarkMainlandBiome(), BiomeDictionary.Type.FOREST, "dark_mainland");
        OTLBiomes.relic_mainland = register(new RelicMainlandBiome(), BiomeDictionary.Type.PLAINS, "relic_mainland");
        OTLBiomes.magic_forest = register(new MagicForestBiome(), BiomeDictionary.Type.FOREST, "magic_forest");
        // Ocean
        OTLBiomes.magic_ocean = register(new MagicOceanBiome(), BiomeDictionary.Type.OCEAN, "magic_ocean");
        OTLBiomes.lost_sea = register(new LostSeaBiome(), BiomeDictionary.Type.OCEAN, "lost_sea");
        // Island
        OTLBiomes.magic_island = register(new MagicIslandBiome(), BiomeDictionary.Type.PLAINS, "magic_island");
        OTLBiomes.gas_island = register(new GasIslandBiome(), BiomeDictionary.Type.PLAINS, "gas_island");
        OTLBiomes.volcanic_island = register(new VolcanicIslandBiome(), BiomeDictionary.Type.PLAINS, "volcanic_island");
        /////////////////////////////////////////////////////////////////////
    }

    private static Biome register(Biome biome, BiomeDictionary.Type type, String name) {
        biome.setRegistryName(OnTheLand.MOD_ID, name);
        ForgeRegistries.BIOMES.register(biome);
        BiomeDictionary.addTypes(biome, type);
        BiomeManager.addSpawnBiome(biome);
        return biome;
    }
}