package ontheland.init;

import net.minecraft.item.Item;
import net.minecraft.item.ItemTier;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import ontheland.api.item.OTLItems;
import ontheland.common.item.CuttingDeviceItem;
import ontheland.common.item.Foods;
import ontheland.common.item.HammerItem;
import ontheland.api.group.OTLGroup;
import ontheland.common.item.Food;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModItems {
    @SubscribeEvent
    public static void onRegisterItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(OTLItems.cyan_tree_leaves, OTLItems.amber_ore, OTLItems.pinus_caribaea_leaves, OTLItems.pinus_palustris_leaves, OTLItems.pinus_taeda_leaves);
        // fruit
        OTLItems.cyan_fruit = registerItem(new Food(Foods.FRUIT), "cyan_fruit");
        OTLItems.ascharite_stone_debris = registerItem("ascharite_stone_debris");
        OTLItems.calcium_carbonate_stone_debris = registerItem("calcium_carbonate_stone_debris");
        OTLItems.colemanite_stone_debris = registerItem("colemanite_stone_debris");
        OTLItems.dolomite_stone_debris = registerItem("dolomite_stone_debris");
        OTLItems.pyrophyllite_stone_debris = registerItem("pyrophyllite_stone_debris");
        OTLItems.timber = registerItem("timber");
        OTLItems.stone_stock = registerItem("stone_stock");
        OTLItems.pine_cone = registerItem("pine_cone");
        OTLItems.pine_nut = registerItem(new Food(Foods.PINE_NUT),"pine_nut");
        OTLItems.blade = registerItem("blade");
        OTLItems.amber = registerItem("amber");
        OTLItems.crankshaft = registerItem("crankshaft");
        OTLItems.connecting_rod = registerItem("connecting_rod");
        // attackSpeedIn -3.0F = 4-3.0 = 1 attackSpeed // attackDamageIn 2F = damage per attack
        OTLItems.stone_cutting_device = registerItem(new CuttingDeviceItem(ItemTier.STONE, 2F, -3.0F, new Item.Properties().group(OTLGroup.tools)), "stone_cutting_device");
        OTLItems.iron_cutting_device = registerItem(new CuttingDeviceItem(ItemTier.IRON, 2F, -2.9F, new Item.Properties().group(OTLGroup.tools)), "iron_cutting_device");
        OTLItems.diamond_cutting_device = registerItem(new CuttingDeviceItem(ItemTier.DIAMOND, 2F, -2.8F, new Item.Properties().group(OTLGroup.tools)), "diamond_cutting_device");
        OTLItems.stone_hammer = registerItem(new HammerItem(ItemTier.STONE, 4F, -3.0F, new Item.Properties().group(OTLGroup.tools)), "stone_hammer");
        OTLItems.iron_hammer = registerItem(new HammerItem(ItemTier.IRON, 3F, -2.9F, new Item.Properties().group(OTLGroup.tools)), "iron_hammer");
        OTLItems.diamond_hammer = registerItem(new HammerItem(ItemTier.DIAMOND, 3F, -2.8F, new Item.Properties().group(OTLGroup.tools)), "diamond_hammer");
    }

    protected static Item registerItem(String name) {
        Item item = new Item(new Item.Properties().group(OTLGroup.materials)).setRegistryName(name);
        ForgeRegistries.ITEMS.register(item);
        return item;
    }

    protected static Item registerItem(Item item, String name) {
        ForgeRegistries.ITEMS.register(item.setRegistryName(name));
        return item;
    }
}