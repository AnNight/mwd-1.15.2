package ontheland.init;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.IBlockColor;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.client.renderer.color.ItemColors;
import ontheland.api.block.OTLBlocks;
import ontheland.api.item.OTLItems;

public class InitColors {
    // block color
    public static IBlockColor pinus_caribaea_block = (p_getColor_1_, p_getColor_2_, p_getColor_3_, p_getColor_4_) -> 6456371;
    public static IBlockColor pinus_leaves_block = (p_getColor_1_, p_getColor_2_, p_getColor_3_, p_getColor_4_) -> 2319364;
    public static IBlockColor cyan_tree_block = (p_getColor_1_, p_getColor_2_, p_getColor_3_, p_getColor_4_) -> 7903880;
    // item color
    public static IItemColor pinus_caribaea_item = (p_getColor_1_, p_getColor_2_) -> 6456371;
    public static IItemColor pinus_leaves_item = (p_getColor_1_, p_getColor_2_) -> 2319364;
    public static IItemColor cyan_tree_item = (p_getColor_1_, p_getColor_2_) -> 7903880;

    public static void init() {
        BlockColors b = Minecraft.getInstance().getBlockColors();
        ItemColors i = Minecraft.getInstance().getItemColors();
        // block colors
        b.register(pinus_caribaea_block, OTLBlocks.pinus_caribaea_leaves);
        b.register(pinus_leaves_block, OTLBlocks.pinus_palustris_leaves, OTLBlocks.pinus_taeda_leaves);
        b.register(cyan_tree_block, OTLBlocks.cyan_tree_leaves);
        // item colors
        i.register(pinus_caribaea_item, OTLItems.pinus_caribaea_leaves);
        i.register(pinus_leaves_item, OTLItems.pinus_palustris_leaves, OTLItems.pinus_taeda_leaves);
        i.register(cyan_tree_item, OTLItems.cyan_tree_leaves);
    }
}