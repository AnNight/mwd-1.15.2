package ontheland.init;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.potion.Effects;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import ontheland.api.block.OTLBlocks;
import ontheland.common.block.*;
import ontheland.common.block.DoorBlock;
import ontheland.common.block.FlowerBlock;
import ontheland.common.block.LeavesBlock;
import ontheland.common.block.LogBlock;
import ontheland.common.block.PressurePlateBlock;
import ontheland.common.block.SaplingBlock;
import ontheland.common.block.StairsBlock;
import ontheland.common.block.TrapDoorBlock;
import ontheland.common.block.trees.CyanTree;
import ontheland.common.block.trees.PinusCaribaeaTree;
import ontheland.common.block.trees.PinusPalustrisTree;
import ontheland.common.block.trees.PinusTaedaTree;
import ontheland.api.group.OTLGroup;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModBlocks {
    @SubscribeEvent
    public static void onRegisterBlocks(RegistryEvent.Register<Block> event) {
        // leaves
        OTLBlocks.cyan_tree_leaves = registerBlockNoItem(new LeavesBlock(), "cyan_tree_leaves");
        OTLBlocks.pinus_caribaea_leaves = registerBlockNoItem(new LeavesBlock(), "pinus_caribaea_leaves");
        OTLBlocks.pinus_palustris_leaves = registerBlockNoItem(new LeavesBlock(), "pinus_palustris_leaves");
        OTLBlocks.pinus_taeda_leaves = registerBlockNoItem(new LeavesBlock(), "pinus_taeda_leaves");
        // portals
        OTLBlocks.otl_portal = registerBlockNoItem(new MagicDimensionPortalBlock(), "otl_portal");
        OTLBlocks.otl_portal_frame = rMaterial(new Block(Block.Properties.create(Material.ROCK, MaterialColor.BLACK).hardnessAndResistance(1.5F, 6.0F).sound(SoundType.STONE)), "otl_portal_frame");
        OTLBlocks.otl_portal_altar = rMaterial(new OtlPortalAltarBlock(), "otl_portal_altar");
        OTLBlocks.spacial_rift_portal = registerBlockNoItem(new SpacialRiftPortalBlock(), "spacial_rift_portal");
        // others
        OTLBlocks.material_making_table = rMaterial(new MaterialMakingTableBlock(), "material_making_table");
        OTLBlocks.cupboard = rBlock(new CupboardBlock(), "cupboard");
        OTLBlocks.wooden_lighting = rBlock(new WoodenLightingBlock(), "wooden_lighting");
        // planks
        OTLBlocks.cyan_tree_planks = rBlock(new PlankBlock(MaterialColor.CYAN), "cyan_tree_planks");
        OTLBlocks.pinus_caribaea_planks = rBlock(new PlankBlock(MaterialColor.SAND), "pinus_caribaea_planks");
        OTLBlocks.pinus_palustris_planks = rBlock(new PlankBlock(MaterialColor.SAND), "pinus_palustris_planks");
        OTLBlocks.pinus_taeda_planks = rBlock(new PlankBlock(MaterialColor.SAND), "pinus_taeda_planks");
        // saplings
        OTLBlocks.cyan_tree_sapling = rMaterial(new SaplingBlock(new CyanTree()), "cyan_tree_sapling");
        OTLBlocks.pinus_caribaea_sapling = rMaterial(new SaplingBlock(new PinusCaribaeaTree()), "pinus_caribaea_sapling");
        OTLBlocks.pinus_palustris_sapling = rMaterial(new SaplingBlock(new PinusPalustrisTree()), "pinus_palustris_sapling");
        OTLBlocks.pinus_taeda_sapling = rMaterial(new SaplingBlock(new PinusTaedaTree()), "pinus_taeda_sapling");
        // logs
        OTLBlocks.cyan_tree_log = rBlock(new LogBlock(MaterialColor.CYAN), "cyan_tree_log");
        OTLBlocks.pinus_caribaea_log = rBlock(new LogBlock(MaterialColor.SAND, MaterialColor.STONE), "pinus_caribaea_log");
        OTLBlocks.pinus_palustris_log = rBlock(new LogBlock(MaterialColor.SAND, MaterialColor.QUARTZ),"pinus_palustris_log");
        OTLBlocks.pinus_taeda_log = rBlock(new LogBlock(MaterialColor.WOOD),"pinus_taeda_log");
        // stripped logs
        OTLBlocks.stripped_cyan_tree_log = rBlock(new LogBlock(MaterialColor.CYAN, MaterialColor.CYAN), "stripped_cyan_tree_log");
        OTLBlocks.stripped_pinus_caribaea_log = rBlock(new LogBlock(MaterialColor.WOOD, MaterialColor.LIGHT_GRAY), "stripped_pinus_caribaea_log");
        OTLBlocks.stripped_pinus_palustris_log = rBlock(new LogBlock(MaterialColor.SAND), "stripped_pinus_palustris_log");
        OTLBlocks.stripped_pinus_taeda_log = rBlock(new LogBlock(MaterialColor.WOOD), "stripped_pinus_taeda_log");
        // woods
        OTLBlocks.cyan_tree_wood = rBlock(new WoodBlock(MaterialColor.CYAN), "cyan_tree_wood");
        OTLBlocks.pinus_caribaea_wood = rBlock(new WoodBlock(MaterialColor.STONE), "pinus_caribaea_wood");
        OTLBlocks.pinus_palustris_wood = rBlock(new WoodBlock(MaterialColor.QUARTZ), "pinus_palustris_wood");
        OTLBlocks.pinus_taeda_wood = rBlock(new WoodBlock(MaterialColor.WOOD), "pinus_taeda_wood");
        // stripped woods
        OTLBlocks.stripped_cyan_tree_wood = rBlock(new WoodBlock(MaterialColor.CYAN), "stripped_cyan_tree_wood");
        OTLBlocks.stripped_pinus_caribaea_wood = rBlock(new WoodBlock(MaterialColor.STONE), "stripped_pinus_caribaea_wood");
        OTLBlocks.stripped_pinus_palustris_wood = rBlock(new WoodBlock(MaterialColor.SAND), "stripped_pinus_palustris_wood");
        OTLBlocks.stripped_pinus_taeda_wood = rBlock(new WoodBlock(MaterialColor.WOOD), "stripped_pinus_taeda_wood");
        // stairs
        OTLBlocks.pinus_caribaea_stairs = rBlock(new StairsBlock(OTLBlocks.pinus_caribaea_planks.getDefaultState(), Block.Properties.from(OTLBlocks.pinus_caribaea_planks)), "pinus_caribaea_stairs");
        OTLBlocks.pinus_palustris_stairs = rBlock(new StairsBlock(OTLBlocks.pinus_palustris_planks.getDefaultState(), Block.Properties.from(OTLBlocks.pinus_palustris_planks)), "pinus_palustris_stairs");
        OTLBlocks.pinus_taeda_stairs = rBlock(new StairsBlock(OTLBlocks.pinus_taeda_planks.getDefaultState(), Block.Properties.from(OTLBlocks.pinus_taeda_planks)), "pinus_taeda_stairs");
        // slabs
        OTLBlocks.pinus_caribaea_slab = rBlock(new SlabBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_caribaea_slab");
        OTLBlocks.pinus_palustris_slab = rBlock(new SlabBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_palustris_slab");
        OTLBlocks.pinus_taeda_slab = rBlock(new SlabBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_taeda_slab");
        // fence
        OTLBlocks.pinus_caribaea_fence = rMaterial(new FenceBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_caribaea_fence");
        OTLBlocks.pinus_palustris_fence = rMaterial(new FenceBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_palustris_fence");
        OTLBlocks.pinus_taeda_fence = rMaterial(new FenceBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_taeda_fence");
        // fence gate
        OTLBlocks.pinus_caribaea_fence_gate = rMaterial(new FenceGateBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_caribaea_fence_gate");
        OTLBlocks.pinus_palustris_fence_gate = rMaterial(new FenceGateBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_palustris_fence_gate");
        OTLBlocks.pinus_taeda_fence_gate = rMaterial(new FenceGateBlock(Block.Properties.create(Material.WOOD, MaterialColor.SAND).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)), "pinus_taeda_fence_gate");
        // trapdoor
        OTLBlocks.pinus_caribaea_trapdoor = rMaterial(new TrapDoorBlock(MaterialColor.SAND), "pinus_caribaea_trapdoor");
        OTLBlocks.pinus_palustris_trapdoor = rMaterial(new TrapDoorBlock(MaterialColor.SAND), "pinus_palustris_trapdoor");
        OTLBlocks.pinus_taeda_trapdoor = rMaterial(new TrapDoorBlock(MaterialColor.SAND), "pinus_taeda_trapdoor");
        // door
        OTLBlocks.pinus_caribaea_door = rMaterial(new DoorBlock(MaterialColor.SAND), "pinus_caribaea_door");
        OTLBlocks.pinus_palustris_door = rMaterial(new DoorBlock(MaterialColor.SAND), "pinus_palustris_door");
        OTLBlocks.pinus_taeda_door = rMaterial(new DoorBlock(MaterialColor.SAND), "pinus_taeda_door");
        // plate
        OTLBlocks.pinus_caribaea_pressure_plate = rMaterial(new PressurePlateBlock(MaterialColor.SAND), "pinus_caribaea_pressure_plate");
        OTLBlocks.pinus_palustris_pressure_plate = rMaterial(new PressurePlateBlock(MaterialColor.SAND), "pinus_palustris_pressure_plate");
        OTLBlocks.pinus_taeda_pressure_plate = rMaterial(new PressurePlateBlock(MaterialColor.SAND), "pinus_taeda_pressure_plate");
        // flower
        OTLBlocks.hai_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 5), "hai_flower");
        OTLBlocks.ling_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 5), "ling_flower");
        OTLBlocks.ji_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 0), "ji_flower");
        OTLBlocks.long_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 5), "long_flower");
        OTLBlocks.ming_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 0), "ming_flower");
        OTLBlocks.ye_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 5), "ye_flower");
        OTLBlocks.liu_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 5), "liu_flower");
        OTLBlocks.qing_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 5), "qing_flower");
        OTLBlocks.xin_flower = rMaterial(new FlowerBlock(Effects.REGENERATION, 8, 0), "xin_flower");
        // stone
        OTLBlocks.amber_ore = registerBlockNoItem(new Block(Block.Properties.create(Material.ROCK).harvestTool(ToolType.PICKAXE).harvestLevel(2).hardnessAndResistance(2.0F).sound(SoundType.STONE)), "amber_ore");
        OTLBlocks.ascharite_stone = rBlock(new StoneBlock(MaterialColor.SAND), "ascharite_stone"); // 硼镁
        OTLBlocks.calcium_carbonate_stone = rBlock(new StoneBlock(MaterialColor.SAND), "calcium_carbonate_stone"); // 石灰
        OTLBlocks.colemanite_stone = rBlock(new StoneBlock(MaterialColor.SAND), "colemanite_stone"); // 硬硼
        OTLBlocks.dolomite_stone = rBlock(new StoneBlock(MaterialColor.SAND), "dolomite_stone"); // 白云
        OTLBlocks.pyrophyllite_stone = rBlock(new StoneBlock(MaterialColor.GRAY), "pyrophyllite_stone"); // 叶腊
    }

    private static Block registerBlock(Block block, String name, BlockItem blockItem) {
        block.setRegistryName(name);
        blockItem.setRegistryName(name);
        ForgeRegistries.BLOCKS.register(block);
        ForgeRegistries.ITEMS.register(blockItem);
        return block;
    }

    private static Block rBlock(Block block, String name) {
        return registerBlock(block, name, new BlockItem(block, new Item.Properties().group(OTLGroup.blocks)));
    }

    private static Block rMaterial(Block block, String name) {
        return registerBlock(block, name, new BlockItem(block, new Item.Properties().group(OTLGroup.materials)));
    }

    private static Block rNullGroup(Block block, String name) {
        return registerBlock(block, name, new BlockItem(block, new Item.Properties()));
    }

    private static Block registerBlockNoItem(Block block, String name) {
        block.setRegistryName(name);
        ForgeRegistries.BLOCKS.register(block);
        return block;
    }
}
