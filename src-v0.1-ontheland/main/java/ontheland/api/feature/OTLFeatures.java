package ontheland.api.feature;

import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.FlowersFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import ontheland.api.block.OTLBlocks;
import ontheland.common.world.gen.feature.*;

public class OTLFeatures {
    //Conifer Trees
    public static final AbstractTreeFeature<NoFeatureConfig> PINUS_CARIBAEA_TREE = new PinusTreeFeature(NoFeatureConfig::deserialize, false, false, OTLBlocks.pinus_caribaea_log.getDefaultState(), OTLBlocks.pinus_caribaea_leaves.getDefaultState(), OTLBlocks.pinus_caribaea_sapling);
    public static final AbstractTreeFeature<NoFeatureConfig> PINUS_PALUSTRIS_TREE = new PinusTreeFeature(NoFeatureConfig::deserialize, false, false, OTLBlocks.pinus_palustris_log.getDefaultState(), OTLBlocks.pinus_palustris_leaves.getDefaultState(), OTLBlocks.pinus_palustris_sapling);
    public static final AbstractTreeFeature<NoFeatureConfig> PINUS_TAEDA_TREE = new PinusTreeFeature(NoFeatureConfig::deserialize, false, false, OTLBlocks.pinus_taeda_log.getDefaultState(), OTLBlocks.pinus_taeda_leaves.getDefaultState(), OTLBlocks.pinus_taeda_sapling);
    //Vanilla Biomes Features
    public static final FlowersFeature PINUS_CARIBAEA_FLOWER = new PinusCaribaeaFlowersFeature(NoFeatureConfig::deserialize);
    public static final FlowersFeature PINUS_PALUSTRIS_FLOWER = new PinusPalustrisFlowersFeature(NoFeatureConfig::deserialize);
    public static final FlowersFeature PINUS_TAEDA_FLOWER = new PinusTaedaFlowersFeature(NoFeatureConfig::deserialize);
}