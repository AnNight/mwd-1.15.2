package ontheland.api.block;

import net.minecraft.block.Block;
import net.minecraft.block.LeavesBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;

public class OTLBlocks {
    public static Block amber_ore;
    // brick
    public static Block relic_brick;
    // portal
    public static Block otl_portal;
    public static Block otl_portal_altar;
    public static Block otl_portal_frame;
    public static Block spacial_rift_portal;
    // chinese herbal medicine
    public static Block solanum_nigrum; // 龙葵
    // log
    public static Block laurel_log; // 月桂
    public static Block cyan_tree_log;
    public static Block pinus_caribaea_log;
    public static Block pinus_palustris_log;
    public static Block pinus_taeda_log;
    // stripped log
    public static Block stripped_cyan_tree_log;
    public static Block stripped_pinus_caribaea_log;
    public static Block stripped_pinus_palustris_log;
    public static Block stripped_pinus_taeda_log;
    // wood
    public static Block cyan_tree_wood;
    public static Block pinus_caribaea_wood;
    public static Block pinus_palustris_wood;
    public static Block pinus_taeda_wood;
    // stripped wood
    public static Block stripped_cyan_tree_wood;
    public static Block stripped_pinus_caribaea_wood;
    public static Block stripped_pinus_palustris_wood;
    public static Block stripped_pinus_taeda_wood;
    // planks
    public static Block cyan_tree_planks;
    public static Block pinus_caribaea_planks;
    public static Block pinus_palustris_planks;
    public static Block pinus_taeda_planks;
    // sapling
    public static Block cyan_tree_sapling;
    public static Block pinus_caribaea_sapling;
    public static Block pinus_palustris_sapling;
    public static Block pinus_taeda_sapling;
    // stairs
    public static Block cracked_stone_brick_stairs;
    public static Block cyan_tree_stairs;
    public static Block pinus_caribaea_stairs;
    public static Block pinus_palustris_stairs;
    public static Block pinus_taeda_stairs;
    // slab
    public static Block cracked_stone_brick_slabs;
    public static Block cyan_tree_slab;
    public static Block pinus_caribaea_slab;
    public static Block pinus_palustris_slab;
    public static Block pinus_taeda_slab;
    // fence
    public static Block cyan_tree_fence;
    public static Block pinus_caribaea_fence;
    public static Block pinus_palustris_fence;
    public static Block pinus_taeda_fence;
    // fence gate
    public static Block cyan_tree_fence_gate;
    public static Block pinus_caribaea_fence_gate;
    public static Block pinus_palustris_fence_gate;
    public static Block pinus_taeda_fence_gate;
    // trapdoor
    public static Block cyan_tree_trapdoor;
    public static Block pinus_caribaea_trapdoor;
    public static Block pinus_palustris_trapdoor;
    public static Block pinus_taeda_trapdoor;
    // door
    public static Block cyan_tree_door;
    public static Block pinus_caribaea_door;
    public static Block pinus_palustris_door;
    public static Block pinus_taeda_door;
    public static Block laboratory_door;
    public static Block factory_door;
    public static Block jail_door;
    // pressure plate
    public static Block cyan_tree_pressure_plate;
    public static Block pinus_caribaea_pressure_plate;
    public static Block pinus_palustris_pressure_plate;
    public static Block pinus_taeda_pressure_plate;
    // leaves
    public static Block cyan_tree_leaves;
    public static Block pinus_caribaea_leaves;
    public static Block pinus_palustris_leaves;
    public static Block pinus_taeda_leaves;
    // flower
    public static Block hai_flower;
    public static Block ling_flower;
    public static Block ji_flower;
    public static Block long_flower;
    public static Block ming_flower;
    public static Block ye_flower;
    public static Block liu_flower;
    public static Block qing_flower;
    public static Block xin_flower;
    // stone
    public static Block ascharite_stone; // peng mei
    public static Block calcium_carbonate_stone; // shi hui
    public static Block colemanite_stone; // ying peng gai
    public static Block dolomite_stone; // bai yun
    public static Block pyrophyllite_stone; // ye la
    // barrel
    public static Block food_barrel; // 4 * 8
    public static Block potion_barrel; // 4 * 9
    public static Block seed_barrel; // 4 * 8
    // other
    public static Block purple_soil;
    public static Block material_making_table;
    public static Block cupboard;
    public static Block wooden_lighting;
}