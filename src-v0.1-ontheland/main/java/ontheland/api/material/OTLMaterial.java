package ontheland.api.material;

import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;

public final class OTLMaterial {
    public static final Material MOD_STONE = new Material(MaterialColor.GRAY, false, true, true, true, false, false, false, PushReaction.NORMAL);
}