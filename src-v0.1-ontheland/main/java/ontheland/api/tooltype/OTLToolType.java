package ontheland.api.tooltype;

import net.minecraftforge.common.ToolType;

public class OTLToolType {
    public static final ToolType CUTTING_DEVICE = ToolType.get("cutting_device");
    public static final ToolType HAMMER = ToolType.get("hammer");
}