package ontheland.api.soundevent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import ontheland.OnTheLand;

public class OTLSoundEvents {
    public static final SoundEvent PORTAL_CREATE = new SoundEvent(new ResourceLocation(OnTheLand.MOD_ID, "portal_create"));
    public static final SoundEvent BACKGROUND = new SoundEvent(new ResourceLocation(OnTheLand.MOD_ID, "background"));
}