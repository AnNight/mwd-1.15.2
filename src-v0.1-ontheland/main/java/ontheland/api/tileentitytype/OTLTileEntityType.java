package ontheland.api.tileentitytype;

import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.registries.ObjectHolder;
import ontheland.api.block.OTLBlocks;
import ontheland.common.tileentity.CupboardTileEntity;
import ontheland.common.tileentity.MaterialMakingTableTileEntity;

public class OTLTileEntityType {
    public static TileEntityType<?> materialMakingTableTileEntityType = TileEntityType.Builder.create(MaterialMakingTableTileEntity::new, OTLBlocks.material_making_table).build(null);
    public static TileEntityType<?> cupboardTileEntityType = TileEntityType.Builder.create(CupboardTileEntity::new, OTLBlocks.cupboard).build(null);
}