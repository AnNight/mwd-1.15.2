package ontheland.api.item;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import ontheland.api.block.OTLBlocks;
import ontheland.api.group.OTLGroup;

public class OTLItems {
    public static Item amber_ore = new BlockItem(OTLBlocks.amber_ore, new Item.Properties().group(OTLGroup.blocks)).setRegistryName(OTLBlocks.amber_ore.getRegistryName());
    // chinese herbal medicine
    // stone debris
    public static Item ascharite_stone_debris;
    public static Item calcium_carbonate_stone_debris;
    public static Item colemanite_stone_debris;
    public static Item dolomite_stone_debris;
    public static Item pyrophyllite_stone_debris;
    // leaves
    public static Item cyan_tree_leaves = new BlockItem(OTLBlocks.cyan_tree_leaves, new Item.Properties().group(OTLGroup.materials)).setRegistryName(OTLBlocks.cyan_tree_leaves.getRegistryName());
    public static Item pinus_caribaea_leaves = new BlockItem(OTLBlocks.pinus_caribaea_leaves, new Item.Properties().group(OTLGroup.materials)).setRegistryName(OTLBlocks.pinus_caribaea_leaves.getRegistryName());
    public static Item pinus_palustris_leaves = new BlockItem(OTLBlocks.pinus_palustris_leaves, new Item.Properties().group(OTLGroup.materials)).setRegistryName(OTLBlocks.pinus_palustris_leaves.getRegistryName());
    public static Item pinus_taeda_leaves = new BlockItem(OTLBlocks.pinus_taeda_leaves, new Item.Properties().group(OTLGroup.materials)).setRegistryName(OTLBlocks.pinus_taeda_leaves.getRegistryName());
    // materials
    public static Item timber;
    public static Item stone_stock;
    public static Item blade;
    public static Item pine_cone; // �ɹ�
    public static Item pine_nut; // ����
    public static Item amber;
    // tools
    public static Item stone_cutting_device;
    public static Item iron_cutting_device;
    public static Item diamond_cutting_device;
    public static Item stone_hammer;
    public static Item iron_hammer;
    public static Item diamond_hammer;
    // fruit
    public static Item cyan_fruit;
    // other
    public static Item scroll; // ����
    public static Item crankshaft; // ����
    public static Item connecting_rod; // ����
}