package ontheland.api.containertype;

import net.minecraft.inventory.container.ContainerType;
import ontheland.common.inventory.container.CupboardContainer;
import ontheland.common.inventory.container.MaterialMakingTableContainer;

public class OTLContainerType {
    public static ContainerType<MaterialMakingTableContainer> materialMakingTableContainerType = new ContainerType<>(MaterialMakingTableContainer::create);
    public static ContainerType<CupboardContainer> cupboardContainerType = new ContainerType<>(CupboardContainer::create);
}