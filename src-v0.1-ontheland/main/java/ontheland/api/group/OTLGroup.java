package ontheland.api.group;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import ontheland.api.item.OTLItems;

public class OTLGroup {
    public static ItemGroup tools = new ItemGroup("ontheland_tools") {
        public ItemStack createIcon() {
            return new ItemStack(OTLItems.iron_cutting_device);
        }
    };

    public static ItemGroup blocks = new ItemGroup("ontheland_blocks") {
        public ItemStack createIcon() {
            return new ItemStack(OTLItems.amber_ore);
        }
    };

    public static ItemGroup materials = new ItemGroup("ontheland_materials") {
        public ItemStack createIcon() {
            return new ItemStack(OTLItems.timber);
        }
    };
}