package ontheland.event;

import io.netty.buffer.Unpooled;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.world.RegisterDimensionsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import ontheland.common.capability.mana.IMana;
import ontheland.common.capability.mana.Mana;
import ontheland.common.capability.mana.ManaProvider;
import ontheland.common.capability.thirst.IThirst;
import ontheland.common.capability.thirst.Thirst;
import ontheland.common.capability.thirst.ThirstProvider;
import ontheland.OnTheLand;
import ontheland.common.world.dimension.OTLDimensions;

@SuppressWarnings("unused")
public class EventHandler {
    @SubscribeEvent
    public void registerDimension(RegisterDimensionsEvent event) {
        OTLDimensions.MAGIC_DIM_TYPE = DimensionManager.registerOrGetDimension(OTLDimensions.MAGIC_DIM_ID, OTLDimensions.MAGIC_DIM, new PacketBuffer(Unpooled.buffer(16)), true);
        OTLDimensions.SPACIAL_RIFT_DIM_TYPE = DimensionManager.registerOrGetDimension(OTLDimensions.SPACIAL_RIFT_DIM_ID, OTLDimensions.SPACIAL_RIFT_DIM, new PacketBuffer(Unpooled.buffer(16)), false);
    }

    @SubscribeEvent
    public void playerLogsIn(PlayerEvent.PlayerLoggedInEvent event) {
        event.getPlayer().sendMessage(new TranslationTextComponent("ontheland.title"));
    }

//    @SubscribeEvent
//    public void livingTickEvent(LivingEvent.LivingUpdateEvent event) {
//        LivingEntity entity = event.getEntityLiving();
//        if (entity instanceof ClientPlayerEntity) {
//        }
//    }


    /**
     * Run on Server & Client.<br>
     */
    @SubscribeEvent
    public void onPlayerEatsFlesh(LivingEntityUseItemEvent.Finish event) {
        if (Items.ROTTEN_FLESH == event.getItem().getItem() && event.getEntity() instanceof ServerPlayerEntity) {
            ServerPlayerEntity player = (ServerPlayerEntity) event.getEntity();
            Thirst.getFromPlayer(player).limitedReduce(1);
            Thirst.updateClient(player, Thirst.getFromPlayer(player));
            Mana.getFromPlayer(player).limitedReduceValue(1);
            Mana.updateClient(player, Mana.getFromPlayer(player));
        }
    }
}