package ontheland.event;

import net.minecraft.inventory.container.ContainerType;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.ModDimension;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import ontheland.api.containertype.OTLContainerType;
import ontheland.api.soundevent.OTLSoundEvents;
import ontheland.api.tileentitytype.OTLTileEntityType;
import ontheland.common.world.dimension.OTLDimensions;
import ontheland.common.world.dimension.magic.MagicModDimension;
import ontheland.common.world.dimension.spacialrift.SpacialRiftModDimension;

import static ontheland.OnTheLand.MOD_ID;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class RegistryEventHandler {
    @SubscribeEvent
    public static void onContainerTypeRegistry(final RegistryEvent.Register<ContainerType<?>> event) {
        event.getRegistry().registerAll(
                OTLContainerType.cupboardContainerType.setRegistryName("cupboard"),
                OTLContainerType.materialMakingTableContainerType.setRegistryName("material_making_table")
        );
    }

    @SubscribeEvent
    public static void onTileEntityTypeRegistry(final RegistryEvent.Register<TileEntityType<?>> event) {
        event.getRegistry().registerAll(
                OTLTileEntityType.cupboardTileEntityType.setRegistryName("cupboard"),
                OTLTileEntityType.materialMakingTableTileEntityType.setRegistryName("material_making_table")
        );
    }

    @SubscribeEvent
    public static void onSoundEventRegistry(final RegistryEvent.Register<SoundEvent> event) {
        event.getRegistry().registerAll(
                OTLSoundEvents.BACKGROUND.setRegistryName(new ResourceLocation(MOD_ID, "background")),
                OTLSoundEvents.PORTAL_CREATE.setRegistryName(new ResourceLocation(MOD_ID, "portal_create"))
        );
    }

    @SubscribeEvent
    public static void onModDimensionRegistry(final RegistryEvent.Register<ModDimension> event) {
        event.getRegistry().register(new MagicModDimension().setRegistryName(OTLDimensions.MAGIC_DIM_ID));
        event.getRegistry().register(new SpacialRiftModDimension().setRegistryName(OTLDimensions.SPACIAL_RIFT_DIM_ID));
    }
}
