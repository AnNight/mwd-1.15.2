package ontheland.network;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import ontheland.common.capability.mana.IMana;
import ontheland.common.capability.mana.Mana;
import ontheland.common.capability.mana.ManaProvider;

import java.util.function.Supplier;

public class UpdateClientMana {
    private final CompoundNBT nbt;

    public UpdateClientMana(int entityId, CompoundNBT nbt){
        nbt.putInt("entityId", entityId);
        this.nbt = nbt;
    }

    public UpdateClientMana(CompoundNBT nbt){
        this.nbt = nbt;
    }

    public static void encoder(UpdateClientMana msg, PacketBuffer buf){
        buf.writeCompoundTag(msg.nbt);
    }

    public static UpdateClientMana decoder(PacketBuffer buf){
        return new UpdateClientMana(buf.readCompoundTag());
    }

    public static void handle(UpdateClientMana msg, Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = (PlayerEntity) Minecraft.getInstance().world.getEntityByID(msg.nbt.getInt("entityId"));
            IMana cap = Mana.getFromPlayer(player);
            ManaProvider.MANA_CAP.readNBT(cap, null, msg.nbt);
        });
        ctx.get().setPacketHandled(true);
    }
}
