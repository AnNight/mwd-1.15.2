package ontheland.network;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import ontheland.common.capability.thirst.IThirst;
import ontheland.common.capability.thirst.Thirst;
import ontheland.common.capability.thirst.ThirstProvider;

import java.util.function.Supplier;

public class UpdateClientThirst {
    private final CompoundNBT nbt;

    public UpdateClientThirst(int entityId, CompoundNBT nbt){
        nbt.putInt("entityId", entityId);
        this.nbt = nbt;
    }

    public UpdateClientThirst(CompoundNBT nbt){
        this.nbt = nbt;
    }

    public static void encoder(UpdateClientThirst msg, PacketBuffer buf){
        buf.writeCompoundTag(msg.nbt);
    }

    public static UpdateClientThirst decoder(PacketBuffer buf){
        return new UpdateClientThirst(buf.readCompoundTag());
    }

    public static void handle(UpdateClientThirst msg, Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = (PlayerEntity) Minecraft.getInstance().world.getEntityByID(msg.nbt.getInt("entityId"));
            IThirst cap = Thirst.getFromPlayer(player);
            ThirstProvider.THIRST_CAP.readNBT(cap, null, msg.nbt);
        });
        ctx.get().setPacketHandled(true);
    }
}