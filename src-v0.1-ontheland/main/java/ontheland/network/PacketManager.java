package ontheland.network;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import ontheland.OnTheLand;

public class PacketManager {
    private static int id = 0;
    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel THIRST = NetworkRegistry.newSimpleChannel(
            new ResourceLocation(OnTheLand.MOD_ID, "thirst"),
            () -> PROTOCOL_VERSION,
            PROTOCOL_VERSION::equals,
            PROTOCOL_VERSION::equals
    );
    public static final SimpleChannel MANA = NetworkRegistry.newSimpleChannel(
            new ResourceLocation(OnTheLand.MOD_ID, "mana"),
            () -> PROTOCOL_VERSION,
            PROTOCOL_VERSION::equals,
            PROTOCOL_VERSION::equals
    );

    public static int nextId() {
        return id++;
    }

    public static void register(){
        THIRST.registerMessage(
                nextId(),
                UpdateClientThirst.class,
                UpdateClientThirst::encoder,
                UpdateClientThirst::decoder,
                UpdateClientThirst::handle
        );
        MANA.registerMessage(
                nextId(),
                UpdateClientMana.class,
                UpdateClientMana::encoder,
                UpdateClientMana::decoder,
                UpdateClientMana::handle
        );
    }

    public static void sendThirstTo(ServerPlayerEntity player, Object msg) {
        THIRST.sendTo(msg, player.connection.getNetworkManager(), NetworkDirection.PLAY_TO_CLIENT);
    }

    public static void sendManaTo(ServerPlayerEntity player, Object msg) {
        MANA.sendTo(msg, player.connection.getNetworkManager(), NetworkDirection.PLAY_TO_CLIENT);
    }
}