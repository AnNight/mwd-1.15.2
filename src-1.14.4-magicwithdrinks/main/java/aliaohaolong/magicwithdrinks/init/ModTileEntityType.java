package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDTileEntityType;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModTileEntityType {
    @SubscribeEvent
    public static void onRegisterTileEntityType(final RegistryEvent.Register<TileEntityType<?>> event) {
        event.getRegistry().registerAll(
                MWDTileEntityType.WATER_PURIFIER,
                MWDTileEntityType.DRINK_MAKING_MACHINE,
                MWDTileEntityType.MANA_EXTRACTOR,
                MWDTileEntityType.MILL,
                MWDTileEntityType.BLENDER,
                MWDTileEntityType.BREWING_MACHINE
        );
    }
}