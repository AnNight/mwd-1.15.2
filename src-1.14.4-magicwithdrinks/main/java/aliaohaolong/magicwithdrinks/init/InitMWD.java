package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDDrinks;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.item.ExtraAttribute;
import aliaohaolong.magicwithdrinks.common.recipe.DrinkRecipe;
import net.minecraft.item.Items;

public class InitMWD {
    public static void init() {
        initDrinksRecipes();
        initExtractingRecipes();
    }

    private static void initDrinksRecipes() {
        DrinkRecipe.DRINK_RECIPES.add(new DrinkRecipe(24000, false, MWDDrinks.APPLE_WINE, Items.APPLE, Items.APPLE, Items.APPLE, Items.SUGAR)); // 20
        DrinkRecipe.DRINK_RECIPES.add(new DrinkRecipe(19200, false, MWDDrinks.SWEET_BERRIES_WINE, Items.SWEET_BERRIES, Items.SWEET_BERRIES, Items.SWEET_BERRIES, Items.SWEET_BERRIES)); // 16
    }

    private static void initExtractingRecipes() {
//        ExtractingRecipe.EXTRACTING_RECIPES.add(new ExtractingRecipe(Items.APPLE, 2, 200, 4));
    }
}