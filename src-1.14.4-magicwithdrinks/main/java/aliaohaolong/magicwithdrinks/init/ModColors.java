package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.drink.DrinkUtils;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ModColors {
    @SubscribeEvent
    public static void onColorHandlerItem(ColorHandlerEvent.Item event) {
        event.getItemColors().register((stack, p_getColor_2_) -> p_getColor_2_ > 0 ? -1 : DrinkUtils.getColorFromStack(stack), MWDItems.DRINK);
        event.getItemColors().register((stack, p_getColor_2_) -> 7903880, MWDItems.CYAN_LEAVES);
    }

    @SubscribeEvent
    public static void onColorHandlerBlock(ColorHandlerEvent.Block event) {
        event.getBlockColors().register((blockState, reader, pos, p_getColor_4_) -> reader.getBiome(pos).getFoliageColor(pos),
                MWDBlocks.MOONLIGHT_VINE,
                MWDBlocks.MOONLIGHT_VINE_PLANT
        );
        event.getBlockColors().register((blockState, reader, pos, p_getColor_4_) -> 7903880, MWDBlocks.CYAN_LEAVES);
    }
}