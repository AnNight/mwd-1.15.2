package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDDrinks;
import aliaohaolong.magicwithdrinks.common.drink.Drink;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModDrinks {
    @SubscribeEvent
    public static void onRegisterDrinks(RegistryEvent.Register<Drink> event) {
        event.getRegistry().registerAll(
                MWDDrinks.WATER,
                MWDDrinks.PURIFIED_WATER,
                MWDDrinks.SUGAR_WATER,

                MWDDrinks.APPLE_JUICE,
                MWDDrinks.MELON_JUICE,
                MWDDrinks.CARROT_JUICE,
                MWDDrinks.PUMPKIN_JUICE,
                MWDDrinks.SWEET_BERRIES_JUICE,

                MWDDrinks.APPLE_WINE,
                MWDDrinks.SWEET_BERRIES_WINE,

                MWDDrinks.ACTIVE_SUBSTANCE
        );
    }
}