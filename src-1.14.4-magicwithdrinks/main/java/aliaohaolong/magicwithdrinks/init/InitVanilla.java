package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDBiomes;
import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.api.MWDCriteriaTriggers;
import aliaohaolong.magicwithdrinks.api.MWDFeatures;
import aliaohaolong.magicwithdrinks.common.advancements.criterion.DrinkTrigger;
import com.google.common.collect.Maps;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.FireBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeManager;

public class InitVanilla {
    public static void init() {
        initCriteriaTriggers();
        initBiomeDictionary();
        initBiome();
        initStrippability();
        initFlammability();
        initOres();
    }

    private static void initCriteriaTriggers() {
        MWDCriteriaTriggers.drinkTrigger = CriteriaTriggers.register(new DrinkTrigger());
    }

    public static void initBiomeDictionary() {
        BiomeDictionary.addTypes(MWDBiomes.RELIC_PLAINS, BiomeDictionary.Type.HOT, BiomeDictionary.Type.SPARSE, BiomeDictionary.Type.WET, BiomeDictionary.Type.RARE, BiomeDictionary.Type.OVERWORLD, MWDBiomes.MWD_RICH);
        for (Biome biome : BiomeDictionary.getBiomes(BiomeDictionary.Type.FOREST)) {
            if (!BiomeDictionary.hasType(biome, BiomeDictionary.Type.MOUNTAIN) && !BiomeDictionary.hasType(biome, BiomeDictionary.Type.COLD)) {
                BiomeDictionary.addTypes(biome, MWDBiomes.MWD_THIN);
            }
        }
        for (Biome biome :BiomeDictionary.getBiomes(BiomeDictionary.Type.SWAMP)) {
            BiomeDictionary.addTypes(biome, MWDBiomes.MWD_RICH);
        }
    }

    private static void initBiome() {
        BiomeManager.addBiome(BiomeManager.BiomeType.WARM, new BiomeManager.BiomeEntry(MWDBiomes.RELIC_PLAINS, 10));
        BiomeManager.addSpawnBiome(MWDBiomes.RELIC_PLAINS);
        BiomeProvider.BIOMES_TO_SPAWN_IN.add(MWDBiomes.RELIC_PLAINS);
    }

    private static void initStrippability () {
        registerStrippability(MWDBlocks.CYAN_LOG, MWDBlocks.STRIPPED_CYAN_LOG);
        registerStrippability(MWDBlocks.CYAN_WOOD, MWDBlocks.STRIPPED_CYAN_WOOD);
    }

    private static void registerStrippability(Block original, Block stripped) {
        AxeItem.BLOCK_STRIPPING_MAP = Maps.newHashMap(AxeItem.BLOCK_STRIPPING_MAP);
        AxeItem.BLOCK_STRIPPING_MAP.put(original, stripped);
    }

    private static void initFlammability() {
        FireBlock fireBlock = (FireBlock) Blocks.FIRE;
        fireBlock.setFireInfo(MWDBlocks.CYAN_LOG, 5, 5);
        fireBlock.setFireInfo(MWDBlocks.STRIPPED_CYAN_LOG, 5, 5);
        fireBlock.setFireInfo(MWDBlocks.CYAN_WOOD, 5, 5);
        fireBlock.setFireInfo(MWDBlocks.STRIPPED_CYAN_WOOD, 5, 5);
        fireBlock.setFireInfo(MWDBlocks.CYAN_LEAVES, 30, 60);
        fireBlock.setFireInfo(MWDBlocks.CYAN_PLANKS, 5, 20);
        fireBlock.setFireInfo(MWDBlocks.CYAN_STAIRS, 5, 20);
        fireBlock.setFireInfo(MWDBlocks.CYAN_SLAB, 5, 20);
        fireBlock.setFireInfo(MWDBlocks.CYAN_FENCE, 5, 20);
        fireBlock.setFireInfo(MWDBlocks.CYAN_FENCE_GATE, 5, 20);
        fireBlock.setFireInfo(MWDBlocks.WATER_PURIFIER, 5, 5);
        fireBlock.setFireInfo(MWDBlocks.DRINK_MAKING_MACHINE, 5, 5);
    }

    private static void initOres() {
        for (Biome biome : BiomeDictionary.getBiomes(MWDBiomes.MWD_THIN)) {
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(
                    MWDFeatures.MAGIC_GEM, IFeatureConfig.NO_FEATURE_CONFIG, Placement.COUNT_RANGE,
                    new CountRangeConfig(1, 16, 21, 53) /*16-48*/));
        }
        for (Biome biome : BiomeDictionary.getBiomes(MWDBiomes.MWD_RICH)) {
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(
                    MWDFeatures.MAGIC_GEM, IFeatureConfig.NO_FEATURE_CONFIG, Placement.COUNT_RANGE,
                    new CountRangeConfig(1, 16, 21, 53) /*16-48*/));
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(
                    MWDFeatures.ACHILLES, IFeatureConfig.NO_FEATURE_CONFIG, Placement.COUNT_RANGE,
                    new CountRangeConfig(1, 16, 21, 61) /*16-56*/));
        }
        for (Biome biome : BiomeDictionary.getBiomes(BiomeDictionary.Type.OVERWORLD)) {
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(
                    Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, MWDBlocks.SILVER_ORE.getDefaultState(), 8), Placement.COUNT_RANGE,
                    new CountRangeConfig(3, 0, 0, 32) /*0-32*/));
        }
        for (Biome biome : BiomeDictionary.getBiomes(BiomeDictionary.Type.OVERWORLD)) {
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(
                    Feature.ORE, new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, MWDBlocks.DENATURED_SILICON_ORE.getDefaultState(), 8), Placement.COUNT_RANGE,
                    new CountRangeConfig(2, 0, 0, 48) /*0-48*/));
        }
    }
}