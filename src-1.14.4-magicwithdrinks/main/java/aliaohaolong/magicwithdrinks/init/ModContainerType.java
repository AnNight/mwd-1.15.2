package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDContainerType;
import net.minecraft.inventory.container.ContainerType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModContainerType {
    @SubscribeEvent
    public static void onRegisterContainerType(final RegistryEvent.Register<ContainerType<?>> event) {
        event.getRegistry().registerAll(
                MWDContainerType.WATER_PURIFIER,
                MWDContainerType.DRINK_MAKING_MACHINE,
                MWDContainerType.MANA_EXTRACTOR,
                MWDContainerType.MILL,
                MWDContainerType.BLENDER
        );
    }
}