package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModItems {
    @SubscribeEvent
    public static void onRegisterItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(
                MWDItems.DRINK_BOTTLE, MWDItems.DRINK, MWDItems.WOODEN_CASING, MWDItems.IRON_CASING, MWDItems.DIAMOND_CORE, MWDItems.MAGIC_CORE,

                // wand
                MWDItems.PLANTS_WAND,

                // material
                MWDItems.COAL_NUGGET, MWDItems.CHARCOAL_NUGGET,
                MWDItems.HARD_STONE_POWDER, MWDItems.CATALYST, MWDItems.SMALL_PILE_OF_IMPURITY, MWDItems.IMPURITY,

                // battery
                MWDItems.MAGIC_GEM_BATTERY,

                // filter material & card
                MWDItems.COTTONY_FILTER_SLICE, MWDItems.IMPROVED_FEATHERY_FILTER_SLICE,
                MWDItems.WOOLEN_FILTER_SLICE, MWDItems.IMPROVED_WOOLEN_FILTER_SLICE,
                MWDItems.FEATHERY_FILTER_SLICE, MWDItems.IMPROVED_COTTONY_FILTER_SLICE,
//                MWDItems.INTENSIVE_FEATHERY_FILTER_SLICE, MWDItems.INTENSIVE_COTTONY_FILTER_SLICE,
                MWDItems.ELEMENTARY_CAPACITY_CARD, MWDItems.INTERMEDIATE_CAPACITY_CARD, MWDItems.ADVANCED_CAPACITY_CARD,
                MWDItems.ELEMENTARY_EFFICIENCY_CARD, MWDItems.INTERMEDIATE_EFFICIENCY_CARD, MWDItems.ADVANCED_EFFICIENCY_CARD,

                // crop
                MWDItems.COTTON_SEEDS, MWDItems.COTTON,

                // machine
                MWDItems.WATER_PURIFIER, MWDItems.DRINK_MAKING_MACHINE, MWDItems.MANA_EXTRACTOR, MWDItems.MILL, MWDItems.BLENDER, MWDItems.BREWING_MACHINE,

                // block
                MWDItems.HARD_STONE, MWDItems.ACHILLES_BLOCK, MWDItems.SILVER_BLOCK,

                // ore
                MWDItems.MAGIC_GEM_ORE, MWDItems.ACHILLES_ORE, MWDItems.SILVER_ORE, MWDItems.DENATURED_SILICON_ORE,

                // something related to ores
                MWDItems.MAGIC_GEM, MWDItems.ACHILLES_INGOT, MWDItems.ACHILLES_NUGGET, MWDItems.SILVER_INGOT, MWDItems.SILVER_NUGGET, MWDItems.DENATURED_SILICON,

                // tool
                MWDItems.ACHILLES_SWORD, MWDItems.ACHILLES_PICKAXE, MWDItems.ACHILLES_AXE, MWDItems.ACHILLES_SHOVEL, MWDItems.ACHILLES_HOE,

                // equip
                MWDItems.ACHILLES_HELMET, MWDItems.ACHILLES_CHESTPLATE, MWDItems.ACHILLES_LEGGINGS, MWDItems.ACHILLES_BOOTS,

                // flower
                MWDItems.MOONLIGHT_VINE, MWDItems.MOONLIGHT_BERRIES, MWDItems.CYAN_MUSHROOM, MWDItems.WISDOM_FLOWER, MWDItems.ACHILLES_FLOWER,
                MWDItems.RED_PELTATUM, MWDItems.FUCHSIA_PELTATUM,

                // tree
                MWDItems.CYAN_FRUIT, MWDItems.CYAN_LOG, MWDItems.CYAN_WOOD, MWDItems.STRIPPED_CYAN_LOG, MWDItems.STRIPPED_CYAN_WOOD, MWDItems.CYAN_PLANKS, MWDItems.CYAN_LEAVES, MWDItems.CYAN_SAPLING,
                MWDItems.CYAN_STAIRS, MWDItems.CYAN_SLAB, MWDItems.CYAN_PRESSURE_PLATE, MWDItems.CYAN_FENCE, MWDItems.CYAN_FENCE_GATE, MWDItems.CYAN_BUTTON, MWDItems.CYAN_TRAPDOOR, MWDItems.CYAN_DOOR
        );
    }
}