package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import net.minecraft.block.Block;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModBlocks {
    @SubscribeEvent
    public static void onRegisterBlocks(RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll(
                // crop
                MWDBlocks.COTTON,

                // machine
                MWDBlocks.WATER_PURIFIER, MWDBlocks.DRINK_MAKING_MACHINE, MWDBlocks.MANA_EXTRACTOR, MWDBlocks.MILL, MWDBlocks.BLENDER, MWDBlocks.BREWING_MACHINE,

                // block
                MWDBlocks.HARD_STONE, MWDBlocks.ACHILLES_BLOCK, MWDBlocks.SILVER_BLOCK,

                // ore
                MWDBlocks.MAGIC_GEM_ORE, MWDBlocks.ACHILLES_ORE, MWDBlocks.SILVER_ORE, MWDBlocks.DENATURED_SILICON_ORE,

                // flower
                MWDBlocks.MOONLIGHT_VINE, MWDBlocks.MOONLIGHT_VINE_PLANT,
                MWDBlocks.CYAN_MUSHROOM, MWDBlocks.POTTED_CYAN_MUSHROOM,
                MWDBlocks.WISDOM_FLOWER, MWDBlocks.ACHILLES_FLOWER,
                MWDBlocks.RED_PELTATUM, MWDBlocks.POTTED_RED_RELTATUM,
                MWDBlocks.FUCHSIA_PELTATUM, MWDBlocks.POTTED_FUCHSIA_PELTATUM,

                // tree
                MWDBlocks.CYAN_LOG, MWDBlocks.STRIPPED_CYAN_LOG, MWDBlocks.CYAN_WOOD, MWDBlocks.STRIPPED_CYAN_WOOD, MWDBlocks.CYAN_PLANKS, MWDBlocks.CYAN_LEAVES, MWDBlocks.CYAN_SAPLING, MWDBlocks.POTTED_CYAN_SAPLING,
                MWDBlocks.CYAN_STAIRS, MWDBlocks.CYAN_SLAB, MWDBlocks.CYAN_PRESSURE_PLATE, MWDBlocks.CYAN_FENCE, MWDBlocks.CYAN_FENCE_GATE, MWDBlocks.CYAN_BUTTON, MWDBlocks.CYAN_TRAPDOOR, MWDBlocks.CYAN_DOOR
        );
    }
}