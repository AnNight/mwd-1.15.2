package aliaohaolong.magicwithdrinks.init;

import aliaohaolong.magicwithdrinks.api.MWDExtraAttributes;
import aliaohaolong.magicwithdrinks.common.item.ExtraAttribute;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@SuppressWarnings("unused")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModExtraAttributes {
    @SubscribeEvent
    public static void onRegisterDrinks(RegistryEvent.Register<ExtraAttribute> event) {
        for (ExtraAttribute extraAttribute : MWDExtraAttributes.EXTRA_ATTRIBUTES)
            event.getRegistry().register(extraAttribute);
    }
}