package aliaohaolong.magicwithdrinks.client.gui.screen;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.BlenderContainer;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class BlenderScreen extends ContainerScreen<BlenderContainer> {
    private final ResourceLocation GUI = new ResourceLocation(MWD.MOD_ID, "textures/gui/container/blender.png");

    public BlenderScreen(BlenderContainer container, PlayerInventory inv, ITextComponent component) {
        super(container, inv, component);
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String str = this.title.getFormattedText();
        font.drawString(str, (float)(xSize / 2 - font.getStringWidth(str) / 2), 6.0F, 4210752);
        font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, (float)(ySize - 96 + 2), 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        assert minecraft != null;
        minecraft.getTextureManager().bindTexture(GUI);
        int i = guiLeft;
        int j = guiTop;
        blit(i, j, 0, 0, xSize, ySize);
        if (container.isBurning()) {
            int scaled = container.getBurnLeftScaled();
            blit(i + 56, j + 36 + 12 - scaled, 176, 12 - scaled, 14, scaled + 1);
        }
        if (container.isCooking()) {
            int scaled = container.getCookProgressionScaled();
            blit(i + 79, j + 34, 176, 14, scaled + 1, 16);
        }
    }
}