package aliaohaolong.magicwithdrinks.client.gui.screen;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.DrinkMakingMachineContainer;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class DrinkMakingMachineScreen extends ContainerScreen<DrinkMakingMachineContainer> {
    private static final ResourceLocation GUI = new ResourceLocation(MWD.MOD_ID, "textures/gui/container/drink_making_machine.png");
    private DrinkMakingMachineContainer container;

    public DrinkMakingMachineScreen(DrinkMakingMachineContainer container, PlayerInventory inv, ITextComponent component) {
        super(container, inv, component);
        this.container = container;
    }

    @Override
    protected void init() {
        super.init();
        this.addButton(new ImageButton(this.guiLeft + 7, this.height / 2 - 32, 17, 9, 176, 0, 18, GUI, (p_onPress_1_ -> {
            PacketManager.sendDMMActToServer(this.container.getPos(), false);
        })));
        this.addButton(new ImageButton(this.guiLeft + 7, this.height / 2 - 23, 17, 9, 176, 9, 18, GUI, (p_onPress_1_ -> {
            PacketManager.sendDMMActToServer(this.container.getPos(), true);
        })));
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        this.renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.font.drawString(this.title.getFormattedText(), 8.0F, 6.0F, 4210752);
        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, 72.0F, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.minecraft.getTextureManager().bindTexture(GUI);
        int relX = (this.width - 176) / 2;
        int relY = (this.height - 166) / 2;
        this.blit(relX, relY, 0, 0, 176, 166);
        int weight = this.container.getCookScaled();
        if (this.container.isCooking()) {
            this.blit(this.width / 2 + 30, this.height / 2 - 68, 193, 0, 22, 15);
            this.blit(this.width / 2 - 80, this.height / 2 - 42, 0, 166, 160 - weight, 4);
        }
    }
}