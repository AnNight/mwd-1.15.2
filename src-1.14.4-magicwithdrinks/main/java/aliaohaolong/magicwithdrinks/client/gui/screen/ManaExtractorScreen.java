package aliaohaolong.magicwithdrinks.client.gui.screen;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.ManaExtractorContainer;
import aliaohaolong.magicwithdrinks.common.tileentity.ManaExtractorTileEntity;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class ManaExtractorScreen extends ContainerScreen<ManaExtractorContainer> {
    private final ResourceLocation GUI = new ResourceLocation(MWD.MOD_ID, "textures/gui/container/mana_extractor.png");
    private byte catalystHeight = 0;

    public ManaExtractorScreen(ManaExtractorContainer container, PlayerInventory inv, ITextComponent component) {
        super(container, inv, component);
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        this.renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.font.drawString(this.title.getFormattedText(), 8.0F, 6.0F, 4210752);
        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, 72.0F, 4210752);
        String s = this.container.getBuffer() + "/" + this.container.getMaxBuffer();
        this.font.drawString(s, (float)(this.xSize / 2 - this.font.getStringWidth(s) / 2 - 8), 64F, 4210752);
        s = this.container.getCatalyst() + "/" + ManaExtractorTileEntity.getMaxCatalystSize();
        this.font.drawString(s, (float)(this.xSize / 2 - this.font.getStringWidth(s) / 2 - 54), 36F, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        assert this.minecraft != null;
        this.minecraft.getTextureManager().bindTexture(GUI);
        int relX = (this.width - 176) / 2;
        int relY = (this.height - 166) / 2;
        this.blit(relX, relY, 0, 0, 176, 166);
        if (this.container.isCooking()) {
            this.catalystHeight++;
            if (this.catalystHeight > 28) this.catalystHeight = 0;
            this.blit(this.width / 2 + 16, this.height / 2 - 56, 187, 0, 7, this.container.getCookScaled() + 1);
            this.blit(this.width / 2 - 16, this.height / 2 - 61 + 28 - catalystHeight, 176, 28 - catalystHeight, 11, catalystHeight);
        }
        if (this.container.hasCatalyst()) {
            int scaled = this.container.getCatalystScaled();
            this.blit(this.width / 2 - 24, this.height / 2 - 31 - scaled, 176, 60 - scaled, 4, scaled);
        }
        if (this.container.hasValue())
            this.blit(this.width / 2 - 39, this.height / 2 - 26, 176, 60, this.container.getBufferScaled(), 4);
    }
}