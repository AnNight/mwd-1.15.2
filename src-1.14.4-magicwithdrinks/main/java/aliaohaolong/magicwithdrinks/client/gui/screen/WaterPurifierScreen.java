package aliaohaolong.magicwithdrinks.client.gui.screen;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.WaterPurifierContainer;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class WaterPurifierScreen extends ContainerScreen<WaterPurifierContainer> {
    private final ResourceLocation GUI = new ResourceLocation(MWD.MOD_ID, "textures/gui/container/water_purifier.png");

    public WaterPurifierScreen(WaterPurifierContainer container, PlayerInventory inv, ITextComponent component) {
        super(container, inv, component);
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(); // darken
        super.render(mouseX, mouseY, partialTicks); // draw mouse
        this.renderHoveredToolTip(mouseX, mouseY); // show tips of items when mouse pass
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.font.drawString(this.title.getFormattedText(), 8.0F, 6.0F, 4210752);
        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, 72.0F, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        assert this.minecraft != null;
        this.minecraft.getTextureManager().bindTexture(GUI);
        int relX = (this.width - 176) / 2;
        int relY = (this.height - 166) / 2;
        this.blit(relX, relY, 0, 0, 176, 166);
        if (this.container.isBurning()) {
            int burnLeftScaled = this.container.getBurnLeftScaled();
            this.blit(this.width / 2 - 53, this.height / 2 - 35 - burnLeftScaled, 176, 12 - burnLeftScaled, 14, burnLeftScaled + 1);
            if (this.container.isCooking()) {
                int cookProgressionScaled = this.container.getCookProgressionScaled();
                if (cookProgressionScaled > 11) {
                    this.blit(this.width / 2 - 29, this.height / 2 - 47, 176, 13, 11, 16);
                    this.blit(this.width / 2, this.height / 2 - 47, 187, 13, cookProgressionScaled - 11, 16);
                } else {
                    this.blit(this.width / 2 - 29, this.height / 2 - 47, 176, 13, cookProgressionScaled, 16);
                }
            }
        }
    }
}