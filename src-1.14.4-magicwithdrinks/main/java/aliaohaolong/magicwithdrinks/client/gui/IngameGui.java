package aliaohaolong.magicwithdrinks.client.gui;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import net.minecraft.client.Minecraft;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.ResourceLocation;

public class IngameGui {
    private static final ResourceLocation ICONS = new ResourceLocation(MWD.MOD_ID,"textures/gui/icons.png");
    private static final ResourceLocation original = new ResourceLocation("minecraft", "textures/gui/icons.png");

    public static void renderWater() {
        Minecraft mc = Minecraft.getInstance();
        if (!mc.player.isAlive()) return;
        int value = IWater.getFromPlayer(mc.player).getValue();
        int x = mc.mainWindow.getScaledWidth() / 2 + 83;
        int y = mc.player.areEyesInFluid(FluidTags.WATER) || mc.player.getAir() < mc.player.getMaxAir() ? mc.mainWindow.getScaledHeight() - 59 : mc.mainWindow.getScaledHeight() - 49;

        mc.getTextureManager().bindTexture(ICONS);
        if (value == 0) {
            for (int i = 0; i < 10; i++) {
                mc.ingameGUI.blit(x - i * 8, y, 33, 0, 7, 9);
            }
        } else if (value <= 25) {
            int control = value / 5;
            int half = 5 - value % 5;
            for (int i = 0; i < 10; i++) {
                if (i < control)
                    mc.ingameGUI.blit(x - i * 8, y, 27, 0, 7, 9);
                else if (i == control) {
                    mc.ingameGUI.blit(x - i * 8, y, 15, 0, 7, 9);
                    mc.ingameGUI.blit(x - i * 8 + half + 1, y, 28 + half, 0, 6 - half, 9);
                } else
                    mc.ingameGUI.blit(x - i * 8, y, 15, 0, 7, 9);
            }
        } else {
            int control = value / 5;
            int half = 5 - value % 5;
            for (int i = 0; i < 10; i++) {
                if (i < control)
                    mc.ingameGUI.blit(x - i * 8, y, 21, 0, 7, 9);
                else if (i == control) {
                    mc.ingameGUI.blit(x - i * 8, y, 15, 0, 7, 9);
                    mc.ingameGUI.blit(x - i * 8 + half + 1, y, 22 + half, 0, 6 - half, 9);
                } else
                    mc.ingameGUI.blit(x - i * 8, y, 15, 0, 7, 9);
            }
        }
        mc.getTextureManager().bindTexture(original);
    }

    public static void renderMana() {
        Minecraft mc = Minecraft.getInstance();
        if (!mc.player.isAlive()) return;
        IMana mana = IMana.getFromPlayer(mc.player);
        int x = mc.mainWindow.getScaledWidth() / 100 * 2;
        int y = mc.mainWindow.getScaledHeight() / 100 * 3;
        int mutable = (int) ((double) mana.getValue() / mana.getLimit() * 120);
        mc.getTextureManager().bindTexture(ICONS);
        mc.ingameGUI.blit(x, y, 0, 0, 15, 133);
        mc.ingameGUI.blit(x + 4, y + 10, 15, 9, 7, mutable);
        mc.ingameGUI.blit(x + 4, y + 10 + mutable, 22, 9 + mutable, 7, 133 - mutable);
        if (mana.getLock())
            mc.ingameGUI.blit(x + 4, y + 10, 29, 9, 7, 120);
        mc.getTextureManager().bindTexture(original);
    }
}