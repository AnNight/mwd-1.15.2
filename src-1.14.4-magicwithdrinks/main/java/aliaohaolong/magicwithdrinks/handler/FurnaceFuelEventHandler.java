package aliaohaolong.magicwithdrinks.handler;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FurnaceFuelEventHandler {
    @SubscribeEvent
    public void onFurnaceFuelBurnTime(FurnaceFuelBurnTimeEvent event) {
        if (event.getItemStack().getItem() == MWDItems.COAL_NUGGET)
            event.setBurnTime(320);
        else if (event.getItemStack().getItem() == MWDItems.CHARCOAL_NUGGET)
            event.setBurnTime(320);
    }
}