package aliaohaolong.magicwithdrinks.handler;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.drink.Drink;
import aliaohaolong.magicwithdrinks.common.item.ExtraAttribute;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import net.minecraft.block.Blocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.Difficulty;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.RegistryBuilder;

import java.util.Random;

@SuppressWarnings("unused")
public class EventHandler {
    @SubscribeEvent
    public static void onNewRegistry(RegistryEvent.NewRegistry event) {
        RegistryBuilder<Drink> drinkRegistryBuilder = new RegistryBuilder<>();
        drinkRegistryBuilder.setType(Drink.class);
        ResourceLocation drinkRL = new ResourceLocation(MWD.MOD_ID, "drink");
        drinkRegistryBuilder.setName(drinkRL);
        drinkRegistryBuilder.setDefaultKey(drinkRL);
        drinkRegistryBuilder.create();

        RegistryBuilder<ExtraAttribute> extraAttributeRegistryBuilder = new RegistryBuilder<>();
        extraAttributeRegistryBuilder.setType(ExtraAttribute.class);
        ResourceLocation extraAttributeRL = new ResourceLocation(MWD.MOD_ID, "extra_attribute");
        extraAttributeRegistryBuilder.setName(extraAttributeRL);
        extraAttributeRegistryBuilder.setDefaultKey(extraAttributeRL);
        extraAttributeRegistryBuilder.create();
    }

    /**
     * MC invoke twice this per tick.
     * Run on Server & Client.<br>
     */
    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
        if (event.phase == TickEvent.Phase.START && event.side.isServer() && event.player.isAlive() && !event.player.isCreative() && !event.player.isSpectator()) {
            boolean sync = false;
            IWater water = IWater.getFromPlayer(event.player);
            if (event.player.world.getDifficulty() != Difficulty.PEACEFUL) {
                if (water.getValue() > 0) {
                    int temperature = (int) Math.sqrt(event.player.getEntityWorld().getBiome(event.player.getPosition()).getDefaultTemperature() + 0.2) * 3;
                    if (temperature >= 1 && !event.player.isInWaterOrBubbleColumn() && event.player.getEntityWorld().getDayTime() > 2000 && event.player.getEntityWorld().getDayTime() < 14000) if (water.operationInner(temperature)) sync = true;
                    if (event.player.isSwimming() || event.player.isRidingOrBeingRiddenBy(event.player) || event.player.isElytraFlying()) if (water.operationInner(2)) sync = true;
                    if (event.player.isBurning() && !event.player.isImmuneToFire()) if (water.operationInner(80)) sync = true;
                    if (event.player.isInLava() && !event.player.isImmuneToFire()) if (water.operationInner(120)) sync = true;
                    if (event.player.isSprinting()) {
                        if (water.operationInner(50)) sync = true;
                    } else if (water.operationInner(1)) sync = true;
                }
                if (water.getValue() == 0 || water.getValue() > IWater.MAX - 3) if (water.tick(event.player)) sync = true;
            } else if (water.getValue() < IWater.MAX) if (water.operationInner(-1200)) sync = true;
            if (sync) PacketManager.sendWaterTo((ServerPlayerEntity) event.player, water);
        }
    }

    @SubscribeEvent
    public void onBreak(BlockEvent.BreakEvent event) {
        if (event.getPlayer() instanceof ServerPlayerEntity) {
            tool(event.getPlayer(), 14);
            if ((event.getState().getBlock() == Blocks.GRASS || event.getState().getBlock() == Blocks.TALL_GRASS) &&
                    !event.getPlayer().isCreative() && !event.getPlayer().isSpectator() &&
                    event.getWorld().getRandom().nextInt(16) == 0) {
                InventoryHelper.spawnItemStack(event.getWorld().getWorld(), event.getPos().getX(), event.getPos().getY(), event.getPos().getZ(), new ItemStack(MWDItems.COTTON_SEEDS));
            }
        }
    }

    @SubscribeEvent
    public void onEntityPlace(BlockEvent.EntityPlaceEvent event) {
        if (event.getEntity() instanceof ServerPlayerEntity) {
            tool((PlayerEntity) event.getEntity(), 10);
        }
    }

    @SubscribeEvent
    public void onAttackEntity(AttackEntityEvent event) {
        if (event.getPlayer() instanceof ServerPlayerEntity) {
            tool((PlayerEntity) event.getEntityLiving(), 18);
        }
    }

    @SubscribeEvent
    public void onLivingJump(LivingEvent.LivingJumpEvent event) {
        if (event.getEntityLiving() instanceof ServerPlayerEntity) {
            tool((PlayerEntity) event.getEntityLiving(), 100);
        }
    }

    @SubscribeEvent
    public void onUseItem(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof ServerPlayerEntity && event.getEntityLiving().isAlive()) {
            ExtraAttribute extraAttribute = MWDForgeRegistries.getExtraAttributes().getValue(event.getItem().getItem().getRegistryName());
            if (extraAttribute != null) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                IWater water = IWater.getFromPlayer(player);
                boolean sync = false;
                if (extraAttribute.getPossibility() > 0) {
                    if (new Random().nextInt(extraAttribute.getPossibility()) == 0) {
                        if (water.limitedReduce(extraAttribute.getWater())) sync = true;
                    } else {
                        if (water.limitedIncrease(extraAttribute.getWater())) sync = true;
                    }
                } else if (extraAttribute.getWater() != 0) {
                    if (water.limitedIncrease(extraAttribute.getWater())) sync = true;
                }
                if (sync) PacketManager.sendWaterTo((ServerPlayerEntity) player, water);
                IMana mana = IMana.getFromPlayer(player);
                sync = false;
                if (extraAttribute.getMana() != 0) {
                    if (mana.limitedIncreaseValue(extraAttribute.getMana())) sync = true;
                }
                if (extraAttribute.getMax_mana() != 0) {
                    if (mana.limitedIncreaseLimit(extraAttribute.getMax_mana())) sync = true;
                }
                if (sync) PacketManager.sendManaTo((ServerPlayerEntity) player, mana);
            }
        }
    }

    @SubscribeEvent
    public void onItemTooltip(ItemTooltipEvent event) {
        ExtraAttribute extraAttribute = MWDForgeRegistries.getExtraAttributes().getValue(event.getItemStack().getItem().getRegistryName());
        if (extraAttribute != null) {
            ITextComponent water = null;
            if (extraAttribute.getPossibility() > 0)
                water = new TranslationTextComponent("drink.lore.waterP", extraAttribute.getWater()).applyTextStyle(TextFormatting.RED);
            else if (extraAttribute.getWater() > 0)
                water = new TranslationTextComponent("drink.lore.water+", extraAttribute.getWater()).applyTextStyle(TextFormatting.BLUE);
            else if (extraAttribute.getWater() < 0)
                water = new TranslationTextComponent("drink.lore.water", extraAttribute.getWater()).applyTextStyle(TextFormatting.RED);

            ITextComponent mana = null;
            if (extraAttribute.getMana() > 0)
                mana = new TranslationTextComponent("drink.lore.mana+", extraAttribute.getMana()).applyTextStyle(TextFormatting.BLUE);
            else if (extraAttribute.getMana() < 0)
                mana = new TranslationTextComponent("drink.lore.mana", extraAttribute.getMana()).applyTextStyle(TextFormatting.RED);

            ITextComponent max_mana = null;
            if (extraAttribute.getMax_mana() > 0)
                max_mana = new TranslationTextComponent("drink.lore.max_mana+", extraAttribute.getMax_mana()).applyTextStyle(TextFormatting.BLUE);
            else if (extraAttribute.getMax_mana() < 0)
                max_mana = new TranslationTextComponent("drink.lore.max_mana", extraAttribute.getMax_mana()).applyTextStyle(TextFormatting.RED);

            if (event.getFlags() == ITooltipFlag.TooltipFlags.ADVANCED) {
                if (event.getItemStack().hasTag()) {
                    ITextComponent name = event.getToolTip().get(event.getToolTip().size() - 2);
                    ITextComponent tag = event.getToolTip().get(event.getToolTip().size() - 1);
                    event.getToolTip().remove(event.getToolTip().size() - 1);
                    event.getToolTip().remove(event.getToolTip().size() - 1);
                    if (water != null) event.getToolTip().add(water);
                    if (mana != null) event.getToolTip().add(mana);
                    if (max_mana != null) event.getToolTip().add(max_mana);
                    event.getToolTip().add(name);
                    event.getToolTip().add(tag);
                } else {
                    ITextComponent last = event.getToolTip().get(event.getToolTip().size() - 1);
                    event.getToolTip().remove(event.getToolTip().size() - 1);
                    if (water != null) event.getToolTip().add(water);
                    if (mana != null) event.getToolTip().add(mana);
                    if (max_mana != null) event.getToolTip().add(max_mana);
                    event.getToolTip().add(last);
                }
            } else {
                if (water != null) event.getToolTip().add(water);
                if (mana != null) event.getToolTip().add(mana);
                if (max_mana != null) event.getToolTip().add(max_mana);
            }
        }
    }

    /**
     * Run on server.
     */
    private static void tool(PlayerEntity player, int value) {
        if (player.isAlive() && !player.isCreative() && !player.isSpectator() && player.world.getDifficulty() != Difficulty.PEACEFUL && !IWater.getFromPlayer(player).getLock()) {
            IWater water = IWater.getFromPlayer(player);
            if (water.operationInner(value))
                PacketManager.sendWaterTo((ServerPlayerEntity) player, water);
        }
    }
}