package aliaohaolong.magicwithdrinks.handler;

import aliaohaolong.magicwithdrinks.client.gui.IngameGui;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ClientEventHandler {
    @SubscribeEvent
    public void onRenderGameOverlays(RenderGameOverlayEvent.Post event) {
        if (event.getType() == RenderGameOverlayEvent.ElementType.HEALTH) {
            IngameGui.renderWater();
            // IngameGui.renderMana();
        }
    }
}