package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.inventory.*;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.ResourceLocation;

@SuppressWarnings("SameParameterValue")
public class MWDContainerType {
    public static final ContainerType<WaterPurifierContainer> WATER_PURIFIER = init(new ContainerType<>(WaterPurifierContainer::new), "water_purifier");
    public static final ContainerType<DrinkMakingMachineContainer> DRINK_MAKING_MACHINE = init(new ContainerType<>(DrinkMakingMachineContainer::new), "drink_making_machine");
    public static final ContainerType<ManaExtractorContainer> MANA_EXTRACTOR = init(new ContainerType<>(ManaExtractorContainer::new), "mana_extractor");
    public static final ContainerType<MillContainer> MILL = init(new ContainerType<>(MillContainer::new), "mill");
    public static final ContainerType<BlenderContainer> BLENDER = init(new ContainerType<>(BlenderContainer::new), "blender");

    private static <C extends Container, CT extends ContainerType<C>> CT init(CT containerType, String name) {
        containerType.setRegistryName(new ResourceLocation(MWD.MOD_ID, name));
        return containerType;
    }
}