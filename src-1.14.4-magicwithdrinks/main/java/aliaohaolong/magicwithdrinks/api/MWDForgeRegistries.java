package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.common.drink.Drink;
import aliaohaolong.magicwithdrinks.common.item.ExtraAttribute;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class MWDForgeRegistries {
    private static IForgeRegistry<Drink> drinks;
    private static IForgeRegistry<ExtraAttribute> extraAttributes;

    public static IForgeRegistry<Drink> getDrinks() {
        if (drinks == null)
            drinks = GameRegistry.findRegistry(Drink.class);
        return drinks;
    }

    public static IForgeRegistry<ExtraAttribute> getExtraAttributes() {
        if (extraAttributes == null)
            extraAttributes = GameRegistry.findRegistry(ExtraAttribute.class);
        return extraAttributes;
    }
}