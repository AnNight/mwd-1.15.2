package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.item.crafting.BlenderRecipe;
import aliaohaolong.magicwithdrinks.common.item.crafting.MillRecipe;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;

public class MWDRecipe {
    public static IRecipeSerializer<MillRecipe> MILLING_S = initS(new MillRecipe.Serializer(), "milling");
    public static IRecipeSerializer<BlenderRecipe> BLENDING_S = initS(new BlenderRecipe.Serializer(), "blending");
    public static IRecipeType<MillRecipe> MILLING_T = initT("milling");
    public static IRecipeType<BlenderRecipe> BLENDING_T = initT("blending");

    private static <R extends IRecipe<?>, RS extends IRecipeSerializer<R>> RS initS(RS recipeSerializer, String name) {
        recipeSerializer.setRegistryName(new ResourceLocation(MWD.MOD_ID, name));
        return recipeSerializer;
    }

    static <T extends IRecipe<?>> IRecipeType<T> initT(String name) {
        return Registry.register(Registry.RECIPE_TYPE, new ResourceLocation(MWD.MOD_ID, name), new IRecipeType<T>() {
            @Override
            public String toString() {
                return name;
            }
        });
    }
}