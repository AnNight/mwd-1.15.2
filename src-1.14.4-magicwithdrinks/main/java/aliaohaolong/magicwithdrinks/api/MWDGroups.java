package aliaohaolong.magicwithdrinks.api;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class MWDGroups {
    public static final ItemGroup BLOCKS = new ItemGroup("magicwithdrinks_blocks") {
        public ItemStack createIcon() {
            return new ItemStack(MWDItems.WOODEN_CASING);
        }
    };

    public static final ItemGroup MATERIALS = new ItemGroup("magicwithdrinks_materials") {
        public ItemStack createIcon() {
            return new ItemStack(MWDItems.FEATHERY_FILTER_SLICE);
        }
    };
}