package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.tileentity.*;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;

public class MWDTileEntityType {
    public static final TileEntityType<?> WATER_PURIFIER = init(TileEntityType.Builder.create(WaterPurifierTileEntity::new, MWDBlocks.WATER_PURIFIER).build(null), "water_purifier");
    public static final TileEntityType<?> DRINK_MAKING_MACHINE = init(TileEntityType.Builder.create(DrinkMakingMachineTileEntity::new, MWDBlocks.DRINK_MAKING_MACHINE).build(null), "drink_making_machine");
    public static final TileEntityType<?> MANA_EXTRACTOR = init(TileEntityType.Builder.create(ManaExtractorTileEntity::new, MWDBlocks.MANA_EXTRACTOR).build(null), "mana_extractor");
    public static final TileEntityType<?> MILL = init(TileEntityType.Builder.create(MillTileEntity::new, MWDBlocks.MILL).build(null), "mill");
    public static final TileEntityType<?> BLENDER = init(TileEntityType.Builder.create(BlenderTileEntity::new, MWDBlocks.BLENDER).build(null), "blender");
    public static final TileEntityType<?> BREWING_MACHINE = init(TileEntityType.Builder.create(BrewingMachineTileEntity::new, MWDBlocks.BREWING_MACHINE).build(null), "brewing_machine");

    private static <T extends TileEntity, TT extends TileEntityType<T>> TT init(TT tileEntityType, String name) {
        tileEntityType.setRegistryName(new ResourceLocation(MWD.MOD_ID, name));
        return tileEntityType;
    }
}