package aliaohaolong.magicwithdrinks.api;

import net.minecraft.state.BooleanProperty;

public class MWDBlockStateProperties {
    public static final BooleanProperty SWITCH = BooleanProperty.create("switch");
    public static final BooleanProperty HAS_FRUIT = BooleanProperty.create("has_fruit");
}