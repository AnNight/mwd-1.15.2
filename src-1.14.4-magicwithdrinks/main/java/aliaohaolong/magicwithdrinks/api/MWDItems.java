package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.common.item.*;
import aliaohaolong.magicwithdrinks.common.item.Foods;
import net.minecraft.block.Block;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;

import java.util.Objects;

public class MWDItems {
    public static final Item DRINK_BOTTLE = new DrinkBottleItem(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("drink_bottle");
    public static final Item DRINK = new DrinkItem(new Item.Properties().maxStackSize(1).group(MWDGroups.MATERIALS)).setRegistryName("drink");
    public static final Item WOODEN_CASING = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("wooden_casing");
    public static final Item IRON_CASING = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("iron_casing");
    public static final Item DIAMOND_CORE = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("diamond_core");
    public static final Item MAGIC_CORE = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("magic_core");

    // wand
    public static final Item PLANTS_WAND = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("plants_wand");

    // material
    public static final Item COAL_NUGGET = new FilterSliceItem(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("coal_nugget");
    public static final Item CHARCOAL_NUGGET = new FilterSliceItem(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("charcoal_nugget");
    public static final Item HARD_STONE_POWDER = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("hard_stone_powder");
    public static final Item CATALYST = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("catalyst");
    public static final Item IMPURITY = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("impurity");
    public static final Item SMALL_PILE_OF_IMPURITY = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("small_pile_of_impurity");

    // battery
    public static final Item MAGIC_GEM_BATTERY = new ManaItem(new Item.Properties().group(MWDGroups.MATERIALS), (short) 16, (byte) 1).setRegistryName("magic_gem_battery");

    // filter slice & card
    public static final Item FEATHERY_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(12).group(MWDGroups.MATERIALS)).setRegistryName("feathery_filter_slice");
    public static final Item IMPROVED_FEATHERY_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(16).group(MWDGroups.MATERIALS)).setRegistryName("improved_feathery_filter_slice");
    public static final Item WOOLEN_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(14).group(MWDGroups.MATERIALS)).setRegistryName("woolen_filter_slice");
    public static final Item IMPROVED_WOOLEN_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(18).group(MWDGroups.MATERIALS)).setRegistryName("improved_woolen_filter_slice");
    public static final Item COTTONY_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(16).group(MWDGroups.MATERIALS)).setRegistryName("cottony_filter_slice");
    public static final Item IMPROVED_COTTONY_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(20).group(MWDGroups.MATERIALS)).setRegistryName("improved_cottony_filter_slice");
//    public static final Item INTENSIVE_FEATHERY_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(40).group(MWDGroups.MATERIALS)).setRegistryName("intensive_feathery_filter_slice");
//    public static final Item INTENSIVE_COTTONY_FILTER_SLICE = new FilterSliceItem(new Item.Properties().maxDamage(50).group(MWDGroups.MATERIALS)).setRegistryName("intensive_cottony_filter_slice");
    public static final Item ELEMENTARY_CAPACITY_CARD = new CapacityCardItem(new Item.Properties().group(MWDGroups.MATERIALS), 32, (byte) 1).setRegistryName("elementary_capacity_card");
    public static final Item INTERMEDIATE_CAPACITY_CARD = new CapacityCardItem(new Item.Properties().group(MWDGroups.MATERIALS), 128, (byte) 4).setRegistryName("intermediate_capacity_card");
    public static final Item ADVANCED_CAPACITY_CARD = new CapacityCardItem(new Item.Properties().group(MWDGroups.MATERIALS), 512, (byte) 16).setRegistryName("advanced_capacity_card");
    public static final Item ELEMENTARY_EFFICIENCY_CARD = new EfficiencyCardItem(new Item.Properties().group(MWDGroups.MATERIALS), (byte) 2).setRegistryName("elementary_efficiency_card");
    public static final Item INTERMEDIATE_EFFICIENCY_CARD = new EfficiencyCardItem(new Item.Properties().group(MWDGroups.MATERIALS), (byte) 4).setRegistryName("intermediate_efficiency_card");
    public static final Item ADVANCED_EFFICIENCY_CARD = new EfficiencyCardItem(new Item.Properties().group(MWDGroups.MATERIALS), (byte) 8).setRegistryName("advanced_efficiency_card");

    //crop
    public static final Item COTTON_SEEDS = new BlockNamedItem(MWDBlocks.COTTON, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("cotton_seeds");
    public static final Item COTTON = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("cotton");

    // machine
    public static final Item WATER_PURIFIER = build(MWDBlocks.WATER_PURIFIER);
    public static final Item DRINK_MAKING_MACHINE = build(MWDBlocks.DRINK_MAKING_MACHINE);
    public static final Item MANA_EXTRACTOR = build(MWDBlocks.MANA_EXTRACTOR);
    public static final Item MILL = build(MWDBlocks.MILL);
    public static final Item BLENDER = build(MWDBlocks.BLENDER);
    public static final Item BREWING_MACHINE = build(MWDBlocks.BREWING_MACHINE);

    // block
    public static final Item HARD_STONE = build(MWDBlocks.HARD_STONE);
    public static final Item ACHILLES_BLOCK = build(MWDBlocks.ACHILLES_BLOCK);
    public static final Item SILVER_BLOCK = build(MWDBlocks.SILVER_BLOCK);

    // ore
    public static final Item MAGIC_GEM_ORE = build(MWDBlocks.MAGIC_GEM_ORE);
    public static final Item ACHILLES_ORE = build(MWDBlocks.ACHILLES_ORE);
    public static final Item SILVER_ORE = build(MWDBlocks.SILVER_ORE);
    public static final Item DENATURED_SILICON_ORE = build(MWDBlocks.DENATURED_SILICON_ORE);

    // something related to ores
    public static final Item MAGIC_GEM = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("magic_gem");
    public static final Item ACHILLES_INGOT = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_ingot");
    public static final Item ACHILLES_NUGGET = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_nugget");
    public static final Item SILVER_INGOT = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("silver_ingot");
    public static final Item SILVER_NUGGET = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("silver_nugget");
    public static final Item DENATURED_SILICON = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("denatured_silicon");

    // tools
    public static final Item ACHILLES_SWORD = new SwordItem(MWDItemTier.ACHILLES, 4, -2.4F, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_sword");
    public static final Item ACHILLES_PICKAXE = new PickaxeItem(MWDItemTier.ACHILLES, 1, -2.8F, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_pickaxe");
    public static final Item ACHILLES_AXE = new AxeItem(MWDItemTier.ACHILLES, 5.0F, -3.0F, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_axe");
    public static final Item ACHILLES_SHOVEL = new ShovelItem(MWDItemTier.ACHILLES, 1.5F, -3.0F, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_shovel");
    public static final Item ACHILLES_HOE = new HoeItem(MWDItemTier.ACHILLES, 0.0F, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_hoe");

    // equip
    public static final Item ACHILLES_HELMET = new ArmorItem(MWDArmorMaterials.ACHILLES, EquipmentSlotType.HEAD, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_helmet");
    public static final Item ACHILLES_CHESTPLATE = new ArmorItem(MWDArmorMaterials.ACHILLES, EquipmentSlotType.CHEST, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_chestplate");
    public static final Item ACHILLES_LEGGINGS = new ArmorItem(MWDArmorMaterials.ACHILLES, EquipmentSlotType.LEGS, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_leggings");
    public static final Item ACHILLES_BOOTS = new ArmorItem(MWDArmorMaterials.ACHILLES, EquipmentSlotType.FEET, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("achilles_boots");

    // flower
    public static final Item MOONLIGHT_VINE = new Item(new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("moonlight_vine");
    public static final Item MOONLIGHT_BERRIES = new MoonlightBerriesItem(MWDBlocks.MOONLIGHT_VINE, new Item.Properties().group(MWDGroups.MATERIALS)).setRegistryName("moonlight_berries");
    public static final Item CYAN_MUSHROOM = build(MWDBlocks.CYAN_MUSHROOM);
    public static final Item WISDOM_FLOWER = build(MWDBlocks.WISDOM_FLOWER);
    public static final Item ACHILLES_FLOWER = build(MWDBlocks.ACHILLES_FLOWER);
    public static final Item RED_PELTATUM = build(MWDBlocks.RED_PELTATUM);
    public static final Item FUCHSIA_PELTATUM = build(MWDBlocks.FUCHSIA_PELTATUM);

    // cyan tree
    public static final Item CYAN_FRUIT = new Item(new Item.Properties().food(Foods.CYAN_FRUIT).group(MWDGroups.MATERIALS)).setRegistryName("cyan_fruit");
    public static final Item CYAN_LOG = build(MWDBlocks.CYAN_LOG);
    public static final Item CYAN_WOOD = build(MWDBlocks.CYAN_WOOD);
    public static final Item STRIPPED_CYAN_LOG = build(MWDBlocks.STRIPPED_CYAN_LOG);
    public static final Item STRIPPED_CYAN_WOOD = build(MWDBlocks.STRIPPED_CYAN_WOOD);
    public static final Item CYAN_LEAVES = build(MWDBlocks.CYAN_LEAVES);
    public static final Item CYAN_SAPLING = build(MWDBlocks.CYAN_SAPLING);
    public static final Item CYAN_PLANKS = build(MWDBlocks.CYAN_PLANKS);
    public static final Item CYAN_STAIRS = build(MWDBlocks.CYAN_STAIRS);
    public static final Item CYAN_SLAB = build(MWDBlocks.CYAN_SLAB);
    public static final Item CYAN_PRESSURE_PLATE = build(MWDBlocks.CYAN_PRESSURE_PLATE);
    public static final Item CYAN_FENCE = build(MWDBlocks.CYAN_FENCE);
    public static final Item CYAN_FENCE_GATE = build(MWDBlocks.CYAN_FENCE_GATE);
    public static final Item CYAN_BUTTON = build(MWDBlocks.CYAN_BUTTON);
    public static final Item CYAN_TRAPDOOR = build(MWDBlocks.CYAN_TRAPDOOR);
    public static final Item CYAN_DOOR = new TallBlockItem(MWDBlocks.CYAN_DOOR, new Item.Properties().group(MWDGroups.BLOCKS)).setRegistryName("cyan_door");

    private static Item build(Block block) {
        return new BlockItem(block, new Item.Properties().group(MWDGroups.BLOCKS)).setRegistryName(Objects.requireNonNull(block.getRegistryName()));
    }
}