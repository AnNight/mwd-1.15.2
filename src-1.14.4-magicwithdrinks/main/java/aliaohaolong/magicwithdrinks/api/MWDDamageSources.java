package aliaohaolong.magicwithdrinks.api;

import net.minecraft.util.DamageSource;

public class MWDDamageSources {
    public static final DamageSource PARCHED = (new DamageSource("parched")).setDamageBypassesArmor().setDamageIsAbsolute();
    public static final DamageSource POISON = (new DamageSource("poison")).setDamageBypassesArmor().setDamageIsAbsolute();
}
