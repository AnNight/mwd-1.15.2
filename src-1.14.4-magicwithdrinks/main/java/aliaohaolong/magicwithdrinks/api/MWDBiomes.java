package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.world.biome.RelicPlainsBiome;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary.Type;

public class MWDBiomes {
    public static final Type MWD_THIN = Type.getType("MWD_THIN");
    public static final Type MWD_RICH = Type.getType("MWD_RICH");

    public static final Biome RELIC_PLAINS = new RelicPlainsBiome().setRegistryName(MWD.MOD_ID, "relic_plains");
}