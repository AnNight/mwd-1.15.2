package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.drink.Drink;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ResourceLocation;

@SuppressWarnings("unused")
public class MWDDrinks {
    public static final Drink UNREAL_DRINK = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "unreal_drink"), 0, 0));
    public static final Drink WATER = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "water"), 3694022, 10));
    public static final Drink PURIFIED_WATER = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "purified_water"), 7915760, 10));
    public static final Drink SUGAR_WATER = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "sugar_water"), 15721648, 10).hunger(1, 0.2F));
    // juice
    public static final Drink APPLE_JUICE = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "apple_juice"), 15841885, 8).hunger(5, 0.5F)); // 4, 0.3; + sugar
    public static final Drink MELON_JUICE = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "melon_juice"), 12529955, 8).hunger(5, 0.8F)); // 2, 0.3; *2 + sugar
    public static final Drink CARROT_JUICE = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "carrot_juice"), 16748041, 8).hunger(4, 0.8F)); // 3, 0.6; + sugar
    public static final Drink PUMPKIN_JUICE = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "pumpkin_juice"), 14912029, 8).hunger(5, 0.3F)); // 4, 0.1; + sugar
    public static final Drink SWEET_BERRIES_JUICE = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "sweet_berries_juice"), 14632574, 8).hunger(5, 0.4F)); // 2, 0.1; *2 + sugar
    // wine
    public static final Drink APPLE_WINE = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "apple_wine"), APPLE_JUICE.getColor(), 2,
            new EffectInstance(Effects.REGENERATION, 320),
            new EffectInstance(Effects.NAUSEA, 240)
    ).hunger(2, 0.15F).setAlwaysEdible());
    public static final Drink SWEET_BERRIES_WINE = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "sweet_berries_wine"), SWEET_BERRIES_JUICE.getColor(), 2,
            new EffectInstance(Effects.SPEED, 480),
            new EffectInstance(Effects.NAUSEA, 360)
    ).hunger(1, 0.05F).setAlwaysEdible());
    // other
    public static final Drink ACTIVE_SUBSTANCE = init(new Drink.Properties(new ResourceLocation(MWD.MOD_ID, "active_substance"), 2752528, -4));

    private static Drink init(Drink.Properties properties) {
        Drink drink = new Drink(properties);
        drink.setRegistryName(drink.getResourceLocation());
        return drink;
    }
}