package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.common.block.*;
import aliaohaolong.magicwithdrinks.common.block.DoorBlock;
import aliaohaolong.magicwithdrinks.common.block.FlowerBlock;
import aliaohaolong.magicwithdrinks.common.block.MushroomBlock;
import aliaohaolong.magicwithdrinks.common.block.OreBlock;
import aliaohaolong.magicwithdrinks.common.block.PressurePlateBlock;
import aliaohaolong.magicwithdrinks.common.block.SaplingBlock;
import aliaohaolong.magicwithdrinks.common.block.StairsBlock;
import aliaohaolong.magicwithdrinks.common.block.TrapDoorBlock;
import aliaohaolong.magicwithdrinks.common.block.WoodButtonBlock;
import aliaohaolong.magicwithdrinks.common.block.trees.CyanTree;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.potion.Effects;
import net.minecraftforge.common.ToolType;

@SuppressWarnings("deprecation")
public class MWDBlocks {
    // crop
    public static final Block COTTON = new CottonBlock(Block.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.CROP)).setRegistryName("cotton");

    // machine
    public static final Block WATER_PURIFIER = new WaterPurifierBlock(Block.Properties.create(Material.WOOD).hardnessAndResistance(2.5F).sound(SoundType.WOOD)).setRegistryName("water_purifier");
    public static final Block DRINK_MAKING_MACHINE = new DrinkMakingMachineBlock(Block.Properties.create(Material.WOOD).hardnessAndResistance(2.5F).sound(SoundType.WOOD)).setRegistryName("drink_making_machine");
    public static final Block MANA_EXTRACTOR = new ManaExtractorBlock(Block.Properties.create(Material.WOOD).hardnessAndResistance(2.0F, 6.0F).sound(SoundType.STONE)).setRegistryName("mana_extractor");
    public static final Block MILL = new MillBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).sound(SoundType.STONE)).setRegistryName("mill");
    public static final Block BLENDER = new BlenderBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).sound(SoundType.STONE)).setRegistryName("blender");
    public static final Block BREWING_MACHINE = new BrewingMachineBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 6.0F).sound(SoundType.STONE)).setRegistryName("brewing_machine");

    // block
    public static final Block HARD_STONE = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(30F, 720F).harvestTool(ToolType.PICKAXE).harvestLevel(3).sound(SoundType.STONE)).setRegistryName("hard_stone");
    public static final Block ACHILLES_BLOCK = new Block(Block.Properties.create(Material.IRON, MaterialColor.CYAN).hardnessAndResistance(3.0F, 6.0F).sound(SoundType.METAL)).setRegistryName("achilles_block");
    public static final Block SILVER_BLOCK = new Block(Block.Properties.create(Material.IRON, MaterialColor.IRON).hardnessAndResistance(2.5F, 5.0F).sound(SoundType.METAL)).setRegistryName("silver_block");

    // ore
    public static final Block MAGIC_GEM_ORE = new OreBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F, 3.0F).harvestTool(ToolType.PICKAXE).harvestLevel(2)).setRegistryName("magic_gem_ore");
    public static final Block ACHILLES_ORE = new OreBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F, 3.0F).harvestTool(ToolType.PICKAXE).harvestLevel(1)).setRegistryName("achilles_ore");
    public static final Block SILVER_ORE = new OreBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F, 3.0F).harvestTool(ToolType.PICKAXE).harvestLevel(2)).setRegistryName("silver_ore");
    public static final Block DENATURED_SILICON_ORE = new OreBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F, 3.0F).harvestTool(ToolType.PICKAXE).harvestLevel(1)).setRegistryName("denatured_silicon_ore");

    // flower
    public static final Block MOONLIGHT_VINE = new MoonlightVineTopBlock(Block.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.WET_GRASS)).setRegistryName("moonlight_vine");
    public static final Block MOONLIGHT_VINE_PLANT = new MoonlightVineBlock(Block.Properties.create(Material.PLANTS).lightValue(8).doesNotBlockMovement().hardnessAndResistance(0.0F).sound(SoundType.PLANT)).setRegistryName("moonlight_vine_plant");
    public static final Block CYAN_MUSHROOM = new MushroomBlock(Block.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.PLANT)).setRegistryName("cyan_mushroom");
    public static final Block POTTED_CYAN_MUSHROOM = new FlowerPotBlock(CYAN_MUSHROOM, Block.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(0.0F)).setRegistryName("potted_cyan_mushroom");
    public static final Block WISDOM_FLOWER = new FlowerBlock(Effects.REGENERATION, 8, 6).setRegistryName("wisdom_flower");
    public static final Block ACHILLES_FLOWER = new FlowerBlock(Effects.REGENERATION, 8, 0).setRegistryName("achilles_flower");
    public static final Block RED_PELTATUM = new FlowerBlock(Effects.REGENERATION, 8, 0).setRegistryName("red_peltatum");
    public static final Block POTTED_RED_RELTATUM = new FlowerPotBlock(RED_PELTATUM, Block.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(0.0F)).setRegistryName("potted_red_peltatum");
    public static final Block FUCHSIA_PELTATUM = new FlowerBlock(Effects.REGENERATION, 8, 0).setRegistryName("fuchsia_peltatum");
    public static final Block POTTED_FUCHSIA_PELTATUM = new FlowerPotBlock(FUCHSIA_PELTATUM, Block.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(0.0F)).setRegistryName("potted_fuchsia_peltatum");

    // cyan tree
    public static final Block CYAN_LOG = new LogBlock(MaterialColor.CYAN, Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(2.0F).sound(SoundType.WOOD)).setRegistryName("cyan_log");
    public static final Block STRIPPED_CYAN_LOG = new LogBlock(MaterialColor.CYAN, Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(2.0F).sound(SoundType.WOOD)).setRegistryName("stripped_cyan_log");
    public static final Block CYAN_WOOD = new LogBlock(MaterialColor.CYAN, Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(2.0F).sound(SoundType.WOOD)).setRegistryName("cyan_wood");
    public static final Block STRIPPED_CYAN_WOOD = new LogBlock(MaterialColor.CYAN, Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(2.0F).sound(SoundType.WOOD)).setRegistryName("stripped_cyan_wood");
    public static final Block CYAN_LEAVES = new LeavesBlock(Block.Properties.create(Material.LEAVES).hardnessAndResistance(0.2F).tickRandomly().sound(SoundType.PLANT)).setRegistryName("cyan_leaves");
    public static final Block CYAN_SAPLING = new SaplingBlock(new CyanTree(), Block.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.PLANT)).setRegistryName("cyan_sapling");
    public static final Block CYAN_PLANKS = new Block(Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)).setRegistryName("cyan_planks");
    public static final Block POTTED_CYAN_SAPLING = new FlowerPotBlock(CYAN_SAPLING, Block.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(0.0F)).setRegistryName("potted_cyan_sapling");
    public static final Block CYAN_STAIRS = new StairsBlock(CYAN_PLANKS.getDefaultState(), Block.Properties.from(CYAN_PLANKS)).setRegistryName("cyan_stairs");
    public static final Block CYAN_SLAB = new SlabBlock(Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)).setRegistryName("cyan_slab");
    public static final Block CYAN_PRESSURE_PLATE = new PressurePlateBlock(PressurePlateBlock.Sensitivity.EVERYTHING, Block.Properties.create(Material.WOOD, MaterialColor.CYAN).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)).setRegistryName("cyan_pressure_plate");
    public static final Block CYAN_FENCE = new FenceBlock(Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)).setRegistryName("cyan_fence");
    public static final Block CYAN_FENCE_GATE = new FenceGateBlock(Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(2.0F, 3.0F).sound(SoundType.WOOD)).setRegistryName("cyan_fence_gate");
    public static final Block CYAN_BUTTON = new WoodButtonBlock(Block.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)).setRegistryName("cyan_button");
    public static final Block CYAN_TRAPDOOR = new TrapDoorBlock(Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(3.0F).sound(SoundType.WOOD)).setRegistryName("cyan_trapdoor");
    public static final Block CYAN_DOOR = new DoorBlock(Block.Properties.create(Material.WOOD, MaterialColor.CYAN).hardnessAndResistance(3.0F).sound(SoundType.WOOD)).setRegistryName("cyan_door");
}