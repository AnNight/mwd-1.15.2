package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.common.item.ExtraAttribute;
import net.minecraft.item.Items;

import java.util.Objects;
import java.util.Vector;

public class MWDExtraAttributes {
    public static final Vector<ExtraAttribute> EXTRA_ATTRIBUTES = new Vector<>();

    public static final ExtraAttribute CYAN_FRUIT = init(new ExtraAttribute.Properties(MWDItems.CYAN_FRUIT).water(3));
    public static final ExtraAttribute APPLE = init(new ExtraAttribute.Properties(Items.APPLE).water(3));
    public static final ExtraAttribute MELON_SLICE = init(new ExtraAttribute.Properties(Items.MELON_SLICE).water(3));
    public static final ExtraAttribute SWEET_BERRIES = init(new ExtraAttribute.Properties(Items.SWEET_BERRIES).water(2));
    public static final ExtraAttribute MILK_BUCKET = init(new ExtraAttribute.Properties(Items.MILK_BUCKET).water(4));
    public static final ExtraAttribute MUSHROOM_STEW = init(new ExtraAttribute.Properties(Items.MUSHROOM_STEW).water(3));
    public static final ExtraAttribute RABBIT_STEW = init(new ExtraAttribute.Properties(Items.RABBIT_STEW).water(3));
    public static final ExtraAttribute BEETROOT_SOUP = init(new ExtraAttribute.Properties(Items.BEETROOT_SOUP).water(3));
    public static final ExtraAttribute PORKCHOP = init(new ExtraAttribute.Properties(Items.PORKCHOP).possibility(5, 3));
    public static final ExtraAttribute COD = init(new ExtraAttribute.Properties(Items.COD).possibility(5, 3));
    public static final ExtraAttribute BEEF = init(new ExtraAttribute.Properties(Items.BEEF).possibility(5, 3));
    public static final ExtraAttribute CHICKEN = init(new ExtraAttribute.Properties(Items.CHICKEN).possibility(5, 3));
    public static final ExtraAttribute RABBIT = init(new ExtraAttribute.Properties(Items.RABBIT).possibility(5, 3));
    public static final ExtraAttribute MUTTON = init(new ExtraAttribute.Properties(Items.MUTTON).possibility(5, 3));
    public static final ExtraAttribute SALMON = init(new ExtraAttribute.Properties(Items.SALMON).possibility(5, 3));
    public static final ExtraAttribute TROPICAL_FISH = init(new ExtraAttribute.Properties(Items.TROPICAL_FISH).possibility(5, 3));
    public static final ExtraAttribute ROTTEN_FLESH = init(new ExtraAttribute.Properties(Items.ROTTEN_FLESH).possibility(10, 2));
    public static final ExtraAttribute PUFFERFISH = init(new ExtraAttribute.Properties(Items.PUFFERFISH).water(-20));
    public static final ExtraAttribute POISONOUS_POTATO = init(new ExtraAttribute.Properties(Items.POISONOUS_POTATO).water(-15));

    private static ExtraAttribute init(ExtraAttribute.Properties properties) {
        ExtraAttribute extraAttribute = new ExtraAttribute(properties);
        extraAttribute.setRegistryName(Objects.requireNonNull(extraAttribute.getItem().getRegistryName()));
        EXTRA_ATTRIBUTES.add(extraAttribute);
        return extraAttribute;
    }
}