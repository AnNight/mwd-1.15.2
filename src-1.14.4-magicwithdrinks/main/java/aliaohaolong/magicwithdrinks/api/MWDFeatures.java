package aliaohaolong.magicwithdrinks.api;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.world.gen.feature.LowTreeFeature;
import aliaohaolong.magicwithdrinks.common.world.gen.feature.RelicFeature;
import aliaohaolong.magicwithdrinks.common.world.gen.feature.RelicPlainsFlowersFeature;
import aliaohaolong.magicwithdrinks.common.world.gen.feature.VeinFeature;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class MWDFeatures {
    // Tree
    public static final Feature<NoFeatureConfig> CYAN_TREE = init(new LowTreeFeature(NoFeatureConfig::deserialize, false, MWDBlocks.CYAN_LOG.getDefaultState(), MWDBlocks.CYAN_LEAVES.getDefaultState(), MWDBlocks.CYAN_SAPLING), "cyan_tree");
    // Vein
    public static final Feature<NoFeatureConfig> MAGIC_GEM = init(new VeinFeature(NoFeatureConfig::deserialize, MWDBlocks.MAGIC_GEM_ORE.getDefaultState(), MWDBlocks.HARD_STONE.getDefaultState(), 3, 1.4F), "magic_gem");
    public static final Feature<NoFeatureConfig> ACHILLES = init(new VeinFeature(NoFeatureConfig::deserialize, MWDBlocks.ACHILLES_ORE.getDefaultState(), MWDBlocks.HARD_STONE.getDefaultState(), 3, 2.0F), "achilles");
    // Huge Vein
//    public static final Feature<NoFeatureConfig> HUGE_MAGIC_GEM = init(new VeinFeature(NoFeatureConfig::deserialize, MWDBlocks.MAGIC_GEM_ORE.getDefaultState(), MWDBlocks.HARD_STONE.getDefaultState(), 30, 10), "huge_magic_gem");
    // Flowers
    public static final Feature<NoFeatureConfig> RELIC_PLAINS_FLOWERS = init(new RelicPlainsFlowersFeature(NoFeatureConfig::deserialize), "relic_plains_flowers_feature");
    // Relic
    public static final Feature<NoFeatureConfig> RELIC = init(new RelicFeature(NoFeatureConfig::deserialize), "relic");

    private static <C extends IFeatureConfig, F extends Feature<C>> F init(F feature, String name) {
        feature.setRegistryName(new ResourceLocation(MWD.MOD_ID, name));
        return feature;
    }
}