package aliaohaolong.magicwithdrinks;

import aliaohaolong.magicwithdrinks.api.MWDContainerType;
import aliaohaolong.magicwithdrinks.client.gui.screen.*;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.mana.Mana;
import aliaohaolong.magicwithdrinks.common.capability.mana.ManaStorage;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.capability.water.Water;
import aliaohaolong.magicwithdrinks.common.capability.water.WaterStorage;
import aliaohaolong.magicwithdrinks.common.command.ModCommands;
import aliaohaolong.magicwithdrinks.handler.CapabilityEventHandler;
import aliaohaolong.magicwithdrinks.handler.ClientEventHandler;
import aliaohaolong.magicwithdrinks.handler.EventHandler;
import aliaohaolong.magicwithdrinks.handler.FurnaceFuelEventHandler;
import aliaohaolong.magicwithdrinks.init.InitMWD;
import aliaohaolong.magicwithdrinks.init.InitVanilla;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import aliaohaolong.magicwithdrinks.proxy.ClientProxy;
import aliaohaolong.magicwithdrinks.proxy.IProxy;
import aliaohaolong.magicwithdrinks.proxy.ServerProxy;
import net.minecraft.client.gui.ScreenManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings({"Convert2MethodRef", "unused"})
@Mod(MWD.MOD_ID)
public class MWD {
    public static IProxy proxy = DistExecutor.runForDist(() -> () -> new ClientProxy(), () -> () -> new ServerProxy());
    public static final String MOD_ID = "magicwithdrinks";
    public static final Logger LOGGER = LogManager.getLogger();

    public MWD() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(EventHandler::onNewRegistry);

        MinecraftForge.EVENT_BUS.register(this);
    }

    void setup(final FMLCommonSetupEvent event) {
        proxy.init();
        MinecraftForge.EVENT_BUS.register(new CapabilityEventHandler());
        MinecraftForge.EVENT_BUS.register(new EventHandler());
        MinecraftForge.EVENT_BUS.register(new FurnaceFuelEventHandler());
        PacketManager.register();
        InitVanilla.init();
        InitMWD.init();
        CapabilityManager.INSTANCE.register(IWater.class, new WaterStorage(), Water::new);
        CapabilityManager.INSTANCE.register(IMana.class, new ManaStorage(), Mana::new);
    }

    void doClientStuff(final FMLClientSetupEvent event) {
        MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
        ScreenManager.registerFactory(MWDContainerType.DRINK_MAKING_MACHINE, DrinkMakingMachineScreen::new);
        ScreenManager.registerFactory(MWDContainerType.MANA_EXTRACTOR, ManaExtractorScreen::new);
        ScreenManager.registerFactory(MWDContainerType.WATER_PURIFIER, WaterPurifierScreen::new);
        ScreenManager.registerFactory(MWDContainerType.MILL, MillScreen::new);
        ScreenManager.registerFactory(MWDContainerType.BLENDER, BlenderScreen::new);
    }

    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        ModCommands.register(event.getCommandDispatcher());
    }
}