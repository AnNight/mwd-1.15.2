package aliaohaolong.magicwithdrinks.network;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public class PacketManager {
    private static final String PROTOCOL_VERSION = "1";
    private static final SimpleChannel HANDLER = NetworkRegistry.ChannelBuilder
            .named(new ResourceLocation(MWD.MOD_ID, "channel"))
            .clientAcceptedVersions(PROTOCOL_VERSION::equals)
            .serverAcceptedVersions(PROTOCOL_VERSION::equals)
            .networkProtocolVersion(() -> PROTOCOL_VERSION)
            .simpleChannel();

    public static void register(){
        int id = 0;
        HANDLER.registerMessage(++id, UpdateClientWaterPacket.class, UpdateClientWaterPacket::encoder, UpdateClientWaterPacket::decoder, UpdateClientWaterPacket::handle);
        HANDLER.registerMessage(++id, UpdateClientManaPacket.class, UpdateClientManaPacket::encoder, UpdateClientManaPacket::decoder, UpdateClientManaPacket::handle);
        HANDLER.registerMessage(++id, UpdateServerDMMActPacket.class, UpdateServerDMMActPacket::encoder, UpdateServerDMMActPacket::decoder, UpdateServerDMMActPacket::handle);
    }

    public static void sendWaterTo(ServerPlayerEntity player, IWater water) {
        HANDLER.sendTo(new UpdateClientWaterPacket(player.getUniqueID(), water), player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
    }

    public static void sendManaTo(ServerPlayerEntity player, IMana mana) {
        HANDLER.sendTo(new UpdateClientManaPacket(player.getUniqueID(), mana), player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
    }

    public static void sendDMMActToServer(int[] pos, boolean next) {
        HANDLER.sendToServer(new UpdateServerDMMActPacket(pos, next));
    }
}