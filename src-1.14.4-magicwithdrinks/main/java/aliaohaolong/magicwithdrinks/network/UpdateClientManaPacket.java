package aliaohaolong.magicwithdrinks.network;

import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.UUID;
import java.util.function.Supplier;

public class UpdateClientManaPacket {
    private final CompoundNBT nbt;

    public UpdateClientManaPacket(UUID uuid, IMana mana){
        CompoundNBT nbt = new CompoundNBT();
        nbt.putLong("uL", uuid.getLeastSignificantBits());
        nbt.putLong("uM", uuid.getMostSignificantBits());
        nbt.putInt("limit", mana.getLimit());
        nbt.putInt("v", mana.getValue());
        nbt.putBoolean("lock", mana.getLock());
        this.nbt = nbt;
    }

    public UpdateClientManaPacket(CompoundNBT nbt){
        this.nbt = nbt;
    }

    public static void encoder(UpdateClientManaPacket msg, PacketBuffer buf){
        buf.writeCompoundTag(msg.nbt);
    }

    public static UpdateClientManaPacket decoder(PacketBuffer buf){
        return new UpdateClientManaPacket(buf.readCompoundTag());
    }

    public static void handle(UpdateClientManaPacket msg, Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = Minecraft.getInstance().world.getPlayerByUuid(new UUID(msg.nbt.getLong("uM"),msg.nbt.getLong("uL")));
            if (player == null) return;
            IMana.getFromPlayer(player).setLimit(msg.nbt.getInt("limit"));
            IMana.getFromPlayer(player).setValue(msg.nbt.getInt("v"));
            IMana.getFromPlayer(player).setLock(msg.nbt.getBoolean("lock"));
        });
        ctx.get().setPacketHandled(true);
    }
}