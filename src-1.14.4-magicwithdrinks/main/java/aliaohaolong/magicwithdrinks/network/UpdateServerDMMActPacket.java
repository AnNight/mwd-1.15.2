package aliaohaolong.magicwithdrinks.network;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.common.tileentity.DrinkMakingMachineTileEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class UpdateServerDMMActPacket {
    private final CompoundNBT nbt;

    public UpdateServerDMMActPacket(int[] pos, boolean next) {
        CompoundNBT nbt = new CompoundNBT();
        nbt.putInt("x", pos[0]);
        nbt.putInt("y", pos[1]);
        nbt.putInt("z", pos[2]);
        nbt.putBoolean("n", next);
        this.nbt = nbt;
    }

    public UpdateServerDMMActPacket(CompoundNBT nbt) {
        this.nbt = nbt;
    }

    public static void encoder(UpdateServerDMMActPacket msg, PacketBuffer buf){
        buf.writeCompoundTag(msg.nbt);
    }

    public static UpdateServerDMMActPacket decoder(PacketBuffer buf){
        return new UpdateServerDMMActPacket(buf.readCompoundTag());
    }

    public static void handle(UpdateServerDMMActPacket msg, Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(() -> {
            TileEntity tileEntity = ctx.get().getSender().world.getTileEntity(new BlockPos(msg.nbt.getInt("x"), msg.nbt.getInt("y"), msg.nbt.getInt("z")));
            if (tileEntity == null) return;
            if (tileEntity instanceof DrinkMakingMachineTileEntity) {
                DrinkMakingMachineTileEntity drinkMakingMachineTileEntity = (DrinkMakingMachineTileEntity) tileEntity;
                drinkMakingMachineTileEntity.next(msg.nbt.getBoolean("n"));
            }
        });
        ctx.get().setPacketHandled(true);
    }
}