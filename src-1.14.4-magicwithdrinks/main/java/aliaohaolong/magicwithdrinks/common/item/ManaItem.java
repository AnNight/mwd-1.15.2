package aliaohaolong.magicwithdrinks.common.item;

import aliaohaolong.magicwithdrinks.common.item.util.IManaItem;
import aliaohaolong.magicwithdrinks.common.item.util.ManaPacket;
import aliaohaolong.magicwithdrinks.common.item.util.ManaUtils;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class ManaItem extends Item implements IManaItem {
    protected final short capacity;
    protected final byte speedPerS;

    public ManaItem(Properties properties, int capacity, byte speedPerS) {
        super(properties);
        this.capacity = (short) capacity;
        this.speedPerS = speedPerS;
    }

    @Override
    public short getCapacity() {
        return this.capacity;
    }

    @Override
    public byte getSpeedPerS() {
        return this.speedPerS;
    }

    @Override
    public boolean isStoreItem() {
        return true;
    }

    @Override
    public ItemStack getDefaultInstance() {
        return ManaUtils.write(super.getDefaultInstance(), new ManaPacket(this.capacity, this.speedPerS));
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        if (this.isInGroup(group)) {
            items.add(this.getDefaultInstance());
        }
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> lore, ITooltipFlag flagIn) {
        short value = ManaUtils.getValue(stack);
        lore.add(new TranslationTextComponent("mana.lore", value < 0 ? this.capacity : value, this.capacity).applyTextStyle(TextFormatting.BLUE));
    }
}