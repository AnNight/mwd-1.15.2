package aliaohaolong.magicwithdrinks.common.item;

public class CapacityCardItem extends ManaItem {
    public CapacityCardItem(Properties properties, int capacity, byte receivingSpeedPerTick) {
        super(properties, capacity, receivingSpeedPerTick);
    }

    @Override
    public boolean isStoreItem() {
        return false;
    }
}