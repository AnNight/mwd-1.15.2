package aliaohaolong.magicwithdrinks.common.item;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class EfficiencyCardItem extends Item {
    private final byte efficiency;

    public EfficiencyCardItem(Properties properties, byte efficiency) {
        super(properties);
        this.efficiency = efficiency;
    }

    public byte getEfficiency() {
        return this.efficiency;
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> lore, ITooltipFlag flagIn) {
        lore.add(new TranslationTextComponent("efficiency.lore", this.efficiency).applyTextStyle(TextFormatting.BLUE));
    }
}