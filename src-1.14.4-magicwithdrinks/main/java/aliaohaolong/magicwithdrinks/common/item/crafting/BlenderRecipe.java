package aliaohaolong.magicwithdrinks.common.item.crafting;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDRecipe;
import aliaohaolong.magicwithdrinks.common.drink.Drink;
import aliaohaolong.magicwithdrinks.common.drink.DrinkUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;

/**
 *  An example:
 *  {
 *    "type": "magicwithdrinks:blending",
 *    "ingredientDrink": "magicwithdrinks:purified_water"
 *    "ingredient": {
 *      "item": "minecraft:apple",
 *      "count": 2
 *    },
 *    "resultDrink": "magicwithdrinks:apple_juice",
 *    "cookingtime": 400
 *  }
 */

@SuppressWarnings("NullableProblems")
public class BlenderRecipe implements IRecipe<IInventory> {
    protected final ResourceLocation id;
    protected final ItemStack ingredientDrink;
    protected final ItemStack ingredient;
    protected final ItemStack result;
    protected final int cookTime;

    public BlenderRecipe(ResourceLocation idIn, ItemStack ingredientDrinkIn, ItemStack ingredientIn, ItemStack resultIn, int cookTimeIn) {
        id = idIn;
        ingredientDrink = ingredientDrinkIn;
        ingredient = ingredientIn;
        result = resultIn;
        cookTime = cookTimeIn;
    }

    public ItemStack getIngredientDrink() {
        return ingredientDrink;
    }

    public ItemStack getIngredient() {
        return ingredient;
    }

    public int getCookTime() {
        return cookTime;
    }

    @Override
    public ItemStack getIcon() {
        return new ItemStack(MWDItems.BLENDER);
    }

    @Override
    public boolean matches(IInventory inv, World worldIn) {
        ItemStack stack0 = inv.getStackInSlot(0);
        ItemStack stack1 = inv.getStackInSlot(1);
        if (stack0.isEmpty() || stack1.isEmpty()) return false;
        return stack0.getItem() == MWDItems.DRINK &&
                DrinkUtils.getDrinkFromStack(stack0) == DrinkUtils.getDrinkFromStack(ingredientDrink) &&
                stack1.getItem() == ingredient.getItem() && stack1.getCount() >= ingredient.getCount();
    }

    @Override
    public ItemStack getCraftingResult(IInventory inv) {
        return result.copy();
    }

    @Override
    public boolean canFit(int width, int height) {
        return true;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return result.copy();
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return MWDRecipe.BLENDING_S;
    }

    @Override
    public IRecipeType<?> getType() {
        return MWDRecipe.BLENDING_T;
    }

    public static class Serializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<BlenderRecipe> {
        @Override
        public BlenderRecipe read(ResourceLocation recipeId, JsonObject json) {
            if (!json.has("ingredientDrink")) throw new JsonSyntaxException("Missing ingredient drinks, expected to find a string or object");
            ItemStack ingredientDrink = DrinkUtils.addDrinkToStack(new ItemStack(MWDItems.DRINK), DrinkUtils.getDrinkFromName(JSONUtils.getString(json, "ingredientDrink")));
            if (!json.has("ingredient")) throw new JsonSyntaxException("Missing ingredient, expected to find a string or object");
            ItemStack ingredient = deserializeItem(JSONUtils.getJsonObject(json, "ingredient"));
            if (!json.has("resultDrink")) throw new JsonSyntaxException("Missing result drinks, expected to find a string or object");
            ItemStack result = DrinkUtils.addDrinkToStack(new ItemStack(MWDItems.DRINK), DrinkUtils.getDrinkFromName(JSONUtils.getString(json, "resultDrink")));
            int cookingtime = JSONUtils.getInt(json, "cookingtime", 200);
            return new BlenderRecipe(recipeId, ingredientDrink, ingredient, result, cookingtime);
        }

        @Nullable
        @Override
        public BlenderRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
            return new BlenderRecipe(recipeId, buffer.readItemStack(), buffer.readItemStack(), buffer.readItemStack(), buffer.readInt());
        }

        @Override
        public void write(PacketBuffer buffer, BlenderRecipe recipe) {
            buffer.writeItemStack(recipe.ingredientDrink);
            buffer.writeItemStack(recipe.ingredient);
            buffer.writeItemStack(recipe.result);
            buffer.writeVarInt(recipe.cookTime);
        }

        private static ItemStack deserializeItem(JsonObject object) {
            String name = JSONUtils.getString(object, "item");
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(name));
            int count = JSONUtils.getInt(object, "count", 1);
            return new ItemStack(item, count);
        }
    }
}