package aliaohaolong.magicwithdrinks.common.item.crafting;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.api.MWDRecipe;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.*;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;

/**
 *  An example:
 * {
 *   "type": "magicwithdrinks:milling",
 *   "ingredient": {
 *     "item": "magicwithdrinks:hard_stone",
 *     "count": 2
 *   },
 *   "result": {
 *     "item": "magicwithdrinks:hard_stone_powder",
 *     "count": 2
 *   },
 *   "cookingtime": 100
 * }
 */

@SuppressWarnings("NullableProblems")
public class MillRecipe implements IRecipe<IInventory> {
    protected final ResourceLocation id;
    protected final ItemStack ingredient;
    protected final ItemStack result;
    protected final int cookTime;

    public MillRecipe(ResourceLocation idIn, ItemStack ingredientIn, ItemStack resultIn, int cookTimeIn) {
        id = idIn;
        ingredient = ingredientIn;
        result = resultIn;
        cookTime = cookTimeIn;
    }

    public int getCookTime() {
        return cookTime;
    }

    public ItemStack getIngredient() {
        return ingredient;
    }

    @Override
    public ItemStack getIcon() {
        return new ItemStack(MWDBlocks.MILL);
    }

    @Override
    public boolean matches(IInventory inv, World worldIn) {
        ItemStack stack = inv.getStackInSlot(0);
        if (stack.isEmpty()) return false;
        return stack.getItem() == ingredient.getItem() && stack.getCount() >= ingredient.getCount();
    }

    @Override
    public ItemStack getCraftingResult(IInventory inv) {
        return result.copy();
    }

    @Override
    public boolean canFit(int width, int height) {
        return true;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return result.copy();
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return MWDRecipe.MILLING_S;
    }

    @Override
    public IRecipeType<?> getType() {
        return MWDRecipe.MILLING_T;
    }

    public static class Serializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<MillRecipe> {
        @Override
        public MillRecipe read(ResourceLocation recipeId, JsonObject json) {
            if (!json.has("ingredient")) throw new JsonSyntaxException("Missing ingredient, expected to find a string or object");
            ItemStack ingredient = deserializeItem(JSONUtils.getJsonObject(json, "ingredient"));
            if (!json.has("result")) throw new JsonSyntaxException("Missing result, expected to find a string or object");
            ItemStack result = deserializeItem(JSONUtils.getJsonObject(json, "result"));
            int cookingtime = JSONUtils.getInt(json, "cookingtime", 200);
            return new MillRecipe(recipeId, ingredient, result, cookingtime);
        }

        @Nullable
        @Override
        public MillRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
            return new MillRecipe(recipeId, buffer.readItemStack(), buffer.readItemStack(), buffer.readInt());
        }

        @Override
        public void write(PacketBuffer buffer, MillRecipe recipe) {
            buffer.writeItemStack(recipe.ingredient);
            buffer.writeItemStack(recipe.result);
            buffer.writeVarInt(recipe.cookTime);
        }

        private static ItemStack deserializeItem(JsonObject json) {
            String name = JSONUtils.getString(json, "item");
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(name));
            int count = JSONUtils.getInt(json, "count", 1);
            return new ItemStack(item, count);
        }
    }
}