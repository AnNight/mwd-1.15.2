package aliaohaolong.magicwithdrinks.common.item.util;

public interface IManaItem {
    short getCapacity();

    byte getSpeedPerS();

    boolean isStoreItem();
}