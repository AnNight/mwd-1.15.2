package aliaohaolong.magicwithdrinks.common.item.util;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;

import javax.annotation.Nullable;

public class ManaUtils {
    @Nullable
    public static ManaPacket read(ItemStack stack) {
        ManaPacket packet = new ManaPacket();
        if (hasManaTag(stack)) {
            CompoundNBT nbt = stack.getOrCreateChildTag("Mana");
            return packet.setValue(nbt.getShort("V")).setCapacity(nbt.getShort("C")).setSpeed(nbt.getByte("S"));
        } else {
            packet = getDefault(stack);
            if (packet != null) {
                write(stack, packet);
            }
            return packet;
        }
    }

    public static ItemStack write(ItemStack stack, ManaPacket value) {
        CompoundNBT nbt = stack.getOrCreateChildTag("Mana");
        nbt.putShort("V", value.getValue());
        nbt.putShort("C", value.getCapacity());
        nbt.putByte("S", value.getSpeed());
        return stack;
    }

    public static boolean hasManaTag(ItemStack stack) {
        if (!stack.hasTag()) return false;
        assert stack.getTag() != null;
        if (!stack.getTag().contains("Mana")) return false;
        CompoundNBT nbt = stack.getOrCreateChildTag("Mana");
        return nbt.contains("V") && nbt.contains("C") && nbt.contains("S");
    }

    /**
     * if (isStoreItem) return ManaPacket;
     * else return null;
     * @param stack stack
     * @return ManaPacket or null
     */
    public static ManaPacket getDefault(ItemStack stack) {
        if (stack.getItem() instanceof IManaItem) {
            IManaItem item = (IManaItem) stack.getItem();
            return new ManaPacket(item.getCapacity(), item.getSpeedPerS());
        }
        return null;
    }

    public static short getValue(ItemStack stack) {
        ManaPacket packet = read(stack);
        if (packet != null)
            return packet.getValue();
        return -1;
    }

    public static boolean hasUnusedCapacity(ItemStack stack) {
        ManaPacket packet = read(stack);
        if (packet != null) {
            return packet.getCapacity() - packet.getValue() > 0;
        }
        return false;
    }
}