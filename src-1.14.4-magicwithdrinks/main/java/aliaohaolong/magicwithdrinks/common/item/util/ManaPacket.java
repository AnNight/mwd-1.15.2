package aliaohaolong.magicwithdrinks.common.item.util;

public class ManaPacket {
    private short value;
    private short capacity;
    private byte speed;

    public ManaPacket() {
        this(0, 0, 0);
    }

    public ManaPacket(int capacity, int speed) {
        this(capacity, capacity, speed);
    }

    public ManaPacket(int value, int capacity, int speed) {
        this.value = (short) value;
        this.capacity = (short) capacity;
        this.speed = (byte) speed;
    }

    public ManaPacket setValue(short value) {
        this.value = value;
        return this;
    }

    public ManaPacket setCapacity(short capacity) {
        this.capacity = capacity;
        return this;
    }

    public ManaPacket setSpeed(byte speed) {
        this.speed = speed;
        return this;
    }

    public short getValue() {
        return value;
    }

    public short getCapacity() {
        return capacity;
    }

    public byte getSpeed() {
        return speed;
    }
}
