package aliaohaolong.magicwithdrinks.common.item;

import net.minecraft.item.Item;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class ExtraAttribute extends ForgeRegistryEntry<ExtraAttribute> {
    private final Item item;
    private final int water;
    private final int possibility;
    private final int mana;
    private final int max_mana;

    public ExtraAttribute(ExtraAttribute.Properties properties) {
        this.item = properties.item;
        this.water = properties.water;
        this.possibility = properties.possibility;
        this.mana = properties.mana;
        this.max_mana = properties.max_mana;
    }

    public Item getItem() {
        return this.item;
    }

    public int getWater() {
        return this.water;
    }

    public int getPossibility() {
        return this.possibility;
    }

    public int getMana() {
        return this.mana;
    }

    public int getMax_mana() {
        return this.max_mana;
    }

    public static class Properties {
        private Item item;
        private int water = 0;
        private int possibility = 0;
        private int mana = 0;
        private int max_mana = 0;

        public Properties(Item item) {
            this.item = item;
        }

        public ExtraAttribute.Properties water(int value) {
            this.water = value;
            return this;
        }

        public ExtraAttribute.Properties possibility(int thirst, int possibility) {
            this.water = thirst;
            this.possibility = possibility;
            return this;
        }

        public ExtraAttribute.Properties mana(int value) {
            this.mana = value;
            return this;
        }

        public ExtraAttribute.Properties max_mana(int value) {
            this.max_mana = value;
            return this;
        }
    }
}