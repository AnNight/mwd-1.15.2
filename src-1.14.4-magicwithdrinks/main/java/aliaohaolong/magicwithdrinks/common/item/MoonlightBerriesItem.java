package aliaohaolong.magicwithdrinks.common.item;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;

public class MoonlightBerriesItem extends BlockItem {
    public MoonlightBerriesItem(Block block, Item.Properties properties) {
        super(block, properties);
    }

    @Override
    public String getTranslationKey() {
        return this.getDefaultTranslationKey();
    }
}