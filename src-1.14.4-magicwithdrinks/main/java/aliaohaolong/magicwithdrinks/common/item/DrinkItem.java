package aliaohaolong.magicwithdrinks.common.item;

import aliaohaolong.magicwithdrinks.api.MWDCriteriaTriggers;
import aliaohaolong.magicwithdrinks.api.MWDDamageSources;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.drink.Drink;
import aliaohaolong.magicwithdrinks.common.drink.DrinkUtils;
import aliaohaolong.magicwithdrinks.api.MWDDrinks;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.EffectUtils;
import net.minecraft.potion.Effects;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

import static aliaohaolong.magicwithdrinks.common.drink.DrinkUtils.getDrinkFromStack;

@SuppressWarnings("NullableProblems")
public class DrinkItem extends Item {
    public DrinkItem(Item.Properties builder) {
        super(builder);
    }

    @Override
    public ItemStack getDefaultInstance() {
        return DrinkUtils.addDrinkToStack(super.getDefaultInstance(), MWDDrinks.WATER);
    }

    @Override
    public String getTranslationKey(ItemStack stack) {
        return this.getTranslationKey() + "." + getDrinkFromStack(stack).getResourceLocation().toString();
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return super.hasEffect(stack) || !DrinkUtils.getEffectsFromStack(stack).isEmpty();
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        if (this.isInGroup(group)) {
            for (Drink drinks : MWDForgeRegistries.getDrinks()) {
                items.add(DrinkUtils.addDrinkToStack(new ItemStack(this), drinks));
            }
        }
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.DRINK;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 32;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        if (IWater.getFromPlayer(playerIn).getValue() < IWater.MAX || getDrinkFromStack(playerIn.getHeldItem(handIn)).canEatWhenFull()) {
            playerIn.setActiveHand(handIn);
            return new ActionResult<>(ActionResultType.SUCCESS, playerIn.getHeldItem(handIn));
        } else
            return new ActionResult<>(ActionResultType.FAIL, playerIn.getHeldItem(handIn));
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity livingEntity) {
        PlayerEntity player = livingEntity instanceof PlayerEntity ? (PlayerEntity) livingEntity : null;
        if (player == null || !player.abilities.isCreativeMode)
            stack.shrink(1);
        if (player instanceof ServerPlayerEntity) {
            CriteriaTriggers.CONSUME_ITEM.trigger((ServerPlayerEntity) player, stack);
            MWDCriteriaTriggers.drinkTrigger.trigger((ServerPlayerEntity) player, stack);
            boolean sync = false;
            Drink drink = getDrinkFromStack(stack);
            int water = drink.getWater();
            if (drink == MWDDrinks.WATER) {
                if (new Random().nextInt(2) == 0) {
                    if (IWater.getFromPlayer(player).limitedReduce(drink.getWater())) sync = true;
                    player.attackEntityFrom(MWDDamageSources.POISON, 4);
                    player.addPotionEffect(new EffectInstance(Effects.WEAKNESS, 1200));
                    player.addPotionEffect(new EffectInstance(Effects.NAUSEA, 320));
                } else {
                    if (IWater.getFromPlayer(player).limitedIncrease(drink.getWater())) sync = true;
                }
            } else if (water < 0) {
                if (IWater.getFromPlayer(player).limitedIncrease(water)) sync = true;
                player.attackEntityFrom(MWDDamageSources.POISON, (int) Math.floor((double) Math.abs(water) / 3));
                player.addPotionEffect(new EffectInstance(Effects.WEAKNESS, Math.max(Math.abs(water) * 60, 600)));
                player.addPotionEffect(new EffectInstance(Effects.NAUSEA, Math.max(Math.abs(water) * 20, 200)));
            } else if (water > 0) {
                if (IWater.getFromPlayer(player).limitedIncrease(water)) sync = true;
            }
            if (sync) PacketManager.sendWaterTo((ServerPlayerEntity) player, IWater.getFromPlayer(player));
            sync = false;
            if (drink.getMana() != 0 || drink.getMax_mana() != 0) {
                if (IMana.getFromPlayer(player).limitedIncreaseLimit(drink.getMax_mana())) sync = true;
                if (IMana.getFromPlayer(player).limitedIncreaseValue(drink.getMana())) sync = true;
            }
            if (drink.getHunger() > 0 || drink.getSaturation() > 0)
                player.getFoodStats().addStats(drink.getHunger(), drink.getSaturation());
            if (sync) PacketManager.sendManaTo((ServerPlayerEntity) player, IMana.getFromPlayer(player));
        }
        if (!worldIn.isRemote) {
            for (EffectInstance effectinstance : DrinkUtils.getEffectsFromStack(stack)) {
                livingEntity.addPotionEffect(new EffectInstance(effectinstance));
            }
        }
        if (player != null)
            player.addStat(Stats.ITEM_USED.get(this));
        if (player == null || !player.abilities.isCreativeMode) {
            if (stack.isEmpty())
                return new ItemStack(MWDItems.DRINK_BOTTLE);
            if (player != null)
                player.inventory.addItemStackToInventory(new ItemStack(MWDItems.DRINK_BOTTLE));
        }
        return stack;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> lores, ITooltipFlag flagIn) {
        Drink drink = DrinkUtils.getDrinkFromStack(stack);
        if (drink.getHunger() > 0)
            lores.add(new TranslationTextComponent("drink.lore.hunger+", drink.getHunger()).applyTextStyle(TextFormatting.BLUE));
        else if (drink.getHunger() < 0)
            lores.add(new TranslationTextComponent("drink.lore.hunger", drink.getHunger()).applyTextStyle(TextFormatting.RED));
        if (drink.getWater() > 0 && drink != MWDDrinks.WATER)
            lores.add(new TranslationTextComponent("drink.lore.water+", drink.getWater()).applyTextStyle(TextFormatting.BLUE));
        else if (drink == MWDDrinks.WATER)
            lores.add(new TranslationTextComponent("drink.lore.waterP", drink.getWater()).applyTextStyle(TextFormatting.RED));
        else if (drink.getWater() < 0)
            lores.add(new TranslationTextComponent("drink.lore.water", drink.getWater()).applyTextStyle(TextFormatting.RED));
        if (drink.getMana() > 0)
            lores.add(new TranslationTextComponent("drink.lore.mana+", drink.getMana()).applyTextStyle(TextFormatting.BLUE));
        else if (drink.getMana() < 0)
            lores.add(new TranslationTextComponent("drink.lore.mana", drink.getMana()).applyTextStyle(TextFormatting.RED));
        if (drink.getMax_mana() > 0)
            lores.add(new TranslationTextComponent("drink.lore.max_mana+", drink.getMax_mana()).applyTextStyle(TextFormatting.BLUE));
        else if (drink.getMax_mana() < 0)
            lores.add(new TranslationTextComponent("drink.lore.max_mana", drink.getMax_mana()).applyTextStyle(TextFormatting.RED));
        if (!drink.getEffects().isEmpty()) {
            for(EffectInstance effectinstance : drink.getEffects()) {
                ITextComponent itextcomponent = new TranslationTextComponent(effectinstance.getEffectName());
                Effect effect = effectinstance.getPotion();
                if (effectinstance.getAmplifier() > 0)
                    itextcomponent.appendText(" ").appendSibling(new TranslationTextComponent("potion.potency." + effectinstance.getAmplifier()));
                if (effectinstance.getDuration() > 20)
                    itextcomponent.appendText(" (").appendText(EffectUtils.getPotionDurationString(effectinstance, 1)).appendText(")");
                lores.add(itextcomponent.applyTextStyle(effect.getEffectType().getColor()));
            }
        }
    }
}