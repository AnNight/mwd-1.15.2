package aliaohaolong.magicwithdrinks.common.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class FilterSliceItem extends Item {
    public FilterSliceItem(Item.Properties properties) {
        super(properties);
    }

    @Override
    public ItemStack getDefaultInstance() {
        ItemStack stack = new ItemStack(this);
        stack.setDamage(0);
        return stack;
    }
}