package aliaohaolong.magicwithdrinks.common.world.gen.feature;

import com.mojang.datafixers.Dynamic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

import java.util.Random;
import java.util.function.Function;

public class VeinFeature extends Feature<NoFeatureConfig> {
    private final BlockState poorOre;
    private final BlockState ore;
    private final int skip;
    private final float range;

    public VeinFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> configFactoryIn, BlockState ore, int skip, float range) {
        this(configFactoryIn, ore, ore, skip, range);
    }

    public VeinFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> configFactoryIn, BlockState ore, BlockState poorOre, int skip, float range) {
        super(configFactoryIn);
        this.poorOre = poorOre;
        this.ore = ore;
        this.skip = Math.max(1, skip);
        this.range = Math.max(1, Math.min(range, 10));
    }

    @Override
    public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random random, BlockPos pos, NoFeatureConfig config) {
        if (random.nextInt(this.skip) != 0 || this.isAir(worldIn.getBlockState(pos).getBlock()))
            return false;
        this.recursion(worldIn, 1, pos.getX(), pos.getY(), pos.getZ(), 0, random);
        return true;
    }

    private void recursion(IWorld world, final int count, final int x, final int y, final int z, final int direction, Random random) {
        BlockPos.MutableBlockPos mutableBlockPos = new BlockPos.MutableBlockPos();
        world.setBlockState(mutableBlockPos.setPos(x, y, z), this.ore, 2);
        final int probability = (int) Math.max(1, Math.pow((double) count / this.range, 3));
        final int next = count + 1;
        if (direction != 2 && this.isValid(world.getBlockState(mutableBlockPos.setPos(x + 1, y, z)).getBlock())) {
            if (probability == 1 || random.nextInt(probability) == 0) {
                random.setSeed(random.nextLong());
                this.recursion(world, next, x + 1, y, z, 1, random);
            } else if (random.nextInt(6) != 0) {
                if (world.getBlockState(mutableBlockPos.setPos(x + 1, y, z)).getBlock() != this.ore.getBlock())
                    world.setBlockState(mutableBlockPos.setPos(x + 1, y, z), this.poorOre, 2);
            }
        }
        if (direction != 4 && this.isValid(world.getBlockState(mutableBlockPos.setPos(x, y + 1, z)).getBlock())) {
            if (probability == 1 || random.nextInt(probability) == 0) {
                random.setSeed(random.nextLong());
                this.recursion(world, next, x, y + 1, z, 3, random);
            } else if (random.nextInt(6) != 0) {
                if (world.getBlockState(mutableBlockPos.setPos(x, y + 1, z)).getBlock() != this.ore.getBlock())
                    world.setBlockState(mutableBlockPos.setPos(x, y + 1, z), this.poorOre, 2);
            }
        }
        if (direction != 6 && this.isValid(world.getBlockState(mutableBlockPos.setPos(x, y, z + 1)).getBlock())) {
            if (probability == 1 || random.nextInt(probability) == 0) {
                random.setSeed(random.nextLong());
                this.recursion(world, next, x, y, z + 1, 5, random);
            } else if (random.nextInt(6) != 0) {
                if (world.getBlockState(mutableBlockPos.setPos(x, y, z + 1)).getBlock() != this.ore.getBlock())
                    world.setBlockState(mutableBlockPos.setPos(x, y, z + 1), this.poorOre, 2);
            }
        }
        if (direction != 1 && this.isValid(world.getBlockState(mutableBlockPos.setPos(x - 1, y, z)).getBlock())) {
            if (probability == 1 || random.nextInt(probability) == 0) {
                random.setSeed(random.nextLong());
                this.recursion(world, next, x - 1, y, z, 2, random);
            } else if (random.nextInt(6) != 0) {
                if (world.getBlockState(mutableBlockPos.setPos(x - 1, y, z)).getBlock() != this.ore.getBlock())
                world.setBlockState(mutableBlockPos.setPos(x - 1, y, z), this.poorOre, 2);
            }
        }
        if (direction != 3 && this.isValid(world.getBlockState(mutableBlockPos.setPos(x, y - 1, z)).getBlock())) {
            if (probability == 1 || random.nextInt(probability) == 0) {
                random.setSeed(random.nextLong());
                this.recursion(world, next, x, y - 1, z, 4, random);
            } else if (random.nextInt(6) != 0) {
                if (world.getBlockState(mutableBlockPos.setPos(x, y - 1, z)).getBlock() != this.ore.getBlock())
                world.setBlockState(mutableBlockPos.setPos(x, y - 1, z), this.poorOre, 2);
            }
        }
        if (direction != 5 && this.isValid(world.getBlockState(mutableBlockPos.setPos(x, y, z - 1)).getBlock())) {
            if (probability == 1 || random.nextInt(probability) == 0) {
                random.setSeed(random.nextLong());
                this.recursion(world, next, x, y, z - 1, 6, random);
            } else if (random.nextInt(6) != 0) {
                if (world.getBlockState(mutableBlockPos.setPos(x, y, z - 1)).getBlock() != this.ore.getBlock())
                world.setBlockState(mutableBlockPos.setPos(x, y, z - 1), this.poorOre, 2);
            }
        }
    }

    private boolean isValid(Block block) {
        return block == Blocks.STONE || block == Blocks.GRANITE || block == Blocks.DIORITE || block == Blocks.ANDESITE || block == Blocks.COAL_ORE || block == Blocks.IRON_ORE ||
                block == Blocks.WATER || block == Blocks.LAVA || block == Blocks.GRAVEL || block == Blocks.DIRT;
    }

    private boolean isAir(Block block) {
        return block == Blocks.AIR || block == Blocks.CAVE_AIR || block == Blocks.VOID_AIR;
    }
}