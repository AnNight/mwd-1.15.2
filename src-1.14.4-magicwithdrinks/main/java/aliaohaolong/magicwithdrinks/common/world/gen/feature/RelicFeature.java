package aliaohaolong.magicwithdrinks.common.world.gen.feature;

import aliaohaolong.magicwithdrinks.MWD;
import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import com.mojang.datafixers.Dynamic;
import net.minecraft.block.*;
import net.minecraft.tileentity.LockableLootTileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.structure.StructurePiece;

import java.util.Random;
import java.util.function.Function;

public class RelicFeature extends Feature<NoFeatureConfig> {
    public static final ResourceLocation CHESTS_RELIC_PLAINS = new ResourceLocation(MWD.MOD_ID, "chests/relic_plains");

    public RelicFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> function) {
        super(function);
    }

    @Override
    public boolean place(IWorld world, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, NoFeatureConfig config) {
        int xSize = rand.nextInt(3) + 2;
        int zSize = rand.nextInt(3) + 2;
        int high = rand.nextInt(3) + 1;
        for (int x = -xSize; x <= xSize; x++) {
            for (int z = -zSize; z <= zSize; z++) {
                if (rand.nextInt(3) == 0)
                    this.setBlockState(world, pos.add(x, -1, z), Blocks.STONE_BRICKS.getDefaultState());
                else if (rand.nextInt(4) == 0)
                    this.setBlockState(world, pos.add(x, -1, z), Blocks.CRACKED_STONE_BRICKS.getDefaultState());
                else if (rand.nextInt(3) == 0)
                    this.setBlockState(world, pos.add(x, -1, z), Blocks.MOSSY_STONE_BRICKS.getDefaultState());
            }
        }
        int chance = high == 1 ? 1 : 2;
        for (int y = 0; y < high; y++) {
            for (int x = -xSize; x < xSize; x++) {
                if (rand.nextInt(y * 2 + 3) < chance)
                    this.place(world, rand, pos.add(x, y, -zSize));
            }
            for (int x = xSize; x > -xSize; x--) {
                if (rand.nextInt(y * 2 + 3) < chance)
                    this.place(world, rand, pos.add(x, y, zSize));
            }
            for (int z = zSize; z > -zSize; z--) {
                if (rand.nextInt(y * 2 + 3) < chance)
                    this.place(world, rand, pos.add(-xSize, y, z));
            }
            for (int z = -zSize; z < zSize; z++) {
                if (rand.nextInt(y * 2 + 3) < chance)
                    this.place(world, rand, pos.add(xSize, y, z));
            }
        }
        int ySize = pos.getY() - 1;
        for (int x = -xSize; x <= xSize; x++) {
            for (int z = -zSize; z <= zSize; z++) {
                boolean grass = true;
                for (int y = -1; y > -ySize; y--) {
                    BlockPos newPos = pos.add(x, y, z);
                    if (world.hasBlockState(newPos, state -> this.isValidPosition(state.getBlock()))) {
                        if (grass) {
                            grass = false;
                            world.setBlockState(newPos, Blocks.GRASS_BLOCK.getDefaultState(), 2);
                        } else {
                            world.setBlockState(newPos, Blocks.DIRT.getDefaultState(), 2);
                        }
                    } else if (!grass) break;
                }
            }
        }
        if (rand.nextInt(5) == 0) {
            int x = rand.nextBoolean() ? rand.nextInt(xSize - 1) : -rand.nextInt(xSize - 1);
            int z = rand.nextBoolean() ? rand.nextInt(zSize - 1) : -rand.nextInt(zSize - 1);
            BlockPos chestPos = pos.add(x, 0, z);
            world.setBlockState(chestPos, StructurePiece.func_197528_a(world, chestPos, Blocks.CHEST.getDefaultState()), 2);
            LockableLootTileEntity.setLootTable(world, rand, chestPos, CHESTS_RELIC_PLAINS);
        }
        return true;
    }

    private void place(IWorld world, Random rand, BlockPos pos) {
        boolean flag = false;
        if (!world.hasBlockState(pos.add(0, -1, 0), state -> this.isValidPosition(state.getBlock())))
            flag = true;
        if (flag) {
            if (rand.nextInt(3) == 0) this.setBlockState(world, pos, Blocks.STONE_BRICKS.getDefaultState());
            else if (rand.nextInt(4) == 0) this.setBlockState(world, pos, Blocks.CRACKED_STONE_BRICKS.getDefaultState());
            else this.setBlockState(world, pos, Blocks.MOSSY_STONE_BRICKS.getDefaultState());
        }
    }

    protected boolean isValidPosition(Block block) {
        return block == Blocks.AIR || block instanceof BushBlock || block instanceof LeavesBlock || block instanceof FlowingFluidBlock;
    }
}