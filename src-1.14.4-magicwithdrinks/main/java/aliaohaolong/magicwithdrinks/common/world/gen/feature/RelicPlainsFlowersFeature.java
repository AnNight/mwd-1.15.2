package aliaohaolong.magicwithdrinks.common.world.gen.feature;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import com.mojang.datafixers.Dynamic;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.feature.FlowersFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

import java.util.Random;
import java.util.function.Function;

@SuppressWarnings("NullableProblems")
public class RelicPlainsFlowersFeature extends FlowersFeature {
    public RelicPlainsFlowersFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> deserializer) {
        super(deserializer);
    }

    @Override
    public BlockState getRandomFlower(Random random, BlockPos pos) {
        return random.nextBoolean() ? MWDBlocks.WISDOM_FLOWER.getDefaultState() : random.nextBoolean() ? Blocks.DANDELION.getDefaultState() : Blocks.POPPY.getDefaultState();
    }
}