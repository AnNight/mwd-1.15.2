package aliaohaolong.magicwithdrinks.common.world.gen.feature;

import com.mojang.datafixers.Dynamic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.gen.IWorldGenerationReader;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

import java.util.Random;
import java.util.Set;
import java.util.function.Function;

@SuppressWarnings("NullableProblems")
public class LowTreeFeature extends AbstractTreeFeature<NoFeatureConfig> {
    private final BlockState log;
    private final BlockState leaf;

    public LowTreeFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> configIn, boolean doBlockNotifyIn, BlockState log, BlockState leaf, Block sapling) {
        super(configIn, doBlockNotifyIn);
        this.log = log;
        this.leaf = leaf;
        this.setSapling((net.minecraftforge.common.IPlantable) sapling);
    }

    @Override
    protected boolean place(Set<BlockPos> changedBlocks, IWorldGenerationReader worldIn, Random rand, BlockPos position, MutableBoundingBox boundsIn) {
        int i = rand.nextInt(4) + 1;
        if (position.getY() > 0 && position.getY() + i + 1 < worldIn.getMaxHeight()) { // Check if this generation method is greater than the world maximum height.
            boolean flag = true;
            // Check
            for (int y = position.getY(); y <= position.getY() + 1 + i; ++y) {
                int k;
                if (y != position.getY() + i - 1) k = 1;
                else k = 0;
                BlockPos.MutableBlockPos mutableBlockPos = new BlockPos.MutableBlockPos();
                for(int x = position.getX() - k; x <= position.getX() + k && flag; ++x) {
                    for (int z = position.getZ() - k; z <= position.getZ() + k && flag; ++z) {
                        if (y >= 0 && y < worldIn.getMaxHeight()) {
                            if (!func_214587_a(worldIn, mutableBlockPos.setPos(x, y, z))) {
                                flag = false;
                            }
                        }
                        else flag = false;
                    }
                }
            }
            // Generate
            if (flag && isSoil(worldIn, position.down(), getSapling())) {
                this.setDirtAt(worldIn, position.down(), position);
                BlockPos blockPos;
                int y = position.getY() + i - 1;
//                 x0x
//                 000
//                 x0x
                for (int x = position.getX() - 1; x <= position.getX() + 1; ++x) {
                    for (int z = position.getZ() - 1; z <= position.getZ() + 1; ++z) {
                        if (Math.abs(position.getX() - x) != 1 || Math.abs(position.getZ() - z) != 1) {
                            blockPos = new BlockPos(x,y,z);
                            if (isAirOrLeaves(worldIn, blockPos))
                                this.setLogState(changedBlocks, worldIn, blockPos, leaf, boundsIn);
                        }
                    }
                }
                blockPos = new BlockPos(position.getX(), ++y, position.getZ());
                if (isAirOrLeaves(worldIn, blockPos))
                    this.setLogState(changedBlocks, worldIn, blockPos, leaf, boundsIn);
                // Place logs
                for (y = 0; y < i; ++y) {
                    if (isAirOrLeaves(worldIn, position.up(y)))
                        this.setLogState(changedBlocks, worldIn, position.up(y), log, boundsIn);
                }
                return true;
            }
        }
        return false;
    }
}