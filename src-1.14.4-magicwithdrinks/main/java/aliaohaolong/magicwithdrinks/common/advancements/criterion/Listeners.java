package aliaohaolong.magicwithdrinks.common.advancements.criterion;

import com.google.common.collect.Sets;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.advancements.ICriterionTrigger;
import net.minecraft.advancements.PlayerAdvancements;

import java.util.Set;

public class Listeners<I extends ICriterionInstance> {
    protected final PlayerAdvancements playerAdvancements;
    protected final Set<ICriterionTrigger.Listener<I>> listeners = Sets.newHashSet();

    public Listeners(PlayerAdvancements playerAdvancementsIn) {
        this.playerAdvancements = playerAdvancementsIn;
    }

    public boolean isEmpty() {
        return this.listeners.isEmpty();
    }

    public void add(ICriterionTrigger.Listener<I> listener) {
        this.listeners.add(listener);
    }

    public void remove(ICriterionTrigger.Listener<I> listener) {
        this.listeners.remove(listener);
    }
}