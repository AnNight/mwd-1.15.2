package aliaohaolong.magicwithdrinks.common.advancements.criterion;

import aliaohaolong.magicwithdrinks.MWD;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.advancements.ICriterionTrigger;
import net.minecraft.advancements.PlayerAdvancements;
import net.minecraft.advancements.criterion.CriterionInstance;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import java.util.List;
import java.util.Map;

public class DrinkTrigger implements ICriterionTrigger<DrinkTrigger.Instance> {
    private static final ResourceLocation ID = new ResourceLocation(MWD.MOD_ID, "drink");
    private final Map<PlayerAdvancements, DrinkTrigger.Listeners> listeners = Maps.newHashMap();

    @Override
    public ResourceLocation getId() {
        return ID;
    }

    @Override
    public void addListener(PlayerAdvancements playerAdvancementsIn, Listener<Instance> listener) {
        DrinkTrigger.Listeners listeners = this.listeners.get(playerAdvancementsIn);
        if (listeners == null) {
            listeners = new DrinkTrigger.Listeners(playerAdvancementsIn);
            this.listeners.put(playerAdvancementsIn, listeners);
        }
        listeners.add(listener);
    }

    @Override
    public void removeListener(PlayerAdvancements playerAdvancementsIn, Listener<Instance> listener) {
        DrinkTrigger.Listeners listeners = this.listeners.get(playerAdvancementsIn);
        if (listeners != null) {
            listeners.remove(listener);
            if (listeners.isEmpty()) {
                this.listeners.remove(playerAdvancementsIn);
            }
        }
    }

    @Override
    public void removeAllListeners(PlayerAdvancements playerAdvancementsIn) {
        this.listeners.remove(playerAdvancementsIn);
    }

    @Override
    public DrinkTrigger.Instance deserializeInstance(JsonObject json, JsonDeserializationContext context) {
        return new DrinkTrigger.Instance(DrinkPredicate.deserialize(json.get("drink")));
    }

    public void trigger(ServerPlayerEntity player, ItemStack stack) {
        DrinkTrigger.Listeners listeners = this.listeners.get(player.getAdvancements());
        if (listeners != null) {
            listeners.trigger(stack);
        }
    }

    public static class Instance extends CriterionInstance {
        private final DrinkPredicate drink;

        public Instance(DrinkPredicate drink) {
            super(DrinkTrigger.ID);
            this.drink = drink;
        }

        public boolean test(ItemStack stack) {
            return this.drink.test(stack);
        }

        @Override
        public JsonElement serialize() {
            JsonObject jsonobject = new JsonObject();
            jsonobject.add("drink", this.drink.serialize());
            return jsonobject;
        }
    }

    static class Listeners extends aliaohaolong.magicwithdrinks.common.advancements.criterion.Listeners<DrinkTrigger.Instance> {
        public Listeners(PlayerAdvancements advancements) {
            super(advancements);
        }

        public void trigger(ItemStack stack) {
            List<Listener<DrinkTrigger.Instance>> list = null;
            for(ICriterionTrigger.Listener<DrinkTrigger.Instance> listener : this.listeners) {
                if (listener.getCriterionInstance().test(stack)) {
                    if (list == null) {
                        list = Lists.newArrayList();
                    }
                    list.add(listener);
                }
            }
            if (list != null) {
                for(ICriterionTrigger.Listener<DrinkTrigger.Instance> listener1 : list) {
                    listener1.grantCriterion(this.playerAdvancements);
                }
            }
        }
    }
}