package aliaohaolong.magicwithdrinks.common.advancements.criterion;

import aliaohaolong.magicwithdrinks.common.drink.Drink;
import aliaohaolong.magicwithdrinks.common.drink.DrinkUtils;
import com.google.gson.*;
import net.minecraft.item.ItemStack;
import net.minecraft.util.JSONUtils;

import javax.annotation.Nullable;

public class DrinkPredicate {
    public static final DrinkPredicate ANY = new DrinkPredicate();
    private final Drink drink;

    public DrinkPredicate() {
        this.drink = null;
    }

    public DrinkPredicate(Drink drink) {
        this.drink = drink;
    }

    public boolean test(ItemStack stack) {
        if (this == ANY) {
            return true;
        } else return DrinkUtils.getDrinkFromStack(stack) == this.drink;
    }

    public static DrinkPredicate deserialize(@Nullable JsonElement element) {
        if (element != null && !element.isJsonNull()) {
            JsonObject jsonobject = JSONUtils.getJsonObject(element, "drink");
            Drink drink = null;
            if (jsonobject.has("drink")) {
                drink = DrinkUtils.getDrinkFromName(JSONUtils.getString(jsonobject, "drink"));
            }
            return new DrinkPredicate(drink);
        } else {
            return ANY;
        }
    }

    public JsonElement serialize() {
        if (this == ANY) {
            return JsonNull.INSTANCE;
        } else {
            JsonObject jsonobject = new JsonObject();
            if (this.drink != null) {
                jsonobject.addProperty("drink", this.drink.getResourceLocation().toString());
            }
            return jsonobject;
        }
    }
}