package aliaohaolong.magicwithdrinks.common.recipe;

import net.minecraft.item.Item;

import javax.annotation.Nullable;

public class ExtractingRecipeUtils {
    @Nullable
    public static ExtractingRecipe getRecipeFromItem(Item item) {
        for (ExtractingRecipe recipe : ExtractingRecipe.EXTRACTING_RECIPES) {
            if (recipe.getItem() == item)
                return recipe;
        }
        return null;
    }

    public static short getValueFromItem(Item item) {
        ExtractingRecipe recipe = getRecipeFromItem(item);
        return recipe == null ? 0 : recipe.getValue();
    }

    public static boolean canBeReceived(Item item) {
        return getRecipeFromItem(item) != null;
    }
}