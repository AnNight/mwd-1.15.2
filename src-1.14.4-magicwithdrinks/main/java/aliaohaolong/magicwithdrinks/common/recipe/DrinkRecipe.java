package aliaohaolong.magicwithdrinks.common.recipe;

import aliaohaolong.magicwithdrinks.api.MWDDrinks;
import aliaohaolong.magicwithdrinks.common.drink.Drink;
import com.google.common.collect.ImmutableList;
import net.minecraft.item.Item;

import java.util.List;
import java.util.Vector;

public class DrinkRecipe {
    public static final Vector<DrinkRecipe> DRINK_RECIPES = new Vector<>();

    private final int cookTimeCount;
    private boolean needFuel;
    private final Drink ingredient;
    private final Drink result;
    private final ImmutableList<Item> ingredients;

    public DrinkRecipe(int time, boolean needFuel, Drink result, Item... items) {
        this(time, needFuel, MWDDrinks.PURIFIED_WATER, result, items);
    }

    public DrinkRecipe(int time, boolean needFuel, Drink ingredient, Drink result, Item... items) {
        this.cookTimeCount = time;
        this.needFuel = needFuel;
        this.ingredient = ingredient;
        this.result = result;
        this.ingredients = ImmutableList.copyOf(items);
    }

    public int getCookTimeCount() {
        return this.cookTimeCount;
    }

    public boolean isNeedFuel() {
        return this.needFuel;
    }

    public Drink getIngredient() {
        return this.ingredient;
    }

    public Drink getResult() {
        return this.result;
    }

    public List<Item> getIngredients() {
        return this.ingredients;
    }
}