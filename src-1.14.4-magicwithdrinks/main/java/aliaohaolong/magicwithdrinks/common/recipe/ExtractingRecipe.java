package aliaohaolong.magicwithdrinks.common.recipe;

import net.minecraft.item.Item;

import java.util.Vector;

public class ExtractingRecipe {
    public static final Vector<ExtractingRecipe> EXTRACTING_RECIPES = new Vector<>();

    private final Item item;
    private final short value;
    private final short tick;
    private final byte cost;

    public ExtractingRecipe(Item item, int value, int tick, int cost) {
        this.item = item;
        this.value = (short) value;
        this.tick = (short) tick;
        this.cost = (byte) cost;
    }

    public Item getItem() {
        return item;
    }

    public short getValue() {
        return value;
    }

    public short getTick() {
        return tick;
    }

    public byte getCost() {
        return cost;
    }
}