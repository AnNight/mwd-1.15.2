package aliaohaolong.magicwithdrinks.common.tileentity;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDTileEntityType;
import aliaohaolong.magicwithdrinks.common.block.ManaExtractorBlock;
import aliaohaolong.magicwithdrinks.common.inventory.ManaExtractorContainer;
import aliaohaolong.magicwithdrinks.common.item.*;
import aliaohaolong.magicwithdrinks.common.item.util.ManaPacket;
import aliaohaolong.magicwithdrinks.common.item.util.ManaUtils;
import aliaohaolong.magicwithdrinks.common.recipe.ExtractingRecipe;
import aliaohaolong.magicwithdrinks.common.recipe.ExtractingRecipeUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

@SuppressWarnings("NullableProblems")
public class ManaExtractorTileEntity extends TileEntity implements INamedContainerProvider, ISidedInventory, ITickableTileEntity {
    /**
     * 0    ingredient  top
     * 1    result      down[when full]
     * 345  extend      north
     * 1    result      south
     * 2    catalyst   east/west
     */
    private NonNullList<ItemStack> stacks = NonNullList.withSize(6, ItemStack.EMPTY);
    private NonNullList<ItemStack> oldStacks = NonNullList.withSize(6, ItemStack.EMPTY);
    private NonNullList<Item> oldItems = NonNullList.withSize(6, Items.AIR);
    /**
     * value / capacity
     * 0    3 slot
     * 1    4 slot
     * 2    5 slot
     */
    private short[][] capacityList = {{0, 0}, {0, 0}, {0, 0}};
    private static final int[] SLOTS_UP = new int[]{1, 0};
    private static final int[] SLOT_DOWN = new int[]{1};
    private static final int[] SLOTS_HORIZONTAL = new int[]{2, 3, 4, 5};
    private short elapsedTime = 0;
    private short totalTime = 0;
    private short buffer = 0;
    private short value = 0;
    private short maxBufferSize = 32;
    private byte catalyst = 0;
    private byte time = 0;
    private final IIntArray packet = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return ManaExtractorTileEntity.this.elapsedTime;
                case 1:
                    return ManaExtractorTileEntity.this.totalTime;
                case 2:
                    return ManaExtractorTileEntity.this.buffer;
                case 3:
                    return ManaExtractorTileEntity.this.maxBufferSize;
                case 4:
                    return ManaExtractorTileEntity.this.catalyst;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    ManaExtractorTileEntity.this.elapsedTime = (short) value;
                    break;
                case 1:
                    ManaExtractorTileEntity.this.totalTime = (short) value;
                    break;
                case 2:
                    ManaExtractorTileEntity.this.buffer = (short) value;
                    break;
                case 3:
                    ManaExtractorTileEntity.this.maxBufferSize = (short) value;
                    break;
                case 4:
                    ManaExtractorTileEntity.this.catalyst = (byte) value;
                    break;
            }
        }

        @Override
        public int size() {
            return 5;
        }
    };

    public ManaExtractorTileEntity() {
        super(MWDTileEntityType.MANA_EXTRACTOR);
    }

    // MWD
    public static short getMaxCatalystSize() {
        return 64;
    }

    private void refreshBuffer() {
        this.maxBufferSize = 32;
        this.buffer = this.value;
        for (byte i = 0; i < 3; i++) {
            ItemStack stack = this.stacks.get(i + 3);
            if (stack.getItem() instanceof CapacityCardItem) {
                ManaPacket packet = ManaUtils.read(stack);
                assert packet != null;
                this.capacityList[i][0] = packet.getValue();
                this.capacityList[i][1] = packet.getCapacity();
                this.buffer += this.capacityList[i][0];
                this.maxBufferSize += this.capacityList[i][1];
            } else {
                this.capacityList[i][0] = 0;
                this.capacityList[i][1] = 0;
            }
        }
    }

    private void refresh() {
        for (int i = 0; i < this.stacks.size(); i++) {
            this.oldStacks.set(i, this.stacks.get(i));
            this.oldItems.set(i, this.stacks.get(i).getItem());
        }
    }

    private void pushCard(int target) {
        ItemStack targetStack = this.stacks.get(target);
        ManaPacket packet = ManaUtils.read(targetStack);
        assert packet != null;
        short maxReceiving = (short) (packet.getCapacity() - packet.getValue());
        if (packet.getSpeed() < maxReceiving)
            maxReceiving = packet.getSpeed();
        if (this.value > 0) {
            short canReceiving = maxReceiving > this.value ? this.value : maxReceiving;
            this.value -= canReceiving;
            this.stacks.set(target, ManaUtils.write(targetStack, packet.setValue((short) (packet.getValue() + canReceiving))));
        }
    }

    private void pushResult() {
        ItemStack targetStack = this.stacks.get(1);
        ManaPacket packet = ManaUtils.read(targetStack);
        assert packet != null;
        short maxReceiving = (short) (packet.getCapacity() - packet.getValue());
        if (packet.getSpeed() < maxReceiving)
            maxReceiving = packet.getSpeed();
        if (this.value > 0) {
            short canReceiving = maxReceiving > this.value ? this.value : maxReceiving;
            maxReceiving -= canReceiving;
            this.value -= canReceiving;
            packet.setValue((short) (packet.getValue() + canReceiving));
        }
        for (byte i = 3; i < 6 && maxReceiving > 0; i++) {
            if (this.capacityList[i - 3][1] > 0) {
                short canReceiving = maxReceiving > this.capacityList[i - 3][0] ? this.capacityList[i - 3][0] : maxReceiving;
                maxReceiving -= canReceiving;
                this.capacityList[i - 3][0] -= canReceiving;
                this.buffer -= canReceiving;
                packet.setValue((short) (packet.getValue() + canReceiving));
                this.stacks.set(i, ManaUtils.write(this.stacks.get(i), Objects.requireNonNull(ManaUtils.read(this.stacks.get(i))).setValue(this.capacityList[i - 3][0])));
            }
        }
        this.stacks.set(1, ManaUtils.write(targetStack, packet));
    }

    private boolean equalsStack(int... array) {
        for (int value : array) {
            if (!this.oldStacks.get(value).equals(this.stacks.get(value), false))
                return false;
            if (this.oldItems.get(value) != this.stacks.get(value).getItem())
                return false;
        }
        return true;
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.mana_extractor");
    }

    // IContainerProvider
    @Nullable
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new ManaExtractorContainer(windowId, playerInventory, this, this.packet);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        this.stacks = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
        ItemStackHelper.loadAllItems(compound, this.stacks);
        refresh();
        for (byte i = 0; i < 3; i++) {
            ManaPacket packet = ManaUtils.read(this.stacks.get(i + 3));
            if (packet != null) {
                this.capacityList[i][0] = packet.getValue();
                this.capacityList[i][1] = packet.getCapacity();
            }
        }
        this.elapsedTime = compound.getShort("elapsedTime");
        this.totalTime = compound.getShort("totalTime");
        this.value = compound.getShort("value");
        this.refreshBuffer();
        this.catalyst = compound.getByte("catalyst");
        this.time = compound.getByte("time");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        ItemStackHelper.saveAllItems(compound, this.stacks);
        compound.putShort("elapsedTime", this.elapsedTime);
        compound.putShort("totalTime", this.totalTime);
        compound.putShort("value", this.value);
        compound.putByte("catalyst", this.catalyst);
        compound.putByte("time", this.time);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 1)
            return this.getStackInSlot(index).isEmpty() && stack.getItem() instanceof ManaItem && ((ManaItem) stack.getItem()).isStoreItem();
        if (index == 0)
            return this.getStackInSlot(index).isEmpty() && ExtractingRecipeUtils.canBeReceived(stack.getItem());
        if (index == 2)
            return stack.getItem() == MWDItems.CATALYST;
        if (index > 2 && index < 6) {
            boolean noneOfTheEfficiencyCard = true;
            for (byte i = 3; i < 6 && noneOfTheEfficiencyCard; i++) {
                if (this.getStackInSlot(i).getItem() instanceof EfficiencyCardItem)
                    noneOfTheEfficiencyCard = false;
            }
            return this.getStackInSlot(index).isEmpty() && (stack.getItem() instanceof CapacityCardItem || (noneOfTheEfficiencyCard && stack.getItem() instanceof EfficiencyCardItem));
        }
        return true;
    }

    @Override
    public int getSizeInventory() {
        return this.stacks.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack stack : this.stacks) {
            if (!stack.isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return index >= 0 && index < this.stacks.size() ? this.stacks.get(index) : ItemStack.EMPTY;
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(this.stacks, index, count);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.stacks, index);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        if (index >= 0 && index < this.stacks.size())
            this.stacks.set(index, stack);
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        assert this.world != null;
        if (this.world.getTileEntity(this.pos) != this) return false;
        else return !(player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) > 64.0D);
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.DOWN)
            return SLOT_DOWN;
        return side == Direction.UP ? SLOTS_UP : SLOTS_HORIZONTAL;
    }

    @Override
    public boolean canInsertItem(int index, ItemStack itemStackIn, @Nullable Direction direction) {
        return this.isItemValidForSlot(index, itemStackIn);
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        return index != 1 || !ManaUtils.hasUnusedCapacity(stack) || direction != Direction.DOWN;
    }

    // IClearable
    @Override
    public void clear() {
        this.stacks.clear();
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean save = false;
        assert this.world != null;
        if (!this.world.isRemote) {
            if (this.catalyst <= 32 && !this.stacks.get(2).isEmpty()) {
                this.stacks.get(2).shrink(1);
                this.catalyst += 32;
            }
            if (!this.equalsStack(3, 4, 5)) {
                this.refreshBuffer();
            }
            boolean needPushResult = this.buffer > 0 && ManaUtils.hasUnusedCapacity(this.stacks.get(1));
            boolean needPushCard = this.value > 0 && ((this.capacityList[0][1] - this.capacityList[0][0] > 0) || (this.capacityList[1][1] - this.capacityList[1][0] > 0) || (this.capacityList[2][1] - this.capacityList[2][0] > 0));
            if (needPushResult || needPushCard) {
                this.time++;
                if (this.time >= 20) {
                    this.time = 0;
                    if (needPushResult)
                        this.pushResult();
                    if (this.value > 0 && this.capacityList[0][1] - this.capacityList[0][0] > 0)
                        this.pushCard(3);
                    if (this.value > 0 && this.capacityList[1][1] - this.capacityList[1][0] > 0)
                        this.pushCard(4);
                    if (this.value > 0 && this.capacityList[2][1] - this.capacityList[2][0] > 0)
                        this.pushCard(5);
                    this.refreshBuffer();
                }
            } else if (this.time != 0) {
                this.time = 0;
            }
            if (this.totalTime > 0) {
                if (this.equalsStack(0)) {
                    this.elapsedTime++;
                    if (this.elapsedTime >= this.totalTime) {
                        short value = ExtractingRecipeUtils.getValueFromItem(this.stacks.get(0).getItem());
                        this.value += value;
                        this.totalTime = 0;
                        this.elapsedTime = 0;
                        this.stacks.get(0).shrink(1);
                        this.refreshBuffer();
                        this.world.setBlockState(this.pos, this.world.getBlockState(this.pos).with(ManaExtractorBlock.SWITCH, Boolean.FALSE), 3);
                    }
                } else {
                    this.totalTime = 0;
                    this.elapsedTime = 0;
                    this.world.setBlockState(this.pos, this.world.getBlockState(this.pos).with(ManaExtractorBlock.SWITCH, Boolean.FALSE), 3);
                }
                save = true;
            }
            if (this.totalTime == 0 && !this.stacks.get(0).isEmpty()) {
                ExtractingRecipe recipe = ExtractingRecipeUtils.getRecipeFromItem(this.stacks.get(0).getItem());
                if (recipe != null && (this.value + recipe.getValue()) <= 32 && this.catalyst >= recipe.getCost()) {
                    this.catalyst -= recipe.getCost();
                    this.totalTime = recipe.getTick();
                    this.elapsedTime = 0;
                    this.world.setBlockState(this.pos, this.world.getBlockState(this.pos).with(ManaExtractorBlock.SWITCH, Boolean.TRUE), 3);
                    save = true;
                }
            }
            refresh();
        }
        if (save) this.markDirty();
    }

    // ICapabilityProvider
    private LazyOptional<? extends IItemHandler>[] handlers = SidedInvWrapper.create(this, Direction.UP, Direction.DOWN, Direction.NORTH);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (!this.removed && side != null && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            if (side == Direction.UP)
                return handlers[0].cast();
            if (side == Direction.DOWN)
                return handlers[1].cast();
            else
                return handlers[2].cast();
        }
        return super.getCapability(cap, side);
    }
}