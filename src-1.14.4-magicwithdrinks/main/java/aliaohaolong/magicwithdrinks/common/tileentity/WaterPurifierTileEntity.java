package aliaohaolong.magicwithdrinks.common.tileentity;

import aliaohaolong.magicwithdrinks.api.MWDDrinks;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDTileEntityType;
import aliaohaolong.magicwithdrinks.common.block.WaterPurifierBlock;
import aliaohaolong.magicwithdrinks.common.drink.DrinkUtils;
import aliaohaolong.magicwithdrinks.common.inventory.WaterPurifierContainer;
import aliaohaolong.magicwithdrinks.common.item.FilterSliceItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@SuppressWarnings("NullableProblems")
public class WaterPurifierTileEntity extends TileEntity implements INamedContainerProvider, ISidedInventory, ITickableTileEntity {
    private static final int[] SLOT_UP = new int[]{0};
    private static final int[] SLOTS_HORIZONTAL = new int[]{1, 2};
    private static final int[] SLOTS_DOWN = new int[]{3, 4};
    private NonNullList<ItemStack> stacks = NonNullList.withSize(5, ItemStack.EMPTY);
    public static final int TOTAL_TIME = 200;
    private int time = 0;
    private int burnTime = 0;
    private int totalBurnTime = 0;
    private final IIntArray data = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return WaterPurifierTileEntity.this.time;
                case 1:
                    return WaterPurifierTileEntity.this.burnTime;
                case 2:
                    return WaterPurifierTileEntity.this.totalBurnTime;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    WaterPurifierTileEntity.this.time = value;
                    break;
                case 1:
                    WaterPurifierTileEntity.this.burnTime = value;
                    break;
                case 2:
                    WaterPurifierTileEntity.this.totalBurnTime = value;
                    break;
            }
        }

        @Override
        public int size() {
            return 3;
        }
    };

    public WaterPurifierTileEntity() {
        super(MWDTileEntityType.WATER_PURIFIER);
    }

    // MWD
    private boolean canPurify() {
        return !this.stacks.get(0).isEmpty() && !this.stacks.get(2).isEmpty() && this.stacks.get(3).isEmpty() && this.stacks.get(4).getCount() < 64;
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.water_purifier");
    }

    // IContainerProvider
    @Nullable
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity player) {
        return new WaterPurifierContainer(windowId, playerInventory, this, this.data);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        this.stacks = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
        ItemStackHelper.loadAllItems(compound, this.stacks);
        this.totalBurnTime = net.minecraftforge.common.ForgeHooks.getBurnTime(this.stacks.get(1));
        this.time = compound.getInt("Time");
        this.burnTime = compound.getInt("BurnTime");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        ItemStackHelper.saveAllItems(compound, this.stacks);
        compound.putInt("Time", this.time);
        compound.putInt("BurnTime", this.burnTime);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 0)
            return DrinkUtils.getDrinkFromStack(stack) == MWDDrinks.WATER;
        if (index == 1)
            return net.minecraftforge.common.ForgeHooks.getBurnTime(stack) > 0;
        if (index == 2)
            return stack.getItem() instanceof FilterSliceItem;
        return false;
    }

    @Override
    public int getSizeInventory() {
        return this.stacks.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack stack : this.stacks) {
            if (!stack.isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return index >= 0 && index < this.stacks.size() ? this.stacks.get(index) : ItemStack.EMPTY;
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(this.stacks, index, count);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.stacks, index);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        if (index >= 0 && index < this.stacks.size())
            this.stacks.set(index, stack);
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        assert this.world != null;
        if (this.world.getTileEntity(this.pos) != this)
            return false;
        else
            return !(player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) > 64.0D);
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.UP)
            return SLOT_UP;
        else
            return side == Direction.DOWN ? SLOTS_DOWN : SLOTS_HORIZONTAL;
    }

    @Override
    public boolean canInsertItem(int index, ItemStack itemStackIn, @Nullable Direction direction) {
        return this.isItemValidForSlot(index, itemStackIn);
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN && index == 1) {
            return stack.getItem() == Items.BUCKET;
        }
        return true;
    }

    // IClearable
    @Override
    public void clear() {
        this.stacks.clear();
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        assert this.world != null;
        if (!this.world.isRemote) {
            boolean originalState = this.burnTime > 0;
            if (this.burnTime > 0)
                this.burnTime--;
            if (this.burnTime > 0 || !this.stacks.get(1).isEmpty() && !this.stacks.get(0).isEmpty()) {
                if (this.burnTime == 0 && this.canPurify()) {
                    this.burnTime = net.minecraftforge.common.ForgeHooks.getBurnTime(this.stacks.get(1));
                    this.totalBurnTime = this.burnTime;
                    if (this.burnTime > 0) {
                        markDirty = true;
                        if (this.stacks.get(1).getItem() != Items.LAVA_BUCKET)
                            this.stacks.get(1).shrink(1);
                        else
                            this.stacks.set(1, new ItemStack(Items.BUCKET));
                    }
                }
                if (this.burnTime > 0 && this.canPurify()) {
                    this.time++;
                    if (this.time == TOTAL_TIME) {
                        this.time = 0;
                        this.stacks.set(0, ItemStack.EMPTY);
                        ItemStack stack = this.stacks.get(2);
                        if (stack.isDamageable()) {
                            stack.setDamage(stack.getDamage() + 1);
                            if (stack.getDamage() >= stack.getMaxDamage())
                                this.stacks.set(2, ItemStack.EMPTY);
                        }
                        this.stacks.set(3, DrinkUtils.addDrinkToStack(new ItemStack(MWDItems.DRINK), MWDDrinks.PURIFIED_WATER));
                        if (this.stacks.get(4).isEmpty())
                            this.stacks.set(4, new ItemStack(MWDItems.SMALL_PILE_OF_IMPURITY));
                        else
                            this.stacks.get(4).shrink(-1);
                        markDirty = true;
                    }
                } else
                    this.time = 0;
            } else if (this.burnTime == 0 && this.time > 0) {
                this.time = MathHelper.clamp(this.time - 2, 0, TOTAL_TIME);
            }
            if (originalState != (this.burnTime > 0)) {
                markDirty = true;
                this.world.setBlockState(this.pos, this.world.getBlockState(this.pos).with(WaterPurifierBlock.SWITCH, this.burnTime > 0), 3);
            }
        }
        if (markDirty) this.markDirty();
    }

    // ICapabilityProvider
    private LazyOptional<? extends IItemHandler>[] handlers = SidedInvWrapper.create(this, Direction.UP, Direction.DOWN, Direction.NORTH);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (!this.removed && side != null && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            if (side == Direction.UP)
                return handlers[0].cast();
            return side == Direction.DOWN ? handlers[1].cast() : handlers[2].cast();
        }
        return super.getCapability(cap, side);
    }
}