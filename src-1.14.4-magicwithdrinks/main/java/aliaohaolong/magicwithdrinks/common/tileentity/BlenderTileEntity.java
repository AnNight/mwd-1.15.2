package aliaohaolong.magicwithdrinks.common.tileentity;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDRecipe;
import aliaohaolong.magicwithdrinks.api.MWDTileEntityType;
import aliaohaolong.magicwithdrinks.common.block.BlenderBlock;
import aliaohaolong.magicwithdrinks.common.block.MillBlock;
import aliaohaolong.magicwithdrinks.common.inventory.BlenderContainer;
import aliaohaolong.magicwithdrinks.common.item.crafting.BlenderRecipe;
import aliaohaolong.magicwithdrinks.common.item.crafting.MillRecipe;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@SuppressWarnings("NullableProblems")
public class BlenderTileEntity extends TileEntity implements INamedContainerProvider, ISidedInventory, ITickableTileEntity {
    private static final int[] SLOT_UP = new int[]{0, 1};
    private static final int[] SLOTS_DOWN = new int[]{2, 3};
    private static final int[] SLOT_HORIZONTAL = new int[]{2};
    private NonNullList<ItemStack> stacks = NonNullList.withSize(4, ItemStack.EMPTY);
    private int burnTime = 0;
    private int burnTotalTime = 0;
    private int cookTime = 0;
    private int cookTotalTime = 0;
    private final IIntArray packet = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return BlenderTileEntity.this.burnTime;
                case 1:
                    return BlenderTileEntity.this.burnTotalTime;
                case 2:
                    return BlenderTileEntity.this.cookTime;
                case 3:
                    return BlenderTileEntity.this.cookTotalTime;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    BlenderTileEntity.this.burnTime = value;
                    break;
                case 1:
                    BlenderTileEntity.this.burnTotalTime = value;
                    break;
                case 2:
                    BlenderTileEntity.this.cookTime = value;
                    break;
                case 3:
                    BlenderTileEntity.this.cookTotalTime = value;
            }
        }

        @Override
        public int size() {
            return 4;
        }
    };

    public BlenderTileEntity() {
        super(MWDTileEntityType.BLENDER);
    }

    // MWD
    private boolean canBlend(IRecipe<?> recipe) {
        if (!(recipe instanceof BlenderRecipe)) return false;
        assert world != null;
        if (!((BlenderRecipe) recipe).matches(this, world)) return false;
        return stacks.get(3).isEmpty();
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.blender");
    }

    // IContainerProvider
    @Nullable
    @Override
    public Container createMenu(int window, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new BlenderContainer(window, playerInventory, this, packet);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        stacks = NonNullList.withSize(getSizeInventory(), ItemStack.EMPTY);
        ItemStackHelper.loadAllItems(compound, stacks);
        burnTotalTime = compound.getInt("burnTotalTime");
        burnTime = compound.getInt("burnTime");
        cookTime = compound.getInt("cookTime");
        cookTotalTime = compound.getInt("cookTotalTime");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        ItemStackHelper.saveAllItems(compound, stacks);
        compound.putInt("burnTotalTime", burnTotalTime);
        compound.putInt("burnTime", burnTime);
        compound.putInt("cookTime", cookTime);
        compound.putInt("cookTotalTime", cookTotalTime);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 0)
            return stack.getItem() == MWDItems.DRINK;
        if (index == 2)
            return net.minecraftforge.common.ForgeHooks.getBurnTime(stacks.get(1)) > 0;
        return index != 3;
    }

    @Override
    public int getSizeInventory() {
        return stacks.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack stack : stacks) {
            if (!stack.isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return index >= 0 && index < stacks.size() ? stacks.get(index) : ItemStack.EMPTY;
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(stacks, index, count);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(stacks, index);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        if (index >= 0 && index < stacks.size())
            stacks.set(index, stack);
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        assert world != null;
        if (this.world.getTileEntity(pos) != this)
            return false;
        else
            return !(player.getDistanceSq((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D) > 64.0D);
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.DOWN)
            return SLOTS_DOWN;
        return side == Direction.UP ? SLOT_UP : SLOT_HORIZONTAL;
    }

    @Override
    public boolean canInsertItem(int index, ItemStack itemStackIn, @Nullable Direction direction) {
        return this.isItemValidForSlot(index, itemStackIn);
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN && index == 2) {
            Item item = stack.getItem();
            return item == Items.WATER_BUCKET || item == Items.BUCKET;
        }
        return true;
    }

    // IClearable
    @Override
    public void clear() {
        stacks.clear();
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        assert world != null;
        if (!world.isRemote) {
            boolean originalState = burnTime > 0;
            if (burnTime > 0)
                burnTime--;
            if (burnTime > 0 || !stacks.get(0).isEmpty() && !stacks.get(1).isEmpty() && !stacks.get(2).isEmpty()) {
                IRecipe<?> recipe = world.getRecipeManager().getRecipe(MWDRecipe.BLENDING_T, this, world).orElse(null);
                if (burnTime == 0 && canBlend(recipe)) {
                    burnTime = net.minecraftforge.common.ForgeHooks.getBurnTime(stacks.get(2));
                    burnTotalTime = burnTime;
                    if (burnTime > 0) {
                        markDirty = true;
                        if (!stacks.get(2).hasContainerItem())
                            stacks.get(2).shrink(1);
                        else
                            stacks.set(2, stacks.get(2).getContainerItem());
                    }
                }
                if (burnTime > 0 && canBlend(recipe)) {
                    if (cookTime == 0 && cookTotalTime == 0) {
                        cookTotalTime = ((BlenderRecipe) recipe).getCookTime();
                    }
                    ++cookTime;
                    if (cookTime == cookTotalTime) {
                        cookTime = 0;
                        cookTotalTime = 0;
                        stacks.set(0, ItemStack.EMPTY);
                        stacks.get(1).shrink(((BlenderRecipe) recipe).getIngredient().getCount());
                        stacks.set(3, recipe.getRecipeOutput());
                        markDirty = true;
                    }
                } else {
                    cookTime = 0;
                    cookTotalTime = 0;
                }
            } else if (burnTime == 0 && cookTime > 0) {
                cookTime = MathHelper.clamp(cookTime - 2, 0, cookTotalTime);
            }
            if (originalState != (burnTime > 0)) {
                markDirty = true;
                world.setBlockState(pos, world.getBlockState(pos).with(BlenderBlock.SWITCH, burnTime > 0), 3);
            }
        }
        if (markDirty)
            this.markDirty();
    }

    // ICapabilityProvider
    private LazyOptional<? extends IItemHandler>[] handlers = SidedInvWrapper.create(this, Direction.UP, Direction.DOWN, Direction.NORTH);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (!this.removed && side != null && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            if (side == Direction.UP)
                return handlers[0].cast();
            if (side == Direction.DOWN)
                return handlers[1].cast();
            else
                return handlers[2].cast();
        }
        return super.getCapability(cap, side);
    }
}