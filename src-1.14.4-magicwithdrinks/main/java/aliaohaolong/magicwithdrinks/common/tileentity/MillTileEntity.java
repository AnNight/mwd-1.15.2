package aliaohaolong.magicwithdrinks.common.tileentity;

import aliaohaolong.magicwithdrinks.api.MWDRecipe;
import aliaohaolong.magicwithdrinks.api.MWDTileEntityType;
import aliaohaolong.magicwithdrinks.common.block.MillBlock;
import aliaohaolong.magicwithdrinks.common.inventory.MillContainer;
import aliaohaolong.magicwithdrinks.common.item.crafting.MillRecipe;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@SuppressWarnings("NullableProblems")
public class MillTileEntity extends TileEntity implements INamedContainerProvider, ISidedInventory, ITickableTileEntity {
    private static final int[] SLOT_UP = new int[]{0};
    private static final int[] SLOTS_DOWN = new int[]{1, 2};
    private static final int[] SLOT_HORIZONTAL = new int[]{1};
    private NonNullList<ItemStack> stacks = NonNullList.withSize(3, ItemStack.EMPTY);
    private int burnTime = 0;
    private int burnTotalTime = 0;
    private int cookTime = 0;
    private int cookTotalTime = 0;
    private final IIntArray packet = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return MillTileEntity.this.burnTime;
                case 1:
                    return MillTileEntity.this.burnTotalTime;
                case 2:
                    return MillTileEntity.this.cookTime;
                case 3:
                    return MillTileEntity.this.cookTotalTime;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    MillTileEntity.this.burnTime = value;
                    break;
                case 1:
                    MillTileEntity.this.burnTotalTime = value;
                    break;
                case 2:
                    MillTileEntity.this.cookTime = value;
                    break;
                case 3:
                    MillTileEntity.this.cookTotalTime = value;
            }
        }

        @Override
        public int size() {
            return 4;
        }
    };

    public MillTileEntity() {
        super(MWDTileEntityType.MILL);
    }

    // MWD
    private static boolean isFuel(ItemStack stack) {
        return net.minecraftforge.common.ForgeHooks.getBurnTime(stack) > 0;
    }

    private boolean canMill(IRecipe<?> recipe) {
        if (!(recipe instanceof MillRecipe)) return false;
        assert world != null;
        if (!((MillRecipe) recipe).matches(this, world)) return false;
        if (!stacks.get(2).isEmpty()) {
            ItemStack stack = stacks.get(2);
            ItemStack stack1 = recipe.getRecipeOutput();
            return stack.getItem() == stack1.getItem() && stack.getCount() + stack1.getCount() <= stack.getMaxStackSize();
        }
        return true;
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.mill");
    }

    // IContainerProvider
    @Nullable
    @Override
    public Container createMenu(int window, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new MillContainer(window, playerInventory, this, packet);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        stacks = NonNullList.withSize(getSizeInventory(), ItemStack.EMPTY);
        ItemStackHelper.loadAllItems(compound, stacks);
        burnTotalTime = compound.getInt("burnTotalTime");
        burnTime = compound.getInt("burnTime");
        cookTime = compound.getInt("cookTime");
        cookTotalTime = compound.getInt("cookTotalTime");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        ItemStackHelper.saveAllItems(compound, stacks);
        compound.putInt("burnTotalTime", burnTotalTime);
        compound.putInt("burnTime", burnTime);
        compound.putInt("cookTime", cookTime);
        compound.putInt("cookTotalTime", cookTotalTime);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 0)
            return true;
        if (index == 1)
            return isFuel(stack);
        return false;
    }

    @Override
    public int getSizeInventory() {
        return stacks.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack stack : stacks) {
            if (!stack.isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return index >= 0 && index < stacks.size() ? stacks.get(index) : ItemStack.EMPTY;
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(stacks, index, count);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(stacks, index);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        if (index >= 0 && index < stacks.size())
            stacks.set(index, stack);
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        assert world != null;
        if (this.world.getTileEntity(pos) != this)
            return false;
        else
            return !(player.getDistanceSq((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D) > 64.0D);
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.DOWN)
            return SLOTS_DOWN;
        return side == Direction.UP ? SLOT_UP : SLOT_HORIZONTAL;
    }

    @Override
    public boolean canInsertItem(int index, ItemStack itemStackIn, @Nullable Direction direction) {
        return this.isItemValidForSlot(index, itemStackIn);
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN && index == 1) {
            Item item = stack.getItem();
            return item == Items.WATER_BUCKET || item == Items.BUCKET;
        }
        return true;
    }

    // IClearable
    @Override
    public void clear() {
        stacks.clear();
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean markDirty = false;
        assert world != null;
        if (!world.isRemote) {
            boolean originalState = burnTime > 0;
            if (burnTime > 0)
                burnTime--;
            if (burnTime > 0 || !stacks.get(1).isEmpty() && !stacks.get(0).isEmpty()) {
                IRecipe<?> recipe = world.getRecipeManager().getRecipe(MWDRecipe.MILLING_T, this, world).orElse(null);
                if (burnTime == 0 && canMill(recipe)) {
                    burnTime = net.minecraftforge.common.ForgeHooks.getBurnTime(stacks.get(1));
                    burnTotalTime = burnTime;
                    if (burnTime > 0) {
                        markDirty = true;
                        if (!stacks.get(1).hasContainerItem())
                            stacks.get(1).shrink(1);
                        else
                            stacks.set(1, stacks.get(1).getContainerItem());
                    }
                }
                if (burnTime > 0 && canMill(recipe)) {
                    if (cookTime == 0 && cookTotalTime == 0) {
                        cookTotalTime = ((MillRecipe) recipe).getCookTime();
                    }
                    ++cookTime;
                    if (cookTime == cookTotalTime) {
                        cookTime = 0;
                        cookTotalTime = 0;
                        stacks.get(0).shrink(((MillRecipe) recipe).getIngredient().getCount());
                        if (stacks.get(2).isEmpty()) {
                            stacks.set(2, recipe.getRecipeOutput());
                        } else {
                            int count = stacks.get(2).getCount();
                            stacks.get(2).setCount(count + recipe.getRecipeOutput().getCount());
                        }
                        markDirty = true;
                    }
                } else {
                    cookTime = 0;
                    cookTotalTime = 0;
                }
            } else if (burnTime == 0 && cookTime > 0) {
                cookTime = MathHelper.clamp(cookTime - 2, 0, cookTotalTime);
            }
            if (originalState != (burnTime > 0)) {
                markDirty = true;
                world.setBlockState(pos, world.getBlockState(pos).with(MillBlock.SWITCH, burnTime > 0), 3);
            }
        }
        if (markDirty)
            this.markDirty();
    }

    // ICapabilityProvider
    private LazyOptional<? extends IItemHandler>[] handlers = SidedInvWrapper.create(this, Direction.UP, Direction.DOWN, Direction.NORTH);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (!this.removed && side != null && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            if (side == Direction.UP)
                return handlers[0].cast();
            if (side == Direction.DOWN)
                return handlers[1].cast();
            else
                return handlers[2].cast();
        }
        return super.getCapability(cap, side);
    }
}