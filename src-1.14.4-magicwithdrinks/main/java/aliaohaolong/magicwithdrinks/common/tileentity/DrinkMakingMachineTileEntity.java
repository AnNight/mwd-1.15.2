package aliaohaolong.magicwithdrinks.common.tileentity;

import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.api.MWDTileEntityType;
import aliaohaolong.magicwithdrinks.common.block.DrinkMakingMachineBlock;
import aliaohaolong.magicwithdrinks.common.drink.Drink;
import aliaohaolong.magicwithdrinks.common.drink.DrinkUtils;
import aliaohaolong.magicwithdrinks.common.recipe.DrinkRecipe;
import aliaohaolong.magicwithdrinks.common.inventory.DrinkMakingMachineContainer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.Vector;

@SuppressWarnings("NullableProblems")
public class DrinkMakingMachineTileEntity extends TileEntity implements INamedContainerProvider, ISidedInventory, ITickableTileEntity {
    /**
     * 0    ingredient1 front
     * 1    ingredient2 behind
     * 2    ingredient3 left
     * 3    ingredient4 right
     * 4    accessory   top
     * 5    accessory   top
     * 6    result      down
     * 7    ingredient  No-
     * 8    ingredient  No-
     * 9    ingredient  No-
     * 10   ingredient  No-
     * 11   accessory   No-
     * 12   accessory   No-
     * 13   result      No-
     */
    private NonNullList<ItemStack> cookStacks = NonNullList.withSize(14, ItemStack.EMPTY);
    private static final int[] SLOT_FRONT = new int[]{0};
    private static final int[] SLOT_BEHIND = new int[]{1};
    private static final int[] SLOT_LEFT = new int[]{2};
    private static final int[] SLOT_RIGHT = new int[]{3};
    private static final int[] SLOTS_UP = new int[]{4, 5};
    private static final int[] SLOT_DOWN = new int[]{6};
    private ItemStack lastStack = ItemStack.EMPTY;
    private NonNullList<Item> lastItems = NonNullList.withSize(7, Items.AIR);
    private Vector<DrinkRecipe> search = new Vector<>();
    private boolean next = false;
    private boolean previous = false;
    private int number = 0;
    private int cookTime = 0;
    private int cookTimeCount = 0;
    private final IIntArray cookData = new IIntArray() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0:
                    return DrinkMakingMachineTileEntity.this.cookTime;
                case 1:
                    return DrinkMakingMachineTileEntity.this.cookTimeCount;
                case 2:
                    return DrinkMakingMachineTileEntity.this.getPos().getX();
                case 3:
                    return DrinkMakingMachineTileEntity.this.getPos().getY();
                case 4:
                    return DrinkMakingMachineTileEntity.this.getPos().getZ();
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0:
                    DrinkMakingMachineTileEntity.this.cookTime = value;
                    break;
                case 1:
                    DrinkMakingMachineTileEntity.this.cookTimeCount = value;
                    break;
                case 2:
                case 3:
                case 4:
                    break;
            }
        }

        @Override
        public int size() {
            return 5;
        }
    };

    public DrinkMakingMachineTileEntity() {
        super(MWDTileEntityType.DRINK_MAKING_MACHINE);
    }

    // INamedContainerProvider
    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("container.magicwithdrinks.drink_making_machine");
    }

    // IContainerProvider
    @Nullable
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity p_createMenu_3_) {
        return new DrinkMakingMachineContainer(windowId, playerInventory, this, this.cookData);
    }

    // TileEntity
    @Override
    public void read(CompoundNBT compound) {
        super.read(compound);
        this.cookStacks = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
        ItemStackHelper.loadAllItems(compound, this.cookStacks);
        this.refresh();
        this.refreshRecipe();
        this.number = compound.getInt("num");
        this.cookTime = compound.getInt("time");
        this.cookTimeCount = compound.getInt("timeCount");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        ItemStackHelper.saveAllItems(compound, this.cookStacks);
        compound.putInt("num", this.number);
        compound.putInt("time", this.cookTime);
        compound.putInt("timeCount", this.cookTimeCount);
        return compound;
    }

    // IInventory
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        Item item = stack.getItem();
        if (index < 4)
            return true;
        else if (index == 4)
            return item == MWDItems.COAL_NUGGET || item == MWDItems.CHARCOAL_NUGGET;
        else if (index == 5) {
            return item == MWDItems.DRINK;
        }
        return false;
    }

    @Override
    public int getSizeInventory() {
        return this.cookStacks.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack stack : this.cookStacks) {
            if (!stack.isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return index >= 0 && index < this.cookStacks.size() ? this.cookStacks.get(index) : ItemStack.EMPTY;
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(this.cookStacks, index, count);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.cookStacks, index);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        if (index >= 0 && index < this.cookStacks.size())
            this.cookStacks.set(index, stack);
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        assert this.world != null;
        if (this.world.getTileEntity(this.pos) != this) {
            return false;
        } else {
            return !(player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) > 64.0D);
        }
    }

    // ISidedInventory
    @Override
    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.UP)
            return SLOTS_UP;
        if (side == Direction.DOWN)
            return SLOT_DOWN;
        if (side == Direction.NORTH)
            return SLOT_FRONT;
        if (side == Direction.SOUTH)
            return SLOT_BEHIND;
        if (side == Direction.EAST)
            return SLOT_LEFT;
        else
            return SLOT_RIGHT;
    }

    @Override
    public boolean canInsertItem(int index, ItemStack itemStackIn, @Nullable Direction direction) {
        return this.isItemValidForSlot(index, itemStackIn);
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        return index < 7;
    }

    // IClearable
    @Override
    public void clear() {
        this.cookStacks.clear();
    }

    // ITickableTileEntity
    @Override
    public void tick() {
        boolean save = false;
        assert this.world != null;
        if (!this.world.isRemote) {
            boolean push = false;
            if (this.cookTime > 0 && this.cookTimeCount > 0) {
                save = true;
                this.cookTime--;
                if (this.cookTime == 0) {
                    this.cookTimeCount = 0;
                    this.cookStacks.set(6, this.cookStacks.get(13));
                    this.world.setBlockState(this.pos, this.world.getBlockState(this.pos).with(DrinkMakingMachineBlock.SWITCH, Boolean.FALSE), 3);
                    push = true;
                }
            } else if (this.cookTime == 0 && this.cookTimeCount == 0 && this.nowNotEqualToLast(6) && !this.cookStacks.get(5).isEmpty() && this.cookStacks.get(6).isEmpty()) {
                for (DrinkRecipe recipe : DrinkRecipe.DRINK_RECIPES) {
                    if (this.accord(recipe)) {
                        save = true;
                        this.cookTimeCount = recipe.getCookTimeCount();
                        this.cookTime = this.cookTimeCount;
                        for (int i = 0; i <= 3; i++) {
                            if (!this.cookStacks.get(i).isEmpty())
                                this.cookStacks.get(i).shrink(1);
                        }
                        if (recipe.isNeedFuel())
                            this.cookStacks.get(4).shrink(1);
                        this.cookStacks.set(5, ItemStack.EMPTY);
                        this.world.setBlockState(this.pos, this.world.getBlockState(this.pos).with(DrinkMakingMachineBlock.SWITCH, Boolean.TRUE), 3);
                        break;
                    }
                }
            }
            if (push || this.cookTime == 0 && this.cookTimeCount == 0 && this.nowNotEqualToLast(5)) {
                save = true;
                this.number = 0;
                this.refreshRecipe();
                if (!search.isEmpty())
                    this.pushRecipe(search.get(0));
                else {
                    for (int i = 7; i < this.cookStacks.size(); i++)
                        this.cookStacks.set(i, ItemStack.EMPTY);
                }
            }
            if (this.next) {
                this.next = false;
                if (this.search.size() > 1 && this.cookTimeCount == 0) {
                    this.number++;
                    if (this.number >= this.search.size())
                        this.number -= this.search.size();
                    this.pushRecipe(this.search.get(this.number));
                    save = true;
                }
            }
            if (this.previous) {
                this.previous = false;
                if (this.search.size() > 1 && this.cookTimeCount == 0) {
                    this.number--;
                    if (this.number < 0)
                        this.number += this.search.size();
                    this.pushRecipe(this.search.get(this.number));
                    save = true;
                }
            }
            this.refresh();
        }
        if (save) this.markDirty();
    }

    private void refresh() {
        this.lastStack = this.cookStacks.get(5);
        for (int i = 0; i < 7; i++) {
            this.lastItems.set(i, this.cookStacks.get(i).getItem());
        }
    }

    private void refreshRecipe() {
        search.removeAllElements();
        int count = 0;
        for (int i = 0; i < 6; i++) {
            if (!this.cookStacks.get(i).isEmpty())
                count++;
        }
        if (count == 1) {
            if (!this.cookStacks.get(4).isEmpty()) {
                if (this.isFuel(this.cookStacks.get(4).getItem())) {
                    for (DrinkRecipe recipe : DrinkRecipe.DRINK_RECIPES) {
                        if (recipe.isNeedFuel())
                            search.add(recipe);
                    }
                }
            } else if (!this.cookStacks.get(5).isEmpty()) {
                if (this.cookStacks.get(5).getItem() == MWDItems.DRINK) {
                    Drink drink = DrinkUtils.getDrinkFromStack(this.cookStacks.get(5));
                    for (DrinkRecipe recipe : DrinkRecipe.DRINK_RECIPES) {
                        if (recipe.getIngredient() == drink)
                            search.add(recipe);
                    }
                }
            } else {
                for (int i = 0; i < 4; i++) {
                    if (!this.cookStacks.get(i).isEmpty()) {
                        Item item = this.cookStacks.get(i).getItem();
                        for (DrinkRecipe recipe : DrinkRecipe.DRINK_RECIPES) {
                            for (Item target : recipe.getIngredients()) {
                                if (item == target) {
                                    search.add(recipe);
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        } else if (count > 1) {
            if (this.isFuel(this.cookStacks.get(4).getItem())) {
                for (DrinkRecipe recipe : DrinkRecipe.DRINK_RECIPES) {
                    if (recipe.isNeedFuel())
                        search.add(recipe);
                }
            } else
                search.addAll(DrinkRecipe.DRINK_RECIPES);
            if (!this.cookStacks.get(5).isEmpty() && !search.isEmpty()) {
                Drink drink = DrinkUtils.getDrinkFromStack(this.cookStacks.get(5));
                search.removeIf(recipe -> recipe.getIngredient() != drink);
            }
            for (int i = 0; i < 4; i++) {
                if (!this.cookStacks.get(i).isEmpty() && !search.isEmpty()) {
                    Item item = this.cookStacks.get(i).getItem();
                    Iterator<DrinkRecipe> iterator = search.iterator();
                    while (iterator.hasNext()) {
                        DrinkRecipe recipe = iterator.next();
                        if (i >= recipe.getIngredients().size())
                            iterator.remove();
                        else if (item != recipe.getIngredients().get(i))
                            iterator.remove();
                    }
                }
            }
        }
    }

    private void pushRecipe(DrinkRecipe recipe) {
        int i = 0;
        for (; i < recipe.getIngredients().size(); i++)
            this.cookStacks.set(i + 7, new ItemStack(recipe.getIngredients().get(i)));
        for (; i < 4; i++)
            this.cookStacks.set(i + 7, ItemStack.EMPTY);
        if (recipe.isNeedFuel())
            this.cookStacks.set(11, new ItemStack(MWDItems.COAL_NUGGET));
        else
            this.cookStacks.set(11, ItemStack.EMPTY);
        this.cookStacks.set(12, DrinkUtils.addDrinkToStack(new ItemStack(MWDItems.DRINK), recipe.getIngredient()));
        this.cookStacks.set(13, DrinkUtils.addDrinkToStack(new ItemStack(MWDItems.DRINK), recipe.getResult()));
    }

    private boolean nowNotEqualToLast(int end) {
        for (int i = 0; i <= end; i++) {
            if (this.lastItems.get(i) != this.cookStacks.get(i).getItem())
                return true;
            if (i == 5) {
                if (!this.lastStack.equals(this.cookStacks.get(5), false))
                    return true;
            }
        }
        return false;
    }

    public void next(boolean next) {
        if (next)
            this.next = true;
        else
            this.previous = true;
    }

    public IIntArray getCookData() {
        return this.cookData;
    }

    private boolean accord(DrinkRecipe recipe) {
        if (recipe.isNeedFuel() && !this.isFuel(this.cookStacks.get(4).getItem()))
            return false;
        if (recipe.getIngredient() != DrinkUtils.getDrinkFromStack(this.cookStacks.get(5)))
            return false;
        int size = recipe.getIngredients().size();
        int i = 0;
        for (; i < size; i++) {
            if (recipe.getIngredients().get(i) != this.cookStacks.get(i).getItem())
                return false;
        }
        for (; i <= 3; i++) {
            if (!this.cookStacks.get(i).isEmpty())
                return false;
        }
        return true;
    }

    private boolean isFuel(Item item) {
        return item == MWDItems.CHARCOAL_NUGGET || item == MWDItems.COAL_NUGGET;
    }

    // ICapabilityProvider
    private LazyOptional<? extends IItemHandler>[] handlers = SidedInvWrapper.create(this, Direction.UP, Direction.DOWN, Direction.NORTH, Direction.SOUTH, Direction.EAST, Direction.WEST);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (!this.removed && side != null && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            if (side == Direction.UP)
                return handlers[0].cast();
            if (side == Direction.DOWN)
                return handlers[1].cast();
            if (side == Direction.NORTH)
                return handlers[2].cast();
            if (side == Direction.SOUTH)
                return handlers[3].cast();
            if (side == Direction.EAST)
                return handlers[4].cast();
            else
                return handlers[5].cast();
        }
        return super.getCapability(cap, side);
    }
}