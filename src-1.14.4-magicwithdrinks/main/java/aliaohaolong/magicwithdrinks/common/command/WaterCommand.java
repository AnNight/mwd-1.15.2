package aliaohaolong.magicwithdrinks.common.command;

import aliaohaolong.magicwithdrinks.common.capability.water.IWater;
import aliaohaolong.magicwithdrinks.common.command.Utils.Optional;
import aliaohaolong.magicwithdrinks.common.command.Utils.Category;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.Collection;

import static aliaohaolong.magicwithdrinks.common.command.Utils.message;

public class WaterCommand {
    protected static ArgumentBuilder<CommandSource, ?> register() {
        return Commands.literal("water")
                .then(Commands.literal("query")
                        .then(Commands.literal("value")
                                .executes((d) -> WaterCommand.query(d, Optional.VALUE))
                                .requires(commandSource -> commandSource.hasPermissionLevel(0))
                                .then(Commands.argument("target", EntityArgument.player())
                                        .executes((d) -> WaterCommand.query(d, EntityArgument.getPlayer(d, "target"), Optional.VALUE))
                                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                                )
                        )
                        .then(Commands.literal("lock")
                                .executes((d) -> WaterCommand.query(d, Optional.LOCK))
                                .requires(commandSource -> commandSource.hasPermissionLevel(0))
                                .then(Commands.argument("targets", EntityArgument.player())
                                        .executes((d) -> WaterCommand.query(d, EntityArgument.getPlayer(d, "target"), Optional.LOCK))
                                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                                )
                        )
                )
                .then(Commands.literal("set")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.literal("value")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("int", IntegerArgumentType.integer(0, IWater.MAX))
                                                .executes((d) -> WaterCommand.set(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.VALUE, false))
                                        )
                                )
                        )
                        .then(Commands.literal("lock")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.literal("true")
                                                .executes((d) -> WaterCommand.set(d, EntityArgument.getPlayers(d, "targets"), 0, Optional.LOCK, true))
                                        )
                                        .then(Commands.literal("false")
                                                .executes((d) -> WaterCommand.set(d, EntityArgument.getPlayers(d, "targets"), 0, Optional.LOCK, false))
                                        )
                                )
                        )
                )
                .then(Commands.literal("add")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.argument("targets", EntityArgument.players())
                                .then(Commands.argument("int", IntegerArgumentType.integer(0, IWater.MAX))
                                        .executes((d) -> WaterCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), false))
                                        .then(Commands.literal("true")
                                                .executes((d) -> WaterCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), true))
                                        )
                                        .then(Commands.literal("false")
                                                .executes((d) -> WaterCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), false))
                                        )
                                )
                        )
                )
                .then(Commands.literal("remove")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.argument("targets", EntityArgument.players())
                                .then(Commands.argument("int", IntegerArgumentType.integer(0, IWater.MAX))
                                        .executes((d) -> WaterCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), false))
                                        .then(Commands.literal("true")
                                                .executes((d) -> WaterCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), true))
                                        )
                                        .then(Commands.literal("false")
                                                .executes((d) -> WaterCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), false))
                                        )
                                )
                        )
                );
    }

    private static int query(CommandContext<CommandSource> context, Optional optional) throws CommandSyntaxException {
        return query(context, context.getSource().asPlayer(), optional);
    }

    private static int query(CommandContext<CommandSource> context, ServerPlayerEntity player, Optional optional) {
        IWater water = IWater.getFromPlayer(player);
        if (optional == Optional.VALUE)
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.query.value", player.getName(), water.getValue()), false);
        else if (optional == Optional.LOCK) {
            if (water.getLock())
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.query.lock.true", player.getName()), false);
            else
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.query.lock.false", player.getName()), false);
        } else return 0;
        return 1;
    }

    private static int set(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, int value, Optional optional, boolean lock) {
        IWater water;
        int count = 0;
        for (ServerPlayerEntity player : targets) {
            water = IWater.getFromPlayer(player);
            if (optional == Optional.VALUE) {
                if (water.setValue(value)) PacketManager.sendWaterTo(player, water);
                else {
                    message(Category.WATER, false, context.getSource(), player.getName());
                    continue;
                }
            } else if (optional == Optional.LOCK && lock) {
                water.setLock(true);
                PacketManager.sendWaterTo(player, water);
            } else if (optional == Optional.LOCK) {
                water.setLock(false);
                PacketManager.sendWaterTo(player, water);
            }
            count++;
        }
        if (optional == Optional.VALUE) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.set.value.single", targets.iterator().next().getName(), value), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.set.value.multiple", count, value), true);
        } else if (optional == Optional.LOCK && lock) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.set.lock.true.single", targets.iterator().next().getName()), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.set.lock.true.multiple", count), true);
        } else if (optional == Optional.LOCK) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.set.lock.false.single", targets.iterator().next().getName()), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.set.lock.false.multiple", count), true);
        }
        return count;
    }

    private static int add(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, int value, boolean limit) {
        IWater water;
        int count = 0;
        int result = 0;
        for (ServerPlayerEntity player : targets) {
            water = IWater.getFromPlayer(player);
            if (limit) {
                if (water.limitedIncrease(value)) PacketManager.sendWaterTo(player, water);
                else {
                    message(Category.WATER, true, context.getSource(), player.getName());
                    continue;
                }
            } else {
                water.unlimitedIncrease(value);
                PacketManager.sendWaterTo(player, water);
            }
            if (count == 0) result = water.getValue();
            count++;
        }
        if (count == 1)
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.add.single", value, targets.iterator().next().getName(), result), true);
        if (count > 1)
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.add.multiple", value, count), true);
        return count;
    }

    private static int remove(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, int value, boolean limit) {
        IWater water;
        int count = 0;
        int result = 0;
        for (ServerPlayerEntity player : targets) {
            water = IWater.getFromPlayer(player);
            if (limit) {
                if (water.limitedReduce(value)) PacketManager.sendWaterTo(player, water);
                else {
                    message(Category.WATER, true, context.getSource(), player.getName());
                    continue;
                }
            } else {
                water.unlimitedReduce(value);
                PacketManager.sendWaterTo(player, water);
            }
            if (count == 0) result = water.getValue();
            count++;
        }
        if (count == 1)
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.remove.single", value, targets.iterator().next().getName(), result), true);
        if (count > 1)
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.remove.multiple", value, count), true);
        return 0;
    }
}