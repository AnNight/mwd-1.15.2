package aliaohaolong.magicwithdrinks.common.command;

import net.minecraft.command.CommandSource;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class Utils {
    protected static void message(Category category, boolean lock, CommandSource commandSource, ITextComponent name) {
        if (lock) {
            if (category == Category.WATER)
                commandSource.sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.locked", name), true);
            else if (category == Category.MANA_VALUE || category == Category.MANA_LIMIT)
                commandSource.sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.locked", name), true);
        } else {
            if (category == Category.WATER)
                commandSource.sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.water.data", name), false);
            else if (category == Category.MANA_VALUE)
                commandSource.sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.data.value", name), true);
            else if (category == Category.MANA_LIMIT)
                commandSource.sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.data.limit", name), true);
        }
    }

    protected enum Optional {
        VALUE,
        LIMIT,
        LOCK
    }

    protected enum Category {
        WATER,
        MANA_VALUE,
        MANA_LIMIT
    }
}
