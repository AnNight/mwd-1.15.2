package aliaohaolong.magicwithdrinks.common.command;

import aliaohaolong.magicwithdrinks.common.capability.mana.IMana;
import aliaohaolong.magicwithdrinks.common.command.Utils.Optional;
import aliaohaolong.magicwithdrinks.common.command.Utils.Category;
import aliaohaolong.magicwithdrinks.network.PacketManager;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.Collection;

import static aliaohaolong.magicwithdrinks.common.command.Utils.message;

public class ManaCommand {
    protected static ArgumentBuilder<CommandSource, ?> register() {
        return Commands.literal("mana")
                .then(Commands.literal("query")
                        .then(Commands.literal("value")
                                .executes((d) -> ManaCommand.query(d, Optional.VALUE))
                                .requires(commandSource -> commandSource.hasPermissionLevel(0))
                                .then(Commands.argument("target", EntityArgument.player())
                                        .executes((d) -> ManaCommand.query(d, EntityArgument.getPlayer(d, "target"), Optional.VALUE))
                                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                                )
                        )
                        .then(Commands.literal("limit")
                                .executes((d) -> ManaCommand.query(d, Optional.LIMIT))
                                .requires(commandSource -> commandSource.hasPermissionLevel(0))
                                .then(Commands.argument("target", EntityArgument.player())
                                        .executes((d) -> ManaCommand.query(d, EntityArgument.getPlayer(d, "target"), Optional.LIMIT))
                                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                                )
                        )
                        .then(Commands.literal("lock")
                                .executes((d) -> ManaCommand.query(d, Optional.LOCK))
                                .requires(commandSource -> commandSource.hasPermissionLevel(0))
                                .then(Commands.argument("target", EntityArgument.player())
                                        .executes((d) -> ManaCommand.query(d, EntityArgument.getPlayer(d, "target"), Optional.LOCK))
                                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                                )
                        )
                )
                .then(Commands.literal("set")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.literal("value")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("int", IntegerArgumentType.integer(0, IMana.MAX))
                                                .executes((d) -> ManaCommand.set(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.VALUE, false))
                                        )
                                )
                        )
                        .then(Commands.literal("limit")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("int", IntegerArgumentType.integer(0, IMana.MAX))
                                                .executes((d) -> ManaCommand.set(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.LIMIT, false))
                                        )
                                )
                        )
                        .then(Commands.literal("lock")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.literal("true")
                                                .executes((d) -> ManaCommand.set(d, EntityArgument.getPlayers(d, "targets"), 0, Optional.LOCK, true))
                                        )
                                        .then(Commands.literal("false")
                                                .executes((d) -> ManaCommand.set(d, EntityArgument.getPlayers(d, "targets"), 0, Optional.LOCK, false))
                                        )
                                )
                        )
                )
                .then(Commands.literal("add")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.literal("value")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("int", IntegerArgumentType.integer(0, IMana.MAX))
                                                .executes((d) -> ManaCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.VALUE, false))
                                                .then(Commands.literal("true")
                                                        .executes((d) -> ManaCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.VALUE, true))
                                                )
                                                .then(Commands.literal("false")
                                                        .executes((d) -> ManaCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.VALUE, false))
                                                )
                                        )
                                )
                        )
                        .then(Commands.literal("limit")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("int", IntegerArgumentType.integer(0, IMana.MAX))
                                                .executes((d) -> ManaCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.LIMIT, false))
                                                .then(Commands.literal("true")
                                                        .executes((d) -> ManaCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.LIMIT, true))
                                                )
                                                .then(Commands.literal("false")
                                                        .executes((d) -> ManaCommand.add(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.LIMIT, false))
                                                )
                                        )
                                )
                        )
                )
                .then(Commands.literal("remove")
                        .requires(commandSource -> commandSource.hasPermissionLevel(2))
                        .then(Commands.literal("value")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("int", IntegerArgumentType.integer(0, IMana.MAX))
                                                .executes((d) -> ManaCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.VALUE, false))
                                                .then(Commands.literal("true")
                                                        .executes((d) -> ManaCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.VALUE, true))
                                                )
                                                .then(Commands.literal("false")
                                                        .executes((d) -> ManaCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.VALUE, false))
                                                )
                                        )
                                )
                        )
                        .then(Commands.literal("limit")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("int", IntegerArgumentType.integer(0, IMana.MAX))
                                                .executes((d) -> ManaCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.LIMIT, false))
                                                .then(Commands.literal("true")
                                                        .executes((d) -> ManaCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.LIMIT, true))
                                                )
                                                .then(Commands.literal("false")
                                                        .executes((d) -> ManaCommand.remove(d, EntityArgument.getPlayers(d, "targets"), IntegerArgumentType.getInteger(d, "int"), Optional.LIMIT, false))
                                                )
                                        )
                                )
                        )
                );
    }

    private static int query(CommandContext<CommandSource> context, Optional optional) throws CommandSyntaxException {
        return query(context, context.getSource().asPlayer(), optional);
    }

    private static int query(CommandContext<CommandSource> context, ServerPlayerEntity player, Optional optional) {
        IMana mana = IMana.getFromPlayer(player);
        if (optional == Optional.VALUE) {
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.query.value", player.getName(), mana.getValue()), false);
        } else if (optional == Optional.LIMIT) {
            context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.query.limit", player.getName(), mana.getLimit()), false);
        } else if (optional == Optional.LOCK) {
            if (mana.getLock())
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.query.lock.true", player.getName()), false);
            else
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.query.lock.false", player.getName()), false);
        } else return 0;
        return 1;
    }

    private static int set(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, int value, Optional optional, boolean lock) {
        IMana mana;
        int count = 0;
        for (ServerPlayerEntity player : targets) {
            mana = IMana.getFromPlayer(player);
            if (optional == Optional.VALUE) {
                if (mana.setValue(value)) PacketManager.sendManaTo(player, mana);
                else {
                    message(Category.MANA_VALUE, false, context.getSource(), player.getName());
                    continue;
                }
            } else if (optional == Optional.LIMIT) {
                if (mana.setLimit(value)) PacketManager.sendManaTo(player, mana);
                else {
                    message(Category.MANA_VALUE, false, context.getSource(), player.getName());
                    continue;
                }
            } else if (optional == Optional.LOCK && lock) {
                mana.setLock(true);
                PacketManager.sendManaTo(player, mana);
            } else if (optional == Optional.LOCK) {
                mana.setLock(false);
                PacketManager.sendManaTo(player, mana);
            } else return 0;
            count++;
        }
        if (optional == Optional.VALUE) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.set.value.single", targets.iterator().next().getName(), value), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.set.value.multiple", count, value), true);
        } else if (optional == Optional.LIMIT) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.set.limit.single", targets.iterator().next().getName(), value), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.set.limit.multiple", count, value), true);
        } else if (optional == Optional.LOCK && lock) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.set.lock.true.single", targets.iterator().next().getName()), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.set.lock.true.multiple", count), true);
        } else if (optional == Optional.LOCK) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.set.lock.false.single", targets.iterator().next().getName()), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.set.lock.false.multiple", count), true);
        }
        return count;
    }

    private static int add(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, int value, Optional optional, boolean limit) {
        IMana mana;
        int count = 0;
        int result = 0;
        for (ServerPlayerEntity player : targets) {
            mana = IMana.getFromPlayer(player);
            if (optional == Optional.VALUE) {
                if (limit) {
                    if (mana.limitedIncreaseValue(value)) PacketManager.sendManaTo(player, mana);
                    else {
                        message(Category.MANA_VALUE, true, context.getSource(), player.getName());
                        continue;
                    }
                } else {
                    mana.unlimitedIncreaseValue(value);
                    PacketManager.sendManaTo(player, mana);
                }
            } else if (optional == Optional.LIMIT) {
                if (limit) {
                    if (mana.limitedIncreaseLimit(value)) PacketManager.sendManaTo(player, mana);
                    else {
                        message(Category.MANA_VALUE, true, context.getSource(), player.getName());
                        continue;
                    }
                } else {
                    mana.unlimitedIncreaseLimit(value);
                    PacketManager.sendManaTo(player, mana);
                }
            } else return count;
            if (count == 0) result = mana.getValue();
            count++;
        }
        if (optional == Optional.VALUE) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.add.value.single", value, targets.iterator().next().getName(), result), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.add.value.multiple", value, count), true);
        } else if (optional == Optional.LIMIT) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.add.limit.single", value, targets.iterator().next().getName(), result), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.add.limit.multiple", value, count), true);
        }
        return count;
    }

    private static int remove(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, int value, Optional optional, boolean limit) {
        IMana mana;
        int count = 0;
        int result = 0;
        for (ServerPlayerEntity player: targets) {
            mana = IMana.getFromPlayer(player);
            if (optional == Optional.VALUE) {
                if (limit) {
                    if (mana.limitedReduceValue(value)) PacketManager.sendManaTo(player, mana);
                    else {
                        message(Category.MANA_VALUE, true, context.getSource(), player.getName());
                        continue;
                    }
                } else {
                    mana.unlimitedReduceValue(value);
                    PacketManager.sendManaTo(player, mana);
                }
            } else if (optional == Optional.LIMIT) {
                if (limit) {
                    if (mana.limitedReduceLimit(value)) PacketManager.sendManaTo(player, mana);
                    else {
                        message(Category.MANA_VALUE, true, context.getSource(), player.getName());
                        continue;
                    }
                } else {
                    mana.unlimitedReduceLimit(value);
                    PacketManager.sendManaTo(player, mana);
                }
            } else return count;
            if (count == 0) {
                if (optional == Optional.VALUE)
                    result = mana.getValue();
                else if (optional == Optional.LIMIT)
                    result = mana.getLimit();
            }
            count++;
        }
        if (optional == Optional.VALUE) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.remove.value.single", value, targets.iterator().next().getName(), result), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.remove.value.multiple", value, count), true);
        } else if (optional == Optional.LIMIT) {
            if (count == 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.remove.limit.single", value, targets.iterator().next().getName(), result), true);
            else if (count > 1)
                context.getSource().sendFeedback(new TranslationTextComponent("commands.magicwithdrinks.energy.mana.remove.limit.multiple", value, count), true);
        }
        return count;
    }
}
