package aliaohaolong.magicwithdrinks.common.command;

import aliaohaolong.magicwithdrinks.MWD;
import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;

public class ModCommands {
    public static void register(CommandDispatcher<CommandSource> dispatcher) {
        dispatcher.register(Commands.literal(MWD.MOD_ID)
                .then(WaterCommand.register())
//                .then(ManaCommand.register())
        );
    }
}