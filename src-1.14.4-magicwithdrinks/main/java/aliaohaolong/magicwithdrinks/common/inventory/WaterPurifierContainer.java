package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.api.MWDContainerType;
import aliaohaolong.magicwithdrinks.common.drink.DrinkUtils;
import aliaohaolong.magicwithdrinks.api.MWDDrinks;
import aliaohaolong.magicwithdrinks.common.inventory.container.FuelSlot;
import aliaohaolong.magicwithdrinks.common.inventory.container.ResultSlot;
import aliaohaolong.magicwithdrinks.common.item.FilterSliceItem;
import aliaohaolong.magicwithdrinks.common.tileentity.WaterPurifierTileEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@SuppressWarnings("NullableProblems")
public class WaterPurifierContainer extends Container {
    private static final int OFFSET = 18;
    private IInventory inventory;
    private IIntArray data;

    public WaterPurifierContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(5), new IntArray(3));
    }

    public WaterPurifierContainer(int windowId, PlayerInventory playerInventory, IInventory inventory, IIntArray data) {
        super(MWDContainerType.WATER_PURIFIER, windowId);
        assertInventorySize(inventory, 5);
        assertIntArraySize(data, 3);
        this.inventory = inventory;
        this.data = data;
        // Custom inventory
        this.addSlot(new Slot(this.inventory, 0, 35, 17){
            @Override
            public boolean isItemValid(ItemStack stack) {
                return DrinkUtils.getDrinkFromStack(stack) == MWDDrinks.WATER;
            }

            @Override
            public int getSlotStackLimit() {
                return 1;
            }
        });
        this.addSlot(new FuelSlot(this.inventory, 1, 35, 53));
        this.addSlot(new Slot(this.inventory, 2, 71, 35){
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() instanceof FilterSliceItem;
            }

            @Override
            public int getSlotStackLimit() {
                return 1;
            }
        });
        this.addSlot(new ResultSlot(this.inventory, 3, 107, 35));
        this.addSlot(new ResultSlot(this.inventory, 4, 125, 35));
        this.trackIntArray(data);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack clickStack = slot.getStack();
            stack = clickStack.copy();
            if (index < 5) {
                if (!this.mergeItemStack(clickStack, 5, 41, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(clickStack, stack);
            } else {
                if (DrinkUtils.getDrinkFromStack(clickStack) == MWDDrinks.WATER) {
                    if (!this.mergeItemStack(clickStack, 0, 1, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (net.minecraftforge.common.ForgeHooks.getBurnTime(clickStack) > 0) {
                    if (!this.mergeItemStack(clickStack, 1, 2, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (clickStack.getItem() instanceof FilterSliceItem) {
                    if (!this.mergeItemStack(clickStack, 2, 3, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index < 32) {
                    if (!this.mergeItemStack(clickStack, 32, 41, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index < 41) {
                    if (!this.mergeItemStack(clickStack, 5, 32, false)) {
                        return ItemStack.EMPTY;
                    }
                }
            }
            if (clickStack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (clickStack.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, clickStack);
        }
        return stack;
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return this.inventory.isUsableByPlayer(playerIn);
    }

    /** Add default slot horizontally */
    private int addDefaultSlot(PlayerInventory playerInventory, int index, int posX, int posY) {
        for (int i = 0; i < 9; i++) {
            addSlot(new Slot(playerInventory, index, posX, posY));
            posX += OFFSET;
            index++;
        }
        return index;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCookProgressionScaled() {
        return this.data.get(0) * 22 / WaterPurifierTileEntity.TOTAL_TIME;
    }

    @OnlyIn(Dist.CLIENT)
    public int getBurnLeftScaled() {
        return this.data.get(1) * 13 / this.data.get(2);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isCooking() {
        return this.data.get(0) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isBurning() {
        return this.data.get(1) > 0;
    }
}