package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.api.MWDContainerType;
import aliaohaolong.magicwithdrinks.common.inventory.container.FuelSlot;
import aliaohaolong.magicwithdrinks.common.inventory.container.ResultSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@SuppressWarnings("NullableProblems")
public class MillContainer extends Container {
    private static final int OFFSET = 18;
    private IInventory inventory;
    private IIntArray data;

    public MillContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(3), new IntArray(4));
    }

    public MillContainer(int windowId, PlayerInventory playerInventory, IInventory inventory, IIntArray data) {
        super(MWDContainerType.MILL, windowId);
        assertInventorySize(inventory, 3);
        assertIntArraySize(data, 4);
        this.inventory = inventory;
        this.data = data;
        // Custom inventory
        this.addSlot(new Slot(this.inventory, 0, 56, 17));
        this.addSlot(new FuelSlot(this.inventory, 1, 56, 53));
        this.addSlot(new ResultSlot(this.inventory, 2, 116, 35));
        this.trackIntArray(data);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return this.inventory.isUsableByPlayer(playerIn);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack clickStack = slot.getStack();
            stack = clickStack.copy();
            if (index < 3) {
                if (!this.mergeItemStack(clickStack, 3, 38, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(clickStack, stack);
            } else {
                if (net.minecraftforge.common.ForgeHooks.getBurnTime(clickStack) > 0) {
                    if (!this.mergeItemStack(clickStack, 1, 2, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (!this.mergeItemStack(clickStack, 0, 1, false)) {
                    if (index < 30) {
                        if (!this.mergeItemStack(clickStack, 30, 39, false)) {
                            return ItemStack.EMPTY;
                        }
                    } else if (index < 39) {
                        if (!this.mergeItemStack(clickStack, 3, 30, false)) {
                            return ItemStack.EMPTY;
                        }
                    }
                }
            }
            if (clickStack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (clickStack.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, clickStack);
        }
        return stack;
    }

    /** Add default slot horizontally */
    private int addDefaultSlot(PlayerInventory playerInventory, int index, int posX, int posY) {
        for (int i = 0; i < 9; i++) {
            addSlot(new Slot(playerInventory, index, posX, posY));
            posX += OFFSET;
            index++;
        }
        return index;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCookProgressionScaled() {
        return data.get(2) * 22 / data.get(3);
    }

    @OnlyIn(Dist.CLIENT)
    public int getBurnLeftScaled() {
        return data.get(0) * 13 / data.get(1);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isCooking() {
        return data.get(2) > 0 && data.get(3) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isBurning() {
        return data.get(0) > 0 && data.get(1) > 0;
    }
}