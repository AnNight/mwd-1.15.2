package aliaohaolong.magicwithdrinks.common.inventory.container;

import aliaohaolong.magicwithdrinks.common.item.CapacityCardItem;
import aliaohaolong.magicwithdrinks.common.item.EfficiencyCardItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

public class CardSlot extends Slot {
    public CardSlot(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        boolean noneOfTheEfficiencyCard = true;
        for (byte i = 3; i < 6 && noneOfTheEfficiencyCard; i++) {
            if (this.inventory.getStackInSlot(i).getItem() instanceof EfficiencyCardItem)
                noneOfTheEfficiencyCard = false;
        }
        return this.inventory.getStackInSlot(this.getSlotIndex()).isEmpty() && (stack.getItem() instanceof CapacityCardItem || (noneOfTheEfficiencyCard && stack.getItem() instanceof EfficiencyCardItem));
    }

    @Override
    public int getSlotStackLimit() {
        return 1;
    }
}