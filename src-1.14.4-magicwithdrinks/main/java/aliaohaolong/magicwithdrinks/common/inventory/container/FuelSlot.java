package aliaohaolong.magicwithdrinks.common.inventory.container;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class FuelSlot extends Slot {
    public FuelSlot(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return net.minecraftforge.common.ForgeHooks.getBurnTime(stack) > 0;
    }

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return stack.getItem() == Items.BUCKET ? 1 : super.getItemStackLimit(stack);
    }
}