package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.api.MWDContainerType;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.inventory.container.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@SuppressWarnings("NullableProblems")
public class DrinkMakingMachineContainer extends Container {
    private static final int OFFSET = 18;
    private IInventory inventory;
    private IIntArray intArray;

    public DrinkMakingMachineContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(14), new IntArray(5));
    }

    public DrinkMakingMachineContainer(int windowId, PlayerInventory playerInventory, IInventory inventory, IIntArray intArray) {
        super(MWDContainerType.DRINK_MAKING_MACHINE, windowId);
        assertInventorySize(inventory, 14);
        assertIntArraySize(intArray, 5);
        this.inventory = inventory;
        this.intArray = intArray;
        // Custom inventory
        addSlot(new Slot(this.inventory, 0, 8, 18));
        addSlot(new Slot(this.inventory, 1, 26, 18));
        addSlot(new Slot(this.inventory, 2, 44, 18));
        addSlot(new Slot(this.inventory, 3, 62, 18));
        addSlot(new Slot(this.inventory, 4, 80, 18){
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() == MWDItems.COAL_NUGGET || stack.getItem() == MWDItems.CHARCOAL_NUGGET;
            }
        });
        addSlot(new Slot(this.inventory, 5, 98, 18){
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() == MWDItems.DRINK;
            }

            @Override
            public int getSlotStackLimit() {
                return 1;
            }
        });
        addSlot(new ResultSlot(this.inventory, 6, 148, 14));
        addSlot(new CannotInteractionSlot(this.inventory, 7, 26, 52));
        addSlot(new CannotInteractionSlot(this.inventory, 8, 44, 52));
        addSlot(new CannotInteractionSlot(this.inventory, 9, 62, 52));
        addSlot(new CannotInteractionSlot(this.inventory, 10, 80, 52));
        addSlot(new CannotInteractionSlot(this.inventory, 11, 98, 52));
        addSlot(new CannotInteractionSlot(this.inventory, 12, 116, 52));
        addSlot(new CannotInteractionSlot(this.inventory, 13, 152, 52));
        this.trackIntArray(intArray);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack clickStack = slot.getStack();
            stack = clickStack.copy();
            if (index < 7) {
                if (!this.mergeItemStack(clickStack, 14, 50, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(clickStack, stack);
            } else if (index > 13) {
                if (clickStack.getItem() == MWDItems.COAL_NUGGET || clickStack.getItem() == MWDItems.CHARCOAL_NUGGET) {
                    if (!this.mergeItemStack(clickStack, 4, 5, false)) {
                        if (index < 41) {
                            if (!this.mergeItemStack(clickStack, 41, 50, false)) {
                                return ItemStack.EMPTY;
                            }
                        } else {
                            if (!this.mergeItemStack(clickStack, 14, 41, false)) {
                                return ItemStack.EMPTY;
                            }
                        }
                    }
                } else if (clickStack.getItem() == MWDItems.DRINK) {
                    if (!this.mergeItemStack(clickStack, 5, 6, false)) {
                        if (!this.mergeItemStack(clickStack, 0, 4, false)) {
                            return ItemStack.EMPTY;
                        }
                    }
                } else if (!this.mergeItemStack(clickStack, 0, 4, false)) {
                    if (index < 41) {
                        if (!this.mergeItemStack(clickStack, 41, 50, false)) {
                            return ItemStack.EMPTY;
                        }
                    } else {
                        if (!this.mergeItemStack(clickStack, 14, 41, false)) {
                            return ItemStack.EMPTY;
                        }
                    }
                }
            }
            if (clickStack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (clickStack.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, clickStack);
        }
        return stack;
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return this.inventory.isUsableByPlayer(playerIn);
    }

    /** Add default slot horizontally */
    private int addDefaultSlot(PlayerInventory playerInventory, int index, int posX, int posY) {
        for (int i = 0; i < 9; i++) {
            addSlot(new Slot(playerInventory, index, posX, posY));
            posX += OFFSET;
            index++;
        }
        return index;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCookScaled() {
        if (this.intArray.get(0) != 0 && this.intArray.get(1) != 0)
            return this.intArray.get(0) * 160 / this.intArray.get(1);
        return 0;
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isCooking() {
        return this.intArray.get(1) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public int[] getPos() {
        return new int[]{this.intArray.get(2), this.intArray.get(3), this.intArray.get(4)};
    }
}