package aliaohaolong.magicwithdrinks.common.inventory;

import aliaohaolong.magicwithdrinks.api.MWDContainerType;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import aliaohaolong.magicwithdrinks.common.inventory.container.CardSlot;
import aliaohaolong.magicwithdrinks.common.item.CapacityCardItem;
import aliaohaolong.magicwithdrinks.common.item.EfficiencyCardItem;
import aliaohaolong.magicwithdrinks.common.item.ManaItem;
import aliaohaolong.magicwithdrinks.common.recipe.ExtractingRecipeUtils;
import net.minecraft.block.Block;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class ManaExtractorContainer extends Container {
    private static final int OFFSET = 18;
    private IInventory inventory;
    private IIntArray intArray;

    public ManaExtractorContainer(int windowId, PlayerInventory playerInventory) {
        this(windowId, playerInventory, new Inventory(6), new IntArray(5));
    }

    public ManaExtractorContainer(int windowId, PlayerInventory playerInventory, IInventory inventory, IIntArray intArray) {
        super(MWDContainerType.MANA_EXTRACTOR, windowId);
        assertInventorySize(inventory, 6);
        assertIntArraySize(intArray, 5);
        this.inventory = inventory;
        this.intArray = intArray;
        // Custom inventory
        this.addSlot(new Slot(this.inventory, 0, 86, 28){
            @Override
            public boolean isItemValid(ItemStack stack) {
                return ExtractingRecipeUtils.canBeReceived(stack.getItem());
            }

            @Override
            public int getSlotStackLimit() {
                return 1;
            }
        });
        this.addSlot(new Slot(this.inventory, 1, 134, 62){
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() instanceof ManaItem && ((ManaItem) stack.getItem()).isStoreItem();
            }

            @Override
            public int getSlotStackLimit() {
                return 1;
            }
        });
        this.addSlot(new Slot(this.inventory, 2, 26, 17){
            @Override
            public boolean isItemValid(ItemStack stack) {
                return stack.getItem() == MWDItems.CATALYST;
            }
        });
        this.addSlot(new CardSlot(this.inventory, 3, 134, 8));
        this.addSlot(new CardSlot(this.inventory, 4, 152, 8));
        this.addSlot(new CardSlot(this.inventory, 5, 152, 26));
        this.trackIntArray(intArray);
        int posX = 8, posY = 84;
        // Default inventory - Hot bar
        int index = addDefaultSlot(playerInventory, 0, posX, posY + 58);
        // Default inventory - Player inventory
        index = addDefaultSlot(playerInventory, index, posX, posY);
        index = addDefaultSlot(playerInventory, index, posX, posY += OFFSET);
        addDefaultSlot(playerInventory, index, posX, posY + OFFSET);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack originalStack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack stack = slot.getStack();
            originalStack = stack.copy();
            if (index >= 0 && index < 6) {
                if (!this.mergeItemStack(stack, 6, 42, false)) {
                    return ItemStack.EMPTY;
                }
            } else {
                if (ExtractingRecipeUtils.canBeReceived(stack.getItem())) {
                    if (!this.mergeItemStack(stack, 0, 1, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                if (stack.getItem() instanceof ManaItem && ((ManaItem) stack.getItem()).isStoreItem()) {
                    if (!this.mergeItemStack(stack, 1, 2, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                if (stack.getItem() == MWDItems.CATALYST) {
                    if (!this.mergeItemStack(stack, 2, 3, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                boolean noneOfTheEfficiencyCard = true;
                for (byte i = 3; i < 6 && noneOfTheEfficiencyCard; i++) {
                    if (this.inventory.getStackInSlot(i).getItem() instanceof EfficiencyCardItem)
                        noneOfTheEfficiencyCard = false;
                }
                if (stack.getItem() instanceof CapacityCardItem || (noneOfTheEfficiencyCard && stack.getItem() instanceof EfficiencyCardItem)) {
                    if (!this.mergeItemStack(stack, 3, 6, false)) {
                        return ItemStack.EMPTY;
                    }
                }
            }
            if (index >= 6 && index < 33) {
                if (!this.mergeItemStack(stack, 33, 42, false)) {
                    return ItemStack.EMPTY;
                }
            }
            if (index >= 33 && index < 42 && !this.mergeItemStack(stack, 6, 33, false)) {
                return ItemStack.EMPTY;
            }
            if (stack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (stack.getCount() == originalStack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, stack);
        }
        return originalStack;
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return this.inventory.isUsableByPlayer(playerIn);
    }

    /** Add default slot horizontally */
    private int addDefaultSlot(PlayerInventory playerInventory, int index, int posX, int posY) {
        for (int i = 0; i < 9; i++) {
            addSlot(new Slot(playerInventory, index, posX, posY));
            posX += OFFSET;
            index++;
        }
        return index;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCookScaled() {
        return this.intArray.get(0) * 27 / this.intArray.get(1);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isCooking() {
        return this.intArray.get(1) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public int getCatalystScaled() {
        return (int) Math.ceil((double) this.intArray.get(4) / 2);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean hasCatalyst() {
        return this.intArray.get(4) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public int getBufferScaled() {
        return this.intArray.get(2) * 64 / this.intArray.get(3);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean hasValue() {
        return this.intArray.get(2) > 0;
    }

    @OnlyIn(Dist.CLIENT)
    public int getBuffer() {
        return this.intArray.get(2);
    }

    @OnlyIn(Dist.CLIENT)
    public int getMaxBuffer() {
        return this.intArray.get(3);
    }

    @OnlyIn(Dist.CLIENT)
    public int getCatalyst() {
        return this.intArray.get(4);
    }
}