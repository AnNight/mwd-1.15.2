package aliaohaolong.magicwithdrinks.common.drink;

import aliaohaolong.magicwithdrinks.api.MWDDrinks;
import aliaohaolong.magicwithdrinks.api.MWDForgeRegistries;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;
import java.util.List;

public class DrinkUtils {
    public static ItemStack addDrinkToStack(ItemStack stack, Drink drink) {
        stack.getOrCreateTag().putString("Drink", drink.getResourceLocation().toString());
        return stack;
    }

    public static Drink getDrinkFromName(String name) {
        return MWDForgeRegistries.getDrinks().getValue(new ResourceLocation(name));
    }

    public static Drink getDrinkFromNBT(@Nullable CompoundNBT tag) {
        return tag == null ? MWDDrinks.UNREAL_DRINK : getDrinkFromName(tag.getString("Drink"));
    }

    public static Drink getDrinkFromStack(ItemStack stack) {
        return getDrinkFromNBT(stack.getTag());
    }

    public static int getColorFromStack(ItemStack stack) {
        return getDrinkFromStack(stack).getColor();
    }

    public static List<EffectInstance> getEffectsFromTag(CompoundNBT tag) {
        return getDrinkFromNBT(tag).getEffects();
    }

    public static List<EffectInstance> getEffectsFromStack(ItemStack stack) {
        return getEffectsFromTag(stack.getTag());
    }
}
