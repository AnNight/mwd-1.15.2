package aliaohaolong.magicwithdrinks.common.drink;

import com.google.common.collect.ImmutableList;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraftforge.registries.ForgeRegistry;
import net.minecraftforge.registries.ForgeRegistryEntry;

import java.util.List;

public class Drink extends ForgeRegistryEntry<Drink> {
    private final ResourceLocation resourceLocation;
    private final int color;
    private final int hunger;
    private final float saturation;
    private final int water;
    private final int mana;
    private final int max_mana;
    private final boolean canEatWhenFull;
    private final ImmutableList<EffectInstance> effects;

    public Drink(Drink.Properties properties) {
        this.resourceLocation = properties.resourceLocation;
        this.color = properties.color;
        this.hunger = properties.hunger;
        this.saturation = properties.saturation;
        this.water = properties.water;
        this.mana = properties.mana;
        this.max_mana = properties.max_mana;
        this.canEatWhenFull = properties.alwaysEdible;
        this.effects = properties.effects;
    }

    public ResourceLocation getResourceLocation() {
        return this.resourceLocation;
    }

    public int getColor() {
        return this.color;
    }

    public int getHunger() {
        return this.hunger;
    }

    public float getSaturation() {
        return this.saturation;
    }

    public int getWater() {
        return this.water;
    }

    public int getMana() {
        return  this.mana;
    }

    public int getMax_mana() {
        return this.max_mana;
    }

    public boolean canEatWhenFull() {
        return this.canEatWhenFull;
    }

    public List<EffectInstance> getEffects() {
        return effects;
    }

    public static class Properties {
        private ResourceLocation resourceLocation;
        private int color;
        private int hunger = 0;
        private float saturation = 0.0F;
        private int water;
        private int mana = 0;
        private int max_mana = 0;
        private boolean alwaysEdible = false;
        private ImmutableList<EffectInstance> effects;

        public Properties(ResourceLocation resource, int color, int water, EffectInstance... effects) {
            this.resourceLocation = resource;
            this.color = Math.max(0, color);
            this.water = water;
            this.effects = ImmutableList.copyOf(effects);
        }

        public Drink.Properties hunger(int hunger, float saturation) {
            this.hunger = Math.max(0, hunger);
            this.saturation = Math.max(0, saturation);
            return this;
        }

        public Drink.Properties mana(int mana) {
            this.mana = mana;
            return this;
        }

        public Drink.Properties max_mana(int max_mana) {
            this.max_mana = max_mana;
            return this;
        }

        public Drink.Properties setAlwaysEdible() {
            this.alwaysEdible = true;
            return this;
        }
    }
}
