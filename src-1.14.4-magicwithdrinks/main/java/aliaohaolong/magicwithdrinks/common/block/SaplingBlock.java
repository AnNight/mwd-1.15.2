package aliaohaolong.magicwithdrinks.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.trees.Tree;

public class SaplingBlock extends net.minecraft.block.SaplingBlock {
    public SaplingBlock(Tree tree, Block.Properties properties) {
        super(tree, properties);
    }
}