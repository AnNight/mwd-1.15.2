package aliaohaolong.magicwithdrinks.common.block;

import aliaohaolong.magicwithdrinks.api.MWDBlockStateProperties;
import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.*;

import java.util.Random;

@SuppressWarnings({"NullableProblems", "deprecation"})
public class MoonlightVineBlock extends Block {
    public static final BooleanProperty HAS_FRUIT = MWDBlockStateProperties.HAS_FRUIT;

    public MoonlightVineBlock(Block.Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(HAS_FRUIT, Boolean.FALSE));
    }

    @Override
    public int getLightValue(BlockState state) {
        return state.get(HAS_FRUIT) == Boolean.TRUE ? super.getLightValue(state) : 0;
    }

    @Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(HAS_FRUIT);
    }

    @Override
    public ItemStack getPickBlock(BlockState state, RayTraceResult target, IBlockReader world, BlockPos pos, PlayerEntity player) {
        return state.get(HAS_FRUIT) ? new ItemStack(MWDItems.MOONLIGHT_BERRIES) : new ItemStack(MWDItems.MOONLIGHT_VINE);
    }

    public static BlockState getRandomState(Random random) {
        return random.nextInt(5) == 0 ? MWDBlocks.MOONLIGHT_VINE_PLANT.getDefaultState().with(MWDBlockStateProperties.HAS_FRUIT, Boolean.TRUE) : MWDBlocks.MOONLIGHT_VINE_PLANT.getDefaultState();
    }

    @Override
    public void tick(BlockState state, World worldIn, BlockPos pos, Random random) {
        if (!state.isValidPosition(worldIn, pos))
            worldIn.destroyBlock(pos, true);
        super.tick(state, worldIn, pos, random);
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        if (facing == Direction.DOWN && !stateIn.isValidPosition(worldIn, currentPos))
            worldIn.getPendingBlockTicks().scheduleTick(currentPos, this, 1);
        return super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
    }

    @Override
    public boolean isValidPosition(BlockState state, IWorldReader worldIn, BlockPos pos) {
        BlockPos blockpos = pos.down();
        BlockState blockstate = worldIn.getBlockState(blockpos);
        Block block = blockstate.getBlock();
        return block == this || block == Blocks.GRASS_BLOCK || block == Blocks.PODZOL;
    }
}