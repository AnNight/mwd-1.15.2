package aliaohaolong.magicwithdrinks.common.block.trees;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.common.world.gen.feature.LowTreeFeature;
import net.minecraft.block.trees.Tree;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

import javax.annotation.Nullable;
import java.util.Random;

public class CyanTree extends Tree {
    @Nullable
    @Override
    protected AbstractTreeFeature<NoFeatureConfig> getTreeFeature(Random random) {
        return new LowTreeFeature(NoFeatureConfig::deserialize, true, MWDBlocks.CYAN_LOG.getDefaultState(), MWDBlocks.CYAN_LEAVES.getDefaultState(), MWDBlocks.CYAN_SAPLING);
    }
}