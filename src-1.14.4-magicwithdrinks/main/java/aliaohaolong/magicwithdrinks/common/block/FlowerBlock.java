package aliaohaolong.magicwithdrinks.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.potion.Effect;

public class FlowerBlock extends net.minecraft.block.FlowerBlock {
    public FlowerBlock(Effect effect, int effectDuration) {
        super(effect, effectDuration, Block.Properties.create(Material.PLANTS).doesNotBlockMovement().hardnessAndResistance(0f).sound(SoundType.PLANT));
    }

    public FlowerBlock(Effect effect, int effectDuration, int lightValue) {
        super(effect, effectDuration, Block.Properties.create(Material.PLANTS).doesNotBlockMovement().hardnessAndResistance(0f).lightValue(lightValue).sound(SoundType.PLANT));
    }
}