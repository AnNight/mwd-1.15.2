package aliaohaolong.magicwithdrinks.common.block;

import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IWorldReader;

import java.util.Random;

public class OreBlock extends Block {
    public OreBlock(Block.Properties properties) {
        super(properties);
    }

    protected int getExperience(Random random) {
        return this == MWDBlocks.MAGIC_GEM_ORE ? MathHelper.nextInt(random, 2, 5) : 0;
    }

    @Override
    public int getExpDrop(BlockState state, IWorldReader world, BlockPos pos, int fortune, int silktouch) {
        return silktouch == 0 ? this.getExperience(RANDOM) : 0;
    }
}
