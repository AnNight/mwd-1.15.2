package aliaohaolong.magicwithdrinks.common.block;

import aliaohaolong.magicwithdrinks.api.MWDBlockStateProperties;
//import aliaohaolong.magicwithdrinks.api.MWDTileEntityType;
//import aliaohaolong.magicwithdrinks.common.tileentity.BrewingMachineTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.entity.player.ServerPlayerEntity;
//import net.minecraft.inventory.InventoryHelper;
//import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.Hand;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.math.BlockRayTraceResult;
//import net.minecraft.world.IBlockReader;
//import net.minecraft.world.World;
//import net.minecraftforge.fml.network.NetworkHooks;
//
//import javax.annotation.Nullable;

public class BrewingMachineBlock extends Block {
    public static final BooleanProperty SWITCH = MWDBlockStateProperties.SWITCH;

    public BrewingMachineBlock(Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(SWITCH, Boolean.FALSE));
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState();
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(SWITCH);
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return false;
    }

//    @Nullable
//    @Override
//    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
//        return MWDTileEntityType.ACTIVE_SUBSTANCE_INCUBATOR.create();
//    }
//
//    @Override
//    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
//        if (state.getBlock() != newState.getBlock()) {
//            TileEntity tileentity = worldIn.getTileEntity(pos);
//            if (tileentity instanceof BrewingMachineTileEntity) {
//                InventoryHelper.dropInventoryItems(worldIn, pos, (BrewingMachineTileEntity) tileentity);
//            }
//            super.onReplaced(state, worldIn, pos, newState, isMoving);
//        }
//    }
//
//    @Override
//    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
//        if (!worldIn.isRemote) {
//            TileEntity tileEntity = worldIn.getTileEntity(pos);
//            if (tileEntity instanceof BrewingMachineTileEntity) {
//                NetworkHooks.openGui((ServerPlayerEntity) player, (INamedContainerProvider) tileEntity, tileEntity.getPos());
//            }
//        }
//        return true;
//    }
}