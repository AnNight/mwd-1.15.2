package aliaohaolong.magicwithdrinks.common.block;

import aliaohaolong.magicwithdrinks.api.MWDBiomes;
import aliaohaolong.magicwithdrinks.api.MWDBlocks;
import aliaohaolong.magicwithdrinks.api.MWDItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.IGrowable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;

import java.util.Random;

@SuppressWarnings({"NullableProblems", "deprecation"})
public class MoonlightVineTopBlock extends Block implements IGrowable {
    public static final IntegerProperty AGE = BlockStateProperties.AGE_0_7;
    protected static final VoxelShape SHAPE = Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 10.0D, 16.0D);

    public MoonlightVineTopBlock(Block.Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(AGE, 0));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(AGE);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }

    @Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public ItemStack getPickBlock(BlockState state, RayTraceResult target, IBlockReader world, BlockPos pos, PlayerEntity player) {
        return new ItemStack(MWDItems.MOONLIGHT_BERRIES);
    }

    @Override
    public void tick(BlockState state, World world, BlockPos pos, Random random) {
        if (!state.isValidPosition(world, pos)) {
            world.destroyBlock(pos, true);
        } else {
            int age = state.get(AGE);
            if (age >= this.getMaxAge()) {
                int height = 1;
                while (world.getBlockState(pos.down(height)).getBlock() instanceof MoonlightVineBlock) height++;
                BlockPos upPos = pos.up();
                world.setBlockState(pos, MoonlightVineBlock.getRandomState(world.getRandom()));
                if (height < this.getMaxHeight(world.getBiome(pos)) && world.isAirBlock(upPos)) world.setBlockState(upPos, this.getDefaultState());
            } else world.setBlockState(pos, state.with(AGE, age + 1), 4);
        }
    }

    @Override
    public boolean isValidPosition(BlockState state, IWorldReader worldIn, BlockPos pos) {
        Block block = worldIn.getBlockState(pos.down()).getBlock();
        return block == Blocks.GRASS_BLOCK || block == MWDBlocks.MOONLIGHT_VINE_PLANT || block == Blocks.PODZOL;
    }

    @Override
    public BlockState updatePostPlacement(BlockState state, Direction facing, BlockState facingState, IWorld world, BlockPos currentPos, BlockPos facingPos) {
        if (!state.isValidPosition(world, currentPos)) {
            if (facing == Direction.DOWN) {
                return Blocks.AIR.getDefaultState();
            }
            world.getPendingBlockTicks().scheduleTick(currentPos, this, 1);
        }
        if (facing == Direction.UP && facingState.getBlock() == this) {
            return MoonlightVineBlock.getRandomState(world.getRandom());
        } else {
            return super.updatePostPlacement(state, facing, facingState, world, currentPos, facingPos);
        }
    }

    protected int getBonemealAgeIncrease(World worldIn) {
        return MathHelper.nextInt(worldIn.rand, 4, 7);
    }

    public int getMaxAge() {
        return 7;
    }

    public int getMaxHeight(Biome biome) {
        return BiomeDictionary.hasType(biome, MWDBiomes.MWD_RICH) ? 12 : BiomeDictionary.hasType(biome, MWDBiomes.MWD_THIN) ? 8 : 6;
    }

    // IGrowable
    @Override
    public boolean canGrow(IBlockReader worldIn, BlockPos pos, BlockState state, boolean isClient) {
        return true;
    }

    @Override
    public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, BlockState state) {
        return true;
    }

    @Override
    public void grow(World world, Random rand, BlockPos pos, BlockState state) {
        int oldAge = state.get(AGE);
        int newAge = this.getBonemealAgeIncrease(world) + oldAge;
        if (newAge >= this.getMaxAge()) {
            int height = 1;
            while (world.getBlockState(pos.down(height)).getBlock() instanceof MoonlightVineBlock) height++;
            BlockPos upPos = pos.up();
            world.setBlockState(pos, MoonlightVineBlock.getRandomState(world.getRandom()));
            if (height < this.getMaxHeight(world.getBiome(pos)) && world.isAirBlock(upPos)) world.setBlockState(upPos, this.getDefaultState());
        } else world.setBlockState(pos, state.with(AGE, newAge), 4);
    }
}