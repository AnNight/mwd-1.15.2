package aliaohaolong.magicwithdrinks.common.capability.mana;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class ManaStorage implements Capability.IStorage<IMana> {
    @Nullable
    @Override
    public INBT writeNBT(Capability<IMana> capability, IMana instance, Direction side) {
        CompoundNBT compoundNBT = new CompoundNBT();
        compoundNBT.putInt("mwd.mana.limit", instance.getLimit());
        compoundNBT.putInt("mwd.mana.value", instance.getValue());
        compoundNBT.putBoolean("mwd.mana.lock", instance.getLock());
        return compoundNBT;
    }

    @Override
    public void readNBT(Capability<IMana> capability, IMana instance, Direction side, INBT nbt) {
        CompoundNBT compoundNBT = (CompoundNBT) nbt;
        instance.setLimit(compoundNBT.getInt("mwd.mana.limit"));
        instance.setValue(compoundNBT.getInt("mwd.mana.value"));
        instance.setLock(compoundNBT.getBoolean("mwd.mana.lock"));
    }
}