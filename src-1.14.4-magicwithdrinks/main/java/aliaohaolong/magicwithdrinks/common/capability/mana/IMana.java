package aliaohaolong.magicwithdrinks.common.capability.mana;

import net.minecraft.entity.LivingEntity;

@SuppressWarnings("UnnecessaryInterfaceModifier")
public interface IMana {
    public static final int MAX = 1000000;

    public default int getMax() {
        return this.MAX;
    }

    public abstract int getValue();

    public abstract int getLimit();

    public abstract boolean getLock();

    public abstract boolean setValue(int value);

    public abstract boolean setLimit(int value);

    public abstract void setLock(boolean value);

    public abstract boolean limitedIncreaseValue(int value);

    public abstract boolean limitedReduceValue(int value);

    public abstract boolean limitedIncreaseLimit(int value);

    public abstract boolean limitedReduceLimit(int value);

    public abstract void unlimitedIncreaseValue(int value);

    public abstract void unlimitedReduceValue(int value);

    public abstract void unlimitedIncreaseLimit(int value);

    public abstract void unlimitedReduceLimit(int value);

    public abstract void copyForRespawn(IMana oldCap);

    public static IMana getFromPlayer(LivingEntity player) {
        return player.getCapability(ManaProvider.MANA_CAP, null).orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!"));
    }
}