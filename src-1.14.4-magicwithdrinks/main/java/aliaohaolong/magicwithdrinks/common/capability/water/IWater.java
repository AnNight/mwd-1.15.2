package aliaohaolong.magicwithdrinks.common.capability.water;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;

@SuppressWarnings("UnnecessaryInterfaceModifier")
public interface IWater {
    public static final int MAX = 50;
    public static final int INNER_MAX = 9600;

    public default int getMax() {
        return this.MAX;
    }

    public abstract int getValue();

    public abstract int getInner();

    public abstract byte getSecond();

    public abstract boolean getLock();

    public abstract boolean setValue(int value);

    public abstract boolean setInner(int value);

    public abstract boolean setSecond(byte value);

    public abstract void setLock(boolean value);

    public abstract boolean limitedIncrease(int value);

    public abstract boolean limitedReduce(int value);

    public abstract void unlimitedIncrease(int value);

    public abstract void unlimitedReduce(int value);

    public abstract boolean operationInner(int value);

    public abstract boolean tick(PlayerEntity player);

    public abstract void copyForRespawn(IWater oldCap);

    public static IWater getFromPlayer(LivingEntity player) {
        return player.getCapability(WaterProvider.WATER_CAP, null).orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!"));
    }
}