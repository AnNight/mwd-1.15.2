package aliaohaolong.magicwithdrinks.common.capability.water;

import aliaohaolong.magicwithdrinks.api.MWDDamageSources;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;

public class Water implements IWater {
    private int value;
    private int inner;
    private byte second;
    private boolean lock;

    public Water() {
        value = 50;
        inner = INNER_MAX;
        second = 0;
        lock = false;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public int getInner() {
        return inner;
    }

    @Override
    public byte getSecond() {
        return second;
    }

    @Override
    public boolean getLock() {
        return lock;
    }

    @Override
    public boolean setInner(int value) {
        if (value <= INNER_MAX && value >= 0) {
            this.inner = value;
            return true;
        }
        return false;
    }

    /**
     * When the new value is available, set the current value to the new value and return true.
     * When the new value is unavailable, return false.
     */
    @Override
    public boolean setValue(int value) {
        check();
        if (value <= MAX && value >= 0) {
            this.value = value;
            return true;
        } else return false;
    }

    @Override
    public boolean setSecond(byte value) {
        if (value <= 40 && value >= 0) {
            this.second = value;
            return true;
        }
        return false;
    }

    @Override
    public void setLock(boolean value) {
        check();
        this.lock = value;
    }

    @Override
    public boolean limitedIncrease(int value) {
        check();
        if (this.lock) return false;
        else {
            int newValue = this.value + value;
            if (newValue > MAX)
                this.inner = INNER_MAX;
            this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
            return true;
        }
    }

    @Override
    public boolean limitedReduce(int value) {
        check();
        if (this.lock) return false;
        else {
            int newValue = this.value - value;
            if (newValue > MAX)
                this.inner = INNER_MAX;
            this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
            return true;
        }
    }

    @Override
    public void unlimitedIncrease(int value) {
        check();
        int newValue = this.value + value;
        if (newValue > MAX)
            this.inner = INNER_MAX;
        this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
    }

    @Override
    public void unlimitedReduce(int value) {
        check();
        int newValue = this.value - value;
        if (newValue > MAX)
            this.inner = INNER_MAX;
        this.value = newValue < 0 ? 0 : Math.min(newValue, MAX);
    }

    /**
     * Run on Server.<br>
     */
    @Override
    public boolean operationInner(int value) {
        boolean sync = false;
        if (!lock && value > 0) {
            inner -= value;
            do {
                if (inner <= 0) {
                    if (this.value > 0) {
                        this.value--;
                        sync = true;
                    }
                    inner += INNER_MAX;
                }
            } while (inner <= 0);
        } else if (!lock && value < 0) {
            inner -= value;
            do {
                if (inner > INNER_MAX) {
                    if (this.value < MAX) {
                        this.value++;
                        sync = true;
                    }
                    inner -= INNER_MAX;
                }
            } while (inner > INNER_MAX);
        }
        return sync;
    }

    /**
     * handler.ThirstHandler#onPlayerTick<br>
     * Run on Server.<br>
     */
    @Override
    public boolean tick(PlayerEntity player) {
        boolean sync = false;
        second--;
        if (second <= 0) {
            second = 20;
            if (!lock && value <= 25)
                player.addPotionEffect(new EffectInstance(Effects.WEAKNESS, 60));
            if (!lock && value == 0) {
                player.addPotionEffect(new EffectInstance(Effects.NAUSEA, 100));
                player.attackEntityFrom(MWDDamageSources.PARCHED, 2F);
            }
            if (!lock && value > MAX - 3) {
                if (player.getHealth() < player.getMaxHealth()) {
                    player.heal(0.25F);
                    sync = this.operationInner(1920);
                }
            }
        }
        return sync;
    }

    @Override
    public void copyForRespawn(IWater oldCap) {
        this.setValue(oldCap.getValue());
        this.setLock(oldCap.getLock());
        this.setInner(oldCap.getInner());
        this.setSecond(oldCap.getSecond());
    }

    private void check() {
        if (value < 0) value = 0;
        if (value > MAX) value = MAX;
        if (inner < 0) inner = 0;
        if (inner > INNER_MAX) inner = INNER_MAX;
    }
}