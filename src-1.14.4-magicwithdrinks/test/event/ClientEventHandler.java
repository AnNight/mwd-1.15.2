package aliaohaolong.magicwithdrinks.event;

import aliaohaolong.magicwithdrinks.client.gui.ModIngameGui;
import aliaohaolong.magicwithdrinks.client.renderer.tileentity.TestTileEntityRenderer;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ClientEventHandler {
    @SubscribeEvent
    public void onRenderGameOverlays(RenderGameOverlayEvent.Post event) {
        if (event.getType() == RenderGameOverlayEvent.ElementType.HEALTH)
            ModIngameGui.render();
    }

    @SubscribeEvent
    public void onTextureStitch(TextureStitchEvent.Pre event) {
//        if (!event.getMap().getTextureLocation().equals(AtlasTexture.LOCATION_BLOCKS_TEXTURE))
//            return;
//        event.addSprite(TestTileEntityRenderer.TEST_TEXTURE);
    }
}