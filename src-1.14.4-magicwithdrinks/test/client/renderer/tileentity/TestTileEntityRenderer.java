package aliaohaolong.magicwithdrinks.client.renderer.tileentity;

import aliaohaolong.magicwithdrinks.common.tile.TestTileEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;

@SuppressWarnings("NullableProblems")
public class TestTileEntityRenderer extends TileEntityRenderer<TestTileEntity> {
//    public static final ResourceLocation TEST_ATLAS = new ResourceLocation(MWD.MOD_ID, "atlas/test.png");
    public static final ResourceLocation TEST_TEXTURE = new ResourceLocation("block/white_wool");
//    private final ModelRenderer test;

    public TestTileEntityRenderer(TileEntityRendererDispatcher dispatcher) {
        super(dispatcher);
//        test = new ModelRenderer(16, 16, 0, 0);
//        test.addBox(7, 0, 7, 2, 1, 2, 0.0F);
    }

    @Override
    public void render(TestTileEntity tileEntity, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLightIn, int combinedOverlayIn) {
        TextureAtlasSprite sprite = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(TEST_TEXTURE);
        IVertexBuilder builder = buffer.getBuffer(RenderType.getTranslucent());

        matrixStack.push();
        matrixStack.translate(0.5, 0, 0.5); // 移出
//        matrixStack.rotate(Vector3f.YP.rotationDegrees()); // 旋转
//        matrixStack.scale(); // 缩放
        matrixStack.translate(-0.5, 0, -0.5); // 移入
        addBox(sprite, builder, matrixStack, 2, 2, 2, 12, 12, 12);

//        Material material = Atlases.getChestMaterial(null, ChestType.SINGLE, true);
//        IVertexBuilder builder = material.getBuffer(buffer, RenderType::getEntityCutout);
//        test.render(matrixStack, builder, combinedLightIn, combinedOverlayIn);
        matrixStack.pop();
    }

    private void add(IVertexBuilder builder, MatrixStack matrixStack, float x, float y, float z, float u, float v) {
        builder.pos(matrixStack.getLast().getMatrix(), x, y, z)
                .color(1.0f, 1.0f, 1.0f, 0.8f)
                .tex(u, v) // 贴图坐标
                .lightmap(0, 240)
                .normal(1, 0, 0)
                .endVertex();
    }

    private void addBox(TextureAtlasSprite sprite, IVertexBuilder builder, MatrixStack matrixStack, float x, float y, float z, float weight, float height, float depth) {
        x /= 16;
        y /= 16;
        z /= 16;
        float xm = x + weight / 16;
        float ym = y + height / 16;
        float zm = z + depth / 16;
        add(builder, matrixStack, x, y, zm, sprite.getMinU(), sprite.getMinV());
        add(builder, matrixStack, xm, y, zm, sprite.getMaxU(), sprite.getMinV());
        add(builder, matrixStack, xm, ym, zm, sprite.getMaxU(), sprite.getMaxV());
        add(builder, matrixStack, x, ym, zm, sprite.getMinU(), sprite.getMaxV());

        add(builder, matrixStack, x, ym, z, sprite.getMinU(), sprite.getMaxV());
        add(builder, matrixStack, xm, ym, z, sprite.getMaxU(), sprite.getMaxV());
        add(builder, matrixStack, xm, y, z, sprite.getMaxU(), sprite.getMinV());
        add(builder, matrixStack, x, y, z, sprite.getMinU(), sprite.getMinV());

        add(builder, matrixStack, x, y, z, sprite.getMinU(), sprite.getMinV());
        add(builder, matrixStack, xm, y, z, sprite.getMaxU(), sprite.getMinV());
        add(builder, matrixStack, xm, y, zm, sprite.getMaxU(), sprite.getMaxV());
        add(builder, matrixStack, x, y, zm, sprite.getMinU(), sprite.getMaxV());

        add(builder, matrixStack, x, ym, zm, sprite.getMinU(), sprite.getMaxV());
        add(builder, matrixStack, xm, ym, zm, sprite.getMaxU(), sprite.getMaxV());
        add(builder, matrixStack, xm, ym, z, sprite.getMaxU(), sprite.getMinV());
        add(builder, matrixStack, x, ym, z, sprite.getMinU(), sprite.getMinV());

        add(builder, matrixStack, xm, y, z, sprite.getMinU(), sprite.getMinV());
        add(builder, matrixStack, xm, ym, z, sprite.getMaxU(), sprite.getMinV());
        add(builder, matrixStack, xm, ym, zm, sprite.getMaxU(), sprite.getMaxV());
        add(builder, matrixStack, xm, y, zm, sprite.getMinU(), sprite.getMaxV());

        add(builder, matrixStack, x, y, zm, sprite.getMinU(), sprite.getMaxV());
        add(builder, matrixStack, x, ym, zm, sprite.getMaxU(), sprite.getMaxV());
        add(builder, matrixStack, x, ym, z, sprite.getMaxU(), sprite.getMinV());
        add(builder, matrixStack, x, y, z, sprite.getMinU(), sprite.getMinV());
    }
}