package aliaohaolong.magicwithdrinks.common.tile;

import aliaohaolong.magicwithdrinks.api.MWDTypes;
import net.minecraft.tileentity.TileEntity;

public class TestTileEntity extends TileEntity {
    public TestTileEntity() {
        super(MWDTypes.T_TEST);
    }
}