public static final Block TEST = new TestBlock(Block.Properties.create(Material.ROCK)).setRegistryName("test");
public static final Item TEST = build(MWDBlocks.TEST);
private static Item build(Block block) {
    return new BlockItem(block, new Item.Properties().group(MWDGroupsAndMaterials.BLOCKS)).setRegistryName(Objects.requireNonNull(block.getRegistryName()));
}
public static final TileEntityType<TestTileEntity> T_TEST = init(TileEntityType.Builder.create(TestTileEntity::new, MWDBlocks.TEST).build(null), "test");