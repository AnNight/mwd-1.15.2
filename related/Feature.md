`单位：ME(Mana Energy)、魔法 Magic、魔力 Mana`

<html>
<div style="font-family:verdana;padding:20px;border-radius:10px;border:10px solid #EE872A;">
<b>版本：0.4.0<br>
日期：2020-11-??<br>
参与者：aliaohaolong@qq.com</b>

***
```
* 机器
  * 新增净水器。
  * 新增饮水机。
  * 新增魔法饮水机。
* 魔法
  * 新增法力导管。
  * 新增创造法力方块。
* 矿石
  * 新增紫罗兰矿石。
    * 紫罗兰矿石在主世界的0~32格间生成。
    * 紫罗兰矿石可以熔炼为紫罗兰锭。
  * 新增蓝金矿石。
    * 挖掘蓝金矿石掉落蓝金粉。
* 农作物
  * 新增棉作物。
    * 种植方式与原版小麦相同。
    * 共7个生长阶段。
    * 可使用骨粉加速生长。
    * 收获成熟的棉作物将会掉落1到2个棉花和最多3个棉花种子。
* 其他方块
  * 新增紫罗兰玻璃。
* 其他物品
  * 新增药剂瓶。
  * 新增羽毛过滤片、羊毛过滤片、棉过滤片、竹过滤片和金制过滤片。
```
</div>
</html>

***
<html>
<div style="font-family:verdana;padding:20px;border-radius:10px;border:10px solid #EE872A;">
<b>Version: 0.4.0<br>
Date: 2020-11-??<br>
Participator: aliaohaolong@qq.com</b>

***
```
* Machine
  * Added water purifiers.
  * Added water fountains.
  * Added magic water fountains.
* Magic
  * Added mana conduits.
  * Added creative mana blocks.
* Ore
  * Added violet ores.
    * Violet ores are generated between 0 to 32 height in overworld.
    * Violet ores can be smelted to violet ingot.
  * 新增蓝金矿石。
    * 挖掘掉落蓝金粉。
* Crop
  * 新增棉作物。
    * 种植方式与原版小麦相同。
    * 共7个生长阶段。
    * 可使用骨粉加速生长。
    * 收获成熟的棉作物将会掉落1到2个棉花和最多3个棉花种子。
* Other blocks
  * 新增紫罗兰玻璃。
* Other items
  * 新增药剂瓶。
  * 新增羽毛过滤片、羊毛过滤片、棉过滤片、竹过滤片和金制过滤片。
```
</div>
</html>


```
[0.4.0 Alpha][2020-11-??] [ALiaoHaolong]
1. Added water purifiers().
2. Added
```
```
1. 火焰树的原木、木头、脱皮原木、脱皮木头、树叶、树苗、花盆中的树苗。
2. 月光树的原木、木头、脱皮原木、脱皮木头、树叶、树苗、花盆中的树苗。
```
```
[0.4.1 Alpha][2020-11-??] [ALiaoHaolong]
1. 火焰树的楼梯、台阶、压力板、栅栏、栅栏门、按钮、活板门、门。
2. 月光树的楼梯、台阶、压力板、栅栏、栅栏门、按钮、活板门、门。
```
```
[0.4.2 Alpha][2020-11-??] [ALiaoHaolong]
1. 新的生物群系——月光岛
a. 月光藤在此自然生成。
```
```
[0.4.3 Alpha][2020-11-??] [ALiaoHaolong]
1. 新的生物群系——火焰岛
a. 火焰树在此自然生成。
```