|矿石名称|对应物|位置|备注|
|-|-|-|-|
|魔晶石矿石(Mana Crystal Ore)|魔晶石(Mana Crystal)|"rich"&"thin"|制作电池，给机器充能|
|紫罗兰矿石(Violet Ore)|紫罗兰锭(Violet Ingot)|"rich"&"thin"|
|蓝金矿石(Blue Gold Ore)|蓝金粉(Blue Gold Powder)|"rich"|类红石
|化石矿石(Fossil Ore)|化石(Fossil)|O/N/M|常见，制作药剂
|变性化石矿石(Denatured Fossil Ore)|变性化石(Denatured Fossil)|O|稀有，制作药剂
|琥珀矿石(Amber Ore)|琥珀(Amber)|森林和丛林|
**相关工具与盔甲**

|Name|剑|镐|斧|铲|锄|头盔|胸甲|护腿|靴子|
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|紫罗兰(Violet)|
***

|Name|主|附|伴生|位置|
|-|:-:|:-:|-|-|
|魔法(Magic)|x|x||遗迹平原|
|火焰树(Blaze)||||火焰岛|
|月光(Moonlight)||||月光岛|
|静谧(Serenity)||||
主件：原木/木头/脱皮原木/脱皮木头/木板/树叶/树苗/盆栽树苗  
附件：楼梯/台阶/栅栏/栅栏门/压力板/按钮/活板门/门
***
青
红橙
黄白
蓝