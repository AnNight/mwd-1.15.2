M-模型？忘了  
T-贴图  
R-合成表  
L-掉落物  
F-功能方法  

|Name|M|T|R|L|F|P.S.|
|-|:-:|:-:|:-:|:-:|:-:|-|
|药剂瓶(Potion Bottle)|x|x|x|-|x|
|药剂(Potion)|x|x|-|-|x|
|净水器(Water Purifier)|x|x|x|x|x|4铁.熔炉
|棉花(Cotton Crops)|x|x|-|x|x|
|棉花(Cotton)|x|x|-|x|x|
|棉花种子(Cotton Seeds)|x|x|-|o|x|
|羽毛过滤片(Feathery Filter Slice)|x|x|x|-|-|10
|羊毛过滤片(Woolen Filter Slice)|x|x|x|-|-|24
|棉过滤片(Cottony Filter Slice)|x|x|x|-|-|64
|竹过滤片(Bamboo Filter Slice)|x|x|x|-|-|88
|金制过滤片(Golden Filter Slice)|x|x|x|-|-|800
|紫罗兰矿石(Violet Ore)|x|x|-|x|x|主_0~32_9_3
|紫罗兰锭(Violet Ingot)|x|x|x|-|-|
|紫罗兰玻璃(Violet Glass)|x|x|x|x|-|
|创造法力方块(Creative Mana Block)|x|x|-|x|x|
|蓝金矿石(Blue Gold Ore)|x|x|-|x|x|主_0~16_6_2
|蓝金粉(Blue Gold Powder)|x|x|-|o|-|1~3
|饮水机(Water Fountain)|x|x|x|x|x|6铁2桶1熔炉
|魔法饮水机(Magic Water Fountain)|x|x|x|x|x|5铁2桶.蓝核.魔石电池
|法力导管(Mana Conduit)|x|||x|x|
|蓝金核心(Blue Gold Core)|x|x|x|-|-|8蓝金粉.金
|魔晶石矿石(Mana Crystal Ore)|x|x|-|x|x|主_0~16_8_1
|魔晶石(Mana Crystal)|x|x|-|o||
|魔晶石电池(Mana Crystal Battery)|x|x|x|-||4魔石+4紫锭+蓝核
|高级魔晶石电池(Advanced Mana Crystal Battery)|x|x||-||6+3钻石
|料理机(Blender)|x|x|x|x|x|4铁.烟熏炉
|基础ME卡(Elementary ME Card)|x|x||-|-||
|中级ME卡(Intermediate ME Card)|x|x||-|-||
|高级ME卡(Advanced ME Card)|x|x||-|-||
|生物电池(Biomass Battery)||||-||毒马铃薯
|高级生物电池(Advanced Biomass Battery)||||-||
|ME提取机(ME Extractor)|x|||x|x|
|精炼盒子(Smelting Box)|x|||||
|纯净水(Purified Water)|x|x|x|-|-|
|糖水(Sugar Water)|x|x|x|-|-|糖20|
|苹果汁(Apple Juice)|x|x|x|-|-|苹果120|
|西瓜汁(Melon Juice)|x|x|x|-|-|2西瓜片100|
|胡萝卜汁(Carrot Juice)|x|x|x|-|-|胡萝卜120|
|南瓜汁(Pumpkin Juice)|x|x|x|-|-|南瓜120|
|甜浆果汁(Sweet Berries Juice)|x|x|x|-|-|2甜浆果100|
|烈焰法杖(Flame Wand)
|冰霜法杖(Frost Wand)
|自然法杖(Nature Wand)
|诡秘法杖(Mystery Wand)
|||||||
|橡树皮(Oak Bark)|||||-|
|云杉树皮(Spruce Bark)|||||-|
|白桦树皮(Birch Bark)|||||-|
|丛林树皮(Jungle Bark)|||||-|
|深色橡树皮(Dark Oak Bark)|||||-|
|金合欢树皮(Acacia Bark)|||||-|

***
### MWD

|Name|R|P.S.|
|-|:-:|-|
|水-药剂(Water)|
|纯净水
|虚幻药剂(Unreal Drink)||
|火焰药剂(Blaze Potion)||火焰原木
|冰霜药剂||雪、冰霜花
|劣质的自然药剂(Shoddy Nature Potion)||
|植物药剂||树苗
|经典的火焰药剂(Classy Blaze Potion)||
|自然药剂(Nature Potion)||
|经典的自然药剂(Classy Nature Potion)||
|活性物质(Active Substance)||
|经典催化剂(First-Level Catalyst)||
|催化剂(Second-Level Catalyst)||
|劣质催化剂(Third-Level Catalyst)||
|蓝金溶液||