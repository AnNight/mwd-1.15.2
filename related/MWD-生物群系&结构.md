#### 新增生物群系表

|完成|维度|群系名称|群系字典|depth|scale|
|:-:|:-:|-|-|-|-|
|×|O|魔法岛(Magic Island)|mwd_thin|?|?|
|×|M|魔法岛(Magic Island)|mwd_rich|?|?|
|x|M|遗迹平原(Relic Plains)|hot/sparse/wet/rare/overworld/mwd_thin|?|?|
|x|M|月光岛(Moonlight Island)|mwd_rich|?|?|
|×|M|火山岛(Volcanic Island)||?|?|
|x|M|荒岛(Barren Island)||?|?|
|×|M|魔法遗迹平原(Magic Relic Plains)|mwd_rich|?|?|
|×|M|魔法森林(Magic Forest)|mwd_rich|?|?|
***
* **[O] 魔法岛(Magic Island)**
  * 树
    - [ ] ？
  * 花草藤菇
    - [ ] ？
  * 结构
    - [ ] ？
* **[M] 魔法岛(Magic Island)**
  * 树
    - [ ] ？
  * 花草藤菇
    - [ ] ？
  * 结构
    - [ ] ？
* **月光岛(Moonlight Island)**
  * 树
    - [ ] 月光树(Moonlight Tree)[稠密]
  * 花草
    - [ ] 月光藤[正常]
    - [ ] 天蓝蘑菇[大量]
  * 结构
    - [ ] 热泉
* **遗迹平原(Relic Plains)**
  * 树
    - [x] 主：低矮的青树(Cyan Tree)
    - [ ] 辅：高大的青树(Cyan Tree)
    - [x] 辅：稀疏的橡树(Oak)
  * 花草
    - [x] 智慧花
    - [ ] 岩兰草(Vetiver)
  * 结构
    - [ ] 圣坛
    - [x] 残破的房子
* **火山岛(Volcanic Island)**
  * 树
    - [ ] 火焰树(Blaze Tree)
  * 花草
    - [ ] 红色蔓生天竺葵(Red Peltatum)
    - [ ] 紫红色蔓生天竺葵(Fuchsia Peltatum)
  * 结构
    - [ ] 岩浆湖
* **荒岛(Barren Island)**
* **魔法遗迹平原(Magic Relic Plains)**
  * 树
  * 花草
    - [ ] 岩兰草(Vetiver)
  * 结构
* **魔法森林(Magic Forest)**
  * 树
  * 花草
    - [ ] 岩兰草(Vetiver)
    - [ ] 青蘑菇
  * 结构
    - [ ] 散落的巨大的石块
***
#### 结构(Struction)/遗迹(Monument)
walsh flower沃尔什花  
eve flower伊芙花  
kukes flower库克斯花  
josiah flower约西亚  
魔草  
花  
魔法农作物  
魔法树  
果子  
掉落物 
***
#### 结构
|名称|位置|
|-|-|
|残破的房子|遗迹平原(Relic Plains)|
|圣坛(shrine)|遗迹平原(Relic Plains)|
|热泉|月光岛(Moonlight Island)|
|散落的巨大的石块|魔法森林(Magic Forest)|
***
depth	scale	T	downfall	category	name
-1.8	0.1	0.5			depth ocean
-1.0	0.1	0.5			ocean
-0.5	0.0	0.5			river
0.0	0.025	0.8	0.4		beach
0.1	0.2	0.7	0.8		forest
0.1	0.2	0.95	0.9		jungle
0.125	0.05	0.8	0.4		plain
0.125	0.05	2.0	0.0		desert
0.2	0.2	0.25	0.8		taiga
0.2	0.4	0.7	0.8		dark_forest_hill
0.45	0.3	0.6			hill
1.0	0.5	0.2	0.3		mountain

wild_mainland
witchcraft_mainland
dark_mainland
0.2	0.2	0.7	0.4	plain	relic_mainland
0.25	0.2	0.65	0.5	forest	magic_forest

ocean: 0
plains: 1
desert: 2
mountains: 3
forest: 4
taiga: 5
swamp: 6
river: 7
nether: 8
the end: 9
frozen ocean: 10
frozen river: 11
snowy tundra: 12
snowy mountains: 13
mushroom fields: 14
mushroom field shore: 15
beach: 16
desert hills: 17
wooded hills: 18
taiga hills: 19
mountain edge: 20
jungle: 21
jungle hills: 22
jungle edge: 23
deep ocean: 24
stone shore: 25
snowy beach: 26
birch forest: 27
birch forest hills: 28
dark forest: 29
snowy taiga: 30
snowy taiga hills: 31
giant tree taiga: 32
giant tree taiga hills: 33
wooded mountains: 34
savanna: 35
savanna plateau: 36
badlands: 37
wooded badlands plateau: 38
badlands plateau: 39
small end islands: 40
end midlands: 41
end highlands: 42
end barrens: 43
warm ocean: 44
lukewarm ocean: 45
cold ocean: 46
deep warm ocean: 47
deep lukewarm ocean: 48
deep cold ocean: 49
deep frozen ocean: 50
ontheland:pinus caribaea forest: 51
ontheland:pinus palustris forest: 52
ontheland:pinus taeda forest: 53
ontheland:wild mainland: 54
the void: 127
sunflower plains: 129
desert lakes: 130
gravelly mountains: 131
flower forest: 132
taiga mountains: 133
swamp hills: 134
ice spikes: 140
modified jungle: 149
modified jungle edge: 151
tall birch forest: 155
tall birch hills: 156
dark forest hills: 157
snowy taiga mountains: 158
giant spruce taiga: 160
giant spruce taiga hills: 161
modified gravelly mountains: 162
shattered savanna: 163
shattered savanna plateau: 164
eroded badlands: 165
modified wooded badlands plateau: 166
modified badlands plateau: 167
bamboo jungle: 168
bamboo jungle hills: 169